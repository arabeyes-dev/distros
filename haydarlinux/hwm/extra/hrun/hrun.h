/****************************************************************************
** Form interface generated from reading ui file 'hrun.ui'
**
** Created: Thu Aug 16 01:33:22 2001
**      by:  The User Interface Compiler (uic)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/
#ifndef HRUN_H
#define HRUN_H

#include <qvariant.h>
#include <qdialog.h>

#include <qcheckbox.h>
#include <qcombobox.h>
#include <qlabel.h>
#include <qpushbutton.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qimage.h>
#include <qpixmap.h>
#include <qprocess.h>
#include <qtimer.h>
#include <defaults.h>
#include <libhwm.h>

class hrun : public QDialog
{ 
    Q_OBJECT

public:
    hrun( QWidget* parent = 0, const char* name = 0, bool modal = FALSE, WFlags fl = 0 );
    ~hrun();

    QLabel* descLbl;
    QLabel* openLbl;
    QComboBox* OpenCombo;
    QCheckBox* xtermChk;
    QPushButton* okBtn;
    QPushButton* cancelBtn;
    QPushButton* browseBtn;


public slots:
    void OKpressed();

protected slots:
    void Cancelpressed();
    void runFinished();
    void saveChanges();
    void init();

protected:
    QVBoxLayout* Layout4;
    QHBoxLayout* Layout3;
    QHBoxLayout* Layout4_2;
};

#endif // HRUN_H
