/****************************************************************************
** ui.h extension file, included from the uic-generated form implementation.
**
** If you wish to add, delete or rename functions or slots use
** Qt Designer which will update this file, preserving your code. Create an
** init() function in place of a constructor, and a destroy() function in
** place of a destructor.
*****************************************************************************/
#include <qiconview.h> 

#include <defaults.h> 

class ivItem : public QIconViewItem
{
    Q_OBJECT
    
    public:
    ivItem(QIconView* parent, const QString& name, const QPixmap& pixmap) : QIconViewItem(parent, name, pixmap){};
    virtual ~ivItem(){};
    void setFileName(QString fn){ filename = fn;};
    QString getFileName(void){ return filename;};
    void setDirName(QString dn){ dirname = dn;};
    QString getDirName(void){ return dirname;};
   
    protected:
    QString filename;
    QString dirname;
};

/*
ivItem: :ivItem(QIconView* parent, const QString& name, const QPixmap& pixmap)
	: QIconViewItem(parent, name, pixmap)
{
    
}
*/
void iconForm::init()
{
    iconDir->setText(defaults::defaultIconsDir);
}

void iconForm::okBtn_clicked()
{

}
