#ifndef SMENUITEM_H
#define SMENUITEM_H

#include <qframe.h>
#include <qpixmap.h>
#include <qpopupmenu.h>

class sMenuItem : public QWidget
{
    Q_OBJECT
 public:
    sMenuItem( QWidget *parent=0, const char *name=0 );
    /*
    void sMenuItem( QString st = 0, QPixmap *pic = 0, QWidget *parent=0, 
           const char *name=0 );
    */
    void setPixmap(QPixmap pic);
    void setText(QString st){ str = st;};
    void setPopup(QPopupMenu *pop){ popup = pop; haspopup = true;};
    ~sMenuItem(){};
    bool connectItem ( const QObject * receiver, const char * member );
    bool disconnectItem ( const QObject * receiver, const char * member );
    void drawIt(void);
    void setCommand(QString com);
    void setPixSize(int pw, int ph ){ pixWidth=pw; pixHeight=ph;drawIt();};

 signals:
    void clicked(void);

 public slots:
    void pop(void);
    void execCommand(void);

 private:
    QPixmap pix;
    QPixmap orig_pic;

    QString str;
    QString command;
    QPopupMenu *popup;
    bool haspopup;
    int pixWidth;
    int pixHeight;
    

 protected:
    virtual void paintEvent( QPaintEvent * ){ drawIt(); };
    virtual void mouseMoveEvent ( QMouseEvent * e ){ drawIt(); };
    virtual void enterEvent ( QEvent * );
    virtual void leaveEvent ( QEvent * ){ drawIt(); };
    virtual void mouseReleaseEvent( QMouseEvent * e );
    //    virtual void mousePressEvent ( QMouseEvent * e );
    virtual void resizeEvent ( QResizeEvent * ){ drawIt(); };

};

#endif // SMENUITEM_H
