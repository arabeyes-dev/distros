<!DOCTYPE TS><TS>
<context>
    <name>HCloseFrame</name>
    <message>
        <source>HCloseFrame</source>
        <translation></translation>
    </message>
    <message>
        <source>Caption</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>itemsFrame</name>
    <message>
        <source>Form1</source>
        <translation></translation>
    </message>
    <message>
        <source>Your Name</source>
        <translation>إسمك</translation>
    </message>
    <message>
        <source>Mozilla Browser</source>
        <translation>متصفح موزيلا</translation>
    </message>
    <message>
        <source>Mozilla mail</source>
        <translation>بريد موزيلا</translation>
    </message>
    <message>
        <source>Run program</source>
        <translation>شغّل برنامج</translation>
    </message>
    <message>
        <source>All Programs</source>
        <translation>جميع البرامج</translation>
    </message>
    <message>
        <source>My Documents</source>
        <translation>المستندات</translation>
    </message>
    <message>
        <source>Recent Docoments</source>
        <translation>المستندات الحديثة</translation>
    </message>
    <message>
        <source>My Pictures</source>
        <translation>مجلّد الرسوم</translation>
    </message>
    <message>
        <source>My Sound Files</source>
        <translation>مجلّد الأصوات</translation>
    </message>
    <message>
        <source>Control Panel</source>
        <translation>لوحة التحكّم</translation>
    </message>
    <message>
        <source>Kill window</source>
        <translation>إنهاء إطار</translation>
    </message>
    <message>
        <source>End Session</source>
        <translation>إنهاء الجلسة</translation>
    </message>
</context>
</TS>
