/* defaults.h */

#ifndef MENUCONF_H
#define MENUCONF_H

#include "defs.h"

class menuconf
{
    QString grouptmp, rest, fname;
    int groupnum, linenr;
    QList<QString> lns;

public:
    void addMenuShorcut(QString name, QString shortcut, QString icon);

};
#endif
