/****************************************************************************
 **
 **			      mycomputer.h
 **			=========================
 **
 **  begin                : Wed Jun 21 2003
 **  copyright            : (C) 2003-2006 by Haydar Alkaduhimi
 **  email                : haydar@haydarnet.nl
 **
 **  A widget that has a file list and dirlist.
 **
 ****************************************************************************/


/*****************************************************************************
 *
 * Class HDrive
 *
 *****************************************************************************/
    
#include <qstring.h>
#include <qpixmap.h>
#include <QList>


#ifndef HDRIVE_H 
#define HDRIVE_H 

class HDrive
{
 public:
    HDrive(QString path, QString type=0, QPixmap pix=0);
    
    void setType(QString type){ driveType = type; };
    void setPixmap(QPixmap pix){ drivePix = pix; };
    QString path(){ return drivePath;};
    QString type(){ return driveType;};
    QPixmap pixmap(){ return drivePix;};
    
 private:
    QString driveType;
    QString drivePath;
    QPixmap drivePix;
};
#endif

/*****************************************************************************
 *
 * Class MyComputer
 *
 *****************************************************************************/

#ifndef MYCOMPUTER_H 
#define MYCOMPUTER_H 

class MyComputer
{
 public:
    MyComputer();
    
    void rebuildDrives();
    
    static QList<HDrive> driveList;
    
    QPixmap getDrivePix( HDrive *drive, QString size = "large" );
    QPixmap getDirPix( QString filename, QString size = "large" );
    QPixmap getFilePix( QString filename, QString size = "large" );

    QPixmap getPix( QString filename, QString size = "large" );
};
#endif

