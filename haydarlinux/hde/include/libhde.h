//#include <X11/Xlib.h>
//#include <X11/Xutil.h>
//#include <X11/Xos.h>

#include <stdlib.h>
#include <qstring.h>
#include <qtextcodec.h>
#include <qpixmap.h>
#include "hinifile.h"

QString render(QString s, char *coding);
QString render(QString s, QString coding);
QString get_cfile(char *name);

// get Image file
QString get_ifile(const char *name, const char *size);
QString get_ifile(const char *name, const char *size, const char *type);
QString hde_langinfo(QString lng);
void execCmd(QString cmd);
void gradient(QPixmap &pixmap, const QColor &ca, const QColor &cb, QString eff, int ncols);
void makeTray(WId win);
QString getIconFile(QString icn, const char *size);
QString getIconFile(QString icn, const char *size, const char *type);
