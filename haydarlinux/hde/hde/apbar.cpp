/****************************************************************************
 **
 **				apbar.cpp
 **			=========================
 **
 **  begin                : 2001 based on qlwm
 **  copyright            : (C) 2001 - 2003 by Haydar Alkaduhimi, 
 **  email                : haydar@haydar.net
 **  License              : GPL
 **
 **  Manages the applets on the right side of the toolbar
 **
 **  Special Thanks to Alexander Linden <alinden@gmx.de>
 **
 ****************************************************************************/

#include "defs.h"
#include "defaults.h"
#include "qapp.h"
#include "toolbar.h"
#include "apbar.h"
#include "dclock.h"

dclock *dc;

apbar::apbar(QWidget *parent, const char *name) : QWidget(parent, name)
{
	setFixedHeight(defaults::tb_height);
	

	setFixedWidth(1);
	ap.setAutoDelete(TRUE);

	dc = new dclock(this, "dc",Qt::WStyle_NoBorder|Qt::WMouseNoMask) ;

	if(defaults::apbarStyle == "win")
	  dc->setFixedHeight(height() - 8);
	else
	  dc->setFixedHeight(height());
	//	dc.show();

    	// overwrite Qt-defaults because we need SubstructureNotifyMask
	XSelectInput(qt_xdisplay(), winId(),
  		 KeyPressMask | KeyReleaseMask |
  		 ButtonPressMask | ButtonReleaseMask |
  		 KeymapStateMask |
   		 ButtonMotionMask |
  		 PointerMotionMask |
		 EnterWindowMask | LeaveWindowMask |
  		 FocusChangeMask |
  		 ExposureMask |
		 StructureNotifyMask |
		 SubstructureRedirectMask |
		 SubstructureNotifyMask);

}

void apbar::paintBG(){
  //void apbar::paintEvent( QPaintEvent * ){
    if(QFileInfo(qapp::get_cfile(defaults::appbar_bg)).isFile()){
      //QPainter *p = new QPainter(this);

      QImage img(qapp::get_cfile(defaults::appbar_bg));
      if(! img.isNull()){
	QPixmap pix;
	if(QApplication::reverseLayout())
	  pix.convertFromImage(img.mirror(TRUE,FALSE).smoothScale(width(), 
								  height() ));
	else
	  pix.convertFromImage(img.smoothScale(width(), height() ));
	//p->drawPixmap(0,0, pix);
	setPaletteBackgroundPixmap( pix );
	}
      //p->end();
  
    }else if (defaults::apbarStyle == "hde"){
	QPixmap pix(width(), height());
	QPainter p;
	p.begin( &pix );

	QColor firstColor(palette().active().background());

	int h = (height()-4) /2;
	int i = 0;

	for ( i = 0; i < h-1; ++i ) {
	    p.setPen(firstColor.light(100 + (h*3)-(i*3)));
	    p.drawLine(0,i , width(), i); 
	}

	int j = 0;
	for ( i = height()-h-1; i < height() ; ++i ) {
	    j++;
	    p.setPen(firstColor.dark(100 +(j*3)));
	    p.drawLine(0, i , width(), i); 
	}

	p.end();
	setPaletteBackgroundPixmap( pix );

    }else{
	QPixmap pix(width(), height());;
	QPainter p;
	p.begin( &pix );

	QColor firstColor(palette().active().background());

	p.fillRect ( 0 , 0 , width() , height(), QBrush(firstColor) ); 
	p.setPen(firstColor.light(120));
	p.drawLine(0,0 , width(), 0); 
	p.drawLine(0,1 , width(), 1); 

	p.setPen(firstColor.dark(130).dark(130));
	p.drawLine(0,3 , width(), 3); 
	//p.drawLine(0,4 , width(), 4); 
	p.drawLine(0,3 , 0, height()-3); 
	//p.drawLine(1,3 , 1, height()-4); 

	p.setPen(firstColor.light(130).light(130));
	p.drawLine(0, height()-3, width(), height()-3); 
	p.drawLine(0, height()-4, width(), height()-4); 
	p.drawLine(width()-1, 4, width()-1, height()-4); 
	p.drawLine(width()-2, 3, width()-2, height()-4); 

	p.end();
	setPaletteBackgroundPixmap( pix );
    }
}

void apbar::resizeEvent(QResizeEvent *){
  paintBG();
}

void apbar::place_clients(void)
{
	WINDOW *cur;
	int twidth=2;  // total width

	for(cur = ap.first(); cur != NULL; cur = ap.next())
		twidth += 24;
	//	twidth += cur->width+4;

	//	twidth -= 2;
	setUpdatesEnabled(FALSE);
	setFixedWidth(twidth + dc->width() );

	if(! twidth)
	{
		setUpdatesEnabled(TRUE);
		return;
	}	
	int cx = 2;	
	if(QApplication::reverseLayout())
	  cx = 2+ dc->width();

	if(QApplication::reverseLayout()){
	    for(cur = ap.last(); cur != NULL; cur = ap.prev()){
	      if(defaults::apbarStyle == "hde")
		XMoveWindow(qt_xdisplay(), cur->w, cx, 3);
	      else
		XMoveWindow(qt_xdisplay(), cur->w, cx, 6);

	      XWindowAttributes attr;
	      if(XGetWindowAttributes(qt_xdisplay(), cur->w, &attr))
		{
		  XMapWindow(qt_xdisplay(), cur->w);
		  XRaiseWindow(qt_xdisplay(), cur->w);
		}
	      cx += 22;
	      //	      cx += cur->width+2;
	    }
	}else{
	    for(cur = ap.first(); cur != NULL; cur = ap.next()){
	      if(defaults::apbarStyle == "hde")
		XMoveWindow(qt_xdisplay(), cur->w, cx, 4);
	      else
		XMoveWindow(qt_xdisplay(), cur->w, cx, 6);

	      XWindowAttributes attr;
	      if(XGetWindowAttributes(qt_xdisplay(), cur->w, &attr)){
		XMapWindow(qt_xdisplay(), cur->w);
		XRaiseWindow(qt_xdisplay(), cur->w);
	      }
	      cx += 24;
	      //	      cx += cur->width+4;
	    }
	}
	if(QApplication::reverseLayout()){
	  if(defaults::apbarStyle == "win")
	    dc->move ( 2, 4 );
	  else
	    dc->move ( 2, 0 );
	}else{
	  if(defaults::apbarStyle == "win")
	    dc->move ( cx-5, 4 );
	  else
	    dc->move ( cx-2, 0 );
	}
	/*
	if(defaults::apbarStyle == "hde")
	  dc->move ( 0, 4 );
	else
	  dc->move ( 0, y() + 6 );
	*/

	paintBG();
	//setUpdatesEnabled(TRUE);
}

void apbar::paletteChange(const QPalette &)  // property event will change client colors
{
	WINDOW *cur;
	XPropertyEvent xe;

	for(cur = ap.first(); cur != NULL; cur = ap.next())
	{
		xe.type = PropertyNotify;
		xe.window = qt_xrootwin();
		xe.state = PropertyNewValue;
		xe.atom = qapp::wm_resource_manager;
		XSendEvent(qt_xdisplay(), cur->w, True, PropertyChangeMask, (XEvent *)&xe);
	}
}

void apbar::release_all(void)
{
	WINDOW *cur;

	for(cur = ap.first(); cur != NULL; cur = ap.next())
	  XReparentWindow(qt_xdisplay(), cur->w, qt_xrootwin(), 0, 0);
		
	ap.clear();
}		

bool apbar::add(Window w, int number, QString &name)
{
	WINDOW *cur;
	int i=2,pnum=1;

	for(cur = ap.first(); cur != NULL; cur = ap.next())
	{
	    //	    if(cur->name == name)
	    if(cur->w == w)
		return FALSE;   // already exists

		if(number > cur->number)
			pnum = i;

		i++;	
	}

	XWindowAttributes attr;
	if(! XGetWindowAttributes(qt_xdisplay(), w, &attr))
		return TRUE;

	int twidth=2;  // total width
	for(cur = ap.first(); cur != NULL; cur = ap.next())
	  twidth += 22;
	//	  twidth += cur->width+2;

	//	if(twidth+attr.width+100 > tb_pb->width())
	if(twidth+120 > tb_pb->width())
	{
		cerr << "WM: No space left on toolbar\n";
		return FALSE;
	}

	WINDOW *client = new WINDOW;

	client->w = w;
	client->name = name;
	//	client->width = attr.width;
	client->width = 20;
	client->number = number;

	ap.insert(pnum-1, client);
	
	XSetWindowBorderWidth(qt_xdisplay(), w, 0);
	if(QApplication::reverseLayout() && 
	   (defaults::apbarStyle == "hde"))
	  XResizeWindow(qt_xdisplay(), w, 20, defaults::tc_height);
	//  XResizeWindow(qt_xdisplay(), w, attr.width, defaults::tc_height);
	else
	  XResizeWindow(qt_xdisplay(),w,20, defaults::tb_height-10);
	//  XResizeWindow(qt_xdisplay(),w,attr.width,defaults::tb_height-10);

	XReparentWindow(qt_xdisplay(), w, winId(), 0, 0);

	place_clients();

	return TRUE;
}

bool apbar::client_exists(Window w)
{
	WINDOW *cur;

	for(cur = ap.first(); cur != NULL; cur = ap.next())
	{
		if(cur->w == w)
			return TRUE;
	}
	return FALSE;
}

bool apbar::remove(Window w)
{
	WINDOW *cur;
	
	for(cur = ap.first(); cur != NULL; cur = ap.next())
	{
		if(cur->w == w)
		{
			ap.remove();
			place_clients();
			return TRUE;
		}
	}
	return FALSE;
}

void apbar::remove(void)
{
	WINDOW *cur;
	bool place = FALSE;

	for(cur = ap.first(); cur != NULL; cur = ap.next())
	{
		while(cur != NULL && qapp::apclients[cur->name] == 0)
		{
			ap.remove();
			XKillClient(qt_xdisplay(), cur->w);
			place = TRUE;
			cur = ap.current();
		}
	}
	if(place)
		place_clients();
}
