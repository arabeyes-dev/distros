/****************************************************************************
 *
 **********************************************************************/

#ifndef HWMSTYLE_H
#define HWMSTYLE_H

#ifndef QT_H
#include "qwindowsstyle.h"
#endif // QT_H



class QPalette;

#if defined(QT_PLUGIN)
#define Q_EXPORT_STYLE_HWM
#else
#define Q_EXPORT_STYLE_HWM Q_EXPORT
#endif

class Q_EXPORT_STYLE_HWM hwmStyle : public QWindowsStyle
{
    Q_OBJECT
public:
    hwmStyle();
    virtual ~hwmStyle();

    void drawComplexControl( ComplexControl control,
			     QPainter *p,
			     const QWidget *widget,
			     const QRect &r,
			     const QColorGroup &cg,
			     SFlags how = Style_Default,
			     SCFlags sub = SC_All,
			     SCFlags subActive = SC_None,
			     const QStyleOption& = QStyleOption::Default ) const;

protected:
    bool hwmstyle;

private:	
    hwmStyle( const hwmStyle & );
    hwmStyle& operator=( const hwmStyle & );
};



#endif // HWMSTYLE_H
