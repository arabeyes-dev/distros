/****************************************************************************
 **
 **				hcloseframe.cpp
 **			=========================
 **
 **  begin                : Tue Jul 02 2002
 **  copyright            : (C) 2002 by Haydar Alkaduhimi
 **  email                : haydar@haydar.net
 **
 **  A frame with close button
 **
 ****************************************************************************/

#include "hcloseframe.h"

#include <qvariant.h>
#include <qlabel.h>
#include <qtoolbutton.h>
#include <qmime.h>
#include <qdragobject.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>
#include <qimage.h>
#include <qpixmap.h>

#include <defaults.h>


HCloseFrame::HCloseFrame( QWidget* parent,  const char* name, WFlags fl )
    : QWidget( parent, name, fl )
{
    if ( !name )
	setName( "HCloseFrame" );
    resize( 178, 344 ); 
    setCaption( trUtf8( "HCloseFrame" ) );

    widgetList.setAutoDelete( TRUE );
    hidden = false;

    wdgetLayout = new QVBoxLayout( this, 0, 0, "wdgetLayout"); 
    //    captionLayout = new QHBoxLayout( this, 0, 0, "captionLayout"); 
    captionLayout = new QHBoxLayout( 0, 0, 0, "captionLayout"); 

    // Ad a caption label
    captionLable = new QLabel( this, "captionLable" );
    captionLable->setMinimumSize( QSize( 20, 20 ) );
    captionLable->setMaximumSize( QSize( 32767, 20 ) );
    captionLable->setText( trUtf8( "Caption" ) );
    captionLable->setPaletteForegroundColor(QColor("white"));
    captionLable->setPaletteBackgroundColor(QColor("darkGray"));
    captionLayout->addWidget( captionLable );

    // Make a close button
    closeBtn = new QToolButton( this, "closeBtn" );
    closeBtn->setMinimumSize( QSize( 20, 20 ) );
    closeBtn->setMaximumSize( QSize( 20, 20 ) );
    closeBtn->setPaletteBackgroundColor(QColor("darkGray"));


    hideBtn = new QToolButton( this, "closeBtn" );
    hideBtn->setMinimumSize( QSize( 20, 20 ) );
    hideBtn->setMaximumSize( QSize( 20, 20 ) );
    hideBtn->setPaletteBackgroundColor(QColor("darkGray"));

    // the close button should have a bold X caption
    if(defaults::get_cfile("images/tactivex.png").isNull()){
	QFont closeBtn_font(  closeBtn->font() );
        closeBtn_font.setBold( TRUE );
        closeBtn->setFont( closeBtn_font );  
        closeBtn->setText(  "X"  );
    }else{
	//closeBtn->setFlat( true );
	closeBtn->setPixmap(
		QPixmap(defaults::get_cfile("images/tactivex.png")));
    }

    if(defaults::get_cfile("images/tactiveup.png").isNull()){
	QFont hideBtn_font(  hideBtn->font() );
        hideBtn_font.setBold( TRUE );
        hideBtn->setFont( hideBtn_font );  
        hideBtn->setText(  "-"  );
    }else{
	//closeBtn->setFlat( true );
	hideBtn->setPixmap(
		QPixmap(defaults::get_cfile("images/tactiveup.png")));
    }


    // Make it flat
    closeBtn->setAutoRaise(true);
    hideBtn->setAutoRaise(true);

    captionLayout->addWidget( hideBtn );
    captionLayout->addWidget( closeBtn );
    wdgetLayout->addLayout( captionLayout );

    wdgetLayout->setAlignment( Qt::AlignTop );

    // Hide on click
    connect( closeBtn, SIGNAL( clicked() ), this, SLOT( hide() ) );
    connect( hideBtn, SIGNAL( clicked() ), this, SLOT( showHideWidgets() ) );
}

HCloseFrame::~HCloseFrame()
{
}

void HCloseFrame::changeCaption(QString cap){ 
    setCaption(" " + cap); 
    captionLable->setText(" " + cap);
}

void HCloseFrame::addWidget(QWidget *w){
    widgetList.append(w);
    wdgetLayout->add(w);
}

void HCloseFrame::hideWidgets()
{
    QWidget *w;
    for ( w = widgetList.first(); w; w = widgetList.next() )
	w->hide();

    if(defaults::get_cfile("images/tactivedown.png").isNull()){
	QFont hideBtn_font(  hideBtn->font() );
        hideBtn_font.setBold( TRUE );
        hideBtn->setFont( hideBtn_font );  
        hideBtn->setText(  "+"  );
    }else{
	//closeBtn->setFlat( true );
	hideBtn->setPixmap(
		QPixmap(defaults::get_cfile("images/tactivedown.png")));
    }
    hidden = true;

}

void HCloseFrame::showWidgets()
{
    QWidget *w;
    for ( w = widgetList.first(); w; w = widgetList.next() )
	w->show();

    if(defaults::get_cfile("images/tactiveup.png").isNull()){
	QFont hideBtn_font(  hideBtn->font() );
        hideBtn_font.setBold( TRUE );
        hideBtn->setFont( hideBtn_font );  
        hideBtn->setText(  "-"  );
    }else{
	//closeBtn->setFlat( true );
	hideBtn->setPixmap(
		QPixmap(defaults::get_cfile("images/tactiveup.png")));
    }

    hidden = false;

}

void HCloseFrame::showHideWidgets()
{
    if( hidden )
	showWidgets();
    else
	hideWidgets();
}

