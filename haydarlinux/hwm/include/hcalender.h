/****************************************************************************
**
**	hcalender.h
**
****************************************************************************/
#ifndef HCALENDER_H
#define HCALENDER_H

#include <qvariant.h>
#include <qwidget.h>
#include <qcombobox.h>
#include <qframe.h>
#include <qspinbox.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>
#include <qstringlist.h> 
#include <qdatetm.h>
#include <qpainter.h>
#include <qtable.h> 
//#include "defs.h"



class calFrame : public QTable
{ 
    Q_OBJECT
public:
    calFrame( QWidget* parent = 0, const char* name = 0, 
	      QDate date=QDate::currentDate() );
    ~calFrame();

    void setDate(QDate date){dt = date; refresh();};
    void refresh();
    void paintCell( QPainter * p, int row, int col, const QRect & cr,
			    bool selected );

    virtual void paintFocus( QPainter * p, const QRect & cr );

protected:
    QDate dt;
    //    virtual void drawContents ( QPainter * );

};

class HCalender : public QWidget
{ 
    Q_OBJECT

 public:
    HCalender( QWidget* parent = 0, const char* name = 0, WFlags fl = 0 );
    ~HCalender();

    bool showLines;
    QStringList monthNames;
    QComboBox* monthCombo;
    QSpinBox* yearSpin;
    calFrame* Cal;

    void reset(void);

 public slots:
    void changed();
    void calChanged( int, int );

 signals:
    void dateChanged(QDate);
 
 protected:
    QVBoxLayout* cLayout;
    QHBoxLayout* dateLayout;
    QDateTime dt;
};

#endif // HCALENDER_H
