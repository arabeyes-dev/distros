<!DOCTYPE TS><TS>
<context>
    <name>bgForm</name>
    <message>
        <source>Desktop</source>
        <translation>Bereaublad</translation>
    </message>
    <message>
        <source>Chose your background:</source>
        <translation>Kies uw achtergrond:</translation>
    </message>
    <message>
        <source>No Background</source>
        <translation>Geen Achtergrond</translation>
    </message>
    <message>
        <source>Browse</source>
        <translation>Bladeren</translation>
    </message>
    <message>
        <source>Position:</source>
        <translation>Positie:</translation>
    </message>
    <message>
        <source>Center</source>
        <translation>Gecentreerd</translation>
    </message>
    <message>
        <source>Tile</source>
        <translation>Naast elkaar</translation>
    </message>
    <message>
        <source>Stretch</source>
        <translation>Spreiden</translation>
    </message>
    <message>
        <source>OK</source>
        <translation></translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annuleren</translation>
    </message>
    <message>
        <source>Apply</source>
        <translation>Toepassen</translation>
    </message>
</context>
<context>
    <name>clrForm</name>
    <message>
        <source>Appearance</source>
        <translation>Vormgeving</translation>
    </message>
    <message>
        <source>Inactive Window</source>
        <translation>Niet-active venster</translation>
    </message>
    <message>
        <source>X</source>
        <translation></translation>
    </message>
    <message>
        <source>Start</source>
        <translation></translation>
    </message>
    <message>
        <source>Active Window</source>
        <translation>Active venster</translation>
    </message>
    <message>
        <source>Style:</source>
        <translation>Style:</translation>
    </message>
    <message>
        <source>Size:</source>
        <translation>Grote:</translation>
    </message>
    <message>
        <source>Color2:</source>
        <translation>Kleur2:</translation>
    </message>
    <message>
        <source>Desktop</source>
        <translation>Bereaublad</translation>
    </message>
    <message>
        <source>Active titlebar</source>
        <translation>Active titlebar</translation>
    </message>
    <message>
        <source>Inactive titlebar</source>
        <translation>Niet-active titlebar</translation>
    </message>
    <message>
        <source>Titlebar button</source>
        <translation>Titlebar knop</translation>
    </message>
    <message>
        <source>Start button</source>
        <translation>Start knop</translation>
    </message>
    <message>
        <source>Item:</source>
        <translation>Item:</translation>
    </message>
    <message>
        <source>Color1:</source>
        <translation>Kleur1:</translation>
    </message>
    <message>
        <source>Font:</source>
        <translation>Lettertype:</translation>
    </message>
    <message>
        <source>Name:</source>
        <translation>Naam:</translation>
    </message>
    <message>
        <source>Text Color:</source>
        <translation>Tekst kleur:</translation>
    </message>
    <message>
        <source>Choose</source>
        <translation>Kies</translation>
    </message>
    <message>
        <source>OK</source>
        <translation></translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annuleren</translation>
    </message>
    <message>
        <source>Apply</source>
        <translation>Toepassen</translation>
    </message>
</context>
<context>
    <name>hDisplay</name>
    <message>
        <source>Display</source>
        <translation></translation>
    </message>
    <message>
        <source>Desktop</source>
        <translation>Bereaublad</translation>
    </message>
    <message>
        <source>Appearance</source>
        <translation>Vormgeving</translation>
    </message>
    <message>
        <source>OK</source>
        <translation></translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annuleren</translation>
    </message>
    <message>
        <source>apply</source>
        <translation>Toepassen</translation>
    </message>
</context>
</TS>
