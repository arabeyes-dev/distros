#include <qapplication.h>
#include <defaults.h>
#include "deskitem.h"

#include <iostream>
using std:: cout; using std::endl;


int main( int argc, char ** argv )
{
    bool hasDesktop =false;
    if(argc < 2){
	cout << "Please Specify a Menu/Desktop file\n";
	exit(1);
    }else{
	hasDesktop = true;
    }
    QApplication a( argc, argv );

    defaults::read_config();
    QTranslator translator( 0 );
    QTranslator qt_translator( 0 );
    translator.load( "hdatetime_" + defaults::lng,defaults::get_cfile("lng"));

    QString QTDIR = getenv( "QTDIR" );

    qt_translator.load( QString( "qt_%1" ).arg( defaults::lng ), 
			QTDIR + "/translations" );

    a.installTranslator( &qt_translator );
    a.installTranslator( &translator );
    

    deskitem w;
    if(hasDesktop){
	QString filename = argv[1];
	/*
	if(argc > 2){
	    for( int i = 2; i < argc; i++){
		filename += " "; 
		filename += argv[i];
	    }
	}
	*/
	cout << argv[1] << endl;
	if(! w.setFile(filename)){
	    //	    cout << "The file: \"" << filename << "\" not found\n";
	    exit(1);
	}
    }
    w.show();
    a.connect( &a, SIGNAL( lastWindowClosed() ), &a, SLOT( quit() ) );
    return a.exec();
}
