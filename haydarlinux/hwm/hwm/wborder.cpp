/****************************************************************************
 **
 **				wborder.cpp
 **			=========================
 **
 **  begin                : 2001 based on qlwm
 **  copyright            : (C) 2001 - 2002 by Haydar Alkaduhimi
 **  email                : haydar@haydar.net
 **  License              : GPL
 **
 **  Creates the window borders
 **
 ****************************************************************************/

//#include "defs.h"
#include "defaults.h"
#include "qapp.h"
//#include "moc_wborder.cpp"

#include "wborder.h"

bool leftshown = false;
bool withhelp  = false;

httip::httip( QWidget* parent, const QString& tip )
    : QLabel( NULL, "httip", WStyle_StaysOnTop + 
	      WStyle_Customize + WStyle_NoBorder + WStyle_Tool )
{
//    setMargin(1);
    setIndent(0);
    setFrameStyle( QFrame::Plain | QFrame::Box );
    setLineWidth(1);
    setAlignment( AlignLeft | AlignTop );
    setText(tip);
    adjustSize();

    // Use stock Qt tooltip colors as defined in qapplication.cpp
    QColorGroup clrGroup( Qt::black, QColor(255,255,220),
			  QColor(96,96,96), Qt::black, Qt::black,
			  Qt::black, QColor(255,255,220) );
    setPalette( QPalette( clrGroup, clrGroup, clrGroup ) );
    
    btn = parent;
    connect(&hideTimer, SIGNAL(timeout()), SLOT(hideTip()));
    connect(&showTimer, SIGNAL(timeout()), SLOT(showTip()));
}

httip::~httip()
{
}

void httip::setTipText( const QString& tip )
{
	bool visible = isVisible();

	if (visible) 
		hide();

	setText(tip);
	adjustSize();
	positionTip();

	if (visible) 
		showTip();
}

void httip::enterTip()
{
	// Show after 1 second
	showTimer.start( 1000, TRUE );
}

void httip::leaveTip()
{
	if (hideTimer.isActive())
		hideTimer.stop();
	if (showTimer.isActive())
		showTimer.stop();
	if (isVisible())
		hide();
}

/* Internal */
void httip::showTip()
{
	if (isVisible())
		return;

	// Ignore empty tooltips
	if (text().isEmpty())
		return;

	positionTip();

	show();
	raise();

	// Hide after 10 seconds
	hideTimer.start( 10000, TRUE );
}

/* Internal */
void httip::hideTip()
{
	if (isVisible())
		hide();
}

/* Internal */
void httip::positionTip(){
    QPoint p = btn->mapToGlobal(btn->rect().bottomLeft()) + QPoint(0, 16);

    if (p.x() < 0) p.setX(0);
    if (p.y() < 0) p.setY(0); 
    move(p); 

    // Ensure we don't get enter/leave event race conditions when a
    // tooltip is over a button.
    QRect btnGlobalRect( btn->mapToGlobal(btn->rect().topLeft()),
			 btn->mapToGlobal(btn->rect().bottomRight()) );
    QRect tipGlobalRect( mapToGlobal(rect().topLeft()),
			 mapToGlobal(rect().bottomRight()) );
    if (btnGlobalRect.intersects( tipGlobalRect )){
	// Only intersects when button is at lower part of screen
	p.setY( btn->mapToGlobal(btn->rect().topLeft()).y()-height()- 5 );
	move(p);
    }
}




wframe::wframe(QWidget *parent, const char *name) : QLabel(parent, name)
{
    setFrameStyle(QFrame::Panel|QFrame::Raised);
    setLineWidth(0);
    setMinimumSize( 1, 1 );

    //    setSizePolicy(QSizePolicy(QSizePolicy::Expanding, 
    //		      QSizePolicy::Expanding)); 

    //    setBackgroundColor("black");
    setBackgroundMode(Qt::X11ParentRelative); 
}


void wframe::set_text(QString btxt, bool center)
{
    //btxt = QTextCodec::codecForName("utf8")->toUnicode(btxt);
    if(QApplication::reverseLayout())
	setAlignment(center?Qt::AlignCenter:Qt::AlignRight|Qt::AlignVCenter);
    else
	setAlignment(center?Qt::AlignCenter:Qt::AlignLeft|Qt::AlignVCenter);

    setText(QString::fromUtf8(btxt));
    //QPainter p(this);
    //p.drawText(x()+2, 1, width()-2, height(), 
    //	     AlignLeft|AlignVCenter, btxt);
    //p.end;
    setMinimumWidth ( 1 );
}


void wframe::drawbg(void)
{
    QPixmap qp(width(), height());
    QPainter painter(&qp);
    QColor firstColor(Qt::gray);
    //if (defaults::windowsBtns){
    /*
    if ((defaults::styleName == "win98Style") || 
	(defaults::styleName == "win95Style")){
    */
    if ( defaults::titleStyle == "win"){
	firstColor = QApplication::palette().active().background();
    }else
	firstColor = palette().active().background();
    
    qp.fill(firstColor);
    
    //QColor bg1Color(topLevelWidget()->palette().active().background());
    int i = 0;
    
    //int h = (height() - 4) / 2;

	
    if(frameName == "ulframe"){    
	for ( i = height()/3; i < height(); ++i ) {
	    painter.setPen(firstColor.dark(120));
	    painter.drawLine(0,i , width(), i); 
	}

	for ( i = 2*height()/3; i < height(); ++i ) {
	    painter.setPen(firstColor.dark(150).dark(120));
	    painter.drawLine(0,i , width(), i); 
	}
	//if (defaults::windowsBtns){
	/*
	if ((defaults::styleName == "win98Style") || 
	    (defaults::styleName == "win95Style")){
	*/
	if ( defaults::titleStyle == "win"){
	    painter.setPen(QColor(Qt::black));
	    painter.drawLine(0,height()-1 , width(), height()-1);   
	}
	setBackgroundPixmap(qp);


    }else if(frameName == "uframe"){
	int x = height()/3;

	painter.setPen(firstColor);
	for ( i = 0; i < height(); ++i ) {
	    painter.drawLine(0,i , width(), i); 
	}

   	painter.setPen(firstColor.light(120));
	for ( i = x+1; i < 2*x; ++i ) {
	    painter.drawLine(0,i , width(), i); 
	    painter.drawLine(i,x+1 , i, height()); 
	}


	painter.setPen(firstColor.light(130));
	for ( i = 1; i < x+1; ++i ) {
	    painter.drawLine(0,i , width(), i); 
	    painter.drawLine(i,0 , i, height()); 
	}

	// The dark side
   	painter.setPen(firstColor.dark(120));
	for ( i = width()-(2*x)-1; i < width()-x-1; ++i ) {
	    painter.drawLine(i,x+1 , i, height()); 
	}

   	painter.setPen(firstColor.dark(150).dark(120));
	for ( i = width()-x-1; i < width(); ++i ) {
	    painter.drawLine(i,1 , i, height()); 
	}

	painter.setPen(firstColor.dark(120));
	painter.drawLine(0,0 , width(), 0); 
	painter.drawLine(0,0 , 0, height()); 

	//	if (defaults::windowsBtns){
	/*
	if ((defaults::styleName == "win98Style") || 
	    (defaults::styleName == "win95Style")){
	*/
	if ( defaults::titleStyle == "win"){
	    painter.setPen(Qt::black);
	    painter.drawLine(width()-1, 0, width()-1, height()); 
	}
	
	setBackgroundPixmap(qp);

    }else if(frameName == "luframe"){

	int x = width()/3;
	painter.setPen(firstColor);
	for ( i = 0; i < defaults::lowerborderheight; ++i ) {
	    painter.drawLine(i, 0, i, height()); 
	}

	if(QApplication::reverseLayout()){
	    painter.setPen(firstColor.dark(120));
	    for ( i = width()- (2*x)-1; i < width() - x-1; ++i ) {
		painter.drawLine(i,0 , i, height()); 
	    }

	    painter.setPen(firstColor.dark(150).dark(120));
	    for ( i = width() - x-1; i < width(); ++i ) {
		painter.drawLine(i, 0, i, height()); 
	    }

	    //	    if (defaults::windowsBtns){
	    /*
	    if ((defaults::styleName == "win98Style") || 
		(defaults::styleName == "win95Style")){
	    */
	    if ( defaults::titleStyle == "win"){
		painter.setPen(Qt::black);
		painter.drawLine(width()-1,0 , width()-1, height()); 
	    }

	}else{
	    painter.setPen(firstColor.light(130));
	    for ( i = 1; i < x+1 ; ++i ) {
	    painter.drawLine(i,0 , i, height()); 
	    }
	    
	    painter.setPen(firstColor.light(120));
	    for ( i = x+1; i < 2*x; ++i ) {
		painter.drawLine(i,0 , i, height()); 
	    }

	
	    painter.setPen(firstColor.dark(120));
	    painter.drawLine(0,0 , 0, height()); 
	}
	setBackgroundPixmap(qp);

    }else if(frameName == "ruframe"){
	int x = width()/3;
	if(QApplication::reverseLayout()){
	    painter.setPen(firstColor);
	    for ( i = 0; i < defaults::lowerborderheight; ++i ) {
		painter.drawLine(i, 0, i, height()); 
	    }
	    painter.setPen(firstColor.light(130));
	    for ( i = 1; i < x+1 ; ++i ) {
	    painter.drawLine(i,0 , i, height()); 
	    }
	    
	    painter.setPen(firstColor.light(120));
	    for ( i = x+1; i < 2*x; ++i ) {
		painter.drawLine(i,0 , i, height()); 
	    }

	
	    painter.setPen(firstColor.dark(120));
	    painter.drawLine(0,0 , 0, height()); 

	}else{
	    painter.setPen(firstColor);
	    for ( i = width()- defaults::lowerborderheight; 
		  i < width() - (2*x)-1 ; ++i ) {
		painter.drawLine(i,0 , i, height()); 
	    }

	    painter.setPen(firstColor.dark(120));
	    for ( i = width()- (2*x)-1; i < width() - x-1; ++i ) {
		painter.drawLine(i,0 , i, height()); 
	    }

	    painter.setPen(firstColor.dark(150).dark(120));
	    for ( i = width() - x-1; i < width(); ++i ) {
		painter.drawLine(i, 0, i, height()); 
	    }

	    //if (defaults::windowsBtns){
	    /*
	    if ((defaults::styleName == "win98Style") || 
		(defaults::styleName == "win95Style")){
	    */
	    if ( defaults::titleStyle == "win"){
		painter.setPen(Qt::black);
		painter.drawLine(width()-1,0 , width()-1, height()); 
	    }
	}
	setBackgroundPixmap(qp);

    }else if(frameName == "ullframe"){

	//qp.resize ( width(), height() );
	if(QApplication::reverseLayout()){

	    for ( i = height()/3; i < height(); ++i ) {
		painter.setPen(firstColor.dark(120));
		painter.drawLine(0,i , width(), i); 
	    }


	    for ( i = 2*height()/3; i < height(); ++i ) {
		painter.setPen(firstColor.dark(150).dark(120));
		painter.drawLine(0,i , width(), i); 
	    }



	    for ( i = width() - (2*height()/3)-1 ;  i < width() - 
		      (height()/3)-1 ; ++i){
		painter.setPen(firstColor.dark(120));
		painter.drawLine(i, 0, i,  (2* height()/3)-1); 
	    }
	
	    for ( i = width()- (height()/3)-1 ;  i < width() ; ++i){
		painter.setPen(firstColor.dark(150).dark(120));
		painter.drawLine(i, 0, i, height()); 
	    }



	    //if (defaults::windowsBtns){
	    /*
	    if ((defaults::styleName == "win98Style") || 
		(defaults::styleName == "win95Style")){
	    */
	    if ( defaults::titleStyle == "win"){
		painter.setPen(Qt::black);
		painter.drawLine(0, height()-1 , width(), height()-1); 
		painter.drawLine(width()-1, 0 , width()-1, height()); 
	    }

    //	setBackgroundPixmap(qp);

	}else{	
	    for ( i = height()/3; i < height(); ++i ) {
		painter.setPen(firstColor.dark(120));
		painter.drawLine(0,i , width(), i); 
	    }

	    for ( i = 2*height()/3; i < height(); ++i ) {
		painter.setPen(firstColor.dark(150).dark(120));
		painter.drawLine(0,i , width(), i); 
	    }

	    for ( i = 1; i < (height()/3)+1; ++i ) {
		painter.setPen(firstColor.light(130));
		painter.drawLine(i,0 , i, (2*height()/3)-1); 
	    }
	    /*
	    //if (defaults::windowsBtns){
	    if ((defaults::styleName == "win98Style") || 
		(defaults::styleName == "win95Style")){
	    */
	    if ( defaults::titleStyle == "win"){
		painter.setPen(Qt::black);
		painter.drawLine(1, height()-1, width(), height()-1); 
	    }


	    for ( i = (height()/3)+1; i < 2*(height()/3)+1; ++i ) {
		painter.setPen(firstColor.light(130));
		painter.drawLine(i,0 , i, (height()/3)-1); 
	    }


	    painter.setPen(firstColor.dark(120));
	    painter.drawLine(0,0 , 0, height()); 
	}
	setBackgroundPixmap(qp);
	
    }else if(frameName == "ulrframe"){
	if(QApplication::reverseLayout()){
	    for ( i = height()/3; i < height(); ++i ) {
		painter.setPen(firstColor.dark(120));
		painter.drawLine(0,i , width(), i); 
	    }

	    for ( i = 2*height()/3; i < height(); ++i ) {
		painter.setPen(firstColor.dark(150).dark(120));
		painter.drawLine(0,i , width(), i); 
	    }

	    for ( i = 1; i < (height()/3)+1; ++i ) {
		painter.setPen(firstColor.light(130));
		painter.drawLine(i,0 , i, (2*height()/3)-1); 
	    }
	    /*
	    //if (defaults::windowsBtns){
	    if ((defaults::styleName == "win98Style") || 
		(defaults::styleName == "win95Style")){
	    */
	    if ( defaults::titleStyle == "win"){
		painter.setPen(Qt::black);
		painter.drawLine(1, height()-1, width(), height()-1); 
	    }


	    for ( i = (height()/3)+1; i < 2*(height()/3)+1; ++i ) {
		painter.setPen(firstColor.light(130));
		painter.drawLine(i,0 , i, (height()/3)-1); 
	    }


	    painter.setPen(firstColor.dark(120));
	    painter.drawLine(0,0 , 0, height()); 

	}else{	


	    for ( i = height()/3; i < height(); ++i ) {
		painter.setPen(firstColor.dark(120));
		painter.drawLine(0,i , width(), i); 
	    }


	    for ( i = 2*height()/3; i < height(); ++i ) {
		painter.setPen(firstColor.dark(150).dark(120));
		painter.drawLine(0,i , width(), i); 
	    }


	    for ( i = width() - (2*height()/3)-1 ;  i < width() - 
		      (height()/3)-1 ; ++i){
		painter.setPen(firstColor.dark(120));
		painter.drawLine(i, 0, i,  (2* height()/3)-1); 
	    }
	
	    for ( i = width()- (height()/3)-1 ;  i < width() ; ++i){
		painter.setPen(firstColor.dark(150).dark(120));
		painter.drawLine(i, 0, i, height()); 
	    }
	    /*
	    //if (defaults::windowsBtns){
	    if ((defaults::styleName == "win98Style") || 
		(defaults::styleName == "win95Style")){
	    */
	    if ( defaults::titleStyle == "win"){
		painter.setPen(Qt::black);
		painter.drawLine(0, height()-1 , width(), height()-1); 
		painter.drawLine(width()-1, 0 , width()-1, height()); 
	    }
	}
	setBackgroundPixmap(qp);

    }else if(frameName == "slframe"){
	if(QApplication::reverseLayout()){
	    for ( i = width()/3; i < width(); ++i ) {
		painter.setPen(firstColor.dark(120));
		painter.drawLine(i,0 , i, height()); 
	    }

	    for ( i = 2 * width()/3; i < width(); ++i ) {
		painter.setPen(firstColor.dark(150).dark(120));
		painter.drawLine(i,0 , i, height()); 
	    }

	    /*
	    //if (defaults::windowsBtns){
	    if ((defaults::styleName == "win98Style") || 
		(defaults::styleName == "win95Style")){
	    */
	    if ( defaults::titleStyle == "win"){
		painter.setPen(Qt::black);
		painter.drawLine(width()-1, 0 , width()-1, height()); 
	    }

	}else{
	    for ( i = 1; i < (width()/3)+1; ++i ) {
		painter.setPen(firstColor.light(130));
		painter.drawLine(i,0 , i, height()); 
	    }

	    for ( i = (width()/3)+1; i < (2 * width()/3); ++i ) {
		painter.setPen(firstColor.light(120));
		painter.drawLine(i,0 , i, height()); 
	    }

	    painter.setPen(firstColor.dark(120));
	    painter.drawLine(0,0 , 0, height()); 
	}

	setBackgroundPixmap(qp);


    }else if(frameName == "srframe"){
	if(QApplication::reverseLayout()){
	    for ( i = 1; i < (width()/3)+1; ++i ) {
		painter.setPen(firstColor.light(130));
		painter.drawLine(i,0 , i, height()); 
	    }

	    for ( i = (width()/3)+1; i < (2 * width()/3); ++i ) {
		painter.setPen(firstColor.light(120));
		painter.drawLine(i,0 , i, height()); 
	    }

	    painter.setPen(firstColor.dark(120));
	    painter.drawLine(0, 0 , 0, height()); 
	}else{
	    for ( i = width()/3; i < width(); ++i ) {
		painter.setPen(firstColor.dark(120));
		painter.drawLine(i,0 , i, height()); 
	    }

	    for ( i = 2 * width()/3; i < width(); ++i ) {
		painter.setPen(firstColor.dark(150).dark(120));
		painter.drawLine(i,0 , i, height()); 
	    }

	    /*
	    //if (defaults::windowsBtns){
	    if ((defaults::styleName == "win98Style") || 
		(defaults::styleName == "win95Style")){
	    */
	    if ( defaults::titleStyle == "win"){
		painter.setPen(Qt::black);
		painter.drawLine(width()-1, 0 , width()-1, height()); 
	    }
	}
	setBackgroundPixmap(qp);
    }else if(frameName == "smallframe"){
	int x = height()/3;

	painter.setPen(firstColor);
	for ( i = 0; i < height(); ++i ) {
	    painter.drawLine(0,i , width(), i); 
	}

   	painter.setPen(firstColor.light(120));
	for ( i = x+1; i < 2*x; ++i ) {
	    painter.drawLine(0,i , width(), i); 
	    painter.drawLine(i,x+1 , i, height()); 
	}


	painter.setPen(firstColor.light(130));
	for ( i = 1; i < x+1; ++i ) {
	    painter.drawLine(0,i , width(), i); 
	    painter.drawLine(i,0 , i, height()); 
	}

	// The dark side
   	painter.setPen(firstColor.dark(120));
	for ( i = width()-(2*x)-1; i < width()-x-1; ++i ) {
	    painter.drawLine(i,x+1 , i, height()); 
	}

   	painter.setPen(firstColor.dark(150).dark(120));
	for ( i = width()-x-1; i < width(); ++i ) {
	    painter.drawLine(i,1 , i, height()); 
	}

	painter.setPen(firstColor.dark(120));
	painter.drawLine(0,0 , width(), 0); 
	painter.drawLine(0,0 , 0, height()); 

	/*
	//if (defaults::windowsBtns){
	if ((defaults::styleName == "win98Style") || 
	    (defaults::styleName == "win95Style")){
	*/
	if ( defaults::titleStyle == "win"){
	    painter.setPen(Qt::black);
	    painter.drawLine(width()-1, 0, width()-1, height()); 
	}
	
	setBackgroundPixmap(qp);
    }
    painter.end();
}

void wframe::resizeEvent(QResizeEvent *)
{
    //setMinimumWidth ( 1 );
    repaint();
    drawbg();
}

void wframe::mousePressEvent(QMouseEvent *event)
{
	emit press(event);
	emit press();

	if(event->button() == QMouseEvent::RightButton)
	{
		emit right_press();
	}	
	else if(event->button() == QMouseEvent::LeftButton)
	{
		emit left_press(event);
		emit left_press();
	}
	else if(event->button() == QMouseEvent::MidButton)
	{
		emit mid_press();
	}
}

void wframe::mouseReleaseEvent(QMouseEvent *event)
{
	emit release(event);

	if(event->button() == QMouseEvent::RightButton)
	{
		emit right_release();
	}
	else if(event->button() == QMouseEvent::LeftButton)
	{
		emit left_release(event);
	}
	else if(event->button() == QMouseEvent::MidButton)
	{
		emit mid_release();
	}
}

void wframe::mouseMoveEvent(QMouseEvent *event)
{
	emit mouse_move(event);
}




wbutton::wbutton(QWidget *parent, const char *name) : QPushButton(parent, name)
{
    buttonTip = NULL;

    if(defaults::flaticons)
	setBackgroundMode(Qt::X11ParentRelative); 

    //    if ( envelope.mask() )
    //setMask( *envelope.mask() );

}

void wbutton::setTipText(const QString& newTip)
{
    if (buttonTip)
	buttonTip->setTipText( newTip );
    else{
	buttonTip = new httip(this, newTip);
	buttonTip->setTipText( newTip );
    }
}

wbutton::~wbutton()
{
}

void wbutton::enterEvent(QEvent *e)
{
    if (buttonTip)
        buttonTip->enterTip();
    QPushButton::enterEvent( e );
}

void wbutton::leaveEvent(QEvent *e)
{
    if (buttonTip)
        buttonTip->leaveTip();
    QPushButton::leaveEvent( e );
}

void wbutton::mousePressEvent(QMouseEvent *event)
{

    if (buttonTip)
        buttonTip->leaveTip();

    emit press(event);
    emit press();
    
    if(event->button() == QMouseEvent::RightButton){
	emit right_press();
    }	
    else if(event->button() == QMouseEvent::LeftButton){
	emit left_press(event);
	emit left_press();
	QPushButton::mousePressEvent( event );
    }
    else if(event->button() == QMouseEvent::MidButton){
	emit mid_press();
    }
}

void wbutton::mouseReleaseEvent(QMouseEvent *event)
{
  if (buttonTip)
    buttonTip->leaveTip();

  emit release(event);

  if(event->button() == QMouseEvent::RightButton)
  {
    emit right_release();
  }
  else if(event->button() == QMouseEvent::LeftButton)
  {
    emit left_release(event);
    QPushButton::mouseReleaseEvent( event );
  }
  else if(event->button() == QMouseEvent::MidButton)
  {
    emit mid_release();
  }
}

void lsborder::drawbg(void)
{
    midframe->drawbg();
    lframe->drawbg();
}


lsborder::lsborder(QWidget *parent, const char *name) : QWidget(parent, name)
{
	layout = new QVBoxLayout(this);
	Q_CHECK_PTR(layout);
	midframe = new wframe(this, "lsmidframe");
	Q_CHECK_PTR(midframe);
	midframe->frameName = "slframe";

	lframe = new wframe(this, "lslframe");
	Q_CHECK_PTR(lframe);
	lframe->frameName = "slframe";
	
	lframe->setMaximumHeight( defaults::lowerborderwidth );

	setFixedWidth(defaults::lowerborderheight);

	midframe->setCursor(QCursor(Qt::sizeHorCursor));
	if(QApplication::reverseLayout())
	    lframe->setCursor(QCursor(Qt::sizeFDiagCursor));
	else
	    lframe->setCursor(QCursor(Qt::sizeBDiagCursor));

	layout->add(midframe);
	layout->add(lframe);
}


void rsborder::drawbg(void)
{
    midframe->drawbg();
    lframe->drawbg();

}

rsborder::rsborder(QWidget *parent, const char *name) : QWidget(parent, name)
{
	layout = new QVBoxLayout(this);
	Q_CHECK_PTR(layout);

	midframe = new wframe(this, "rsmidframe");
	Q_CHECK_PTR(midframe);
	midframe->frameName = "srframe";

	lframe = new wframe(this, "rslframe");
	Q_CHECK_PTR(lframe);
	lframe->frameName = "srframe";

	
	lframe->setMaximumHeight( defaults::lowerborderwidth );

	setFixedWidth(defaults::lowerborderheight);

	midframe->setCursor(QCursor(Qt::sizeHorCursor));
    if(QApplication::reverseLayout())
	lframe->setCursor(QCursor(Qt::sizeBDiagCursor));
    else
	lframe->setCursor(QCursor(Qt::sizeFDiagCursor));

	layout->add(midframe);
	layout->add(lframe);
}



void lborder::drawbg(void)
{
    leftframe->drawbg();
    midframe->drawbg();
    rightframe->drawbg();

}


lborder::lborder(QWidget *parent, const char *name) : QWidget(parent, name)
{
	layout = new QHBoxLayout(this);
	Q_CHECK_PTR(layout);
	leftframe = new wframe(this, "lleftframe");
	Q_CHECK_PTR(leftframe);
	leftframe->frameName = "ullframe";
	midframe = new wframe(this, "lmidframe");
	Q_CHECK_PTR(midframe);
	midframe->frameName = "ulframe";

	rightframe = new wframe(this, "lrightframe");
	Q_CHECK_PTR(rightframe);
	rightframe->frameName = "ulrframe";

	leftframe->setMaximumWidth(defaults::lowerborderwidth);
	rightframe->setMaximumWidth(defaults::lowerborderwidth);
	setFixedHeight(defaults::lowerborderheight);

    if(QApplication::reverseLayout())
	leftframe->setCursor(QCursor(Qt::sizeFDiagCursor));
    else
	leftframe->setCursor(QCursor(Qt::sizeBDiagCursor));

    midframe->setCursor(QCursor(Qt::sizeVerCursor));

    if(QApplication::reverseLayout())
	rightframe->setCursor(QCursor(Qt::sizeBDiagCursor));
    else
	rightframe->setCursor(QCursor(Qt::sizeFDiagCursor));

    layout->add(leftframe);
    layout->add(midframe);
    layout->add(rightframe);
}

void uborder::set_small(void)
{
    uframe->hide();
    small = true;

	setFixedHeight(defaults::lowerborderheight);
	//if(defaults::windowsBtns){
	/*
	if ((defaults::styleName == "win98Style") || 
	    (defaults::styleName == "win95Style")){
	*/
	if ( defaults::titleStyle == "win"){
	    minbtn->setFixedWidth(defaults::lowerborderwidth);
	    minbtn->setPixmap(QPixmap());
	    
	    maxbtn->setFixedWidth(defaults::lowerborderwidth);
	    maxbtn->setPixmap(QPixmap());
	}else{
	    leftframe->setFixedWidth(defaults::lowerborderwidth);
	    leftframe->setPixmap(QPixmap());
	}
	rightframe->setFixedWidth(defaults::lowerborderwidth);
	rightframe->setPixmap(QPixmap());
	midframe->setText("");
}

void uborder::set_normal(void)
{
    setFixedHeight(defaults::windowbuttonsize);
    uframe->show();
    small = false;
    /*
    //if(defaults::windowsBtns){
    if ((defaults::styleName == "win98Style") || 
	(defaults::styleName == "win95Style")){
    */
    if ( defaults::titleStyle == "win"){
	minbtn->setFixedWidth(defaults::iconsize);
	minbtn->setPixmap(*qapp::minbtnpix);
	
	maxbtn->setFixedWidth(defaults::iconsize);
	maxbtn->setPixmap(*qapp::maxbtnpix);
    }else{
	leftframe->setFixedWidth(defaults::iconsize);
	leftframe->setPixmap(*qapp::leftwinpix);
    }
    rightframe->setFixedWidth(defaults::iconsize);
    rightframe->setPixmap(*qapp::rightwinpix);
}

void uborder::set_max(void)
{

    luframe->hide();
    ruframe->hide();
    uframe->hide();
    layout2->setMargin(0);
    //layout2->setMargin((defaults::windowbuttonsize-defaults::iconsize)/ 2);
    layout->setMargin((defaults::lowerborderheight/2)+2);
}

void uborder::noTopBorder(void)
{
    uframe->hide();
    //    layout2->setMargin(2);
    layout2->setMargin((defaults::windowbuttonsize-defaults::iconsize) / 2);
    //    layout->setMargin((defaults::lowerborderheight/2)+2);
}

void uborder::drawbg(void){
//void uborder::paintEvent ( QPaintEvent * ){

    QPixmap qp(width(), height());
    QPainter painter(&qp);
    QColor firstColor(palette().active().background());
    /*
    if ((defaults::styleName == "win98Style") || 
	(defaults::styleName == "win95Style")){
    */
    if(!qapp::titlepix->isNull()){
	if( (!qapp::untitlepix->isNull()) 
	    && (topLevelWidget()->palette().active().background() == 
		defaults::inactive_bg) ){
	    painter.drawTiledPixmap( 0, 0, width(), height(), 
				     *qapp::untitlepix);

	    if (!qapp::untitlerightpix->isNull()){
		if(QApplication::reverseLayout())
		    painter.drawPixmap(0, 0, *qapp::untitlerightpix);
		else
		    painter.drawPixmap(width()-qapp::untitlerightpix->width(),0, *qapp::untitlerightpix);

	    }

	    if (!qapp::untitleleftpix->isNull()){
		if(QApplication::reverseLayout())
		    painter.drawPixmap(width()-qapp::untitleleftpix->width(),0, *qapp::untitleleftpix);
		else
		    painter.drawPixmap(0, 0, *qapp::untitleleftpix);
	    }

	}else{
	    painter.drawTiledPixmap( 0, 0, width(), height(), 
			       *qapp::titlepix);

	    if (!qapp::titlerightpix->isNull()){
		if(QApplication::reverseLayout())
		    painter.drawPixmap(0, 0, *qapp::titlerightpix);
		else
		    painter.drawPixmap(width()-qapp::titlerightpix->width(),0, *qapp::titlerightpix);

	    }

	    if (!qapp::titleleftpix->isNull()){
		if(QApplication::reverseLayout())
		    painter.drawPixmap(width()-qapp::titleleftpix->width(),0, *qapp::titleleftpix);
		else
		    painter.drawPixmap(0, 0, *qapp::titleleftpix);
	    }
	}

    }else if ( defaults::titleStyle == "win"){
	setBackgroundColor(QApplication::palette().active().background());
	firstColor = QApplication::palette().active().background();
	setPalette(QApplication::palette().active().background());

	if (topLevelWidget()->palette().active().background() == 
	    defaults::active_bg)
	    setPaletteForegroundColor(defaults::active_fg);
	else
	    setPaletteForegroundColor(defaults::inactive_fg);

	QColor bg1Color(topLevelWidget()->palette().active().background());
	QColor bg2Color(Qt::white);

	if (bg1Color == defaults::active_bg)
	    bg2Color = defaults::active_bg2;
	else
	    bg2Color = defaults::inactive_bg2;

	/*
	if (defaults::styleName == "win98Style"){
	*/
	if ( defaults::titleStyle == "win"){
	    if(QApplication::reverseLayout())
		gradient(qp, bg2Color, bg1Color, QString(""), 1);
	    else
		gradient(qp, bg1Color, bg2Color, QString(""), 1);
	}else
	    qp.fill(bg1Color);


	int i =0;
	int x = defaults::lowerborderheight/3;

	painter.setPen(firstColor);
	for ( i = height()- x ; i < height() ; ++i ) {
	    painter.drawLine(0,i , width(), i); 
	}

	    
	//painter.end();
	setBackgroundPixmap(qp);

	luframe->drawbg();
	ruframe->drawbg();
	uframe->drawbg();

    }else{

	qp.fill(firstColor);

	int i = 0;

	int h = (height() - 4) / 2;

	painter.setPen(firstColor.dark(100+h*3));
	painter.drawLine(0,0 , width(), 0); 

	painter.setPen(firstColor.light(100+h*3));
 
	for ( i = 1; i < 3; ++i ) {
	    painter.setPen(firstColor.light(100+(h*3)-i));
	    painter.drawLine(0,i , width(), i); 
	}

	for ( i = 0; i < h-1; ++i ) {
	    painter.setPen(firstColor.dark(100 + ((h*3)-3)-(i*3)));
	    painter.drawLine(0,i+4 , width(), i+4); 
	}



	int j = 0;

	for ( i = height()-h; i < height()-1 ; ++i ) {
	    j++;
	    painter.setPen(firstColor.light(100 +(j*3)));
	    painter.drawLine(0, i , width(), i); 
	}


	painter.setPen(firstColor.dark(100+(h*3)));
	
	for ( i = height()-2; i < height(); ++i ) {
	    painter.drawLine(0,i , width(), i); 
	}


	painter.setPen(firstColor.dark(130));
	painter.drawLine(0,0 , 0, height()); 

	painter.setPen(firstColor.light(130));
	painter.drawLine(1,1 , 1, height()); 

	painter.setPen(firstColor.light(120));
	painter.drawLine(2,2 , 2, height()); 



	painter.setPen(firstColor.dark(120));
	painter.drawLine(width()-3,2 , width()-3, height()); 

	painter.setPen(firstColor.dark(130));
	painter.drawLine(width()-2,1 , width()-2, height()); 

	painter.setPen(firstColor.dark(150).dark(120));
	painter.drawLine(width()-1,0 , width()-1, height()); 

	setBackgroundPixmap(qp);
    }
    setBackgroundPixmap(qp);
    painter.end();

    luframe->drawbg();
    ruframe->drawbg();
    uframe->drawbg();

}

void uborder::checkForSize(QSize s){


    if(defaults::windowsBtns){
	if(leftshown && withhelp){
	    if(s.width()< defaults::iconsize
		     + (2*defaults::lowerborderheight)){
		if(iconframe->isVisible())
		    iconframe->hide();
		if(minbtn->isVisible())
		    minbtn->hide();
		if(maxbtn->isVisible())
		    maxbtn->hide();
		if(helpbtn->isVisible())
		    helpbtn->hide();
		if(rightframe->isVisible())
		    rightframe->hide();

	    }else if(s.width()<(2*defaults::iconsize)
		     + (2*defaults::lowerborderheight)){
		if(iconframe->isVisible())
		    iconframe->hide();
		if(minbtn->isVisible())
		    minbtn->hide();
		if(maxbtn->isVisible())
		    maxbtn->hide();
		if(helpbtn->isVisible())
		    helpbtn->hide(); 

	    }else if(s.width()<(4*defaults::iconsize)
		     + (2*defaults::lowerborderheight)){
		if(iconframe->isVisible())
		    iconframe->hide();
		if(minbtn->isVisible())
		    minbtn->hide();
		if(maxbtn->isVisible())
		    maxbtn->hide();

	    }if(s.width() < (5*defaults::iconsize)+ 
	       (2*defaults::lowerborderheight)){
		if(iconframe->isVisible())
		    iconframe->hide();
	    }else{
		if(iconframe->isHidden())
		    iconframe->show();
		if(minbtn->isHidden())
		    minbtn->show();
		if(maxbtn->isHidden())
		    maxbtn->show();
		if(helpbtn->isHidden())
		    helpbtn->show();
		if(rightframe->isHidden())
		    rightframe->show();

	    }
	    minbtn->setFixedWidth(defaults::iconsize+2);
	    maxbtn->setFixedWidth(defaults::iconsize+2);
	    helpbtn->setFixedWidth(defaults::iconsize+2);
	}else if(leftshown){
	    if(s.width()< defaults::iconsize
		     + (2*defaults::lowerborderheight)){
		if(iconframe->isVisible())
		    iconframe->hide();
		if(minbtn->isVisible())
		    minbtn->hide();
		if(maxbtn->isVisible())
		    maxbtn->hide();
		if(rightframe->isVisible())
		    rightframe->hide();

	    }else if(s.width()<(3*defaults::iconsize)
		     + (2*defaults::lowerborderheight)){
		if(iconframe->isVisible())
		    iconframe->hide();
		if(minbtn->isVisible())
		    minbtn->hide();
		if(maxbtn->isVisible())
		    maxbtn->hide();
	    
	    }else if(s.width()<(4*defaults::iconsize)
		    + (2*defaults::lowerborderheight)){
		if(iconframe->isVisible())
		    iconframe->hide();
	    }else{
		if(iconframe->isHidden())
		    iconframe->show();
		if(minbtn->isHidden())
		    minbtn->show();
		if(maxbtn->isHidden())
		    maxbtn->show();
		if(rightframe->isHidden())
		    rightframe->show();
	    }
	    minbtn->setFixedWidth(defaults::iconsize+2);
	    maxbtn->setFixedWidth(defaults::iconsize+2);
	}else if(withhelp){
	    if(s.width()< defaults::iconsize
		     + (2*defaults::lowerborderheight)){
		if(iconframe->isVisible())
		    iconframe->hide();
		if(helpbtn->isVisible())
		    helpbtn->hide();
		if(rightframe->isVisible())
		    rightframe->hide();

	    }else if(s.width()<(2*defaults::iconsize)
		     + (2*defaults::lowerborderheight)){
		if(iconframe->isVisible())
		    iconframe->hide();
		if(helpbtn->isVisible())
		    helpbtn->hide(); 

	    }else if(s.width()<(3*defaults::iconsize)
		     +(2*defaults::lowerborderheight)){
		if(iconframe->isVisible())
		    iconframe->hide();
	    }else{
		if(iconframe->isHidden())
		    iconframe->show();
		if(helpbtn->isHidden())
		    helpbtn->show();
		if(rightframe->isHidden())
		    rightframe->show();
	    }
	    helpbtn->setFixedWidth(defaults::iconsize+2);
	}else{

	}

    }else{

    }

    iconframe->setFixedWidth(defaults::windowbuttonsize-6);
    rightframe->setFixedWidth(defaults::iconsize+2);

}
void uborder::resizeEvent(QResizeEvent *e){

    checkForSize(e->size());
	//    leftshown = showleft;
	//    withhelp  = hashelp;

    //defaults::windowbuttonsize
    //defaults::windowsBtns
    repaint();
    drawbg();
    midframe->repaint();
}

uborder::uborder(bool showleft, QWidget *parent, const char *name, 
		 bool hashelp) : QWidget(parent, name)
{

    leftshown = showleft;
    withhelp  = hashelp;

    small = false;
    setFixedHeight(defaults::windowbuttonsize);

    /*
    //if (defaults::windowsBtns)
    if ((defaults::styleName == "win98Style") || 
	(defaults::styleName == "win95Style"))
    */
    if ( defaults::titleStyle == "win")
	setPalette(QApplication::palette().active().background());


    layout1 = new QVBoxLayout(this, 0, -1, "layout1");
    Q_CHECK_PTR(layout1);

    uframe = new wframe(this, "uframe");
    Q_CHECK_PTR(uframe);
    uframe->frameName = "uframe";
    uframe->setFixedHeight(defaults::lowerborderheight);
    uframe->setCursor(QCursor(Qt::sizeVerCursor));

    layout1->addWidget(uframe);


    layout = new QHBoxLayout(0, 0, -1,"layout");
    Q_CHECK_PTR(layout);
    //layout->setSpacing(0);
    luframe = new wframe(this, "luframe");
    Q_CHECK_PTR(luframe);
    luframe->frameName = "luframe";
    luframe->setFixedWidth(defaults::lowerborderheight);
    if(QApplication::reverseLayout())
	luframe->setCursor(QCursor(Qt::sizeBDiagCursor));
    else
	luframe->setCursor(QCursor(Qt::sizeFDiagCursor));

    layout->addWidget(luframe);

    layout->addSpacing(2);

    iconframe = new wframe(this, "uiconframe");
    Q_CHECK_PTR(iconframe);
    iconframe->frameName = "uiconframe";
    iconframe->setFixedSize(defaults::windowbuttonsize
			      -defaults::lowerborderheight, 
			      defaults::windowbuttonsize
			      -defaults::lowerborderheight);
    layout->addWidget(iconframe, 0, Qt::AlignTop);
    iconframe->setPixmap(*qapp::defaultpix);


    midframe = new wframe(this, "umidframe");
    Q_CHECK_PTR(midframe);
    midframe->setFixedHeight(defaults::windowbuttonsize
			     -defaults::lowerborderheight);
    midframe->frameName = "umidframe";

    layout->addWidget(midframe, 0, Qt::AlignTop);

    layout2 = new QHBoxLayout(layout, -1, "layout2");

    /*
	if ((defaults::styleName == "win98Style") || 
	    (defaults::styleName == "win95Style"))
    */
    if ( defaults::titleStyle == "win")
	    //    if (defaults::windowsBtns)
	layout2->setMargin(2);

    if(hashelp){
	helpbtn = new wbutton(this, "uhelpbtn");
	Q_CHECK_PTR(helpbtn);
	//helpbtn->setTipText(tr("Press to get context help"));

	/*
	//if (defaults::windowsBtns)
	if ((defaults::styleName == "win98Style") || 
	    (defaults::styleName == "win95Style"))
	*/
     if ( defaults::titleStyle == "win")
	    helpbtn->setMaximumSize(defaults::iconsize+2,
				  defaults::iconsize);
	else
	    helpbtn->setMaximumSize(defaults::iconsize+2,
				  defaults::iconsize);
	
	helpbtn->setPixmap(*qapp::helpbtnpix);
	if(defaults::flaticons){
	    helpbtn->setFlat(true);
	    helpbtn->setBackgroundMode(Qt::X11ParentRelative); 
	    if ( qapp::helpbtnpix->mask() )
		helpbtn->setMask( *qapp::helpbtnpix->mask() );
	}

	layout2->addWidget(helpbtn, 0, Qt::AlignTop);
    }

    if(showleft){
	
	if(defaults::windowsBtns){
	    minbtn = new wbutton(this, "uminbtn");
	    Q_CHECK_PTR(minbtn);
	    //minbtn->setTipText(tr("Press to maximmize"));

	    minbtn->setMinimumWidth(1);
	    /*
	    //	    if (defaults::windowsBtns)
	    if ((defaults::styleName == "win98Style") || 
		(defaults::styleName == "win95Style"))
	    */
	    if ( defaults::titleStyle == "win")
		minbtn->setMaximumSize(defaults::iconsize+2,
					 defaults::iconsize);
	    else
		
		minbtn->setMaximumSize(defaults::iconsize+2,
				     defaults::iconsize+2);
	    
	    minbtn->setPixmap(*qapp::minbtnpix);

	    if(defaults::flaticons){
		minbtn->setFlat(true);
		minbtn->setBackgroundMode(Qt::X11ParentRelative); 

		if ( qapp::minbtnpix->mask() )
		    minbtn->setMask( *qapp::minbtnpix->mask() );
	    }

	    layout2->addWidget(minbtn, 0, Qt::AlignTop);

	    maxbtn = new wbutton(this, "umaxbtn");
	    Q_CHECK_PTR(maxbtn);
	    //maxbtn->setTipText(tr("Press to maximmize"));

	    maxbtn->setMinimumWidth(1);
	    /*
	    // if (defaults::windowsBtns)
	    if ((defaults::styleName == "win98Style") || 
		(defaults::styleName == "win95Style"))
	    */
	    if ( defaults::titleStyle == "win")
		maxbtn->setMaximumSize(defaults::iconsize+2,
				       defaults::iconsize);
	    else
		maxbtn->setMaximumSize(defaults::iconsize+2,
				     defaults::iconsize+2);
	    
	    maxbtn->setPixmap(*qapp::maxbtnpix);

	    if(defaults::flaticons){
		maxbtn->setFlat(true);
		maxbtn->setBackgroundMode(Qt::X11ParentRelative); 

		if ( qapp::maxbtnpix->mask() )
		    maxbtn->setMask( *qapp::maxbtnpix->mask() );
	    }

	    layout2->addWidget(maxbtn, 0, Qt::AlignTop);

	}else{
	    leftframe = new wbutton(this, "uleftframe");
	    Q_CHECK_PTR(leftframe);


	    leftframe->setMinimumWidth(1);
	    if (defaults::windowsBtns)
		leftframe->setMaximumSize(defaults::iconsize+2, 
					  defaults::iconsize);
	    else
		leftframe->setMaximumSize(defaults::iconsize+2, 
					  defaults::iconsize+2);
	    //leftframe->setTipText(tr("Press RightButton to maximmize, left button to minimize"));
	    leftframe->setPixmap(*qapp::leftwinpix);

	    if(defaults::flaticons){
		leftframe->setFlat(true);
		leftframe->setBackgroundMode(Qt::X11ParentRelative); 
	    
		if ( qapp::leftwinpix->mask() )
		    leftframe->setMask( *qapp::leftwinpix->mask() );
	    }

	    layout2->addWidget(leftframe, 0, Qt::AlignTop);
	}
    }
	
    rightframe = new wbutton(this, "urightframe");
    //rightframe->setTipText(tr("Press to close"));
    Q_CHECK_PTR(rightframe);
    rightframe->setMinimumWidth(1);

    /*
    //    if (defaults::windowsBtns)
    if ((defaults::styleName == "win98Style") || 
	(defaults::styleName == "win95Style"))
    */
    if ( defaults::titleStyle == "win")
	rightframe->setMaximumSize(defaults::iconsize+2,
			     defaults::iconsize);
    else
	rightframe->setMaximumSize(defaults::iconsize+2,
			     defaults::iconsize+2);
    

    if(defaults::flaticons){
	rightframe->setFlat(true);
	rightframe->setBackgroundMode(Qt::X11ParentRelative); 

        if ( qapp::rightwinpix->mask() )
	    rightframe->setMask( *qapp::rightwinpix->mask() );
    }

    rightframe->setPixmap(*qapp::rightwinpix);

    
    if (defaults::windowsBtns)
	layout2->addSpacing(2);
    layout2->addWidget(rightframe, 0, Qt::AlignTop);


    ruframe = new wframe(this, "ruframe");
    Q_CHECK_PTR(ruframe);
    ruframe->frameName = "ruframe";
    ruframe->setFixedWidth(defaults::lowerborderheight);
    if(QApplication::reverseLayout())
	ruframe->setCursor(QCursor(Qt::sizeFDiagCursor));
    else
	ruframe->setCursor(QCursor(Qt::sizeBDiagCursor));

    layout->add(ruframe);

    layout1->addLayout(layout);
    //    checkForSize(size());
}
