/*
*  File     : toolbar.cpp
*  Writen by: haydar@haydar.net, alinden@gmx.de
*  Copyright: GPL
*
*  Draw the toolbar and place items on it
*/

#include "defs.h"
#include "qapp.h"
#include "toolbar.h"
#include <qstyle.h>

pager   *tb_pg;     // pager
winlist *tb_wl;     // winlist
menu    *tb_mn;     // menu
procbar *tb_pb;     // procbar
apbar   *tb_ap;     // apbox

Toolbar::Toolbar(QWidget *parent, const char *name) : QFrame(parent, name)
{
	layout = new QHBoxLayout(this);
	layout->setSpacing(1);
	
	setFrameStyle(QFrame::WinPanel|QFrame::Raised);
	setLineWidth(1);

	setGeometry(0, (defaults::toolbar_top)?(0):(QApplication::desktop()->height()-defaults::tb_height),
	QApplication::desktop()->width(), defaults::tb_height);

	layout->addSpacing(1);

	// menu
	if(defaults::show_menu)
	{
		tb_mn = new menu(this, "menu");
		layout->add(tb_mn);
	}	
	else
		tb_mn = new menu(0, "menu");
		
	tb_mn->readmenu();

	layout->addSpacing(4);

	// pager
	tb_pg = new pager(this, "pager");
	layout->add(tb_pg);
	layout->addSpacing(3);

	// winlist
	if(defaults::show_winlist)
	{
		tb_wl = new winlist(this, "winlist");
		layout->add(tb_wl);
	}
	else
		tb_wl = new winlist(0, "winlist");


	if(defaults::show_menu || defaults::show_winlist)
		addsep();

	// procbar
	tb_pb = new procbar(this, "procbar");
	tb_pb->setFixedHeight(defaults::tb_height);
	layout->add(tb_pb);
	layout->setStretchFactor(tb_pb, 100 );

	addsep();

	tb_ap = new apbar(this, "apbar");
	layout->add(tb_ap);
	//	layout->addSpacing(5);

	show();
}

void Toolbar::paintEvent( QPaintEvent * ){
    
    if(QFileInfo(qapp::get_cfile(defaults::toolbar_bg)).isFile()){
	//QPixmap qp(width(), height());
	//QPainter painter(&qp);

	QImage img(qapp::get_cfile(defaults::toolbar_bg));
	if(! img.isNull())  // make scaled pixmap
	{
	    QPainter *p = new QPainter(this);
	    QPixmap pix;
	    pix.convertFromImage(img.smoothScale(width(), height() ));
	    //	    painter.drawPixmap(0,0, pix);
	    p->drawPixmap(0,0, pix);

	    p->end();
	    //	    setBackgroundPixmap(qp);
	}
	//	painter.end();
	
    }else if ( defaults::taskStyle == "hwm" ){
	//	QPixmap qp(width(), height());

	int h = (height() - 2)/2; // (height() - 4)/2;

	QColor firstColor(palette().active().background());
	//	qp.fill(firstColor);
	//	QPainter painter(&qp);
	QPainter painter(this);

	//qp.fill(firstColor);
	painter.setPen(firstColor.light(100 + (h*2)));

	//
	//
	//

	int i = 0;

	//    int h = (height() - 4) / 2;

	painter.setPen(firstColor.dark(100+h*2));
	painter.drawLine(0,0 , width(), 0); 

	painter.setPen(firstColor.light(100+h*2));
 
	for ( i = 1; i < 3; ++i ) {
	    painter.setPen(firstColor.light(100+(h*2)-i));
	    painter.drawLine(0,i , width(), i); 
	}

	for ( i = 0; i < h-1; ++i ) {
	    painter.setPen(firstColor.dark(100 + ((h*2)-3)-(i*2)));
	    painter.drawLine(0,i+4 , width(), i+4); 
	}


	int j = 0;

	for ( i = height()-h; i < height()-1 ; ++i ) {
	    j++;
	    painter.setPen(firstColor.light(100 +(j*2)));
	    painter.drawLine(0, i , width(), i); 
	}


	painter.setPen(firstColor.dark(100+(h*2)));
 
	for ( i = height()-2; i < height(); ++i ) {
	    painter.drawLine(0,i , width(), i); 
	}


	painter.setPen(firstColor.dark(130));
	painter.drawLine(0,0 , 0, height()); 

	painter.setPen(firstColor.light(130));
	painter.drawLine(1,1 , 1, height()); 

	painter.setPen(firstColor.light(120));
	painter.drawLine(2,2 , 2, height()); 



	painter.setPen(firstColor.dark(120));
	painter.drawLine(width()-3,2 , width()-3, height()); 

	painter.setPen(firstColor.dark(130));
	painter.drawLine(width()-2,1 , width()-2, height()); 

	painter.setPen(firstColor.dark(150).dark(120));
	painter.drawLine(width()-1,0 , width()-1, height()); 


	
	if(QApplication::reverseLayout()){
	    for ( i = 0; i < h; ++i ) {
		painter.setPen(firstColor.light(100 + (h*3) -(i*3)));
		painter.drawLine(0 ,i , 6, i); 
	    }

	    j = 0;
	    for ( i = height()-h-1; i < height() ; ++i ) {
		j++;
		painter.setPen(firstColor.dark(100 +(j*3)));
		painter.drawLine(0, i , 6, i); 
	    }
	


	    //painter.setPen(firstColor.dark(100 + (h*3)));
	    //painter.drawLine(0,1 , width(), 1); 
    
	    //	    painter.setPen(firstColor.light(100+(h*3)));
	    //painter.drawLine(0,height()-2 , width(), height()-2); 

	}else{

	    for ( i = 0; i < h; ++i ) {
		painter.setPen(firstColor.light(100 + (h*3) -(i*3)));
		painter.drawLine(width() - 6 ,i , width(), i); 
	    }

	    j = 0;
	    for ( i = height()-h-1; i < height() ; ++i ) {
		j++;
		painter.setPen(firstColor.dark(100 +(j*3)));
		painter.drawLine(width()-6, i , width(), i); 
	    }
	


	    //	    painter.setPen(firstColor.dark(100 + (h*3)));
	    //painter.drawLine(0,1 , width(), 1); 
    
	    //	    painter.setPen(firstColor.light(100+(h*3)));
	    // painter.drawLine(0,height()-2 , width(), height()-2); 

	}

	painter.end();
	//	setBackgroundPixmap(qp);
    }else{
    
	QPainter paint( this );
        //drawFrame( &paint );
	QColor firstColor(palette().active().background());
	paint.setPen(firstColor.light(130));
	paint.drawLine(0 ,0 , width(), 0); 
	paint.drawLine(0 ,1 , width(), 1); 
	paint.drawLine(0 ,0 , 0, height()); 
	paint.drawLine(1 ,0 , 1, height()); 

	paint.end();	
	}

}

void Toolbar::paletteChange(const QPalette &)
{
	xwindow *client;

	for(client=clients.first(); client != NULL; client = clients.next())
		client->repaint();
}

void Toolbar::addsep(void)
{
    if (defaults::taskStyle != "hwm"){
	layout->addSpacing(2);
	QFrame *frame = new QFrame(this);
	frame->setLineWidth(1);
	frame->setMidLineWidth(0);
	//	frame->setFrameStyle(QFrame::VLine|QFrame::Sunken);
	frame->setFrameStyle(QFrame::Panel|QFrame::Raised);
	frame->setFixedHeight(height()-8);
	frame->setFixedWidth(3);
	layout->addWidget(frame);
	layout->addSpacing(2);
    }
}
