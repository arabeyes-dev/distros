/****************************************************************************
**
**
** hnotepad.cpp
** Created by Haydar Alkaduhimi <haydar@haydar.net>
** Copyright (C) 2002 Haydar Net.  All rights reserved.
**
**
*****************************************************************************/

#include "hnotepad.h"
#include <qapplication.h>
#include <qfile.h>
#include <qfiledialog.h>
#include <qpopupmenu.h>
#include <qtextstream.h>
#include <qpainter.h>
#include <qmessagebox.h>
#include <qpaintdevicemetrics.h>
#include <qptrlist.h>
#include <qfontdialog.h>

#include <qtextcodec.h>
#include <qimage.h>
#include <qpixmap.h>

static const char* const image0_data[] = { 
"40 40 14 1",
". c None",
"g c #000000",
"a c #008080",
"k c #208080",
"l c #20c0ff",
"i c #60c0ff",
"c c #60ffff",
"# c #808080",
"d c #80c0ff",
"e c #80ffff",
"j c #a0c0ff",
"f c #c0c0ff",
"h c #c0ffff",
"b c #ffffff",
"........................................",
"........................................",
"........................................",
"..............##.##.##.##.##.##.........",
".............abaabaabaabaabaaba##.......",
"............acdeedeedeedeedeeafagg......",
"............abbbbbbbhbhbhhbhhbigf#g.....",
"...........aebbbbbhbbhbhbhhhhjagf#g.....",
"...........aebbbhbbhhbhbhhhhhigff#g.....",
"..........aebbbhbhhbhhhhhhhhdagff#g.....",
"..........aehbggggggggghhjejigffb#g.....",
".........aebhhbhhhhhhhhejhhdagffb#g.....",
".........aehbgggggggggjhedeigkkii#g.....",
"........aehhhhhhhhhejbejejjagffbb#g.....",
"........aehhhhhejejhejdjedigfffbb#g.....",
".......aehhjhejhhjejjeedjeagkkiii#g.....",
".......aehhhjhejdejdeddedigfffbbb#g.....",
"......aejejejejeejdejdijdagfffbbb#g.....",
"......aehjejjedjdeijicdcigkkkiiii#g.....",
".....aedejejejedidcijiiiagfffbbbb#g.....",
".....aeejejjjidcdidiiiiigffffbbbb#g.....",
"....aejedjjicdidiiiiiiiagkkkiiiii#g.....",
"....aejjiciediiiiiiiiiigffffbbbbb#g.....",
"...aedcddjiiiiiiiiiiiiagffffbbbbb#g.....",
"...aeidiciiiiiiiiililigkkkkiiiiii#g.....",
"...addiiiiiiiiiiililiagffffbbbbbb#g.....",
"....aaaaaaaaaaaaaaaaagfffffbbbbbb#g.....",
".....ggggggggggggggggkkkkkiiiiiii#g.....",
".............#bfffffffffffbbbbbbb#g.....",
".............#bfbffffffffbbbbbbbb#g.....",
"..............#bbbbbbbbbbbbbbbbbf#g.....",
"...............##################g......",
"................ggggggggggggggggg.......",
"........................................",
"........................................",
"........................................",
"........................................",
"........................................",
"........................................",
"........................................"};


const bool no_writing = FALSE;
int undoID, redoID;
QPopupMenu *edit;
//QMenuData *editRedo;

static QPtrList<QTextCodec> *codecList = 0;

enum { Uni = 0, MBug = 1, Lat1 = 2, Local = 3, Guess = 4, Codec = 5 };


Editor::Editor( QWidget * parent , const char * name )
    : QMainWindow( parent, name, WDestructiveClose )
{
    //    m = new QMenuBar( this, "menu" );
    findString = "";

    QPixmap image0( ( const char** ) image0_data );
    setIcon( image0 );
    filename = QString::null;
    QPopupMenu * file = new QPopupMenu();
    Q_CHECK_PTR( file );
    menuBar()->insertItem( tr("&File"), file );

    file->insertItem( tr("&New"),   this, SLOT(newDoc()),   CTRL+Key_N );
    file->insertItem( tr("&Open"),  this, SLOT(load()),     CTRL+Key_O );
    file->insertItem( tr("&Save"),  this, SLOT(save()),     CTRL+Key_S );
    file->insertSeparator();
    open_as = new QPopupMenu();
    file->insertItem( tr("Open &as"),  open_as );
    save_as = new QPopupMenu();
    file->insertItem( tr("Save &as"),  save_as );
    file->insertItem( tr("Add &encoding"), this, SLOT(addEncoding()) );
#ifndef QT_NO_PRINTER
    file->insertSeparator();
    file->insertItem( tr("&Print"), this, SLOT(print()),    CTRL+Key_P );
#endif
    file->insertSeparator();
    file->insertItem( tr("&Close"), this, SLOT(close()),CTRL+Key_W );
    file->insertItem( tr("&Quit"),  qApp, SLOT(closeAllWindows()),CTRL+Key_Q);

    connect( save_as, SIGNAL(activated(int)), this,SLOT(saveAsEncoding(int)));
    connect( open_as, SIGNAL(activated(int)), this,SLOT(openAsEncoding(int)));
    rebuildCodecList();

    e = new QMultiLineEdit( this, "editor" );
    setCentralWidget(e);

    edit = new QPopupMenu();
    Q_CHECK_PTR( edit );
    menuBar()->insertItem( tr("&Edit"), edit );

    undoID = edit->insertItem(tr("&Undo"), e, SLOT( undo() ),CTRL+Key_Z );
    redoID = edit->insertItem( tr("&Redo"), e, SLOT( redo() ),CTRL+Key_Y);
    edit->setItemEnabled( undoID, FALSE );
    edit->setItemEnabled( redoID, FALSE );
    edit->insertSeparator();




    edit->insertItem( tr("&Copy"), e, SLOT(copy()),CTRL+Key_C );
    edit->insertItem( tr("&Paste"), e, SLOT(paste()),CTRL+Key_V);
    edit->insertItem( tr("Cu&t"), e, SLOT(cut()),CTRL+Key_X );
    edit->insertItem( tr("Clear"), e, SLOT(clear()),Key_Delete);
    edit->insertSeparator();


    edit->insertItem( tr("To &uppercase"), this, SLOT(toUpper()),CTRL+Key_U );
    edit->insertItem( tr("To &lowercase"), this, SLOT(toLower()),CTRL+Key_L);
#ifndef QT_NO_FONTDIALOG
    edit->insertSeparator();
    edit->insertItem( tr("&Select Font") ,this, SLOT(font()), CTRL+Key_F );
#endif

    edit->insertSeparator();
    edit->insertItem( tr("&Find"), this, SLOT(find()),CTRL+Key_F );
    edit->insertItem( tr("Find &Next"), this, SLOT(findNext()),Key_F3);


    QPopupMenu * help = new QPopupMenu( this );
    menuBar()->insertItem( tr("&Help"), help );

    help->insertItem( tr("&About"), this, SLOT(about()), Key_F1 );


    changed = FALSE;




    connect( e, SIGNAL( textChanged() ), this, SLOT( textChanged() ) );

    QFont unifont("arial"); //unifont.setPixelSize(16);
    e->setFont( unifont );

    e->setFocus();
}

Editor::~Editor()
{
}

int Editor::testCode(QString code){
    int indx = codecList->find( QTextCodec::codecForName(code ));
    if (indx > -1){
	return indx;
    }
    return 3;
}


void Editor::font()
{
#ifndef QT_NO_FONTDIALOG
    bool ok;
    QFont f = QFontDialog::getFont( &ok, e->font() );
    if ( ok ) {
        e->setFont( f );
    }
#endif
}

void Editor::rebuildCodecList()
{
    delete codecList;
    codecList = new QPtrList<QTextCodec>;
    QTextCodec *codec;
    int i;
    for (i = 0; (codec = QTextCodec::codecForIndex(i)); i++)
	codecList->append( codec );
    int n = codecList->count();
    for (int pm=0; pm<2; pm++) {
	QPopupMenu* menu = pm ? open_as : save_as;
	menu->clear();
	QString local = "Local (";
	local += QTextCodec::codecForLocale()->name();
	local += ")";
	menu->insertItem( local, Local );
	menu->insertItem( "Unicode", Uni );
	menu->insertItem( "Latin1", Lat1 );
	menu->insertItem( "Microsoft Unicode", MBug );
	if ( pm )
	    menu->insertItem( "[guess]", Guess );
	for ( i = 0; i < n; i++ )
	    menu->insertItem( codecList->at(i)->name(), Codec + i );
    }
}

void Editor::newDoc()
{
    Editor *ed = new Editor;
    if ( qApp->desktop()->size().width() < 450
	 || qApp->desktop()->size().height() < 450 ) {
	ed->showMaximized();
    } else {
	ed->resize( 400, 400 );
	ed->show();
    }
}


void Editor::load()
{
#ifndef QT_NO_FILEDIALOG
    filename = QFileDialog::getOpenFileName( QString::null, QString::null, this );
    if ( !filename.isEmpty() )
	load( filename, -1 );
#endif
}

void Editor::load( const QString& fileName, int code )
{
    QFile f( fileName );
    if ( !f.open( IO_ReadOnly ) )
	return;

    filename = fileName;
    e->setAutoUpdate( FALSE );

    QTextStream t(&f);
    if ( code >= Codec ){
	t.setCodec( codecList->at(code-Codec) );
	enc = codecList->at(code-Codec)->name();
    }else if ( code == Uni )
	t.setEncoding( QTextStream::Unicode );
    else if ( code == MBug )
	t.setEncoding( QTextStream::UnicodeReverse );
    else if ( code == Lat1 )
	t.setEncoding( QTextStream::Latin1 );
    else if ( code == Guess ) {
	QFile f(fileName);
	f.open(IO_ReadOnly);
	char buffer[256];
	int l = 256;
	l=f.readBlock(buffer,l);
	QTextCodec* codec = QTextCodec::codecForContent(buffer, l);
	if ( codec ) {
	    QMessageBox::information(this,"Encoding",QString("Codec: ")+codec->name());
	    t.setCodec( codec );
	}
    }
    e->setText( t.read() );
    f.close();

    e->setAutoUpdate( TRUE );
    e->repaint();
    setCaption( fileName );

    changed = FALSE;
}

void Editor::openAsEncoding( int code )
{
#ifndef QT_NO_FILEDIALOG
    //storing filename (proper save) is left as an exercise...
    filename = QFileDialog::getOpenFileName( QString::null, QString::null, 
					     this );
    if ( !filename.isEmpty() )
	(void) load( filename, code );
#endif
}

bool Editor::save()
{
#ifndef QT_NO_FILEDIALOG
    //storing filename (proper save) is left as an exercise...
    if ( filename.isEmpty() )
	filename = QFileDialog::getSaveFileName( QString::null, 
						   QString::null, this );
    if(!enc.isEmpty()){
	if ( !filename.isEmpty() )
	    return saveAs( filename, testCode(enc)+5 );

    }else{
	if ( !filename.isEmpty() )
	    return saveAs( filename );
    }
    return FALSE;
#endif
}

void Editor::saveAsEncoding( int code )
{
#ifndef QT_NO_FILEDIALOG
    //storing filename (proper save) is left as an exercise...
    filename = QFileDialog::getSaveFileName( QString::null, QString::null, 
					     this );
    if ( !filename.isEmpty() )
	(void) saveAs( filename, code );
#endif
}

void Editor::addEncoding()
{
#ifndef QT_NO_FILEDIALOG
    QString fn = QFileDialog::getOpenFileName( QString::null, "*.map", this );
    if ( !fn.isEmpty() ) {
	QFile f(fn);
	if (f.open(IO_ReadOnly)) {
	    if (QTextCodec::loadCharmap(&f)) {
		rebuildCodecList();
	    } else {
		QMessageBox::warning(0,"Charmap error",
		    "The file did not contain a valid charmap.\n\n"
		    "A charmap file should look like this:\n"
		       "  <code_set_name> thename\n"
		       "  <escape_char> /\n"
		       "  % alias thealias\n"
		       "  CHARMAP\n"
		       "  <tokenname> /x12 <U3456>\n"
		       "  <tokenname> /xAB/x12 <U0023>\n"
		       "  ...\n"
		       "  END CHARMAP\n"
		);
	    }
	}
    }
#endif
}


bool Editor::saveAs( const QString& fileName, int code )
{
    QFile f( fileName );
    if ( no_writing || !f.open( IO_WriteOnly ) ) {
	QMessageBox::warning(this,"I/O Error",
		    QString("The file could not be opened.\n\n")
			+fileName);
	return FALSE;
    }
    QTextStream t(&f);
    if ( code >= Codec )
	t.setCodec( codecList->at(code-Codec) );
    else if ( code == Uni )
	t.setEncoding( QTextStream::Unicode );
    else if ( code == MBug )
	t.setEncoding( QTextStream::UnicodeReverse );
    else if ( code == Lat1 )
	t.setEncoding( QTextStream::Latin1 );
    t << e->text();
    f.close();
    setCaption( fileName );
    changed = FALSE;
    return TRUE;
}

void Editor::print()
{
#ifndef QT_NO_PRINTER
    if ( printer.setup(this) ) {		// opens printer dialog
	printer.setFullPage(TRUE);		// we'll set our own margins
	QPainter p;
	p.begin( &printer );			// paint on printer
	p.setFont( e->font() );
	QFontMetrics fm = p.fontMetrics();
	QPaintDeviceMetrics metrics( &printer ); // need width/height
						 // of printer surface
	const int MARGIN = metrics.logicalDpiX() / 2; // half-inch margin
	int yPos        = MARGIN;		// y position for each line

	for( int i = 0 ; i < e->numLines() ; i++ ) {
	    if ( printer.aborted() )
		break;
	    if ( yPos + fm.lineSpacing() > metrics.height() - MARGIN ) {
		// no more room on this page
		if ( !printer.newPage() )          // start new page
		    break;                           // some error
		yPos = MARGIN;			 // back to top of page
	    }
	    p.drawText( MARGIN, yPos, metrics.width() - 2*MARGIN,
			fm.lineSpacing(), ExpandTabs, e->textLine( i ) );
	    yPos += fm.lineSpacing();
	}
	p.end();				// send job to printer
    }
#endif
}


void Editor::closeEvent( QCloseEvent *event )
{
    event->accept();

    if ( changed ) { // the text has been changed
	switch ( QMessageBox::warning( this, tr("HDE Notepad"),
					tr("Save changes to Document?"),
					tr("&Yes"),
					tr("&No"),
					tr("Cancel"),
					0, 2) ) {
	case 0: // yes
	    if ( save() )
		event->accept();
	    else
		event->ignore();
	    break;
	case 1: // no
	    event->accept();
	    break;
	default: // cancel
	    event->ignore();
	    break;
	}
    }
    filename = QString::null;
}

void Editor::toUpper()
{
    e->setText(e->text().upper());
}

void Editor::toLower()
{
    e->setText(e->text().lower());
}

void Editor::textChanged()
{
    changed = TRUE;
    edit->setItemEnabled( redoID, false );

    if( e->isUndoAvailable() )
	edit->setItemEnabled( undoID, true );
    else
	edit->setItemEnabled( undoID, false );

    if( e->isRedoAvailable() )
	edit->setItemEnabled( redoID, true );
    else
	edit->setItemEnabled( redoID, false );
}


void Editor::about()
{
    QMessageBox::about( this, "HDE Notepad",
			"This is a small editor program to be used\n"
			"with hde or Haydar Linux, it also supports\n"
			"almost all Languages and encodings.\n\n"
			"you can also use it with \"--font fontname\"\n"
			"or \"--enc encoding\" to load a document with\n"
			"a special font or encoding");
}


void Editor::find()
{

}

void Editor::findNext()
{

}
