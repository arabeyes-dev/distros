<!DOCTYPE TS><TS>
<context>
    <name>desktp</name>
    <message>
        <source>New</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Arrange icons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Rename</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Properties</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>menu</name>
    <message>
        <source>Kill window</source>
        <translation>Venster Beeindigen</translation>
    </message>
    <message>
        <source>End Session</source>
        <translation>Sessie Beeindigen</translation>
    </message>
</context>
<context>
    <name>winfo</name>
    <message>
        <source>Sticky</source>
        <translation></translation>
    </message>
    <message>
        <source>Skip windowlist</source>
        <translation>Venster lijst annuleren</translation>
    </message>
    <message>
        <source>Small frame</source>
        <translation>Kleine frame</translation>
    </message>
    <message>
        <source>No resize</source>
        <translation>Vaste Grote</translation>
    </message>
    <message>
        <source>No tiling</source>
        <translation>Geen Tilling</translation>
    </message>
    <message>
        <source>Skip keyboard access</source>
        <translation>Toetsenboard toegang annuleren</translation>
    </message>
    <message>
        <source>Skip in screenmode</source>
        <translation>Screenmode annuleren</translation>
    </message>
    <message>
        <source>Apply to all in class</source>
        <translation>Toepassen in alle classen</translation>
    </message>
    <message>
        <source> Title</source>
        <translation></translation>
    </message>
    <message>
        <source> Name</source>
        <translation>Naam</translation>
    </message>
    <message>
        <source> Class</source>
        <translation>Class</translation>
    </message>
    <message>
        <source> Location</source>
        <translation>Locatie</translation>
    </message>
    <message>
        <source> Invocation</source>
        <translation>Innovatie</translation>
    </message>
    <message>
        <source> Geometry</source>
        <translation></translation>
    </message>
    <message>
        <source> Permanent </source>
        <translation></translation>
    </message>
    <message>
        <source> Temporary </source>
        <translation>Tijdelijk</translation>
    </message>
    <message>
        <source>  Cancel   </source>
        <translation>Annuleren</translation>
    </message>
</context>
<context>
    <name>xwindow</name>
    <message>
        <source>&amp;Restore</source>
        <translation>&amp;Vorig formaat</translation>
    </message>
    <message>
        <source>&amp;Iconify</source>
        <translation>&amp;Minimaliseren</translation>
    </message>
    <message>
        <source>Ma&amp;ximize</source>
        <translation>Ma&amp;ximaliseren</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>Sluiten</translation>
    </message>
    <message>
        <source>Press to iconify</source>
        <translation>Minimaliseren</translation>
    </message>
    <message>
        <source>Press to maximize</source>
        <translation>Maximaliseren</translation>
    </message>
    <message>
        <source>Press RightButton to maximmize, left button to minimize</source>
        <translation>Richt knop om te maximaliseren, links om te minimaliseren</translation>
    </message>
    <message>
        <source>Press to get context help</source>
        <translation>Help</translation>
    </message>
    <message>
        <source>Press to close</source>
        <translation>Sluiten</translation>
    </message>
</context>
</TS>
