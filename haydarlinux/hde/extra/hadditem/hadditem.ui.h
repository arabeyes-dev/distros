/****************************************************************************
** ui.h extension file, included from the uic-generated form implementation.
**
** If you wish to add, delete or rename functions or slots use
** Qt Designer which will update this file, preserving your code. Create an
** init() function in place of a constructor, and a destroy() function in
** place of a destructor.
*****************************************************************************/
#include <qfiledialog.h>
#include <qmessagebox.h>

#include <defaults.h>
#include "iconform.h"

void hadditem::init()
{
    iconPix->setPixmap(QPixmap(defaults::get_cfile("images/default.xpm")));
    helpButton()->hide();
    connect(nextButton(), SIGNAL(clicked()), this, SLOT(nextClicked()));
}



void hadditem::nextClicked()
{
    if(currentPage() == page ( 1 ) ){
	if(excEdit->text() == ""){
	    QMessageBox::information( this, trUtf8("Add Item"), trUtf8("Executable field is empty\nPlease put the excutable in this field.") );
	    back();
	}
    }else if(currentPage() == page ( 2 ) ){
	if( (!desktopCheck->isChecked()) && (!menuCheck->isChecked()) ){
	    QMessageBox::information( this, trUtf8("Add Item"), trUtf8("Please select the item type \(Desktop or Menu Item).") );
	    back();
	}
    }
}




void hadditem::browseBtn_clicked()
{
    QString s = QFileDialog::getOpenFileName(getenv("HOME"), QString::null, this, 
					     trUtf8("Chose Executable"), 
					     trUtf8("Chose Executable")); 
    if(!s.isNull())
	excEdit->setText(s);
}


void hadditem::iconBtn_clicked()
{
    iconForm *iform = new iconForm(this);
    if(iform->exec()){
	
    }
}
