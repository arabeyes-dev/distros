/*  
*   File      : hsynedit.h
*   Written by: haydar@haydar.net
*   Copyright : GPL
*
*   Shows a label with current time.
*/

#ifndef HSYNEDIT_H
#define HSYNEDIT_H

//#include <editor.h>
#include <qtextedit.h> 
#include <qstylesheet.h> 



//class HSynEdit : public Editor
class HSynEdit : public QTextEdit
{
    Q_OBJECT

public:
    HSynEdit( QWidget *parent, const char *name );
    ~HSynEdit();

    QTextDocument *document() const { return QTextEdit::document(); }
    //int numLines() const{ return document()->lastParag()->paragId() + 1;};
    QString textLine( int line ) const;

};



#endif
