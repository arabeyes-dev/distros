#include <qapplication.h>
#include "hrun.h"

int main(int argc, char **argv)
{
    QApplication a(argc, argv);

    defaults::read_config();

    QTranslator translator( 0 );
    translator.load( "hrun_" + defaults::lng ,defaults::get_cfile("lng"));
    a.installTranslator( &translator );
    if (defaults::lng == "ar")
	a.setReverseLayout ( true );


    hrun *rn = new hrun(0, "Run");
    a.setMainWidget(rn);
    if(argc > 1){
	//	cout << argv[1] << endl;
	rn->OpenCombo->setCurrentText(argv[1]);
	rn->OKpressed();
    }else
	rn->show();
    a.exec();

    delete rn;
    return (0);
}
