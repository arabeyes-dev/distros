/*  
*   File      : dclock.cpp
*   Written by: haydar@haydar.net
*   Copyright : GPL
*
*   Shows a label with current time.
*/

#include <hde_tbab.h>
#include <defaults.h>



hde_tbab::hde_tbab(QWidget *parent, const char *name) : QLabel(parent, name)
{
    /*
#ifdef Q_WS_X11 
    XClassHint ch;
    ch.res_name = "hde_tbab";
    ch.res_class = "hde_tbab";

    XSetClassHint(qt_xdisplay(), winId(), &ch);
#endif
    */
#ifdef Q_WS_X11 
	Display *dsp = x11Display(); // get the display
	WId win = winId();           // get the window
	int r;
	int data = 1;
	r = XInternAtom(dsp, "KWM_DOCKWINDOW", false);
	XChangeProperty(dsp, win, r, r, 32, 0, (uchar *)&data, 1);
	r = XInternAtom(dsp, "_KDE_NET_WM_SYSTEM_TRAY_WINDOW_FOR", false);
	XChangeProperty(dsp, win, r, XA_WINDOW, 32, 0, (uchar *)&data, 1);

#endif
}

void hde_tbab::drawFrame ( QPainter * p ){
    if (defaults::styleName == "hdeStyle"){
	QColor firstColor(palette().active().background());
	
	int h = (height()-4) /2;
	int i = 0;

	for ( i = 0; i < h-1; ++i ) {
	    p->setPen(firstColor.light(100 + (h*3)-(i*3)));
	    p->drawLine(0,i , width(), i); 
	}

	int j = 0;
	for ( i = height()-h-1; i < height() ; ++i ) {
	    j++;
	    p->setPen(firstColor.dark(100 +(j*3)));
	    p->drawLine(0, i , width(), i); 
	}

	p->end();   
    }else{

    }
}
