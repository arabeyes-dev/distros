/* defaults.h */

#ifndef DEFAULTS_H
#define DEFAULTS_H

//#include "xwindow.h"
#include <startmenu.h>
#include "defs.h"

struct hwmItem {
    QString name;
    QString icon;
    QString exec;
    QString path;
    QString error;
};

class defaults
{

public:
	static int tb_height;
	static int tc_height;
	static int vdesks;
	static bool toolbar_top;
	static bool sideBorders;
	static bool topBorder;
	static bool windowsBtns;
	static bool clickToFocus;
	static int lowerborderwidth;
	static int lowerborderheight;
	static int windowbuttonsize;
	static QFont borderfont;
	static QFont toolbarfont;
	static QColor inactive_bg;
	static QColor active_bg;
	static QColor inactive_bg2;
	static QColor active_bg2;
	static QColor inactive_fg;
	static QColor active_fg;
	static QColor urgent_bg;
	static QColor root_bg;
	static QString root_pix;
	static bool starturgent;
	static QStack <QString> initexec;
	static bool show_menu;
	static bool show_winlist;
	static bool showclientmachines;
	static int autofocustime;
	static bool start_restart;
	static char **argv;
	static int argc;
	static float tleftspace;
	static int maxontab;
	static int wminframe;
	static bool sttiled[10];
	static QString cfdir;
	static QString bgdir;
	static int tmx1,tmx2,tmy1,tmy2;
	static int smx1,smx2,smy1,smy2;
	static int pager_height;
	static QColor pager_active;
	static QColor pager_window;
	static QColor pager_visible;
	static int menuwidth;
	//static QString menutxt;
	static QString coding;
	static QString styleName;
	static QString titleStyle;
	static QString taskStyle;
	static QString apbarStyle;
	static QString lng;
	static QString bgDisplay;
	static QString minicon;
	static QString maxicon;
	static QString resticon;
	static QString closeicon;
	static QString helpicon;
	static QString lefticon;
	static QString deficon;
	static QString starticon;
	static QString startdown;
	static QString starticonmo;
	static QString starticonplace;
	static int startheight;
	static int startwidth;

	static QString toolbar_bg;

	static QString tbButton_bg;
	static QString tbButtondown_bg;
	static QString tbButtonmo_bg;
	static int tbButtonHeight;

	static QColor startButton_fg;
	static QColor apbar_fg;
	static QColor tbButton_fg;

	static QString wininfo_bg;
	static QString wininfodown_bg;
	static QString wininfomo_bg;
	static int wininfoHeight;

	static QString appbar_bg;

	static QString titleimage;
	static QString titlerimage;
	static QString titlelimage;
	static QString untitleimage;
	static QString untitlerimage;
	static QString untitlelimage;
	static QStringList imagesDirs;
	static QString defaultBrowser;
	static QString defaultFM;
	static int iconsize;            // Icons Size
	static bool flaticons;
	static bool withxpmenu;
	static itemsFrame *xpmenu;

	static void readTheme(QString theme);
	static void read_config(void);
	static void read_cfg(void);
	static QString get_cfile(char *name);
	static int get_tb_height(void){return tb_height;};
	static hwmItem readItem(QString path);

	static void addToRecentPrograms(QString path);
};
#endif
