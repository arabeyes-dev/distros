/****************************************************************************
** Image collection for project 'deskitem'.
**
** Generated from reading image files: 
**      images/default.png
**
** Created: Fri Apr 2 23:56:15 2004
**      by: The User Interface Compiler ($Id$)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#include <qimage.h>
#include <qdict.h>
#include <qmime.h>
#include <qdragobject.h>

// images/default.png
static const unsigned char image_0_data[] = {
    0x00,0x00,0x10,0x00,0x78,0x9c,0xed,0xd7,0x41,0x0a,0x80,0x20,0x14,0x84,
    0xe1,0xb9,0x7a,0xbc,0x9b,0x05,0xd3,0xb5,0xb4,0x8d,0xe0,0xa6,0x72,0xf3,
    0xde,0x48,0x4c,0xf0,0x43,0xb8,0xf9,0x10,0x14,0x95,0x17,0x41,0x27,0x2d,
    0x8e,0x68,0xd5,0x01,0x68,0xb3,0xcf,0x93,0x65,0x3d,0xf9,0x71,0x8f,0x95,
    0x94,0xe8,0x8f,0x39,0xe2,0xe5,0xff,0xcf,0x3e,0x16,0xda,0x76,0xfd,0x15,
    0xec,0xbb,0x2f,0x3f,0xfb,0xb3,0x6f,0xdf,0xbe,0x7d,0xfb,0xf6,0x77,0xf3,
    0x95,0xe7,0xbf,0xf2,0xfe,0xbd,0x72,0x67,0xca,0x48,0xfd,0xee,0x71,0xfa,
    0x3a,0x7e,0xd2,0x73,0xde
};

static struct EmbedImage {
    int width, height, depth;
    const unsigned char *data;
    ulong compressed;
    int numColors;
    const QRgb *colorTable;
    bool alpha;
    const char *name;
} embed_image_vec[] = {
    { 32, 32, 32, (const unsigned char*)image_0_data, 103, 0, 0, TRUE, "default.png" },
    { 0, 0, 0, 0, 0, 0, 0, 0, 0 }
};

static QImage uic_findImage( const QString& name )
{
    for ( int i=0; embed_image_vec[i].data; i++ ) {
	if ( QString::fromUtf8(embed_image_vec[i].name) == name ) {
	    QByteArray baunzip;
	    baunzip = qUncompress( embed_image_vec[i].data, 
		embed_image_vec[i].compressed );
	    QImage img((uchar*)baunzip.data(),
			embed_image_vec[i].width,
			embed_image_vec[i].height,
			embed_image_vec[i].depth,
			(QRgb*)embed_image_vec[i].colorTable,
			embed_image_vec[i].numColors,
			QImage::BigEndian
		);
	    img = img.copy();
	    if ( embed_image_vec[i].alpha )
		img.setAlphaBuffer(TRUE);
	    return img;
        }
    }
    return QImage();
}

class MimeSourceFactory_deskitem : public QMimeSourceFactory
{
public:
    MimeSourceFactory_deskitem() {}
    ~MimeSourceFactory_deskitem() {}
    const QMimeSource* data( const QString& abs_name ) const {
	const QMimeSource* d = QMimeSourceFactory::data( abs_name );
	if ( d || abs_name.isNull() ) return d;
	QImage img = uic_findImage( abs_name );
	if ( !img.isNull() )
	    ((QMimeSourceFactory*)this)->setImage( abs_name, img );
	return QMimeSourceFactory::data( abs_name );
    };
};

static QMimeSourceFactory* factory = 0;

void qInitImages_deskitem()
{
    if ( !factory ) {
	factory = new MimeSourceFactory_deskitem;
	QMimeSourceFactory::defaultFactory()->addFactory( factory );
    }
}

void qCleanupImages_deskitem()
{
    if ( factory ) {
	QMimeSourceFactory::defaultFactory()->removeFactory( factory );
	delete factory;
	factory = 0;
    }
}

class StaticInitImages_deskitem
{
public:
    StaticInitImages_deskitem() { qInitImages_deskitem(); }
#if defined(Q_OS_SCO) || defined(Q_OS_UNIXWARE)
    ~StaticInitImages_deskitem() { }
#else
    ~StaticInitImages_deskitem() { qCleanupImages_deskitem(); }
#endif
};

static StaticInitImages_deskitem staticImages;
