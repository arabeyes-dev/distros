HEADERS += hdisplay.h bgform.h clrform.h
SOURCES	+= main.cpp hdisplay.cpp bgform.cpp clrform.cpp

TRANSLATIONS	= lng/hdisplay_ar.ts \
                  lng/hdisplay_nl.ts \
                  lng/hdisplay_es.ts \
                  lng/hdisplay_fr.ts

IMAGES	= images/monitor.png ../../files/images/display.xpm


TEMPLATE	=app
CONFIG	+= qt warn_on release
LANGUAGE	= C++

INCLUDEPATH	+= ../../include
win32:LIBS	+= ../../hwm/lib/hwm.lib
unix:LIBS	+= -L../../lib -lhwm
