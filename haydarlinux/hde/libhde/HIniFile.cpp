/* HIniFile.cpp */ 
   
#include <hinifile.h>
   
#include <qfile.h> 
#include <iostream.h> 
#include <qregexp.h> 
   
#define MAX_LINE_LENGTH 1500 

   
HIniFile::HIniFile(const QString& filename) { 
    setName(filename); 
   
    // create default group 
    setGroup("Default"); 
} 


HIniFile::HIniFile() { }
   

HIniFile::~HIniFile(){
    clear(); 
}


void HIniFile::clear(){
    ConfigMap::Iterator it;

    for(it = map.begin(); it != map.end(); ++it)
	delete it.data();
}


void HIniFile::setName(const QString& filename){
    // assign file name
    fname = filename;
}


void HIniFile::setGroup(const QString& group){
    if (map.find(group) == map.end()){
        // new group
        currentgroup = new HIniGroup(group);
	Q_CHECK_PTR(currentgroup);
	
        map.insert(currentgroup->name(), currentgroup);
    }
    else currentgroup = map[group];
}

const QString& HIniFile::group() const{
    return currentgroup->name();
}

void HIniFile::writeEntry(const QString& key, const QString& value){
    currentgroup->writeEntry(key, value);
}

void HIniFile::writeString(const QString& key, const QString& value){
    currentgroup->writeEntry(key, value);
}

void HIniFile::writeInt(const QString& key, int value){
    QString tmp;
    tmp.setNum(value);

    currentgroup->writeEntry(key, tmp);
}


void HIniFile::writeDouble(const QString& key, double value){
    QString tmp;
    tmp.setNum(value);

    currentgroup->writeEntry(key, tmp);
}

void HIniFile::writeFloat(const QString& key, float value){
    QString tmp;
    tmp.setNum(value);

    currentgroup->writeEntry(key, tmp);
}

void HIniFile::writeBool(const QString& key, bool value){
    QString tmp;
    if (value == true)
	tmp = "TRUE";
    else value = "FALSE";

    currentgroup->writeEntry(key, tmp);
}


const QString& HIniFile::readString(const QString& key, const QString& default_value){

    const  QString& tmp = currentgroup->readEntry(key, default_value);
    if (tmp.compare(key + " =") == 0){
	return(default_value);
    }
    return tmp;
}


int HIniFile::readInt(const QString& key, int default_value){
    QString tmp;
    
    tmp = currentgroup->readEntry(key);
    if (tmp == QString::null)
	return default_value;
    else
    return tmp.toInt();
}


double HIniFile::readDouble(const QString& key, double default_value){
    QString tmp;

    tmp = currentgroup->readEntry(key);
    if (tmp == QString::null)
	return default_value;
    else
	return tmp.toDouble();
}

float HIniFile::readFloat(const QString& key, float default_value){
    QString tmp;

    tmp = currentgroup->readEntry(key);
    if (tmp == QString::null)
	return default_value;
    else
	return tmp.toFloat();
}


bool HIniFile::readBool(const QString& key, bool default_value){
    QString tmp;

    tmp = currentgroup->readEntry(key);
    if (tmp.lower().compare("true") == 0)
	return true;
    if (tmp.lower().compare("false") == 0)
	return false;
    else
	return default_value;
}

void HIniFile::write(){
    if (fname.isEmpty()) return;

    // write to file
    QFile file(name());
    file.open(IO_WriteOnly);

    ConfigMap::Iterator it;

    for(it = map.begin(); it != map.end(); ++it)
	if (!it.data()->isEmpty()){
	    // write group name
	    file.writeBlock("[" + it.key() + "]\n", 3 + it.key().length());

	    // write group data
	    GroupMap::Iterator git;

	    for (git = it.data()->map.begin(); git != it.data()->map.end(); ++git)
		file.writeBlock(git.key() + " = " + git.data() + "\n",
			git.key().length() + 3 + git.data().length() + 1);
        }

    file.close();
}


void HIniFile::read(){
    if (fname.isEmpty()) return;

    // open file
    QFile file(name());
    file.open(IO_ReadOnly);

    QString line;

    while(file.readLine(line, MAX_LINE_LENGTH) != -1){
        // is it a group name?
	int index =-1;
	// TEST
	line = line.simplifyWhiteSpace();
	if (line[0] == '[')
	    index = 0;
	// END TEST

	//int index = line.find("[");

        if (index != -1){
            // get group name
            int index2 = line.findRev("]");

            setGroup(line.mid(index + 1, index2 - index - 1));
	}

        // is it a key = value string
        if (index == -1){
            // simplify white space
            line = line.simplifyWhiteSpace();
            index = line.find("=");
	    /*
            if (index != -1){
                // get key and value
                QString key = line.left(index - 1);
                QString value = line.right(line.length() - (index += 2));

                writeEntry(key, value);
	    }
	    */
            if (index != -1){
                // get key and value
                QString key = line.left(index);
		if(key.at(key.length()-1) == ' ')
		    key = key.left(index-1);

                QString value = line.right(line.length() - (index += 1));
		if(value.at(0) == ' ')
		    value = value.right(value.length()-1);

                writeEntry(key, value);
	    }

	}
    }
    
    file.close();

    // switch to default group
    setGroup("Default");
}

QStringList HIniFile::groupList(){
    QStringList tmp;

    ConfigMap::Iterator it;

    for(it = map.begin(); it != map.end(); ++it)
	tmp.append(it.key());

    return tmp;
} 
