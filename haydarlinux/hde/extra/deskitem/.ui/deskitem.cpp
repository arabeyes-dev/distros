/****************************************************************************
** Form implementation generated from reading ui file 'deskitem.ui'
**
** Created: Fri Apr 2 23:56:03 2004
**      by: The User Interface Compiler ($Id$)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#include "deskitem.h"

#include <qvariant.h>
#include <qpushbutton.h>
#include <qtabwidget.h>
#include <qwidget.h>
#include <qlineedit.h>
#include <qcombobox.h>
#include <qframe.h>
#include <qlabel.h>
#include <qgroupbox.h>
#include <qcheckbox.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>
#include <qimage.h>
#include <qpixmap.h>

#include "../deskitem.ui.h"
/*
 *  Constructs a deskitem as a child of 'parent', with the
 *  name 'name' and widget flags set to 'f'.
 *
 *  The dialog will by default be modeless, unless you set 'modal' to
 *  TRUE to construct a modal dialog.
 */
deskitem::deskitem( QWidget* parent, const char* name, bool modal, WFlags fl )
    : QDialog( parent, name, modal, fl )
{
    if ( !name )
	setName( "deskitem" );

    QWidget* privateLayoutWidget = new QWidget( this, "Layout2" );
    privateLayoutWidget->setGeometry( QRect( 11, 369, 298, 30 ) );
    Layout2 = new QHBoxLayout( privateLayoutWidget, 0, 6, "Layout2"); 
    Spacer1 = new QSpacerItem( 70, 0, QSizePolicy::Expanding, QSizePolicy::Minimum );
    Layout2->addItem( Spacer1 );

    okBtn = new QPushButton( privateLayoutWidget, "okBtn" );
    Layout2->addWidget( okBtn );

    ApplyBtn = new QPushButton( privateLayoutWidget, "ApplyBtn" );
    ApplyBtn->setEnabled( FALSE );
    Layout2->addWidget( ApplyBtn );

    cancelBtn = new QPushButton( privateLayoutWidget, "cancelBtn" );
    Layout2->addWidget( cancelBtn );

    itemTab = new QTabWidget( this, "itemTab" );
    itemTab->setGeometry( QRect( 10, 10, 298, 352 ) );

    tab = new QWidget( itemTab, "tab" );

    QWidget* privateLayoutWidget_2 = new QWidget( tab, "layout18" );
    privateLayoutWidget_2->setGeometry( QRect( 10, 6, 270, 259 ) );
    layout18 = new QVBoxLayout( privateLayoutWidget_2, 5, 6, "layout18"); 

    layout6 = new QHBoxLayout( 0, 0, 6, "layout6"); 

    iconBtn = new QPushButton( privateLayoutWidget_2, "iconBtn" );
    iconBtn->setMinimumSize( QSize( 64, 64 ) );
    iconBtn->setMaximumSize( QSize( 64, 64 ) );
    iconBtn->setPixmap( QPixmap::fromMimeSource( "default.png" ) );
    layout6->addWidget( iconBtn );

    layout5 = new QVBoxLayout( 0, 0, 6, "layout5"); 

    nameEdit = new QLineEdit( privateLayoutWidget_2, "nameEdit" );
    layout5->addWidget( nameEdit );

    layout4 = new QHBoxLayout( 0, 0, 6, "layout4"); 
    spacer2 = new QSpacerItem( 140, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout4->addItem( spacer2 );

    langCombo = new QComboBox( FALSE, privateLayoutWidget_2, "langCombo" );
    langCombo->setEnabled( FALSE );
    layout4->addWidget( langCombo );
    layout5->addLayout( layout4 );
    layout6->addLayout( layout5 );
    layout18->addLayout( layout6 );

    spaceFrame = new QFrame( privateLayoutWidget_2, "spaceFrame" );
    spaceFrame->setFrameShape( QFrame::HLine );
    spaceFrame->setFrameShadow( QFrame::Sunken );
    layout18->addWidget( spaceFrame );

    layout3 = new QHBoxLayout( 0, 0, 6, "layout3"); 

    execLabel = new QLabel( privateLayoutWidget_2, "execLabel" );
    layout3->addWidget( execLabel );

    execEdit = new QLineEdit( privateLayoutWidget_2, "execEdit" );
    layout3->addWidget( execEdit );
    layout18->addLayout( layout3 );

    spaceFrame_2 = new QFrame( privateLayoutWidget_2, "spaceFrame_2" );
    spaceFrame_2->setFrameShape( QFrame::HLine );
    spaceFrame_2->setFrameShadow( QFrame::Sunken );
    layout18->addWidget( spaceFrame_2 );

    layout15 = new QGridLayout( 0, 1, 1, 0, 6, "layout15"); 

    textLabel10 = new QLabel( privateLayoutWidget_2, "textLabel10" );

    layout15->addWidget( textLabel10, 0, 0 );

    textLabel12 = new QLabel( privateLayoutWidget_2, "textLabel12" );

    layout15->addWidget( textLabel12, 1, 0 );

    mimeLbl = new QLabel( privateLayoutWidget_2, "mimeLbl" );

    layout15->addWidget( mimeLbl, 1, 1 );

    typeLbl = new QLabel( privateLayoutWidget_2, "typeLbl" );

    layout15->addWidget( typeLbl, 0, 1 );
    layout18->addLayout( layout15 );

    spaceFrame_3 = new QFrame( privateLayoutWidget_2, "spaceFrame_3" );
    spaceFrame_3->setFrameShape( QFrame::HLine );
    spaceFrame_3->setFrameShadow( QFrame::Sunken );
    layout18->addWidget( spaceFrame_3 );

    layout16 = new QGridLayout( 0, 1, 1, 0, 6, "layout16"); 

    accessedLbl = new QLabel( privateLayoutWidget_2, "accessedLbl" );

    layout16->addWidget( accessedLbl, 0, 1 );

    modifiedLbl = new QLabel( privateLayoutWidget_2, "modifiedLbl" );

    layout16->addWidget( modifiedLbl, 1, 1 );

    textLabel18 = new QLabel( privateLayoutWidget_2, "textLabel18" );

    layout16->addWidget( textLabel18, 1, 0 );

    textLabel16 = new QLabel( privateLayoutWidget_2, "textLabel16" );

    layout16->addWidget( textLabel16, 0, 0 );
    layout18->addLayout( layout16 );

    spaceFrame_4 = new QFrame( privateLayoutWidget_2, "spaceFrame_4" );
    spaceFrame_4->setFrameShape( QFrame::HLine );
    spaceFrame_4->setFrameShadow( QFrame::Sunken );
    layout18->addWidget( spaceFrame_4 );

    layout17 = new QHBoxLayout( 0, 0, 6, "layout17"); 

    textLabel20 = new QLabel( privateLayoutWidget_2, "textLabel20" );
    layout17->addWidget( textLabel20 );

    sizeLbl = new QLabel( privateLayoutWidget_2, "sizeLbl" );
    layout17->addWidget( sizeLbl );
    layout18->addLayout( layout17 );
    itemTab->insertTab( tab, QString("") );

    permissionsTab = new QWidget( itemTab, "permissionsTab" );

    groupBox1 = new QGroupBox( permissionsTab, "groupBox1" );
    groupBox1->setGeometry( QRect( 10, 10, 270, 110 ) );

    QWidget* privateLayoutWidget_3 = new QWidget( groupBox1, "layout10" );
    privateLayoutWidget_3->setGeometry( QRect( 10, 20, 250, 62 ) );
    layout10 = new QGridLayout( privateLayoutWidget_3, 1, 1, 5, 6, "layout10"); 

    textLabel1 = new QLabel( privateLayoutWidget_3, "textLabel1" );

    layout10->addWidget( textLabel1, 0, 0 );

    ownerBox = new QComboBox( FALSE, privateLayoutWidget_3, "ownerBox" );
    ownerBox->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)7, (QSizePolicy::SizeType)0, 0, 0, ownerBox->sizePolicy().hasHeightForWidth() ) );

    layout10->addWidget( ownerBox, 0, 1 );

    groupBox = new QComboBox( FALSE, privateLayoutWidget_3, "groupBox" );
    groupBox->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)7, (QSizePolicy::SizeType)0, 0, 0, groupBox->sizePolicy().hasHeightForWidth() ) );

    layout10->addWidget( groupBox, 1, 1 );

    textLabel2 = new QLabel( privateLayoutWidget_3, "textLabel2" );

    layout10->addWidget( textLabel2, 1, 0 );

    groupBox2 = new QGroupBox( permissionsTab, "groupBox2" );
    groupBox2->setGeometry( QRect( 10, 130, 270, 181 ) );

    QWidget* privateLayoutWidget_4 = new QWidget( groupBox2, "layout14" );
    privateLayoutWidget_4->setGeometry( QRect( 10, 20, 250, 150 ) );
    layout14 = new QVBoxLayout( privateLayoutWidget_4, 5, 6, "layout14"); 

    layout14_2 = new QGridLayout( 0, 1, 1, 0, 6, "layout14_2"); 

    aWrite = new QCheckBox( privateLayoutWidget_4, "aWrite" );

    layout14_2->addWidget( aWrite, 2, 2 );

    textLabel4 = new QLabel( privateLayoutWidget_4, "textLabel4" );

    layout14_2->addWidget( textLabel4, 1, 0 );

    aRead = new QCheckBox( privateLayoutWidget_4, "aRead" );
    aRead->setChecked( TRUE );

    layout14_2->addWidget( aRead, 2, 1 );

    gRead = new QCheckBox( privateLayoutWidget_4, "gRead" );
    gRead->setChecked( TRUE );

    layout14_2->addWidget( gRead, 1, 1 );

    oRead = new QCheckBox( privateLayoutWidget_4, "oRead" );
    oRead->setChecked( TRUE );

    layout14_2->addWidget( oRead, 0, 1 );

    aExec = new QCheckBox( privateLayoutWidget_4, "aExec" );
    aExec->setChecked( TRUE );

    layout14_2->addWidget( aExec, 2, 3 );

    textLabel5 = new QLabel( privateLayoutWidget_4, "textLabel5" );

    layout14_2->addWidget( textLabel5, 2, 0 );

    textLabel3 = new QLabel( privateLayoutWidget_4, "textLabel3" );

    layout14_2->addWidget( textLabel3, 0, 0 );

    gExec = new QCheckBox( privateLayoutWidget_4, "gExec" );
    gExec->setChecked( TRUE );

    layout14_2->addWidget( gExec, 1, 3 );

    oWrite = new QCheckBox( privateLayoutWidget_4, "oWrite" );
    oWrite->setChecked( TRUE );

    layout14_2->addWidget( oWrite, 0, 2 );

    oExec = new QCheckBox( privateLayoutWidget_4, "oExec" );
    oExec->setChecked( TRUE );

    layout14_2->addWidget( oExec, 0, 3 );

    gWrite = new QCheckBox( privateLayoutWidget_4, "gWrite" );

    layout14_2->addWidget( gWrite, 1, 2 );
    layout14->addLayout( layout14_2 );

    line1 = new QFrame( privateLayoutWidget_4, "line1" );
    line1->setFrameShape( QFrame::HLine );
    line1->setFrameShadow( QFrame::Sunken );
    line1->setFrameShape( QFrame::HLine );
    layout14->addWidget( line1 );

    layout13 = new QGridLayout( 0, 1, 1, 0, 6, "layout13"); 

    textView = new QLabel( privateLayoutWidget_4, "textView" );

    layout13->addWidget( textView, 0, 1 );

    textLabel7 = new QLabel( privateLayoutWidget_4, "textLabel7" );

    layout13->addWidget( textLabel7, 1, 0 );

    numberView = new QLabel( privateLayoutWidget_4, "numberView" );

    layout13->addWidget( numberView, 1, 1 );

    textLabel6 = new QLabel( privateLayoutWidget_4, "textLabel6" );

    layout13->addWidget( textLabel6, 0, 0 );
    layout14->addLayout( layout13 );
    itemTab->insertTab( permissionsTab, QString("") );
    languageChange();
    resize( QSize(319, 411).expandedTo(minimumSizeHint()) );
    clearWState( WState_Polished );

    // signals and slots connections
    connect( cancelBtn, SIGNAL( clicked() ), this, SLOT( reject() ) );
    connect( okBtn, SIGNAL( clicked() ), this, SLOT( okBtn_clicked() ) );
    connect( ApplyBtn, SIGNAL( clicked() ), this, SLOT( ApplyBtn_clicked() ) );
    connect( iconBtn, SIGNAL( clicked() ), this, SLOT( iconBtn_clicked() ) );
}

/*
 *  Destroys the object and frees any allocated resources
 */
deskitem::~deskitem()
{
    // no need to delete child widgets, Qt does it all for us
}

/*
 *  Sets the strings of the subwidgets using the current
 *  language.
 */
void deskitem::languageChange()
{
    setCaption( tr( "Item Properties" ) );
    okBtn->setText( tr( "OK" ) );
    ApplyBtn->setText( tr( "Apply" ) );
    cancelBtn->setText( tr( "Cancel" ) );
    iconBtn->setText( QString::null );
    langCombo->clear();
    langCombo->insertItem( QString::null );
    langCombo->insertItem( tr( "ar" ) );
    langCombo->insertItem( tr( "es" ) );
    langCombo->insertItem( tr( "fr" ) );
    langCombo->insertItem( tr( "nl" ) );
    langCombo->insertItem( tr( "tr" ) );
    execLabel->setText( tr( "Executable" ) );
    textLabel10->setText( tr( "Type:" ) );
    textLabel12->setText( tr( "MimeType:" ) );
    mimeLbl->setText( QString::null );
    typeLbl->setText( tr( "Desktop File" ) );
    accessedLbl->setText( tr( "01 Jan 2003" ) );
    modifiedLbl->setText( tr( "01 Jan 2003" ) );
    textLabel18->setText( tr( "Modified:" ) );
    textLabel16->setText( tr( "Accessed:" ) );
    textLabel20->setText( tr( "Size:" ) );
    sizeLbl->setText( tr( "1 KB" ) );
    itemTab->changeTab( tab, tr( "General" ) );
    groupBox1->setTitle( tr( "Owner and Group" ) );
    textLabel1->setText( tr( "Owner:" ) );
    textLabel2->setText( tr( "Group:" ) );
    groupBox2->setTitle( tr( "Access Permissions" ) );
    aWrite->setText( tr( "Write" ) );
    textLabel4->setText( tr( "Group:" ) );
    aRead->setText( tr( "Read" ) );
    gRead->setText( tr( "Read" ) );
    oRead->setText( tr( "Read" ) );
    aExec->setText( tr( "Exec" ) );
    textLabel5->setText( tr( "Others:" ) );
    textLabel3->setText( tr( "Owner:" ) );
    gExec->setText( tr( "Exec" ) );
    oWrite->setText( tr( "Write" ) );
    oExec->setText( tr( "Exec" ) );
    gWrite->setText( tr( "Write" ) );
    textView->setText( tr( "rwx-rw-rw" ) );
    textLabel7->setText( tr( "Number View:" ) );
    numberView->setText( tr( "755" ) );
    textLabel6->setText( tr( "Text View:" ) );
    itemTab->changeTab( permissionsTab, tr( "Permissions" ) );
}

