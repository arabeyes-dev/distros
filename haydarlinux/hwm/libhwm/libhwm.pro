TEMPLATE =  lib
#CONFIG   +=  qt warn_on
CONFIG    += qt thread
HEADERS  +=  ../include/libhwm.h \
	    ../include/lhwmconf.h \ 
	    ../include/defs.h \
	    ../include/hinifile.h \
	    ../include/hinigroup.h \
            ../include/hsynedit.h \
            ../include/hrichedit.h \ 
            ../include/hcalender.h \ 
            ../include/hclock.h \ 
            ../include/hcloseframe.h \
	    ../include/defaults.h \
            ../include/netwm.h  \
            ../include/netwm_def.h \
            ../include/netwm_p.h \
            ../include/smenuitem.h \
            ../include/startmenu.h

SOURCES  +=  libhwm.cpp HIniGroup.cpp HIniFile.cpp defaults.cpp hcalender.cpp\
	     hclock.cpp hwm_tbab.cpp  \ 
             hsynedit.cpp hrichedit.cpp \
             hcloseframe.cpp netwm.cpp smenuitem.cpp startmenu.cpp

unix {
    HEADERS += ../include/hwmstyle.h
    SOURCES += hwmstyle.cpp 
    VERSION = 0.4.0 
}

TRANSLATIONS    = ../files/lng/libhwm_ar.ts \
                  ../files/lng/libhwm_nl.ts \
                  ../files/lng/libhwm_fr.ts \
                  ../files/lng/libhwm_es.ts \
                  ../files/lng/libhwm_tr.ts


TARGET   =  hwm

target.path=$$plugins.path
isEmpty(target.path):target.path=$$QT_PREFIX/plugins
INSTALLS    += target 


DESTDIR	 =  ../lib
INCLUDEPATH += ../include
LANGUAGE	= C++

DEFINES += QT_CLEAN_NAMESPACE
