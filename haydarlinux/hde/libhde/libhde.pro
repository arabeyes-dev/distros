TEMPLATE =  lib
#CONFIG   +=  qt warn_on
CONFIG    += qt thread
HEADERS  +=  ../include/libhde.h \
	    ../include/lhdeconf.h \
	    ../include/defs.h \
	    ../include/hinifile.h \
	    ../include/hinigroup.h \
            ../include/hsynedit.h \
            ../include/hrichedit.h \
            ../include/hcalender.h \
            ../include/hclock.h \
            ../include/hcloseframe.h \
	    ../include/defaults.h \
            ../include/netwm.h \
            ../include/netwm_def.h \
            ../include/netwm_p.h \
            ../include/smenuitem.h \
            ../include/startmenu.h \
            ../include/qhijri.h

SOURCES  +=  libhde.cpp \
	     HIniGroup.cpp \
	     HIniFile.cpp \
	     defaults.cpp \
	     hcalender.cpp\
	     hclock.cpp \
	     hde_tbab.cpp \
             hsynedit.cpp \
	     hrichedit.cpp \
             hcloseframe.cpp \
             netwm.cpp \
	     smenuitem.cpp \
             startmenu.cpp \
             qhijri.cpp


unix {
    HEADERS += ../include/hdestyle.h
    SOURCES += hdestyle.cpp 
    VERSION = 0.4.5 
}

TRANSLATIONS    = ../files/lng/libhde_ar.ts \
                  ../files/lng/libhde_nl.ts \
                  ../files/lng/libhde_fr.ts \
                  ../files/lng/libhde_es.ts \
                  ../files/lng/libhde_tr.ts


TARGET   =  hde

target.path=$$plugins.path
isEmpty(target.path):target.path=$$QT_PREFIX/plugins
INSTALLS    += target 


DESTDIR	 =  ../lib
INCLUDEPATH += ../include
LANGUAGE	= C++

DEFINES += QT_CLEAN_NAMESPACE
