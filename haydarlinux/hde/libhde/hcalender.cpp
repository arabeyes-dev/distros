/****************************************************************************
**
**	hcalender.cpp
**
****************************************************************************/
#include "hcalender.h"
#include <qmessagebox.h> 


//bool isHijri;


/* 
 *  Constructs a HCalender which is a child of 'parent', with the 
 *  name 'name' and widget flags set to 'f'.
 */
HCalender::HCalender( QWidget* parent,  const char* name, bool hijri )
  : QWidget( parent, name )
{
  if ( !name )
    setName( "HCalender" );
  setFixedSize( 294, 180 ); 
  //    QStringList monthNames;
  showLines = true;

  isHijri = hijri;

    if(isHijri){
      monthNames.append(Qhijri::longMonthName ( 1 ));
      for(int i = 2; i <= 12; i++)
	monthNames += Qhijri::longMonthName ( i );
    }else{
      monthNames.append(QDate::longMonthName ( 1 ));
      for(int i = 2; i <= 12; i++)
	monthNames += QDate::longMonthName ( i );
    }
    QWidget* privateLayoutWidget = new QWidget( this, "cLayout" );
    //    privateLayoutWidget->setGeometry( QRect( 5, 5, 285, 174 ) ); 
    privateLayoutWidget->resize(285, 174 );
    cLayout = new QVBoxLayout( privateLayoutWidget, 0, 6, "cLayout"); 

    dateLayout = new QHBoxLayout( 0, 0, 6, "dateLayout"); 

    monthCombo = new QComboBox( FALSE, privateLayoutWidget, "monthCombo" );
    monthCombo->insertStringList( monthNames );
    dateLayout->addWidget( monthCombo );

    yearSpin = new QSpinBox( privateLayoutWidget, "yearSpin" );
    yearSpin->setMaxValue( 40000 );
    dateLayout->addWidget( yearSpin );
    cLayout->addLayout( dateLayout );

    Cal = new calFrame( privateLayoutWidget, "Cal",  QDate::currentDate(),
			isHijri);
    Cal->setPaletteBackgroundColor( QColor( 255, 255, 255 ) );
    Cal->setFrameShape( QFrame::StyledPanel );
    Cal->setFrameShadow( QFrame::Sunken );
    cLayout->addWidget( Cal );

    reset();

    connect( monthCombo, SIGNAL( activated( int ) ), this, SLOT( changed() ) );
    connect( yearSpin, SIGNAL( valueChanged ( int ) ), this, 
	     SLOT( changed() ) );

    connect( Cal, SIGNAL( currentChanged( int, int ) ), this, 
	     SLOT( calChanged( int, int ) ) );

}


void HCalender::reset(void){
    dt = QDateTime::currentDateTime();
    hd = Qhijri::currentDate();

    if(isHijri){
      yearSpin->setValue( hd.year() );
      monthCombo->setCurrentItem(hd.month()-1);

      QDate da = hd.getDate();
      Cal->setDate( da );
      //      Cal->setDate(dt.date());
    }else{
      yearSpin->setValue( dt.date().year() );
      monthCombo->setCurrentItem(dt.date().month()-1);
      Cal->setDate(dt.date());
    }
}

/*  
 *  Destroys the object and frees any allocated resources
 */
HCalender::~HCalender()
{
    // no need to delete child widgets, Qt does it all for us
}

void HCalender::changed()
{

  if(isHijri){
    hd.setYMD( yearSpin->value(), monthCombo->currentItem()+1, 
	       hd.day() );
    QDate da = hd.getDate();
    dt.setDate ( da );
  }else{
    dt.setDate ( QDate(yearSpin->value(), monthCombo->currentItem()+1, 
		     dt.date().day() ) );
  }
    Cal->setDate(dt.date());

    emit dateChanged(dt.date());
}


void HCalender::calChanged(int r, int c)
{
  if(isHijri){
    if(Cal->text(r, c) != "")
      hd.setYMD(yearSpin->value(), monthCombo->currentItem()+1, 
		Cal->text(r, c).toInt() );
    else
      hd.setYMD( yearSpin->value(), monthCombo->currentItem()+1, 
		       hd.day() );

    QDate da = hd.getDate();
    dt.setDate ( da );

  }else{
    if(Cal->text(r, c) != "")
      dt.setDate ( QDate(yearSpin->value(), monthCombo->currentItem()+1, 
			 Cal->text(r, c).toInt() ) );
    else
      dt.setDate ( QDate(yearSpin->value(), monthCombo->currentItem()+1, 
			 dt.date().day() ) );
  }
    Cal->setDate(dt.date());
    
    emit dateChanged(dt.date());

    //    QMessageBox::information( this, "HCalender", dt.date().toString() );
}



/**************************************************************************
 * 
 *	Calender Grid
 *
 **************************************************************************/


calFrame::calFrame( QWidget* parent,  const char* name, QDate date, 
		    bool hijri )
    : QTable( 6, 7, parent, name )
{

  isHijri = hijri;

    setSorting( false );
    setShowGrid( false );
    setReadOnly( true );

    setSelectionMode( QTable::Single );

    horizontalHeader()->setResizeEnabled( false );
    horizontalHeader()->setClickEnabled( false );

    horizontalHeader()->setPaletteBackgroundColor( QColor("darkBlue") );
    horizontalHeader()->setPaletteForegroundColor( QColor("white") );

    setVScrollBarMode( QScrollView::AlwaysOff );
    setHScrollBarMode( QScrollView::AlwaysOff );
    setMouseTracking( true );

    hd = Qhijri::currentDate();
    hd.setYMD( dt.year(), dt.month(), dt.day() );
 

    for(int i = 0; i <= 7; ++i){

      if(isHijri){
	Qhijri hdd;
	horizontalHeader()->setLabel( i-1, hdd.shortDayName ( i ) );
      }else
	horizontalHeader()->setLabel( i-1, QDate::shortDayName ( i ) );
      horizontalHeader()-> resizeSection( i, 40 );
    }
    setLeftMargin(1);

    //    horizontalHeader()->adjustHeaderSize () ;

    refresh();
}

calFrame::~calFrame()
{
    // no need to delete child widgets, Qt does it all for us
}

void calFrame::refresh()
{
  int i = 0;

  for(i= 0; i < numRows(); i++ )
    for(int j= 0; j < numCols (); j++ )
      setText (i, j, "");


  Qhijri hd2( hd.year(), hd.month(), 1 );
  //hd.setYMD( dt.year(), dt.month(), 1 );

  QDate tempDate(dt.year(), dt.month(), 1  );
  int firstDay = -1;
  
  int j = 0;
  
  if( isHijri ){
    firstDay = hd2.dayOfWeek() -1;

    if(firstDay < 0)
      firstDay = firstDay + 7;


      for(i= firstDay; i <= numRows(); i++ )
	setText ( 0, i, QString::number(++j) );


      for(i = 0; i < 7; i++)
	setText ( 1, i, QString::number(++j) );

      for(i = 0; i < 7; i++)
	setText ( 2, i, QString::number(++j) );

      for(i = 0; i < 7; i++)
	setText ( 3, i, QString::number(++j) );

      for(i = 0; (i < 7) && (j < hd.daysInMonth()) ; i++)
	setText ( 4, i, QString::number(++j) );


      for(i = 0; (i < 7) && (j < hd.daysInMonth() ); i++)
	setText ( 5, i, QString::number(++j) );

      /*    
      for(i = 0; i < 7; i++)
	setText ( 1, i, QString::number(i+8-firstDay) );

      for(i = 0; i < 7; i++)
	setText ( 2, i, QString::number(i+15-firstDay) );
      
      for(i = 0; i < 7; i++)
	setText ( 3, i, QString::number(i+22-firstDay) );


      for(i = 0; (i < 7) && (i+29-firstDay <= hd2.daysInMonth() ); i++)
	setText ( 4, i, QString::number(i+29-firstDay) );

      if(36-firstDay < hd2.daysInMonth())
	for(i = 0; (i < 7) && (i+36-firstDay <= hd2.daysInMonth() ); i++)
	  setText ( 5, i, QString::number(i+36-firstDay) );
     
      */

      
      for(i= 0; i < numRows(); i++ )
	for( j= 0; j < numCols (); j++ )
	  
	  if(text(i, j) == QString::number(hd.day()) ){
	    setCurrentCell ( i, j );
	    break;
	  }
      

    }else{
      firstDay = tempDate.dayOfWeek() -1;
      for(i= firstDay; i <= numRows(); i++ )
	setText ( 0, i, QString::number(++j) );
    
      for(i = 0; i < 7; i++)
	setText ( 1, i, QString::number(i+8-firstDay) );

      for(i = 0; i < 7; i++)
	setText ( 2, i, QString::number(i+15-firstDay) );
      
      for(i = 0; i < 7; i++)
	setText ( 3, i, QString::number(i+22-firstDay) );


      for(i = 0; (i < 7) && (i+29-firstDay <= tempDate.daysInMonth() ); i++)
	setText ( 4, i, QString::number(i+29-firstDay) );

      if(36-firstDay < tempDate.daysInMonth())
	for(i = 0; (i < 7) && (i+36-firstDay <= tempDate.daysInMonth() ); i++)
	  setText ( 5, i, QString::number(i+36-firstDay) );
     

      for(i= 0; i < numRows(); i++ )
	for( j= 0; j < numCols (); j++ )
	  
	  if(text(i, j) == QString::number(dt.day()) ){
	    setCurrentCell ( i, j );
	    break;
	  }
    }
}

void calFrame::paintCell( QPainter * p, int row, int col, const QRect & cr,
			     bool selected )
{
	p->fillRect( cellRect(row, col),  QColor("white"));
	p->drawText ( cellRect(row, col), Qt::AlignHCenter|Qt::AlignVCenter, 
		      text(row, col) );
}

void calFrame::paintFocus ( QPainter * p, const QRect & cr )
{
	p->fillRect( cellRect(currentRow(),currentColumn() ), 
		     QColor("darkBlue"));
	p->setPen( QColor("white") ); 
	p->drawText ( cellRect(currentRow(),currentColumn() ), 
		      Qt::AlignHCenter|Qt::AlignVCenter , 
		      text(currentRow(), currentColumn()) );
} 

void calFrame::setDate(QDate date)
{
  dt = date; 
  hd.GregorianToHijri( dt.year(), dt.month(), dt.day());
  //hd.setYMD( date.year(), date.month(), date.day());

  //cout << hd.year() << "-" << hd.month() << "-" << hd.day() << endl;
  refresh();
};
