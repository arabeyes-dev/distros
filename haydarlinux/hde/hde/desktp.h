/* desktp.h */

#include "defs.h"

#ifndef DESKTP_H
#define DESKTP_H
class desktopIconDrag : public QIconDrag
{
    Q_OBJECT

public:
    desktopIconDrag( QWidget * dragSource, const char* name = 0 );

    const char* format( int i ) const;
    QByteArray encodedData( const char* mime ) const;
    static bool canDecode( QMimeSource* e );
    void append( const QIconDragItem &item, const QRect &pr, const QRect &tr, const QString &url );

private:
    QStringList urls;

};

class desktopItem : public QIconViewItem
{
 public:
    desktopItem( QIconView *parent ) :
        QIconViewItem( parent ) {
	  //	  setDragEnabled ( true );
	  setDropEnabled(true);
	}
	
    virtual bool acceptDrop( const QMimeSource *mime ) const;
    void setPath(QString p){ path = p; };
    QString getPath(){ return path; };
    void setFilename(QString f){ filename = f; };
    QString getFilename(){ return filename; };

 protected:
    virtual void dropped( QDropEvent *e, const QValueList<QIconDragItem> & );
    QString path;
    QString filename;
};


class desktp : public QIconView
{
	Q_OBJECT
	    
 protected:
    void createIcon(QString path, QString fname);
    virtual void dragEnterEvent ( QDragEnterEvent * );
    virtual void dropEvent ( QDropEvent * );
    virtual QDragObject *dragObject();

 protected slots:
    void slotNewItem( QDropEvent *evt, const QValueList<QIconDragItem>& list );
    //void dropped( QDropEvent *e, const QValueList<QIconDragItem> & );

 public slots:

    void timeout(void);
    void iconDblClicked(QIconViewItem *);
    void iconReturnPressed( QIconViewItem * );
    void iconClicked( QIconViewItem * item );
    void rightClicked( QIconViewItem* item, const QPoint& pos );
    void prop( void );
    void newItem( void );
    void deleteItem();
    void renameItem();
    void itemProp( void );

 public:
    QPopupMenu *rightmnu;
    desktp(QWidget *parent=0, const char *name=0, WFlags f = 0);
    void refreshIV(void);
    //    QDragObject *dragObject();

};
#endif
