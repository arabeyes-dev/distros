/* 
*  File      :  biff.cpp
*  Written by:  alinden@gmx.de
*  Copyright :  GPL
*
*  Pops up a small envelope if mail is present.
*  Changes color if new mail arrives.
*/

#include <qfileinfo.h>
#include <qrect.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include "conf.h"
#include "mail.xpm"
#include "newmail.xpm"
#include "biff.h"
#include <defaults.h>

defaults def;

biff::biff(QWidget *parent, const char *name) : QWidget(parent, name)
{
	QString s(getenv("MAIL"));
	QFileInfo fi = s;
	if(! fi.exists()) 
	{
		s = MAILDIR;
		s += getlogin();
		fi.setFile(s);
	}

	if(fi.exists())
	{
		mailfile = fi.absFilePath();
		//		startTimer(3000);
		QTimer *timer = new QTimer(this);
		connect(timer, SIGNAL(timeout()), SLOT(timeout()));
		timer->start(5000);  // signal every 5 seconds
    	}
	else
	{
		perror((const char *)s);
		exit(1);
	}

    QImage img( mail_xpm );

    hasmail.convertFromImage( img );
    if ( !hasmail.mask() )
	if ( img.hasAlphaBuffer() ) {
	    QBitmap bm;
	    bm = img.createAlphaMask();
	    hasmail.setMask( bm );
	} else {
	    QBitmap bm;
	    bm = img.createHeuristicMask();
	    hasmail.setMask( bm );
	}


    QImage img1( newmail_xpm );

    newmail.convertFromImage( img1 );
    if ( !newmail.mask() )
	if ( img1.hasAlphaBuffer() ) {
	    QBitmap bm;
	    bm = img1.createAlphaMask();
	    newmail.setMask( bm );
	} else {
	    QBitmap bm;
	    bm = img1.createHeuristicMask();
	    newmail.setMask( bm );
	}

    //	hasmail = QPixmap((const char **)mail_xpm);
    //	newmail = QPixmap((const char **)newmail_xpm);
	
	mailstate = Empty;
}


void biff::timeout(void)
//void biff::timerEvent(QTimerEvent *)
{
	static mailbox *mbox = NULL;
	
	QFileInfo fi(mailfile);
	
	if(mailstate != Empty && fi.size() == 0)
	{
		if(mbox != NULL)
		{
			delete mbox;
			mbox = NULL;
		}
		mailstate = Empty;
	}
	else if(mailstate != Newmail && fi.lastModified() > fi.lastRead())
	{
		if(mbox == NULL)
			mbox = new mailbox();

		mbox->set_pixmap(newmail);
		mailstate = Newmail;
	}
	else if(mailstate != Hasmail && fi.size() && fi.lastRead() > fi.lastModified())
	{
		if(mbox == NULL)
			mbox = new mailbox();

		mbox->set_pixmap(hasmail);
		mailstate = Hasmail;
	}
}

mailbox::mailbox(QWidget *parent, const char *name) : QWidget(parent, name)
{
	resize(16, 16);
	show();
}

void mailbox::set_pixmap(QPixmap &pix)
{
	envelope = pix;
	repaint(FALSE);
}

void mailbox::paintEvent(QPaintEvent *paint)
{
    paint_mailbox();
}

void mailbox::resizeEvent(QResizeEvent *)
{
    paint_mailbox();
}

void mailbox::paint_mailbox(void)
{

    //if(biff::mailstate != biff::Empty){
	//QPainter *p = new QPainter(this);

	/*
	def.read_cfg();

	if ((def.styleName == "hdeStyle")||(def.styleName == "WinXPStyle")){
	    QColor firstColor(palette().active().background());
	

	    int h = (height()-2) /2;
	    int i = 0;

	    for ( i = 0; i < h+1; ++i ) {
		p->setPen(firstColor.light(100 + (h*3)-(i*3)));
		p->drawLine(0,i , width(), i); 
	    }

	    int j = 0;
	    for ( i = height()-h-1; i < height() ; ++i ) {
		j++;
		p->setPen(firstColor.dark(100 +(j*3)));
		p->drawLine(0, i , width(), i); 
	    }

	}else{

	}
	p->end();
    //}
    */
	/*
    QPixmap pix(width(), height());

    QBitmap mask(width(), height(), TRUE);  // all transparent
    QPainter painter(&mask);
 
    painter.drawPixmap(0, (height()/2)-(envelope.height()/2), envelope);
    pix.setMask(mask);
    if ( pix.mask() )
	    setMask( *pix.mask() );

    updateMask();

    p->drawPixmap(0, (height()/2)-(envelope.height()/2), envelope);
	*/


    //
    //

    QPainter *p = new QPainter(this);


    QPixmap pix(width(), height());
    //    p->drawPixmap(0, (height()/2)-(envelope.height()/2), envelope);
    p->drawPixmap(0, 0, envelope);
    
    p->end();

    if ( envelope.mask() )
	setMask( *envelope.mask() );

    /*
	setBackgroundPixmap( envelope );

	if ( envelope.mask() )
	    setMask( *envelope.mask() );
	
	//	bitBlt(this, 0, (height()/2)-(envelope.height()/2), &envelope);
	*/
}
