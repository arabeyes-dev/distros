/****************************************************************************
 *
 **********************************************************************/

#ifndef HWMSTYLE_H
#define HWMSTYLE_H

#include <qstyle.h>
#include <qcommonstyle.h>
#include <qbitmap.h>

#include <qintdict.h>


class QPalette;

#if defined(QT_PLUGIN)
#define Q_EXPORT_STYLE_HWM
#else
#define Q_EXPORT_STYLE_HWM Q_EXPORT
#endif

class Q_EXPORT_STYLE_HWM hdeStyle : public QCommonStyle //QWindowsStyle
{
    Q_OBJECT
 public:
    hdeStyle();
    virtual ~hdeStyle();


    void polish( QWidget* widget );
    void unPolish( QWidget* widget );

    virtual void polishPopupMenu( QPopupMenu* );

    void drawGradient( QPainter* p, 
		       const QRect& r, 
		       QColor clr,
		       bool horizontal, 
		       bool isoff, bool=false) const;

    int styleHint( StyleHint sh, 
		   const QWidget *w = 0,
		   const QStyleOption &opt = QStyleOption::Default,
		   QStyleHintReturn* shr = 0 ) const;

    void drawPrimitive( PrimitiveElement pe,
			QPainter *p,
			const QRect &r,
			const QColorGroup &cg,
			SFlags flags = Style_Default,
			const QStyleOption& = QStyleOption::Default ) const;


    void drawControl( ControlElement element,
		      QPainter *p,
		      const QWidget *widget,
		      const QRect &r,
		      const QColorGroup &cg,
		      SFlags how = Style_Default,
		      const QStyleOption& = QStyleOption::Default ) const;

    void drawComplexControl( ComplexControl control,
			 QPainter *p,
			 const QWidget *widget,
			 const QRect &r,
			 const QColorGroup &cg,
			 SFlags how = Style_Default,
			 SCFlags sub = SC_All,
			 SCFlags subActive = SC_None,
			 const QStyleOption& = QStyleOption::Default ) const;

    QStyle::SubControl hdeStyle::querySubControl( ComplexControl control,
				const QWidget *widget,
				const QPoint &pos,
				const QStyleOption& = QStyleOption::Default)
	const;

    QRect querySubControlMetrics(ComplexControl control,
				 const QWidget *widget,
				 SubControl sc,
				 const QStyleOption& = QStyleOption::Default)
	const;

    int pixelMetric( PixelMetric metric, const QWidget *w=0 ) const;

protected:
    bool hdestyle;
    bool eventFilter( QObject *object, QEvent *event );
		QWidget *hoverWidget;

private:	
    hdeStyle( const hdeStyle & );
    hdeStyle& operator=( const hdeStyle & );
    QStyle *winstyle;
};



#endif // HWMSTYLE_H
