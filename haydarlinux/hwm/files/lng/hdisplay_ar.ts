<!DOCTYPE TS><TS>
<context>
    <name>bgForm</name>
    <message>
        <source>Desktop</source>
        <translation>سطح المكتب</translation>
    </message>
    <message>
        <source>Chose your background:</source>
        <translation>اختر خلفيتك:</translation>
    </message>
    <message>
        <source>No Background</source>
        <translation>لا خلفية</translation>
    </message>
    <message>
        <source>Browse</source>
        <translation>تصفح</translation>
    </message>
    <message>
        <source>Position:</source>
        <translation>موقع:</translation>
    </message>
    <message>
        <source>Center</source>
        <translation>توسط</translation>
    </message>
    <message>
        <source>Tile</source>
        <translation>عنوان</translation>
    </message>
    <message>
        <source>Stretch</source>
        <translation>تمدد</translation>
    </message>
    <message>
        <source>OK</source>
        <translation>موافق</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>إلغاء الأمر</translation>
    </message>
    <message>
        <source>Apply</source>
        <translation>تطبيق</translation>
    </message>
</context>
<context>
    <name>clrForm</name>
    <message>
        <source>Appearance</source>
        <translation>المضهر</translation>
    </message>
    <message>
        <source>Inactive Window</source>
        <translation>الإطار الغير مفعل</translation>
    </message>
    <message>
        <source>X</source>
        <translation></translation>
    </message>
    <message>
        <source>Start</source>
        <translation>إبدأ</translation>
    </message>
    <message>
        <source>Active Window</source>
        <translation>الإطار المفعل</translation>
    </message>
    <message>
        <source>Style:</source>
        <translation>شكل:</translation>
    </message>
    <message>
        <source>Size:</source>
        <translation>حجم:</translation>
    </message>
    <message>
        <source>Color2:</source>
        <translation>لون2:</translation>
    </message>
    <message>
        <source>Desktop</source>
        <translation>سطح المكتب</translation>
    </message>
    <message>
        <source>Active titlebar</source>
        <translation>شريط العنوان المفعل</translation>
    </message>
    <message>
        <source>Inactive titlebar</source>
        <translation>شريط العنوان الغير مفعل</translation>
    </message>
    <message>
        <source>Titlebar button</source>
        <translation>زر شريط العنوان</translation>
    </message>
    <message>
        <source>Start button</source>
        <translation>زر البداية</translation>
    </message>
    <message>
        <source>Item:</source>
        <translation>شيء:</translation>
    </message>
    <message>
        <source>Color1:</source>
        <translation>لون1:</translation>
    </message>
    <message>
        <source>Font:</source>
        <translation>خط:</translation>
    </message>
    <message>
        <source>Name:</source>
        <translation>إسم:</translation>
    </message>
    <message>
        <source>Text Color:</source>
        <translation>لون النص:</translation>
    </message>
    <message>
        <source>Choose</source>
        <translation>اختر</translation>
    </message>
    <message>
        <source>OK</source>
        <translation>موافق</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>إلغاء الأمر</translation>
    </message>
    <message>
        <source>Apply</source>
        <translation>تطبيق</translation>
    </message>
</context>
<context>
    <name>hDisplay</name>
    <message>
        <source>Display</source>
        <translation>شاشة</translation>
    </message>
    <message>
        <source>Desktop</source>
        <translation>سطح المكتب</translation>
    </message>
    <message>
        <source>Appearance</source>
        <translation>المضهر</translation>
    </message>
    <message>
        <source>OK</source>
        <translation>موافق</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>إلغاء الأمر</translation>
    </message>
    <message>
        <source>apply</source>
        <translation>تطبيق</translation>
    </message>
</context>
</TS>
