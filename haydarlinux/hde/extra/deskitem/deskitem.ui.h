/****************************************************************************
** ui.h extension file, included from the uic-generated form implementation.
**
** If you wish to add, delete or rename functions or slots use
** Qt Designer which will update this file, preserving your code. Create an
** init() function in place of a constructor, and a destroy() function in
** place of a destructor.
*****************************************************************************/
#include <qfile.h>
#include <qregexp.h>
#include <defaults.h>
#include <libhde.h>
#include <qmessagebox.h> 

void deskitem::okBtn_clicked()
{

}


void deskitem::ApplyBtn_clicked()
{

}


void deskitem::iconBtn_clicked()
{

}


bool deskitem::setFile(QString filename)
{
    filename = QFile::decodeName ( QCString(filename) );
    if(!QFileInfo::QFileInfo(filename).exists()){
	cout << "File: \"" << filename << "\" does not exists\n";
	QMessageBox::critical( this, "Application name",
				  "File: \"" +filename +
				  "\" does not exists\n" );
	return false;
    }
    
    hdeItem ditem = defaults::readItem(filename);
 
    if(!ditem.isDesktop)
	cout << "is not a Desktop file\n\n";
    
    QString iname(ditem.name);
    
    iname = iname.replace( QRegExp("%2f"), " " );
    nameEdit->setText(iname);

    QString svar = ditem.icon;
    svar = getIconFile(svar, "large");
    if (svar == "" || ! QFile( svar ).exists())
	iconBtn->setPixmap(QPixmap(defaults::get_cfile("images/default.xpm")));
    else
	iconBtn->setPixmap(QPixmap(svar));

   
    
    return true;
}


bool deskitem::newFunction()
{

}
