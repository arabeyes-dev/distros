/*  
*   File      : hsynedit.cpp
*   Written by: haydar@haydar.net
*   Copyright : GPL
*
*   A small syntax editor.
*/

#include "hsynedit.h"
#include <private/qrichtext_p.h>
//#include "syntaxhighliter_cpp.h"


HSynEdit::HSynEdit( QWidget *parent, const char *name ) 
    : QTextEdit( parent, name )
{
    setTextFormat(Qt::PlainText);
    //    QTextEdit::document()->setPreProcessor( new SyntaxHighlighter_CPP );

}

HSynEdit::~HSynEdit(){

}

QString HSynEdit::textLine( int line ) const
{
    //    if ( line < 0 || line >= numLines() )
    //return QString::null;
    QString str = QTextEdit::document()->paragAt( line )->string()->toString();
    //QString str = text ( line );
    str.truncate( str.length() - 1 );
    return str;
}

