HEADERS += hdisplay.h bgform.h clrform.h
SOURCES	+= main.cpp hdisplay.cpp bgform.cpp clrform.cpp

TRANSLATIONS	= ../../files/lng/hdisplay_ar.ts \
                  ../../files/lng/hdisplay_nl.ts \
                  ../../files/lng/hdisplay_es.ts \
                  ../../files/lng/hdisplay_fr.ts \
                  ../../files/lng/hdisplay_tr.ts 

IMAGES	= images/monitor.png ../../files/images/display.xpm


TEMPLATE	=app
CONFIG	+= qt warn_on release
LANGUAGE	= C++

INCLUDEPATH	+= ../../include
win32:LIBS	+= ../../hde/lib/hde.lib
unix:LIBS	+= -L../../lib -lhde
