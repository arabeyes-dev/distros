/****************************************************************************
** hadditem meta object code from reading C++ file 'hadditem.h'
**
** Created: Fri Apr 2 23:55:41 2004
**      by: The Qt MOC ($Id$)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#undef QT_NO_COMPAT
#include "../.ui/hadditem.h"
#include <qmetaobject.h>
#include <qapplication.h>

#include <private/qucomextra_p.h>
#if !defined(Q_MOC_OUTPUT_REVISION) || (Q_MOC_OUTPUT_REVISION != 26)
#error "This file was generated using the moc from 3.3.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

const char *hadditem::className() const
{
    return "hadditem";
}

QMetaObject *hadditem::metaObj = 0;
static QMetaObjectCleanUp cleanUp_hadditem( "hadditem", &hadditem::staticMetaObject );

#ifndef QT_NO_TRANSLATION
QString hadditem::tr( const char *s, const char *c )
{
    if ( qApp )
	return qApp->translate( "hadditem", s, c, QApplication::DefaultCodec );
    else
	return QString::fromLatin1( s );
}
#ifndef QT_NO_TRANSLATION_UTF8
QString hadditem::trUtf8( const char *s, const char *c )
{
    if ( qApp )
	return qApp->translate( "hadditem", s, c, QApplication::UnicodeUTF8 );
    else
	return QString::fromUtf8( s );
}
#endif // QT_NO_TRANSLATION_UTF8

#endif // QT_NO_TRANSLATION

QMetaObject* hadditem::staticMetaObject()
{
    if ( metaObj )
	return metaObj;
    QMetaObject* parentObject = QWizard::staticMetaObject();
    static const QUMethod slot_0 = {"nextClicked", 0, 0 };
    static const QUMethod slot_1 = {"browseBtn_clicked", 0, 0 };
    static const QUMethod slot_2 = {"iconBtn_clicked", 0, 0 };
    static const QUMethod slot_3 = {"languageChange", 0, 0 };
    static const QMetaData slot_tbl[] = {
	{ "nextClicked()", &slot_0, QMetaData::Public },
	{ "browseBtn_clicked()", &slot_1, QMetaData::Public },
	{ "iconBtn_clicked()", &slot_2, QMetaData::Public },
	{ "languageChange()", &slot_3, QMetaData::Protected }
    };
    metaObj = QMetaObject::new_metaobject(
	"hadditem", parentObject,
	slot_tbl, 4,
	0, 0,
#ifndef QT_NO_PROPERTIES
	0, 0,
	0, 0,
#endif // QT_NO_PROPERTIES
	0, 0 );
    cleanUp_hadditem.setMetaObject( metaObj );
    return metaObj;
}

void* hadditem::qt_cast( const char* clname )
{
    if ( !qstrcmp( clname, "hadditem" ) )
	return this;
    return QWizard::qt_cast( clname );
}

bool hadditem::qt_invoke( int _id, QUObject* _o )
{
    switch ( _id - staticMetaObject()->slotOffset() ) {
    case 0: nextClicked(); break;
    case 1: browseBtn_clicked(); break;
    case 2: iconBtn_clicked(); break;
    case 3: languageChange(); break;
    default:
	return QWizard::qt_invoke( _id, _o );
    }
    return TRUE;
}

bool hadditem::qt_emit( int _id, QUObject* _o )
{
    return QWizard::qt_emit(_id,_o);
}
#ifndef QT_NO_PROPERTIES

bool hadditem::qt_property( int id, int f, QVariant* v)
{
    return QWizard::qt_property( id, f, v);
}

bool hadditem::qt_static_property( QObject* , int , int , QVariant* ){ return FALSE; }
#endif // QT_NO_PROPERTIES
