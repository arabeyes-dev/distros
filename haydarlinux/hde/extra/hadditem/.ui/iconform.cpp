/****************************************************************************
** Form implementation generated from reading ui file 'iconform.ui'
**
** Created: Fri Apr 2 23:55:28 2004
**      by: The User Interface Compiler ($Id$)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#include "iconform.h"

#include <qvariant.h>
#include <qpushbutton.h>
#include <qlabel.h>
#include <qlineedit.h>
#include <qiconview.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>
#include <qimage.h>
#include <qpixmap.h>

#include "../iconform.ui.h"
/*
 *  Constructs a iconForm as a child of 'parent', with the
 *  name 'name' and widget flags set to 'f'.
 *
 *  The dialog will by default be modeless, unless you set 'modal' to
 *  TRUE to construct a modal dialog.
 */
iconForm::iconForm( QWidget* parent, const char* name, bool modal, WFlags fl )
    : QDialog( parent, name, modal, fl )
{
    if ( !name )
	setName( "iconForm" );

    QWidget* privateLayoutWidget = new QWidget( this, "layout23" );
    privateLayoutWidget->setGeometry( QRect( 10, 10, 290, 330 ) );
    layout23 = new QVBoxLayout( privateLayoutWidget, 11, 6, "layout23"); 

    textLabel3 = new QLabel( privateLayoutWidget, "textLabel3" );
    layout23->addWidget( textLabel3 );

    layout21 = new QHBoxLayout( 0, 0, 6, "layout21"); 

    iconDir = new QLineEdit( privateLayoutWidget, "iconDir" );
    layout21->addWidget( iconDir );

    iconDirBrowse = new QPushButton( privateLayoutWidget, "iconDirBrowse" );
    layout21->addWidget( iconDirBrowse );
    layout23->addLayout( layout21 );

    textLabel4 = new QLabel( privateLayoutWidget, "textLabel4" );
    layout23->addWidget( textLabel4 );

    iconView1 = new QIconView( privateLayoutWidget, "iconView1" );
    layout23->addWidget( iconView1 );

    layout22 = new QHBoxLayout( 0, 0, 6, "layout22"); 
    spacer9 = new QSpacerItem( 90, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout22->addItem( spacer9 );

    okBtn = new QPushButton( privateLayoutWidget, "okBtn" );
    layout22->addWidget( okBtn );

    cancelBtn = new QPushButton( privateLayoutWidget, "cancelBtn" );
    layout22->addWidget( cancelBtn );
    layout23->addLayout( layout22 );
    languageChange();
    resize( QSize(307, 346).expandedTo(minimumSizeHint()) );
    clearWState( WState_Polished );

    // signals and slots connections
    connect( cancelBtn, SIGNAL( clicked() ), this, SLOT( reject() ) );
    connect( okBtn, SIGNAL( clicked() ), this, SLOT( okBtn_clicked() ) );
    init();
}

/*
 *  Destroys the object and frees any allocated resources
 */
iconForm::~iconForm()
{
    // no need to delete child widgets, Qt does it all for us
}

/*
 *  Sets the strings of the subwidgets using the current
 *  language.
 */
void iconForm::languageChange()
{
    setCaption( tr( "Select Icon" ) );
    textLabel3->setText( tr( "Look for icons in this directory:" ) );
    iconDirBrowse->setText( tr( "Browse" ) );
    textLabel4->setText( tr( "Select an icon from the list below:" ) );
    okBtn->setText( tr( "OK" ) );
    cancelBtn->setText( tr( "Cancel" ) );
}

