/*  
*   File      : hwm_tbab.h
*   Written by: haydar@haydar.net
*   Copyright : GPL
*
*   Shows a label with current time.
*/

#include <qlabel.h>

class hwm_tbab : public QLabel
{

protected:
	virtual void drawFrame ( QPainter * p );
public:
	hwm_tbab(QWidget *parent=0, const char *name=0);
};
