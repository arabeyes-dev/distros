/* wborder.h */

#ifndef WBORDER_H
#define WBORDER_H

// define a frame that emits mouse events
#include "defs.h"
class httip: public QLabel
{
	Q_OBJECT

	public:
		httip( QWidget* parent, const QString& tip );
		~httip();
		void setTipText( const QString& tip );
		void enterTip();
		void leaveTip();
	private slots:
		void showTip();
		void hideTip();
	private:
		void positionTip();
		QTimer   showTimer;
		QTimer   hideTimer;
		QWidget* btn;
};

class wframe : public QLabel
{
	Q_OBJECT
	
signals:
	void press(QMouseEvent *);
	void press(void);
	void release(QMouseEvent *);
	void mouse_move(QMouseEvent *);
	void left_press(QMouseEvent *);
	void left_press(void);
	void right_press(void);
	void left_release(QMouseEvent *);
	void right_release(void);
	void mid_press(void);
	void mid_release(void);

protected:
	virtual void mousePressEvent(QMouseEvent *);
	virtual void mouseReleaseEvent(QMouseEvent *);
	virtual void mouseMoveEvent(QMouseEvent *);
	virtual void resizeEvent(QResizeEvent *);

public:
	QString frameName;
	wframe(QWidget *parent=0, const char *name=0);
	void set_text(QString, bool);
	void drawbg(void);
};


class wbutton : public QPushButton
{
	Q_OBJECT
	
 private:
	httip* buttonTip; 
 signals:
	void press(QMouseEvent *);
	void press(void);
	void release(QMouseEvent *);
	void mouse_move(QMouseEvent *);
	void left_press(QMouseEvent *);
	void left_press(void);
	void right_press(void);
	void left_release(QMouseEvent *);
	void right_release(void);
	void mid_press(void);
	void mid_release(void);

protected:
	virtual void mousePressEvent(QMouseEvent *);
	virtual void mouseReleaseEvent(QMouseEvent *);
	void enterEvent(QEvent *);
	void leaveEvent(QEvent *);

public:
	wbutton(QWidget *parent=0, const char *name=0);
	~wbutton();
	void setTipText(const QString& newTip);
};


// right side window border

class rsborder : public QWidget
{
	QVBoxLayout *layout;

public:
	//wframe *uframe;
	wframe *midframe;
	wframe *lframe;
	
	rsborder(QWidget *parent=0, const char *name=0);
	void drawbg(void);
};

// left side window border

class lsborder : public QWidget
{
	QVBoxLayout *layout;

public:
	//wframe *uframe;
	wframe *midframe;
	wframe *lframe;
	
	lsborder(QWidget *parent=0, const char *name=0);
	void drawbg(void);
};



// lower window border

class lborder : public QWidget
{
	QHBoxLayout *layout;

public:
	wframe *leftframe;
	wframe *midframe;
	wframe *rightframe;
	
	lborder(QWidget *parent=0, const char *name=0);
	void drawbg(void);
};

// upper window border

class uborder : public QWidget
{
    bool small;

 protected:
    QHBoxLayout* layout;
    QVBoxLayout* layout1;
    virtual void resizeEvent(QResizeEvent *);
    void checkForSize(QSize s);
public:
    bool issmall(void) {return small;};
    QHBoxLayout* layout2;
    wframe 	*luframe;
    wframe 	*ruframe;
    wframe 	*uframe;
    wframe 	*iconframe;
    wbutton 	*leftframe;
    wframe 	*midframe;
    wbutton 	*rightframe;

    wbutton 	*minbtn;
    wbutton 	*maxbtn;
    wbutton 	*helpbtn;

    uborder(bool showleft=TRUE, QWidget *parent=0, const char *name=0, bool hashelp = false);
    void set_small(void);      // set to smaller frame
    void set_max(void);      // set to smaller frame
    void set_normal(void);     // set to normal frame
    void drawbg(void);
    void noTopBorder(void);
};
#endif
