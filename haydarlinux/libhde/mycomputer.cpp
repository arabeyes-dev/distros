/****************************************************************************
 **
 **			     hmycomputer.cpp
 **			=========================
 **
 **  begin                : Wed Jun 21 2003
 **  copyright            : (C) 2003-2006 by Haydar Alkaduhimi
 **  email                : haydar@haydarnet.nl
 **
 **  A widget that has a file list and dirlist.
 **
 ****************************************************************************/

#include <qfile.h> 
#include <qfileinfo.h> 
#include <qmime.h>
//#include <qdragobject.h>
#include <qfiledialog.h> 

#include "mycomputer.h"
#include <libhde.h> 
#include <QTextStream>

#ifdef Q_WS_WIN 
#include <windows.h>
#else

#endif 

#include <iostream>
using std:: cout; using std::endl;using std::cerr;

static QPixmap load_pixmap( const QString &name )
{
  //  QString nam = ":/images/" + name;
  QPixmap pix(":/images/" + name);
  return pix;
}

/*****************************************************************************
 *
 * Class HDrive
 *
 *****************************************************************************/

HDrive::HDrive(QString path, QString type, QPixmap pix)
{
    drivePath = path;
    driveType = type;
    drivePix = pix;
}

/*****************************************************************************
 *
 * Class MyComputer
 *
 *****************************************************************************/
QList<HDrive> MyComputer::driveList;

MyComputer::MyComputer()
{
  rebuildDrives();
}

void MyComputer::rebuildDrives()
{
    driveList.clear();
    
#ifdef Q_WS_WIN 
/*
    QFileInfo *itemFileInfo = new QFileInfo;
    const QFileInfoList* roots = &QDir::drives();
    QPtrListIterator<QFileInfo> i(*roots);
    QFileInfo* fi;
    while ( (fi = *i) ) {
        ++i;

	if(GetDriveTypeA(fi->filePath().left(2).ascii()) == 2){
	  QPixmap fddpix = load_pixmap("fdd.png");
	  driveList.append( new HDrive(fi->filePath(), "Fdd", fddpix));
	}else if(GetDriveTypeA(fi->filePath().left(2).ascii()) == 3){

	  QPixmap hddpix = uic_load_pixmap_fileWidget("hdd.png");
	  driveList.append( new HDrive(fi->filePath(), "Hdd", 
				       hddpix));
	}else if(GetDriveTypeA(fi->filePath().left(2).toAscii()) > 4){
	  QPixmap cdpix = uic_load_pixmap_fileWidget("cd.png");
	  driveList.append( new HDrive(fi->filePath(), "CD", 
				       cdpix));
	}
 
//(void) new HFileListItem( this, fi, "Drive");
    }
    */
#else
    QFileInfo *fi = new QFileInfo(QDir::home().path());
    QPixmap homepix = QPixmap(get_ifile("folder_home.png", 
					       "large", "filesystems"));
    if(homepix.isNull())
      homepix = load_pixmap("home.png");
	      
    driveList.append( HDrive(fi->filePath(), "Home", homepix));

    QString fstabFile;

    if (QFile::exists(QString::fromLatin1("/etc/fstab"))) { // Linux, ...
      fstabFile = "/etc/fstab";
    } else if (QFile::exists(QString::fromLatin1("/etc/vfstab"))) {
      fstabFile = "/etc/vfstab";
    }

    if ( !fstabFile.isEmpty() ) {
      QFile f( fstabFile );
      f.open( QIODevice::ReadOnly );
      QTextStream stream( &f );
      stream.setCodec( QTextCodec::codecForName("ISO-8859-1") );
      
      while ( !stream.atEnd() ) {
	
	QString line = stream.readLine();
	line = line.simplified();
	
	if (!line.isEmpty() && line[0] == '/') { 
	  
	  QStringList lst = line.split( ' ' );
	  
	  QString it;
	  it = lst.at(1);
	  
	  if(it[0] == '/'){  
	    if( (lst.at(2)).contains( "ext" )){
	      
	      QFileInfo *fi = new QFileInfo(it);
	      QPixmap hddpix =QPixmap(get_ifile("hdd_linux_unmount.png", 
						"large", "devices"));
	      
	      if(hddpix.isNull())
		hddpix =QPixmap(get_ifile("hdd_unmount.png", 
					  "large", "devices"));
	      if(hddpix.isNull())
		hddpix = load_pixmap("hdd.png");
	      
	      driveList.append( HDrive(fi->filePath(), "LHdd", hddpix));
	    }else if((lst.at(2)).contains( "vfat" )){
	      
	      QFileInfo *fi = new QFileInfo(it);
	      QPixmap hddpix =QPixmap(get_ifile("hdd_windows_unmount.png", 
						"large", "devices"));

	      if(hddpix.isNull())
		hddpix =QPixmap(get_ifile("hdd_unmount.png", 
					  "large", "devices"));
	      if(hddpix.isNull())
		hddpix = load_pixmap("hdd.png");
	      
	      driveList.append( HDrive(fi->filePath(), "WHdd", hddpix));
      
	    }else if( (lst.at(2)).contains("iso9660") 
		      ||  (lst.at(0)).contains("cdrom") || 
		      (lst.at(1)).contains("cdrom") ){
	      
	      QFileInfo *fi = new QFileInfo(it);
	      QPixmap cdpix =QPixmap(get_ifile("cdrom_unmount.png", 
					       "large", "devices"));
	      if(cdpix.isNull())
		cdpix = load_pixmap("cd.png");
	      
	      driveList.append( HDrive(fi->filePath(), "CD", cdpix));
	      
	      
	    }else if( (lst.at(0)).contains("fd") || 
		      (lst.at(0)).contains("floppy") ){
	      
	      QFileInfo *fi = new QFileInfo(it);
	      QPixmap fddpix =QPixmap(get_ifile("3floppy_unmount.png", 
						"large", "devices"));
	      if(fddpix.isNull())
		fddpix = load_pixmap("fdd.png");
	      
	      driveList.append( HDrive(fi->filePath(), "Fdd", fddpix));

	       
	    }else{
	      
	      QFileInfo *fi = new QFileInfo(it);
	      QPixmap hddpix =QPixmap(get_ifile("hdd_unmount.png", 
						"large", "devices"));
	      if(hddpix.isNull())
		hddpix = load_pixmap("hdd.png");
	      
	      driveList.append( HDrive(fi->filePath(), "Hdd", hddpix));
	      
	    }
	      	    
	  } // if(it[0] == '/')

	} // if (!line.isEmpty() && line[0] == '/')
	
      } // TextStream
	
      
      f.close();
      
    }
#endif
}



QPixmap MyComputer::getDrivePix( HDrive *drive, QString size )
{
    QPixmap pix;
    if(size == "large")
	return drive->pixmap();

    else if(size == "mid"){
	if(drive->type() == "Fdd"){
	    pix =QPixmap(get_ifile("3floppy_unmount.png", "mid", "devices"));
	    if(pix.isNull())
		pix = load_pixmap("fdd32.png");
	}else if(drive->type() == "CD"){
	    pix =QPixmap(get_ifile("cdrom_unmount.png", "mid", "devices"));
	    if(pix.isNull())
		pix = load_pixmap("cd32.png");
	}else if(drive->type() == "Hdd"){
	    pix =QPixmap(get_ifile("hdd_unmount.png", "mid", "devices"));
	    if(pix.isNull())
		pix = load_pixmap("hdd32.png");
	}else if(drive->type() == "LHdd"){
	    pix =QPixmap(get_ifile("hdd_linux_unmount.png", "mid", "devices"));

	    if(pix.isNull())
		pix =QPixmap(get_ifile("hdd_unmount.png", "mid", "devices"));
	    if(pix.isNull())
		pix = load_pixmap("hdd32.png");

	}else if(drive->type() == "WHdd"){
	    pix =QPixmap(get_ifile("hdd_windows_unmount.png","mid","devices"));

	    if(pix.isNull())
		pix =QPixmap(get_ifile("hdd_unmount.png", "mid", "devices"));
	    if(pix.isNull())
		pix = load_pixmap("hdd32.png");

	}else if(drive->type() == "Home"){
	  pix = QPixmap(get_ifile("folder_home.png","mid","filesystems"));

	  if(pix.isNull())
	    pix = load_pixmap("home32.png");
	}
    }else{
	if(drive->type() == "Fdd"){
	    pix =QPixmap(get_ifile("3floppy_unmount.png", "mini", "devices"));
	    if(pix.isNull())
		pix = load_pixmap("fdd16.png");

	}else if(drive->type() == "CD"){
	    pix =QPixmap(get_ifile("cdrom_unmount.png", "mini", "devices"));
	    if(pix.isNull())
		pix = load_pixmap("cd16.png");

	}else if(drive->type() == "Hdd"){
	    pix =QPixmap(get_ifile("hdd_unmount.png", "mini", "devices"));
	    if(pix.isNull())
		pix = load_pixmap("hdd16.png");

	}else if(drive->type() == "LHdd"){
	    pix =QPixmap(get_ifile("hdd_linux_unmount.png", "mini","devices"));

	    if(pix.isNull())
		pix =QPixmap(get_ifile("hdd_unmount.png", "mini", "devices"));
	    if(pix.isNull())
		pix = load_pixmap("hdd16.png");

	}else if(drive->type() == "WHdd"){
	    pix=QPixmap(get_ifile("hdd_windows_unmount.png","mini","devices"));

	    if(pix.isNull())
		pix =QPixmap(get_ifile("hdd_unmount.png", "mini", "devices"));
	    if(pix.isNull())
		pix = load_pixmap("hdd16.png");

	}else if(drive->type() == "Home"){
	    pix = QPixmap(get_ifile("folder_home.png","mini","filesystems"));

	    if(pix.isNull())
		pix = load_pixmap("home16.png");
	}


    }
    return pix;
}

QPixmap MyComputer::getDirPix( QString filename, QString size )
{
    QPixmap pix;
  
    return pix;
}

QPixmap MyComputer::getFilePix( QString filename, QString size )
{
    QPixmap pix;

    return pix;
}

QPixmap MyComputer::getPix( QString filename, QString size )
{
    QPixmap pix;
    //    cout << filename << endl;
   
    
    for ( uint i = 0; i < driveList.count(); ++i ){
      HDrive *tmpDrive = (HDrive *)&driveList.at(i);
      if ( tmpDrive ){
	if( ((HDrive)driveList.at( i )).path() == filename){
	  return getDrivePix( tmpDrive, size );
	}
      }
    }

    
    return pix;
}

