/* CConfigGroup.cpp */
  
#include <hinigroup.h>


HIniGroup::HIniGroup(const QString& groupname){
    setName(groupname);
}


HIniGroup::HIniGroup(){}


void HIniGroup::setName(const QString& gname){
    // assign group name
    groupname = gname;
}


void HIniGroup::writeEntry(const QString& key, const QString& value){
    // replace or insert new key
    map.replace(key, value);
}


const QString& HIniGroup::readEntry(const QString& key, const QString& vdefault) {
    // return value, if exists or default value 
    const QString& tmp = map[key]; 
   
    return (tmp.isEmpty() ? vdefault : tmp); 
}
