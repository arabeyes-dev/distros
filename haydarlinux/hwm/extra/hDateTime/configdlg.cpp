/****************************************************************************
** Form implementation generated from reading ui file 'configdlg.ui'
**
** Created: Sun Oct 28 21:19:24 2001
**      by:  The User Interface Compiler (uic)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/
#include "configdlg.h"

#include <qvariant.h>   // first for gcc 2.7.2
#include <qmime.h>
#include <qdragobject.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>
#include <qimage.h>
#include <qpixmap.h>

#include <qgroupbox.h> 
#include <qpushbutton.h> 

#include <hcalender.h>
#include <hclock.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>

HCalender *hc;

static QPixmap uic_load_pixmap_configDlg( const QString &name )
{
    const QMimeSource *m = QMimeSourceFactory::defaultFactory()->data( name );
    if ( !m )
	return QPixmap();
    QPixmap pix;
    QImageDrag::decode( m, pix );
    return pix;
}

/* 
 *  Constructs a configDlg which is a child of 'parent', with the 
 *  name 'name' and widget flags set to 'f'.
 *
 *  The dialog will by default be modeless, unless you set 'modal' to
 *  TRUE to construct a modal dialog.
 */
configDlg::configDlg( QWidget* parent,  const char* name, bool modal, WFlags fl )
    : QDialog( parent, name, modal, fl )
{
    if ( !name )
	setName( "configDlg" );
    //    resize( 470, 240 ); 
    resize( 450, 240 ); 
    setCaption( tr( "Clock Configuration" ) );

    XClassHint ch;
    ch.res_name = "Clock Configuration";
    ch.res_class = "Clock Configuration";

    XSetClassHint(qt_xdisplay(), winId(), &ch);

    QBoxLayout * hl = new QHBoxLayout( 0 );

    QGroupBox *calGroup = new QGroupBox(this, "calGroup");
    calGroup->setTitle( trUtf8("Calender") );

    hc = new HCalender(calGroup, "HCalender");
    hc->setGeometry( QRect( 10, 20, 220, 140 ) ); 

    hl->addWidget( calGroup );


    QGroupBox *clockGroup = new QGroupBox(this, "clockGroup");
    clockGroup->setTitle( trUtf8("Clock") );

    HClock *hcl = new HClock(clockGroup, "hcl" );
    //    hcl->setGeometry( QRect( 10, 20, 150, 170 ) );
    hcl->setGeometry( QRect( 20, 40, 100, 130 ) );
    hl->addWidget( clockGroup );


    QBoxLayout * vl = new QVBoxLayout( this );
    vl->addLayout(hl);
    vl->addSpacing(10);



    // QBoxLayout *Layout1 = new QHBoxLayout( this );
    QBoxLayout *Layout1 = new QHBoxLayout( 0, 0, 6, "Layout1"); 
    QSpacerItem* spacer = new QSpacerItem( 0, 0, QSizePolicy::Expanding, QSizePolicy::Minimum );
    Layout1->addItem( spacer );

    QPushButton *okBtn = new QPushButton( this, "okBtn" );
    okBtn->setText( trUtf8( "OK" ) );
    okBtn->setDefault( TRUE );
    Layout1->addWidget( okBtn );

    QPushButton *cancelBtn = new QPushButton( this, "cancelBtn" );
    cancelBtn->setText( trUtf8( "Cancel" ) );
    Layout1->addWidget( cancelBtn );

    QPushButton *applyBtn = new QPushButton( this, "applyBtn" );
    applyBtn->setEnabled( FALSE );
    applyBtn->setText( trUtf8( "apply" ) );
    Layout1->addWidget( applyBtn );
    vl->addLayout( Layout1 );

    connect( cancelBtn, SIGNAL( clicked() ), this, SLOT( close() ) );
}

/*  
 *  Destroys the object and frees any allocated resources
 */
configDlg::~configDlg()
{
    // no need to delete child widgets, Qt does it all for us
}

