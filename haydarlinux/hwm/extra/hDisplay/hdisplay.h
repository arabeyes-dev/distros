/****************************************************************************
** Form interface generated from reading ui file 'hdisplay.ui'
**
** Created: Thu May 16 23:02:12 2002
**      by:  The User Interface Compiler (uic)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/
#ifndef HDISPLAY_H
#define HDISPLAY_H

#include <qvariant.h>
#include <qwidget.h>
#include "bgform.h"
#include "clrform.h"

#include <defaults.h>
#include <hinifile.h>
#include <libhwm.h>

class QVBoxLayout; 
class QHBoxLayout; 
class QGridLayout; 
class QPushButton;
class QTabWidget;

class hDisplay : public QWidget
{ 
    Q_OBJECT

public:
    hDisplay( QWidget* parent = 0, const char* name = 0, WFlags fl = 0 );
    ~hDisplay();

    QTabWidget* displayTab;
    bgForm* display;
    clrForm* appearance;
    QPushButton* okBtn;
    QPushButton* cancelBtn;
    QPushButton* applyBtn;


public slots:
    virtual void okBtn_clicked();
    virtual void applyBtn_clicked();
    virtual void displayTab_selected( const QString & );

protected:
    QVBoxLayout* Layout2;
    QHBoxLayout* Layout1;
};

#endif // HDISPLAY_H
