/****************************************************************************
**
**	bgform.h
**      by:  Haydar Alkaduhimi <haydar@haydar.net>
**
****************************************************************************/

#ifndef BGFORM_H
#define BGFORM_H

#include <qvariant.h>
#include <qwidget.h>
#include <qfiledialog.h> 
#include <qpushbutton.h> 


class QVBoxLayout; 
class QHBoxLayout; 
class QGridLayout; 
class QComboBox;
class QLabel;
class QListBox;
class QListBoxItem;


class apBtn : public QPushButton
{ 
    Q_OBJECT

 public:
    apBtn( QWidget* parent = 0, const char* name = 0 );
    ~apBtn(){};

 signals:
    void enabledChang(bool);

 protected:
    virtual void enabledChange ( bool oldEnabled ){ emit enabledChang(isEnabled()); };
};

class bgForm : public QWidget
{ 
    Q_OBJECT

public:
    bgForm( QWidget* parent = 0, const char* name = 0, WFlags fl = 0 );
    ~bgForm();

    QLabel* topImg;
    QLabel* bgImage;
    QLabel* TextLabel1;
    QListBox* imagesList;
    QPushButton* browseBtn;
    QLabel* TextLabel2;
    QComboBox* displayBox;
    QPushButton* OKBtn;
    QPushButton* cancelBtn;
    apBtn* applyBtn;


public slots:
    virtual void init();
    void applyClick();
    void okClick();
    void browseClick();
    void displayChanged(const QString& );
    void imageSelected(const QString& );
    void enabledChange(bool);

 signals:
    void enabledChanged( bool );

protected:
    QVBoxLayout* Layout15;
    QHBoxLayout* Layout5;
    QHBoxLayout* Layout14;
    QVBoxLayout* Layout3;
    QVBoxLayout* Layout13;
    QHBoxLayout* Layout1;

    bool initing ;

    QString bgDisplay;
    QString bgdir; 
    QString root_pix;

    void readCurentDir();
    void selCurentImg();
    void drawImage();


};

#endif // BGFORM_H
