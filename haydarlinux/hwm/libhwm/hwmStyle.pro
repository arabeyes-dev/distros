SOURCES  +=  hwmstyle.cpp
HEADERS  +=  ../include/hwmstyle.h
#DESTDIR   = $QTDIR/plugins/styles
TARGET    = hwmStyle

target.path=../lib
#plugins.path
#isEmpty(target.path):target.path=$$QT_PREFIX/plugins
INSTALLS    += target
TEMPLATE     = lib
CONFIG      += qt warn_on release plugin
CONFIG += thread

INCLUDEPATH += $(QTDIR)/tools/designer/interfaces ../include
DBFILE       = plugin.db
PROJECTNAME  = Plugin
LANGUAGE     = C++


