bool checkForGroup(QString group, bool items){
    QString mline, p1, p2,p3 = "";
    int mnuignore = groupnum;

    for ( uint i = linenr+1; i < lns.count(); ++i ){
	mline = *lns.at(i);
	mline = mline.simplifyWhiteSpace();
	QTextIStream til(&mline);

	til >> p1;  // command 
	    
	til >> p2;  // menu label

	if(items){

	    if(p1 == "End"){
		return false;
	    }

	    if(p2.isEmpty())
		continue;

	    if(p1 == "Entry"){
		p3 = til.readLine();  // command line
		if(p3.isEmpty())
		    p3 ="";

		if(p2[0] == '"'){
		    if(p2[p2.length()-1] != '"'){
			int i = p3.find('"');
			if(i == -1)
			    continue;
			
			p2 += p3.left(i);
			p2 = p2.mid(1, p2.length()-1);
			p3 = p3.right(p3.length()-i-1);
		    }	
		    else p2 = p2.mid(1, p2.length()-2);

		}
		p3 = p3.stripWhiteSpace();

		if (p2 == group){
		    linenr = i;
		    return true;
		}
		
	    }


	    //return false;
	}else{
	    if(p1.isEmpty() || *(const char *)p1 == '#')
		continue;
			
	    if(p1 == "End"){
		if(mnuignore > groupnum){
		    mnuignore--;
		}
	    }
	
	    if(p2.isEmpty())
		continue;

	    if(p1 == "Menu"){
		p3 = til.readLine();  // command line
		if(p3.isEmpty())
		    p3 ="";

		if(p2[0] == '"'){
		    if(p2[p2.length()-1] != '"'){
			int i = p3.find('"');
			if(i == -1)
			    continue;
		
			p2 += p3.left(i);
			p2 = p2.mid(1, p2.length()-1);
			p3 = p3.right(p3.length()-i-1);
		    }
		    else p2 = p2.mid(1, p2.length()-2);

		}
		p3 = p3.stripWhiteSpace();
		if(mnuignore == groupnum){
		    if (p2 == group){
			linenr = i;
			groupnum++;
			mnuignore++;
			return true;
		    }else{
			mnuignore++;
		    }
		}
	    }
	}

    }
    return false;
}


bool nextGroup(QString name){
    int endmnu = -1;

    endmnu = name.find('/');
    if(endmnu != -1){
	grouptmp = name.left(endmnu);
	rest = name.mid(endmnu+1, name.length()-1);
	groupnum++;
	return true;
    }

    return false;
}


void loadmenulist(){
    QString mline = "";

    fname = QString("menuconfig");

    if(fname.isNull())
	return;
		
    QFileInfo fi(fname);

    QFile istr(fname);
    if(! istr.open(IO_ReadOnly)){
	perror("cannot open menu file");
	return;
    }	

    lns.setAutoDelete(TRUE);
    lns.clear();
    while(! istr.atEnd()){
	istr.readLine(mline, 1024);
	lns.append(new QString(mline));
    }

    istr.close();  
}

void savelist(void){
    QString item;
    QFile istr(fname);

    if(! istr.open(IO_WriteOnly)){
	perror("cannot open menu file");
	return;
    }
   
    for ( uint i = 0; i < lns.count(); ++i )
        if ( lns.at(i) )
	    istr.writeBlock((const char *)*lns.at(i), lns.at(i)->length());

    istr.close();
}

void checkForItem(QString name, QString shortcut, QString icon){
    QString spaces = "";

    for ( int g = groupnum; g > 0; g--){
	spaces += "  ";
    }

    QString item = spaces + "Entry  \"" + name + "\"  \"" + shortcut + "\"  "
	+ icon + "\n";
    if(checkForGroup(name, true)){
	lns.remove ( lns.at(linenr));
	lns.insert ( linenr, &item ); 
    }else{
	lns.insert ( linenr, &item ); 
    }
    savelist();
}

void addMenuShorcut2(QString name, QString shortcut, QString icon){
    int groupscount = 0;
    QList<QString> lns1;
    linenr++;

    QFile istr(fname);

    if(! istr.open(IO_WriteOnly)){
	perror("cannot open menu file");
	return;
    }

    for ( int i = 0; i < linenr; ++i ){
	QString *cur = lns.at(i);
	istr.writeBlock((const char *)*cur, cur->length());
    }


    QString spaces = "";
    for ( int g = groupnum; g > 0; g--){
	spaces += "  ";
    }

    while(nextGroup(name)){
	QString tline = spaces;
	
	for ( int g = groupscount; g > 0; g--){
	    tline += "  ";
	}
	tline += "Menu \"";
	tline += grouptmp;
	tline += "\"\n";

	istr.writeBlock(tline, tline.length());
	groupscount++;

	name = rest;
    }

    

    QString item = spaces + "    Entry  \"" + name + "\"  \"" + shortcut + 
    	"\"  " + icon + "\n";

    istr.writeBlock(item, item.length());



    while(groupscount > 0){
	QString tline = spaces;
	groupscount--;
	for ( int g = groupscount; g > 0; g--){
	    tline += "  ";
	}
	tline += "End\n";
	istr.writeBlock(tline, tline.length());

    }

	
    
    for ( int i = linenr; i < lns.count(); ++i ){
	QString *cur = lns.at(i);
	istr.writeBlock((const char *)*cur, cur->length());
    }
    istr.close();
}

void menuconf::addMenuShorcut(QString name, QString shortcut, QString icon){
    grouptmp = "";
    rest     = "";
    groupnum = 0;
    linenr   = 0;

    loadmenulist();

    while(nextGroup(name)){
	if(checkForGroup(grouptmp, false)){
	    name = rest;
	}else{
	    addMenuShorcut2(name, shortcut, icon);
	    return;
	}
    }

    checkForItem(name, shortcut, icon);
}
