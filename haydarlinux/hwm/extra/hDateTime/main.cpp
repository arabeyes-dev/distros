#include <qapplication.h>
#include <defaults.h>
#include "configdlg.h"
//#include <hwm_tbab.h>


int main(int argc, char **argv)
{
    QApplication a(argc, argv);
    
    defaults::read_config();
    QTranslator translator( 0 );
    QTranslator qt_translator( 0 );
    translator.load( "hdatetime_" + defaults::lng,defaults::get_cfile("lng"));

    QString QTDIR = getenv( "QTDIR" );

    qt_translator.load( QString( "qt_%1" ).arg( defaults::lng ), 
			QTDIR + "/translations" );

    a.installTranslator( &qt_translator );
    a.installTranslator( &translator );
    

    configDlg *dc = new configDlg;
    a.setMainWidget(dc);
    dc->show();
    return a.exec();
}
