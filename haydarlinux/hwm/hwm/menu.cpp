/*
*  File      : menu.cpp
*  Written by: haydar@haydar.net
*  Copyright : GPL
*
*  Creates menu button and the XKill function
*/
#include <qdir.h>

#include "defs.h"
#include "defaults.h"
#include "qapp.h"
#include "toolbar.h"
#include "conf.h"
//#include "moc_menu.cpp"
#include <qregexp.h>
#include <qstyle.h>
#include "startmenu.h"

#include "menu.h"


#define kcursor_width 32
#define kcursor_height 32

uchar menu::kcursor_bits[] = {
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0xff, 0xff, 0x01,
   0x80, 0x00, 0x00, 0x01, 0x80, 0x00, 0x00, 0x01, 0x80, 0x08, 0x10, 0x01,
   0x80, 0x10, 0x08, 0x01, 0x80, 0x20, 0x04, 0x01, 0x80, 0x40, 0x02, 0x01,
   0x80, 0x80, 0x01, 0x01, 0x80, 0x80, 0x01, 0x01, 0x80, 0x40, 0x02, 0x01,
   0x80, 0x20, 0x04, 0x01, 0x80, 0x10, 0x08, 0x01, 0x80, 0x08, 0x10, 0x01,
   0x80, 0x00, 0x00, 0x01, 0x80, 0x00, 0x00, 0x01, 0x80, 0xff, 0xff, 0x01,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

uchar menu::kcursor_mask[] = {
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0xc0, 0xff, 0xff, 0x03, 0xc0, 0xff, 0xff, 0x03,
   0xc0, 0x00, 0x00, 0x03, 0xc0, 0x00, 0x00, 0x03, 0xc0, 0x1c, 0x38, 0x03,
   0xc0, 0x38, 0x1c, 0x03, 0xc0, 0x70, 0x0e, 0x03, 0xc0, 0xe0, 0x07, 0x03,
   0xc0, 0xc0, 0x03, 0x03, 0xc0, 0xc0, 0x03, 0x03, 0xc0, 0xe0, 0x07, 0x03,
   0xc0, 0x70, 0x0e, 0x03, 0xc0, 0x38, 0x1c, 0x03, 0xc0, 0x1c, 0x38, 0x03,
   0xc0, 0x00, 0x00, 0x03, 0xc0, 0x00, 0x00, 0x03, 0xc0, 0xff, 0xff, 0x03,
   0xc0, 0xff, 0xff, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

QString menutxt = "";
//bool ISUTF = false; 
bool menupix;


QPixmap createPix(QString path, QString type =0, int h = 22){
    QImage img(path);

    QPixmap pix0;

    if(! img.isNull()){
	QPixmap pix;
	pix.convertFromImage(img.smoothScale(h, h));
	return (pix);
    }
    
    if (type == "prg"){
	//      QImage img(qapp::get_cfile("images/prg.xpm"));
	QImage img(get_ifile("images/prg.xpm", "mini"));
      if(! img.isNull()){
	QPixmap pix;
	pix.convertFromImage(img.smoothScale(h, h));
	return (pix);
      }else
	  //	return (QPixmap( qapp::get_cfile("images/prg.xpm")));
	return (QPixmap( get_ifile("images/prg.xpm", "mini")));
    }

    if(type == "grp"){
	//      QImage img(qapp::get_cfile("images/grp.xpm"));
      QImage img(get_ifile("images/grp.xpm", "mini"));
      if(! img.isNull()){
	QPixmap pix;
	pix.convertFromImage(img.smoothScale(h, h));
	return (pix);
      }else
	  //	return (QPixmap( qapp::get_cfile("images/grp.xpm")));
	return (QPixmap( get_ifile("images/grp.xpm", "mini")));
    }

    return (pix0);
}

/*
QString getIconFile(QString icn)
{
    QString icn1 = icn;
    if(icn[0] != '/'){
	//	  icn = qapp::get_cfile(icn);
	icn1 = get_ifile(icn, "mini");
	//if(icn1.isNull())
	    //icn1 = get_ifile(icn.left(icn.length() - 3) + "png", "mini");
	//if(icn.isNull())
	//  icn = get_ifile(icn1.replace(icn1.length()-4,3,"png"), "mini");
	if(icn1.isNull())
	    icn1 = get_ifile(icn+ ".png", "mini");
	if(icn.isNull())
	    icn1 = get_ifile(icn+ ".xpm", "mini");
	//if(icn.isNull())
	//  icn1 = "/usr/share/icons/hicolor/16x16/apps/" + icn + ".png";
    }

    return icn1;
}
*/

menuMenu::menuMenu(QWidget *parent, const char *name)
  :QPopupMenu( parent, name ){
    glist.setAutoDelete(TRUE);
    glist.clear();

}

int menuMenu::insertMenu( QPixmap pix, QString name, menuMenu *menu, 
			  QString path){
  
  MENULIST *mnulst = new MENULIST;
  mnulst->menu = menu;
  mnulst->path = path;
  
  int i = insertItem(pix, name, menu);
  mnulst->id = i;
  glist.append(mnulst);

  //delete mnulst;
  return i;
}

int menuMenu::insertItm( QPixmap pix, QString name, QString path){
  MENULIST *mnulst = new MENULIST;
  mnulst->menu = NULL;
  mnulst->path = path;
  int i = insertItem(pix, name);
  mnulst->id = i;
  glist.append(mnulst);

  //delete mnulst;
  return i;

}

QString chPath(QString path){
    path = CONFDIR + QString("/") + path.mid( strlen(getenv("HOME"))+6, path.length() );
  return path;
}

menuMenu *menuMenu::checkMenu(QString path){

  path = chPath(path);
  MENULIST *m;
	
  for(m=glist.first(); m != NULL; m=glist.next())
	if(m->path == path)
	    break;

  if(m == NULL || m->menu == NULL)
    return NULL;

  return m->menu;
  
}

int menuMenu::checkItm(QString path){
  
  path = chPath(path);
  MENULIST *m;
	
  for(m=glist.first(); m != NULL; m=glist.next())
	if(m->path == path)
	    break;

  if(m == NULL)
    return -1;

  return m->id;
}


menu::menu(QWidget *parent, const char *name) : QPushButton(parent, name)
{
    if(QFileInfo(qapp::get_cfile(defaults::starticon)).isFile()){
	//cout << qapp::get_cfile(defaults::starticon) << "\n";
	//    if(defaults::styleName == "WinXPStyle"){
	//resize(100, defaults::tb_height);
	//	setFixedWidth(100);
	if((defaults::startheight < 10) && (defaults::startwidth < 10)){
	    setFixedWidth(QPixmap(qapp::get_cfile(defaults::starticon)).width());
	    menupix = true;	
	    setFixedHeight(defaults::tb_height);
	}else{
	    menupix = true;	

	    if(defaults::starticonplace != "MIDDLE")
		setFixedHeight(defaults::tb_height);
	    else if(defaults::startheight > 9)
		setFixedHeight(defaults::startheight);
	    else
		setFixedHeight(defaults::tb_height);

	    if(defaults::startwidth > 9)
		setFixedWidth(defaults::startwidth);
	    else
		setFixedWidth(QPixmap(qapp::get_cfile(defaults::starticon)).width());


	}
    }else
	setFixedHeight(defaults::tc_height);

    killop = FALSE;
    basemenu = new menuMenu(this, "basemenu");
    Q_CHECK_PTR(basemenu);
    connect(basemenu, SIGNAL(activated(int)), this, SLOT(run_cmd(int)));

    if(defaults::withxpmenu){
	defaults::xpmenu = new itemsFrame(0, "formatPart",
			Qt::WStyle_Customize|Qt::WStyle_NoBorder);
	defaults::xpmenu->setPgramsMenu(basemenu);
    }

	
    mlist.setAutoDelete(TRUE);
    mdict.setAutoDelete(TRUE);

    QString mnutxt =  defaults::get_cfile("applnk/.directory");
    if(QFileInfo::QFileInfo(mnutxt).exists()){
	hwmItem ditem = defaults::readItem(mnutxt);
	menutxt = ditem.name;

    }else{
	//setText("");
    }

    if(QString::compare(text(),"")==0 || QString::compare(text(),NULL)==0){
      QImage img(qapp::get_cfile("images/menu.xpm"));

      if(! img.isNull())  // make scaled pixmap
      {
	int wh = defaults::tc_height-4;
	QPixmap pix;
	pix.convertFromImage(img.smoothScale(wh, wh));
	//setPixmap(pix);
	mainpix = pix;
      }
    }else{
      QImage img(qapp::get_cfile("images/menu.xpm"));

      if(! img.isNull())  // make scaled pixmap
      {
	int wh = defaults::tc_height-4;
	QPixmap pix;
	pix.convertFromImage(img.smoothScale(wh, wh));
	mainpix = pix;
      }

    }

    //    if(defaults::styleName == "WinXPStyle"){
    if(menupix){
    //setFixedWidth(QPixmap(qapp::get_cfile("images/xpmenu.xpm")).width());
	//	setFixedWidth(100);

    }else if(menutxt != ""){
	QFontMetrics qf(font());
	//	setFixedWidth(qf.width(menutxt) + pixmap()->width() + 9);
	setFixedWidth(qf.width(menutxt) + height() + 9);
    }else
	setFixedWidth(height());
 
    //    if(defaults::styleName == "WinXPStyle")
    //drawbg();

    setDefault ( false );
}

void menu::resizeEvent(QResizeEvent *){

}

void menu::paintEvent( QPaintEvent * ){
    //    if(defaults::styleName == "WinXPStyle"){
    if(menupix){

	QPainter *p = new QPainter(this);
	if(QFileInfo(qapp::get_cfile(defaults::toolbar_bg)).isFile()){
	    //	    QPixmap qp(width(), height());
	    //QPainter painter(&qp);

	    QImage img(qapp::get_cfile(defaults::toolbar_bg));
	    if(! img.isNull())  // make scaled pixmap
	    {
		QPixmap pix;
		pix.convertFromImage(img.smoothScale(width(), height() ));
		//painter.drawPixmap(0,0, pix);

		//painter.end();
		//setBackgroundPixmap(qp);
		p->drawPixmap(0,0, pix);
	    }
	
	}else{
	    //	QPainter *p = new QPainter(this);
	
	    int h = (height() - 2)/2; // (height() - 4)/2;
	    QColor firstColor(palette().active().background());
	    p->setPen(firstColor.dark(100+h*2));
	    p->drawLine(0,0 , width(), 0); 

	    p->setPen(firstColor.light(100+h*2));
 
	    for ( int i = 1; i < 3; ++i ) {
		p->setPen(firstColor.light(100+(h*2)-i));
		p->drawLine(0,i , width(), i); 
	    }

	    for ( int i = 0; i < h-1; ++i ) {
		p->setPen(firstColor.dark(100 + ((h*2)-3)-(i*2)));
		p->drawLine(0,i+4 , width(), i+4); 
	    }


	    int j = 0;

	    for ( int i = height()-h; i < height()-1 ; ++i ) {
		j++;
		p->setPen(firstColor.light(100 +(j*2)));
		p->drawLine(0, i , width(), i); 
	    }


	    p->setPen(firstColor.dark(100+(h*2)));
 
	    for (int i = height()-2; i < height(); ++i ) {
		p->drawLine(0,i , width(), i); 
	    }
	}

	QImage img(qapp::get_cfile(defaults::starticon));
	if(isDown() && 
	   (QFileInfo(qapp::get_cfile(defaults::startdown)).isFile()))
	    img = QImage(qapp::get_cfile(defaults::startdown));
	else if(hasMouse() && 
	   (QFileInfo(qapp::get_cfile(defaults::starticonmo)).isFile()))
	    img = QImage(qapp::get_cfile(defaults::starticonmo));

	if(! img.isNull())  // make scaled pixmap
	{


	    QPixmap pix;
	    //	if((defaults::startheight < 10) 
	    if( ((defaults::starticonplace == "TOP") || 
		 (defaults::starticonplace == "BUTTOM")) && 
		(defaults::startheight > 9) )
		if(QApplication::reverseLayout())
		    pix.convertFromImage(img.mirror(TRUE,FALSE).smoothScale(width(), defaults::startheight));
		else
		    pix.convertFromImage(img.smoothScale(width(), 
						defaults::startheight));
	    else
		if(QApplication::reverseLayout())
		    pix.convertFromImage(img.mirror(TRUE,FALSE).smoothScale(width(), height()));
		else
		    pix.convertFromImage(img.smoothScale(width(), height()));

	    if( (defaults::starticonplace == "TOP") 
		&& (defaults::startheight > 9) )
		p->drawPixmap(0,0, pix);
	    else if ( (defaults::starticonplace == "BUTTOM") 
		      && (defaults::startheight > 9) )	
		p->drawPixmap(0, height()-defaults::startheight, pix);
	    else
		p->drawPixmap(0,0, pix);
	}
	//
	//
	//
	font().setBold(true);
	//	font().setBold(true);
	setPaletteForegroundColor(defaults::startButton_fg);
	//
	//
	//
	//setPixmap(QPixmap(qapp::get_cfile("images/xpmenu.xpm")));
	//setBackgroundPixmap(QPixmap(qapp::get_cfile("images/xpmenu.xpm")));
	int defheight = 0;
	defheight = height() - defaults::startheight;
	//	p->drawPixmap(3,3, mainpix);
	    if( (defaults::starticonplace == "TOP") 
		&& (defaults::startheight > 9) )
		p->drawText(defaults::startheight+4,(defaults::startheight/2)
			    + (QFontMetrics(font()).height()/2)-3, menutxt);
	    else if( (defaults::starticonplace == "BUTTOM") 
		     && (defaults::startheight > 9) )
		p->drawText(defaults::startheight+4,(defaults::startheight/2)
			    + defheight
			    + (QFontMetrics(font()).height()/2)-3, menutxt);
	    else
		p->drawText(height()+4, (height()/2) + 
			    (QFontMetrics(font()).height()/2)-3, menutxt);
	p->end();

    }else{

	QPainter *p = new QPainter(this);
	
	drawButton( p );
	if(QApplication::reverseLayout()){
	    p->drawPixmap( 9+QFontMetrics(font()).width(menutxt),3, mainpix);
	    p->drawText( 3, (height()/2) + (QFontMetrics(font()).height()/2)
			 -3 , menutxt);
	}else{
	    p->drawPixmap(3,3, mainpix);
	    p->drawText(height()+4, (height()/2) + 
			(QFontMetrics(font()).height()/2)-3, menutxt);
	}

	p->end();
    }
}
void menu::addMenu2(menuMenu *cm, QString path, QString fname){
  if(fname == "." | fname == ".." | fname == "CVS")
    return;

  QString svar = fname;
  QString grp = "";

  if(cm->checkMenu(path + "/" + fname) != NULL){

    QString fullPath = path + "/" + fname + "/.directory";
    if(QFileInfo::QFileInfo(fullPath).exists()){
	hwmItem ditem = defaults::readItem(fullPath);
	svar = ditem.name;
	grp = ditem.icon;

	int ind = cm->checkItm(path + "/" + fname);
	grp = getIconFile(grp, "mini", "apps");
	cm->changeItem ( ind, createPix(grp, "grp", 22), svar);

    }else{
      //    cout << "could not find: " << fullPath << endl;
    }


    return recurse2(cm->checkMenu(path + "/" + fname), path, fname);
  }
  
  QString fullPath = path + "/" + fname + "/.directory";
  if(QFileInfo::QFileInfo(fullPath).exists()){
	hwmItem ditem = defaults::readItem(fullPath);

	svar = ditem.name;
	grp = getIconFile(ditem.icon, "mini", "apps");

  }else{
    //    cout << "could not find: " << fullPath << endl;
  }
  

  menuMenu *nm = new menuMenu(this, "menu_submenu");
  Q_CHECK_PTR(nm);
  connect(nm, SIGNAL(activated(int)), this, SLOT(run_cmd(int)));
  mlist.append(nm);
  cm->insertMenu( createPix(grp, "grp", 22), svar, nm, path + "/" + fname);
  recurse2(nm, path, fname);

}

void menu::addLink2(menuMenu *cm, QString path, QString fname){

  QString svar = fname;
  QString prg, exec= "";

  QString fullPath = path + "/" + fname ;
  if(QFileInfo::QFileInfo(fullPath).exists()){
      hwmItem ditem = defaults::readItem(fullPath);
      svar = ditem.name;
      prg = ditem.icon;

      prg = getIconFile(prg, "mini", "apps");
      //      exec = ditem.exec;
      exec = fullPath;

  }


}

void menu::recurse2(menuMenu *cm, QString path, QString fname){
  QDir di(path+ "/" + fname);
  if(di.exists()){
    di.setFilter( QDir::Dirs | QDir::Files);
    di.setSorting(QDir::Name | QDir::DirsFirst );
    const QFileInfoList *list = di.entryInfoList();
    QFileInfoListIterator it( *list );
    QFileInfo *fi;

    while ( (fi=it.current()) ) {
	if(fi->isDir()){
	    addMenu2(cm, di.path(), fi->fileName().data());


      }else if(fi->isFile() && (fi->extension( FALSE ) == "hwmlnk" || 
			       fi->extension( FALSE ) == "desktop" ))
	addLink2(cm, di.path(), fi->fileName().data());
      ++it;
    }
  }

}

void menu::addMenu(menuMenu *cm, QString path, QString fname){
  if(fname == "." | fname == ".." | fname == "CVS")
    return;

  QString svar = fname;
  QString grp = "";

  QString fullPath = path + "/" + fname + "/.directory";
  if(QFileInfo::QFileInfo(fullPath).exists()){
      hwmItem ditem = defaults::readItem(fullPath);
      svar = ditem.name;
      grp = ditem.icon;
      grp = getIconFile(grp, "mini", "apps");
  }else{
return;
    //    cout << "could not find: " << fullPath << endl;
  }

  menuMenu *nm = new menuMenu(this, "menu_submenu");
  Q_CHECK_PTR(nm);
  connect(nm, SIGNAL(activated(int)), this, SLOT(run_cmd(int)));
  mlist.append(nm);
  cm->insertMenu( createPix(grp, "grp", 22), svar, nm, path + "/" + fname);
  recurse(nm, path, fname);
}


void menu::addLink(menuMenu *cm, QString path, QString fname){
  QString svar = fname;
  QString prg, exec= "";

  QString fullPath = path + "/" + fname ;
  if(QFileInfo::QFileInfo(fullPath).exists()){
      hwmItem ditem = defaults::readItem(fullPath);
      svar = ditem.name;
      prg = ditem.icon;
      prg = getIconFile(prg, "mini", "apps");

      //      exec = ditem.exec;
      exec = fullPath;
    mdict.insert(cm->insertItm(createPix(prg, "prg", 22), svar, fullPath),  
		 new QString(exec));
  }


}

void menu::readUpper(void){
  bool hasUpperMenu = false;
  QDir d( CONFDIR + QString("/applnk/Upper"));
  if(d.exists()){
    d.setFilter( QDir::Dirs | QDir::Files);
    d.setSorting(QDir::Name | QDir::DirsFirst );
    const QFileInfoList *list = d.entryInfoList();
    QFileInfoListIterator it( *list );
    QFileInfo *fi;

    while ( (fi=it.current()) ) {
	if(fi->isDir()){
	    addMenu(basemenu, d.path(), fi->fileName().data());


      }else if(fi->isFile() && (fi->extension( FALSE ) == "hwmlnk" || 
			       fi->extension( FALSE ) == "desktop" )){
	addLink(basemenu, d.path(), fi->fileName().data());
	hasUpperMenu = true;
      }
      ++it;
    }
  }

  
  QString fname(getenv("HOME"));
  QDir di(fname + "/.hwm/applnk/Upper");
  if(di.exists()){
    di.setFilter( QDir::Dirs | QDir::Files);
    di.setSorting(QDir::Name | QDir::DirsFirst );
    const QFileInfoList *list = di.entryInfoList();
    QFileInfoListIterator it( *list );
    QFileInfo *fi;

    while ( (fi=it.current()) ) {
	if(fi->isDir()){
	    addMenu2(basemenu, di.path(), fi->fileName().data());

      }else if(fi->isFile() && (fi->extension( FALSE ) == "hwmlnk" || 
			       fi->extension( FALSE ) == "desktop" )){
	addLink2(basemenu, di.path(), fi->fileName().data());
	hasUpperMenu = true;
      }
      ++it;
    }
  }

  if(hasUpperMenu)
    basemenu->insertSeparator();
}

void menu::readLower(void){
    QDir d( CONFDIR + QString("/applnk/Lower"));
    if(defaults::withxpmenu)
	  d.setPath( CONFDIR + QString("/applnk/Lower/Programs"));
    else
	d.setPath( CONFDIR + QString("/applnk/Lower"));
  if(d.exists()){
    d.setFilter( QDir::Dirs | QDir::Files);
    d.setSorting(QDir::Name | QDir::DirsFirst );
    const QFileInfoList *list = d.entryInfoList();
    QFileInfoListIterator it( *list );
    QFileInfo *fi;

    while ( (fi=it.current()) ) {
	if(fi->isDir()){
	    addMenu(basemenu, d.path(), fi->fileName().data());

	}else if(fi->isFile() && (fi->extension( FALSE ) == "hwmlnk" || 
			      fi->extension( FALSE ) == "desktop" ))
	addLink(basemenu, d.path(), fi->fileName().data());
      ++it;
    }
  }

  
  QString fname(getenv("HOME"));
  QDir di(fname + "/.hwm/applnk/Lower");
  if(defaults::withxpmenu)
      di.setPath(fname + "/.hwm/applnk/Lower/Programs");
  else
      di.setPath(fname + "/.hwm/applnk/Lower");

  if(di.exists()){
    di.setFilter( QDir::Dirs | QDir::Files);
    di.setSorting(QDir::Name | QDir::DirsFirst );
    const QFileInfoList *list = di.entryInfoList();
    QFileInfoListIterator it( *list );
    QFileInfo *fi;

    while ( (fi=it.current()) ) {
	if(fi->isDir()){
	    addMenu2(basemenu, di.path(), fi->fileName().data());
      }else if(fi->isFile() && (fi->extension( FALSE ) == "hwmlnk" || 
			       fi->extension( FALSE ) == "desktop" ))
	addLink2(basemenu, di.path(), fi->fileName().data());
      ++it;
    }
  }

}

void menu::readmenu(void)
{
  basemenu->clear();
  mdict.clear();
  mlist.clear();

  readUpper();
  readLower();

  if(defaults::withxpmenu){
      defaults::xpmenu->exitItem->connectItem( this, SLOT(winkill_all()));
      defaults::xpmenu->killItem->connectItem( this, SLOT(winkill()));
  }else{
    basemenu->insertSeparator();
    
    mdict.insert(basemenu->insertItem(tr("Kill window")), 
		 new QString("KILL WINDOW"));
    mdict.insert(basemenu->insertItem(tr("End Session")),new QString("QUIT"));
  }
}

void menu::winkill(void)
{
    if(QFileInfo(get_cfile("images/xkill.png")).exists()){
	grabMouse(QCursor(QPixmap(get_cfile("images/xkill.png"))));
    }else
	grabMouse(QCursor(QBitmap(kcursor_width, kcursor_height, kcursor_bits, 
		TRUE),QBitmap(kcursor_width, kcursor_height, kcursor_mask, 
		TRUE)));
	
    killop = TRUE;
}

void menu::recurse(menuMenu *cm, QString path, QString fname){

    QDir d(path+ "/" + fname);
    d.setFilter( QDir::Dirs | QDir::Files);
    d.setSorting(QDir::Name | QDir::DirsFirst );
    const QFileInfoList *list = d.entryInfoList();
    QFileInfoListIterator it( *list );
    QFileInfo *fi;

    while ( (fi=it.current()) ) {
	if(fi->isDir()){
	    addMenu(cm, d.path(), fi->fileName().data());

      }else if(fi->isFile() && (fi->extension( FALSE ) == "hwmlnk" || 
			      fi->extension( FALSE ) == "desktop" ))
	addLink(cm, d.path(), fi->fileName().data());
      ++it;
    }


}	

void menu::run_cmd(int id)  // execute menu item
{
	QString cmd = *mdict.find(id);

	if(cmd == "KILL WINDOW")
			winkill();
	else if(cmd == "QUIT") {
			winkill_all();
			qApp->exit();
	} else
			goto LBL_UNMATCHED_ID;

	return;

LBL_UNMATCHED_ID:

	execCmd(cmd);  // fixed
	/*
	execl("/bin/sh", "sh", "-c", (const char*)cmd, NULL);
	perror("cannot run /bin/sh");
	exit(1);
	*/
}

void menu::start_popup(void)
{	
	Window rw,cw;
	int rx,ry,wx,wy;
	unsigned mr;


	if(qapp::menu_open)
		return;
	
	xwindow *focusclient = qapp::focusclient;
	qapp::menu_open = TRUE;
	
	setDown(TRUE);

	if(! defaults::show_menu || qapp::smode)  // show at mouse position
	{
		XQueryPointer(qt_xdisplay(), qt_xrootwin(), &rw, &cw, &rx, &ry, &wx, &wy, &mr);

		if(defaults::withxpmenu){
		    defaults::xpmenu->show();
		    defaults::xpmenu->move(QPoint(rx, ry));
		    //xpmen->grabMouse();
		}else
		    basemenu->exec(QPoint(rx, ry));
	}
	else
	{
		if(! defaults::toolbar_top)   // menu above button
		{
		    if(defaults::withxpmenu){
			if(QApplication::reverseLayout()){
			    QPoint p = mapToGlobal(QPoint(0, 0));
			    QSize s(defaults::xpmenu->size());
			    p.setY(p.y()-s.height()+2);
			    p.setX(p.x()-s.width()+width()+2);
			    defaults::xpmenu->show();
			    defaults::xpmenu->move(p);
			}else{
			    QPoint p = mapToGlobal(QPoint(0, 0));
			    QSize s(defaults::xpmenu->size());
			    p.setY(p.y()-s.height()+2);
			    //p.setX(p.x()-s.width()+width()+2);
			    defaults::xpmenu->show();
			    defaults::xpmenu->move(p);

			}
			//xpmen->grabMouse();
		    }else{
			QPoint p = mapToGlobal(QPoint(0, 0));
			QSize s(basemenu->sizeHint());
			//p.setY(p.y()-s.height());
			p.setY(p.y()-s.height()+2);
			basemenu->exec(p);
		    }
		}
		else
		    if(defaults::withxpmenu){
			defaults::xpmenu->show();
			defaults::xpmenu->move(mapToGlobal(QPoint(0, height())));
			//xpmen->grabMouse();
		    }else
			basemenu->exec(mapToGlobal(QPoint(0, height())));
	}
	qapp::menu_open = FALSE;
	setDown(FALSE);
	
	if(defaults::withxpmenu){
	    XRaiseWindow(qt_xdisplay(), defaults::xpmenu->winId());

	}else
	    XSetInputFocus(qt_xdisplay(), qt_xrootwin(), RevertToPointerRoot, CurrentTime);

	if(focusclient != NULL && clients.find(focusclient) != -1)
		focusclient->setchildfocus(CurrentTime);
}

void menu::mousePressEvent(QMouseEvent *event)
{
	if(killop)  //  window kill operation
	{
		xwindow *client;
		
		killop = FALSE;
		releaseMouse();
		
		if(event->button() != QMouseEvent::LeftButton)
			return;
		
		if((client = (xwindow *)QApplication::widgetAt(event->globalPos())) == NULL)
			return;
		
		XKillClient(qt_xdisplay(), client->client_id());
		XSync(qt_xdisplay(), FALSE);
		
		return;
	}
	start_popup();
}


void menu::winkill_all()
{
	xwindow *client;

	tb_pb->remove_all();
	for(client = clients.first(); client != NULL; client = clients.next()) {
		client->wdestroy();
	}
	clients.clear();
	XSync(qt_xdisplay(), FALSE);
	qApp->exit();
}
