/*  
*   File      : dclock.cpp
*   Written by: haydar@haydar.net
*   Copyright : GPL
*
*   Shows a label with current time.
*/

#include "dclock.h"
#include <qtooltip.h>
#include <qtranslator.h>


defaults defs;

dclock::dclock(QWidget *parent, const char *name, WFlags flag) : QLabel(parent, name, flag)
{

	QTimer *timer = new QTimer(this);
	connect(timer, SIGNAL(timeout()), SLOT(timeout()));
	setAutoResize(TRUE);
	QTime newtime = QTime::currentTime();
	settime(newtime);
	setFixedWidth(width());
	setAutoResize(FALSE);
	timer->start(5000);  // signal every 5 seconds

	setBackgroundMode(Qt::X11ParentRelative); 

	//	makeTray(winId());

}


void dclock::mouseDoubleClickEvent( QMouseEvent * e ){
  /*
    pid_t pid;
    if((pid = fork()) == -1)
    {
	perror("fork()");
	return;
    }	
    if(pid)
	return;
  */
    execCmd("hDateTime");
    return;

}


//void dclock::drawContents ( QPainter * p ){
void dclock::paintEvent ( QPaintEvent * ){
    QPainter * p = new QPainter(this);

    QRect cr = contentsRect();

    p->setFont(font());

    p->setPen(Qt::white);
    cr.moveBy(1, 1);
    p->drawText( cr, Qt::AlignHCenter | Qt::AlignVCenter, text());
    p->setPen(Qt::black);
    cr.moveBy(-1, -1);
    p->drawText( cr, Qt::AlignHCenter | Qt::AlignVCenter, text());

    p->end();
}

void dclock::timeout(void)
{
	QTime newtime = QTime::currentTime();
	if(newtime.minute() != time.minute()){
	    settime(time = newtime);
	    //QToolTip::add( this,  Qt::LocalDate);
	    QToolTip::add( this,  tr(QDateTime::currentDateTime().toString(),
				     "") );

	}
}

void dclock::settime(QTime &tm)
{
    setText(tm.toString().left(5));
}


