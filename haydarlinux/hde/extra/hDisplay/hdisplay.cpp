/****************************************************************************
** Form implementation generated from reading ui file 'hdisplay.ui'
**
** Created: Thu May 16 23:02:15 2002
**      by:  The User Interface Compiler (uic)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/
#include "hdisplay.h"

#include <qvariant.h>
#include <qpushbutton.h>
#include <qtabwidget.h>
#include <qmime.h>
#include <qdragobject.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>
#include <qimage.h>
#include <qpixmap.h>

static QPixmap uic_load_pixmap_hDisplay( const QString &name )
{
    const QMimeSource *m = QMimeSourceFactory::defaultFactory()->data( name );
    if ( !m )
	return QPixmap();
    QPixmap pix;
    QImageDrag::decode( m, pix );
    return pix;
}
/* 
 *  Constructs a hDisplay which is a child of 'parent', with the 
 *  name 'name' and widget flags set to 'f'.
 */
hDisplay::hDisplay( QWidget* parent,  const char* name, WFlags fl )
    : QWidget( parent, name, fl )
{
    if ( !name )
	setName( "hDisplay" );
    resize( 400, 460 ); 
    setCaption( trUtf8( "Display" ) );
    setIcon( uic_load_pixmap_hDisplay( "display.xpm" ) );

    QWidget* privateLayoutWidget = new QWidget( this, "Layout2" );
    privateLayoutWidget->setGeometry( QRect( 10, 10, 380, 450 ) ); 
    Layout2 = new QVBoxLayout( privateLayoutWidget, 0, 6, "Layout2"); 

    displayTab = new QTabWidget( privateLayoutWidget, "displayTab" );

    display = new bgForm( displayTab, "display" );
    displayTab->insertTab( display, trUtf8( "Desktop" ) );

    appearance = new clrForm( displayTab, "appearance" );
    displayTab->insertTab( appearance, trUtf8( "Appearance" ) );
    Layout2->addWidget( displayTab );

    Layout1 = new QHBoxLayout( 0, 0, 6, "Layout1"); 
    QSpacerItem* spacer = new QSpacerItem( 0, 0, QSizePolicy::Expanding, QSizePolicy::Minimum );
    Layout1->addItem( spacer );

    okBtn = new QPushButton( privateLayoutWidget, "okBtn" );
    okBtn->setText( trUtf8( "OK" ) );
    okBtn->setDefault( TRUE );
    Layout1->addWidget( okBtn );

    cancelBtn = new QPushButton( privateLayoutWidget, "cancelBtn" );
    cancelBtn->setText( trUtf8( "Cancel" ) );
    Layout1->addWidget( cancelBtn );

    applyBtn = new QPushButton( privateLayoutWidget, "applyBtn" );
    applyBtn->setEnabled( FALSE );
    applyBtn->setText( trUtf8( "apply" ) );
    Layout1->addWidget( applyBtn );
    Layout2->addLayout( Layout1 );

    // signals and slots connections
    connect( okBtn, SIGNAL( clicked() ), this, SLOT( okBtn_clicked() ) );
    connect( applyBtn, SIGNAL( clicked() ), this, SLOT( applyBtn_clicked() ) );
    connect( cancelBtn, SIGNAL( clicked() ), this, SLOT( close() ) );
    connect( displayTab, SIGNAL( selected(const QString&) ), this, SLOT( displayTab_selected(const QString&) ) );


    display->OKBtn->hide();
    display->cancelBtn->hide();
    display->applyBtn->hide();

    appearance->okBtn->hide();
    appearance->cancelBtn->hide();
    appearance->applyBtn->hide();

    connect( display, SIGNAL(enabledChanged( bool ) ), applyBtn, 
	     SLOT( setEnabled( bool ) ) );
    connect( appearance, SIGNAL(enabledChanged( bool ) ), applyBtn, 
	     SLOT( setEnabled( bool ) ) );
}

/*  
 *  Destroys the object and frees any allocated resources
 */
hDisplay::~hDisplay()
{
    // no need to delete child widgets, Qt does it all for us
}

void hDisplay::okBtn_clicked()
{
    if( applyBtn->isEnabled() )
	applyBtn_clicked();
    close();
}


void hDisplay::applyBtn_clicked()
{

    display->applyClick();
    appearance->applyBtn_clicked();

    applyBtn->setEnabled(false);

    pid_t pid;
    if((pid = fork()) == -1)
    {
	perror("fork()");
	return;
    }	
    if(pid)
	return;

    execCmd("killall -HUP hde");
    return;
}


void hDisplay::displayTab_selected( const QString & )
{

}
