/****************************************************************************
**
**
** main.cpp for HDE Notepad
** Created by Haydar Alkaduhimi <haydar@haydar.net>
** Copyright (C) 2002 - 2003 Haydar Net.  All rights reserved.
**
**
*****************************************************************************/

#include <qapplication.h>
#include "hnotepad.h"
#include "../include/conf.h"
#include <hde/defaults.h>


int main( int argc, char **argv )
{
    QApplication a( argc, argv );

    defaults::read_config();
    QString QTDIR = getenv( "QTDIR" );

    QTranslator *qt_translator = new QTranslator( &a );
    QTranslator *translator = new QTranslator( &a );

    translator->load( QString("hnotepad_%1").arg( defaults::lng ) , 
		      defaults::get_cfile("lng"));

    qt_translator->load( QString( "qt_%1" ).arg( defaults::lng ), 
			 QTDIR + "/translations" );

    a.installTranslator( translator );
    a.installTranslator( qt_translator );

    
    bool isSmall =  qApp->desktop()->size().width() < 450 
		  || qApp->desktop()->size().height() < 450;
    

    Editor *e = new Editor;
    for(int i=1; i < argc; i++){
	if(strcmp(argv[i],"--version")== 0||strcmp(argv[i],"-v") == 0){
	    cerr << "HDE Notepad version: " << DVERSION << endl;
	    return(0);
	}
	
	if(strcmp(argv[i], "--font") == 0 && argc > i+1)
	    e->set_font(argv[++i]);
	

	if(strcmp(argv[i], "--enc") == 0 && argc > i+1)
	    e->enc = argv[++i];

	else{
	    if(!e->enc.isNull()){
		e->load( argv[i], e->testCode(e->enc)+5 );
	    }else{
		e->load( argv[i] );
		e->setFilename(argv[i]);
	
	    }
	}
    }

    if ( isSmall ) {
	e->showMaximized();
    } else {
	e->resize( 400, 400 );
	e->show();
    }

    a.connect( &a, SIGNAL(lastWindowClosed()), &a, SLOT(quit()) );
    return a.exec();
}
