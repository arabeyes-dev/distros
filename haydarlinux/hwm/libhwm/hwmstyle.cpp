/**********************************************************************
 **
 **   hwmStyle
 **     
 **   Crated by: Haydar Alkaduhimi <haydar@haydar.net>
 **
 **********************************************************************/

//#include <moc_hwmstyle.cpp>
#include <hwmstyle.h>

#include <qstyleplugin.h>
#include <qstylefactory.h>
#include <qcommonstyle.h>

#include <qapplication.h>
#include <qdrawutil.h>
#include <qpainter.h>
#include <qpalette.h>
#include <qpixmap.h>

#include <qcombobox.h>
#include <qpushbutton.h>
#include <qscrollbar.h>
#include <qtoolbar.h>
#include <qtoolbutton.h>
#include <qtabbar.h>
#include <qmenubar.h>
#include <qpopupmenu.h>
#include <qwidget.h>
#include <qlabel.h>
#include <qrangecontrol.h>
#include <qcleanuphandler.h>

#include <qslider.h>

#include <limits.h>

static const int windowsItemFrame		=  2; // menu item frame width
static const int windowsSepHeight		=  2; // separator item height
static const int windowsItemHMargin		=  3; // menu item hor text margin
static const int windowsItemVMargin		=  2; // menu item ver text margin
static const int windowsArrowHMargin		=  6; // arrow horizontal margin
static const int windowsTabSpacing		= 12; // space between text and tab
static const int windowsCheckMarkHMargin	=  2; // horiz. margins of check mark
static const int windowsRightBorder		= 12; // right border on windows
static const int windowsCheckMarkWidth		= 12; // checkmarks width on windows
static bool use2000style = TRUE;


class hwmStylePlugin : public QStylePlugin
{
 public:
    hwmStylePlugin() {}
    ~hwmStylePlugin() {}
    
    QStringList keys() const 
    { 
	return QStringList() << "hwmStyle"; 
    }
    
    QStyle* create( const QString& key ) 
    { 
	if ( key == "hwmstyle" ) 
	    return new hwmStyle;
	return 0;
    }
};

Q_EXPORT_PLUGIN( hwmStylePlugin )




hwmStyle::hwmStyle(void) : QCommonStyle()
{
    winstyle = QStyleFactory::create("Windows");
    if(!winstyle) {
	// We don't have the Windows style, neither builtin nor as a plugin.
	// Use any style rather than crashing.
	winstyle = QStyleFactory::create(*(QStyleFactory::keys().begin()));
    }
    hwmstyle = QPixmap::defaultDepth() > 8;
}

hwmStyle::~hwmStyle()
{
}

void hwmStyle::polish(QWidget* widget)
{
    //QWindowsStyle::polish(widget);
    winstyle->polish(widget);
    if (widget->inherits("QPushButton")) {
	widget->installEventFilter(this);
    }

    else if (widget->inherits("QMenuBar")) {
	widget->setBackgroundMode(QWidget::NoBackground);
    } 

}


void hwmStyle::unPolish(QWidget* widget)
{
    if (widget->inherits("QPushButton")) {
	widget->removeEventFilter(this);
    }

    else if (widget->inherits("QMenuBar")) {
	widget->setBackgroundMode(QWidget::PaletteBackground);
    } 

    //QWindowsStyle::unPolish(widget);
    winstyle->unPolish(widget);
}


bool hwmStyle::eventFilter( QObject *object, QEvent *event )
{
    if ( object-inherits("QPushButton") )
    {
	QPushButton* button = (QPushButton*) object;
		
	if ( (event->type() == QEvent::Enter) &&
	     (button->isEnabled()) ) {
	    hoverWidget = button;
	    button->repaint( false );
	} 
	else if ( (event->type() == QEvent::Leave) &&
		  (object == hoverWidget)  ) {
	    hoverWidget = 0L;
	    button->repaint( false );
	}
    }
	
    return false;
}

void hwmStyle::polishPopupMenu(QPopupMenu *p)
{
	if ( !p->testWState( WState_Polished ) )
		p->setCheckable( true );
}

void hwmStyle::drawGradient( QPainter* p, 
			     const QRect& r,
			     QColor c, 
			     bool horizontal, 
			     bool isoff,
			     bool isMenuBar) const
{
    int hi=0;
    if(isoff){
	if(!horizontal){
	    //if(r.height() < 48)
	    hi = (r.height())/2;
	    //else
	    //	hi = 24;

	    int i=0;
	    int s = 0;
	    int a = 17;

	    if(hi < 17)
		a = hi;
	    
	    p->setPen(c);
	    
	    for ( i = r.y(); i < r.y()+r.height()-1; ++i ) {
		p->drawLine(r.x(), i, r.x()+r.width()-1, i);
	    }	

	    if(isMenuBar){
		for ( i = r.y()-2; i < hi+2; ++i ) {
		    s = (hi-i) * a / hi;
		    //p->setPen(c.light(100 + ((hi+2)*2)-(i*2)));
		    p->setPen(c.light(100 +4 + (s*2)));
		    p->drawLine(r.x(),i , r.x()+r.width()-1, i);
		}
	    }else{
		for ( i = r.y(); i < hi; ++i ) {
		    s = (hi-i) * a / hi;

		    //p->setPen(c.light(100 + (hi*2)-(i*2)));
		    p->setPen(c.light(100 + (s*2)));
		    p->drawLine(r.x(),i , r.x()+r.width()-1, i);
		}
	    }

	    int j = 0;
	    for ( i = r.y()+r.height()-hi+1; i < r.y()+r.height() ; ++i ) {
		j++;
		s = j * a / hi;
		//p->setPen(c.dark(100 +(j*2)));
		p->setPen(c.dark(100 +(s*2)));
		p->drawLine(r.x(), i , r.x()+r.width()-1, i); 
	    }
	    
	}else{	// horizontal
	    int hi = (r.width())/2;
	    int i=0;


	    int s = 0;
	    int a = 17;
	    if(hi < 17)
		a = hi;
	
	    p->setPen(c);
	    
	    for ( i = r.x(); i < r.x()+r.width()-1; ++i ) {
		p->drawLine(i, r.y(), i, r.y()+r.height()-1);
	    }	

	    for ( i = r.x(); i < r.x()+hi-1; ++i ) {
		s = (hi-i) * a / hi;
		//p->setPen(c.light(100 + (hi*2)-(i*2)));
		p->setPen(c.light(100 + (s*2)));
		p->drawLine(i,r.y() , i, r.y()+r.height()-1);
	    }


	    int j = 0;
	    for ( i = r.x()+r.width()-hi+1; i< r.x()+r.width()-1 ; ++i ) {
		j++;
		s = j * a / hi;
		p->setPen(c.dark(100 +(s*2)));
		p->drawLine(i, r.y() , i,r.y()+ r.height()-1); 
	    }
	    
	    }	    
	
    }else{	// on
	if(!horizontal){

		int hi = (r.height())/2;
		int i=0;
    
		int s = 0;
		int a = 17;
		if(hi < 17)
		    a = hi;

		p->setPen(c);
    
		for ( i = r.y()+1; i < r.y()+r.height()-1; ++i ) {
		    p->drawLine(r.x()+1, i, r.x()+r.width()-1, i);
		}	

		for ( i = r.y()+2; i < hi-1; ++i ) {
		    s = (hi-i) * a / hi;
		    //p->setPen(c.dark(100 + (hi*2)-(i*2)));
		    p->setPen(c.dark(100 + (s*2)));
		    p->drawLine(r.x(),i , r.x()+r.width()-1, i);
		}


		int j = 0;
		for ( i = r.y()+r.height()-hi+1; i < r.y()+r.height() ; 
		      ++i ) {
		    j++;
		    s = j * a / hi;
		    p->setPen(c.light(100 +(s*2)));
		    p->drawLine(r.x(), i , r.x()+r.width()-1, i); 
		}

	}else { // on + virtical


	}
    }

}


int hwmStyle::styleHint( StyleHint sh, const QWidget *w, 
			 const QStyleOption &opt, 
			 QStyleHintReturn *shr) const
{
    switch (sh)
    {
    case SH_ItemView_ChangeHighlightOnFocus:
    case SH_Slider_SloppyKeyEvents:
	return 0;
	
	// We don't want any spacing below the menu bar when highcolor
    case SH_MainWindow_SpaceBelowMenuBar:
	return (hwmstyle ? 0 : 1);

    case SH_EtchDisabledText:
    case SH_Slider_SnapToValue:
    case SH_PrintDialog_RightAlignButtons:
    case SH_FontDialog_SelectAssociatedText:
    case SH_PopupMenu_AllowActiveAndDisabled:
    case SH_MenuBar_AltKeyNavigation:
    case SH_MenuBar_MouseTracking:
    case SH_PopupMenu_MouseTracking:
    case SH_ComboBox_ListMouseTracking:
	return 1;

    case SH_PopupMenu_SubMenuPopupDelay:
	return 128;
	
    default:
	return QCommonStyle::styleHint(sh, w, opt, shr);
    }
}



void hwmStyle::drawPrimitive( PrimitiveElement pe,
				    QPainter *p,
				    const QRect &r,
				    const QColorGroup &cg,
				    SFlags flags,
				    const QStyleOption& opt ) const
{
    switch (pe) {
    case PE_HeaderSection:{
	// adjust the sunken flag, otherwise headers are drawn
	// sunken...
	if ( flags & Style_Sunken )
	    flags ^= Style_Sunken;

	if( flags & Style_Horizontal )
		flags ^= Style_Horizontal;

	//	flags &= Style_Horizontal;

	drawPrimitive( PE_ButtonBevel, p, r, cg, flags, opt );



	break;
    }
    /*
    case PE_ButtonDefault: {
	if ( ! highcolor ) {
	    int x1, y1, x2, y2;
	    r.coords( &x1, &y1, &x2, &y2 );
	    
	    // Button default indicator
	    p->setPen( cg.shadow() );
	    p->drawLine( x1+1, y1, x2-1, y1 );
	    p->drawLine( x1, y1+1, x1, y2-1 );
	    p->drawLine( x1+1, y2, x2-1, y2 );
	    p->drawLine( x2, y1+1, x2, y2-1 );
	}
	break;
    }
    */

    case PE_ButtonDropDown:
    case PE_ButtonTool:
	{
	    // tool buttons don't change color when pushed in platinum,
	    // so we need to make the mid and button color the same
	    QColorGroup myCG = cg;
	    QBrush fill;

	    // quick trick to make sure toolbuttons drawn sunken
	    // when they are activated...
	    if ( flags & Style_On )
		flags |= Style_Sunken;

	    fill = myCG.brush( QColorGroup::Button );
	    myCG.setBrush( QColorGroup::Mid, fill );
	    drawPrimitive( PE_ButtonBevel, p, r, myCG, flags, opt );

	    break;
	}
	

			
    //case PE_HeaderSection: 
    case PE_ButtonBevel:{
	
	int x,
	    y,
	    w,
	    h;
	r.rect( &x, &y, &w, &h );
	
	QPen oldPen = p->pen();
	if ( w * h < 1600 ||
	     QABS(w - h) > 10 ) {
	    // small buttons
	
   
	    if ( !(flags & (Style_Sunken | Style_Down)) ) {
		p->fillRect( x , y , w , h ,
			     cg.brush(QColorGroup::Button) );


		//
		//
		//
		if ( flags & Style_MouseOver ){
		    if (flags & Style_Horizontal)
			drawGradient( p, r, cg.button().light(110), true, true);
		    else
			drawGradient( p, r, cg.button().light(110), false, true);

		}else{
		  if (flags & Style_Horizontal)
			drawGradient( p, r, cg.button(), true, true);
		    else
			drawGradient( p, r, cg.button(), false, true);
		}		    
		// the bright side
		p->setPen( cg.dark() );
		p->drawLine( x, y, x + w - 1, y );
		p->drawLine( x, y, x, y + h - 1 );

		p->setPen( cg.light() );
		p->drawLine( x + 1, y + 1, x + w - 2, y + 1 );
		p->drawLine( x + 1, y + 1, x + 1, y + h - 2 );

		// the dark side
		p->setPen( cg.mid() );
		p->drawLine( x + 2, y + h - 2, x + w - 2, y + h - 2 );
		p->drawLine( x + w - 2, y + 2, x + w - 2, y + h - 3 );

		p->setPen( cg.dark().dark() );
		p->drawLine( x + 1, y + h - 1, x + w - 1,
			     y + h - 1 );
		p->drawLine( x + w - 1, y + 1,
			     x + w - 1,
			     y + h - 2 );
	    } else {
		p->fillRect(x + 2, y + 2,
			    w - 4, h - 4,
			    cg.brush( QColorGroup::Mid ));


		//
		//
		//
		if ( flags & Style_MouseOver ){
		    if (flags & Style_Horizontal)
			drawGradient( p, r, cg.mid().light(110), true, false);
		    else
			drawGradient( p, r, cg.mid().light(110), false, false);

		}else{
		    if (flags & Style_Horizontal)
			drawGradient( p, r, cg.mid(), true, false);
		    else
			drawGradient( p, r, cg.mid(), false, false);
		}

		// the dark side
		p->setPen( cg.dark().dark() );
		p->drawLine( x, y, x + w - 1, y );
		p->drawLine( x, y, x, y + h - 1 );
		
		p->setPen( cg.mid().dark());
		p->drawLine( x + 1, y + 1,
			     x + w-2, y + 1);
		p->drawLine( x + 1, y + 1,
			     x + 1, y + h - 2 );


		// the bright side!

		p->setPen(cg.button());
		p->drawLine( x + 1, y + h - 2,
			     x + w - 2,
			     y + h - 2 );
		p->drawLine( x + w - 2, y + 1,
			     x + w - 2,
			     y + h - 2 );
		p->setPen(cg.dark());
		p->drawLine(x, y + h - 1,
			    x + w - 1,
			    y + h - 1 );
		p->drawLine(x + w - 1, y,
			    x + w - 1,
			    y + h - 1 );
	    }
	    
	} else {

	    // big ones
	    if ( !(flags & (Style_Sunken | Style_Down)) ) {
		p->fillRect( x + 3, y + 3, w - 6,
			     h - 6,
			     cg.brush(QColorGroup::Button) );
		
		
		if (flags & Style_Horizontal)
		    drawGradient( p, r, cg.button(), true, true);
		else
		    drawGradient( p, r, cg.button(), false, true);

		// the bright side
		p->setPen( cg.button().dark() );
		p->drawLine( x, y, x + w - 1, y );
		p->drawLine( x, y, x, y + h - 1 );

		p->setPen( cg.button() );
		p->drawLine( x + 1, y + 1, x + w - 2, y + 1 );
		p->drawLine( x + 1, y + 1, x + 1, y + h - 2 );
		
		
		p->setPen( cg.light() );
		p->drawLine( x + 2, y + 2, x + 2, y + h - 2 );
		p->drawLine( x + 2, y + 2, x + w - 2, y + 2 );
		// the dark side!

		p->setPen( cg.mid() );
		p->drawLine( x + 3, y + h - 3, x + w - 3, y + h - 3 );
		p->drawLine( x + w - 3, y + 3, x + w - 3, y + h - 3 );

		p->setPen( cg.dark() );
		p->drawLine( x + 2, y + h - 2, x + w - 2, y + h - 2 );
		p->drawLine( x + w - 2, y + 2, x + w - 2, y + h - 2 );

		p->setPen( cg.dark().dark() );
		p->drawLine( x + 1, y + h - 1, x + w - 1, y + h - 1 );
		p->drawLine( x + w - 1, y + 1, x + w - 1, y + h - 1 );
	    } else {
		p->fillRect( x + 3, y + 3, w - 6,
			     h - 6,
			     cg.brush( QColorGroup::Mid ) );

		
		if (flags & Style_Horizontal)
		    drawGradient( p, r, cg.mid(), true, false);
		else
		    drawGradient( p, r, cg.mid(), false, false);


		// the dark side
		p->setPen( cg.dark().dark().dark() );
		p->drawLine( x, y, x + w - 1, y );
		p->drawLine( x, y, x, y + h - 1 );
		
		p->setPen( cg.dark().dark() );
		p->drawLine( x + 1, y + 1,
			     x + w - 2, y + 1 );
		p->drawLine( x + 1, y + 1,
			     x + 1, y + h - 2 );

		p->setPen( cg.mid().dark() );
		p->drawLine( x + 2, y + 2,
			     x + 2, y + w - 2 );
		p->drawLine( x + 2, y + 2,
			     x + w - 2, y + 2 );

		
		// the bright side!
		
		p->setPen( cg.button() );
		p->drawLine( x + 2, y + h - 3,
			     x + w - 3,
			     y + h - 3 );
		p->drawLine( x + w - 3, y + 3,
			     x + w - 3,
			     y + h - 3 );

		p->setPen( cg.midlight() );
		p->drawLine( x + 1, y + h - 2,
			     x + w - 2,
			     y + h - 2 );
		p->drawLine( x + w - 2, y + 1,
			     x + w - 2,
			     y + h - 2 );

		p->setPen( cg.dark() );
		p->drawLine( x, y + h - 1,
			     x + w - 1,
			     y + h - 1 );
		p->drawLine( x + w - 1, y,
			     x + w - 1,
			     y + h - 1 );


		// corners
		//p->setPen( mixedColor(cg.dark().dark().dark(),
		//	cg.dark()) );
		p->setPen(cg.dark().dark().dark());
		p->drawPoint( x, y + h - 1 );
		p->drawPoint( x + w - 1, y );

		//p->setPen( mixedColor(cg.dark().dark(), cg.midlight()));
		p->setPen(cg.dark().dark(120));
		p->drawPoint( x + 1, y + h - 2 );
		p->drawPoint( x + w - 2, y + 1 );

		//p->setPen( mixedColor(cg.mid().dark(), cg.button() ) );
		p->setPen(cg.mid().dark().light(120));
		p->drawPoint( x + 2, y + h - 3 );
		p->drawPoint( x + w - 3, y + 2 );
	    }
	}
	p->setPen( oldPen );
	break;
    }
	    

    // PUSH BUTTON
    case PE_ButtonCommand:{
	
	//if ( flags & Style_MouseOver ) {
	//  QWindowsStyle::drawPrimitive( pe, p, r, cg, flags, opt );
	    
	//  break;
	//}

	    QPen oldPen = p->pen();
	    int x,
		y,
		w,
		h;
	    r.rect( &x, &y, &w, &h);

	    if ( !(flags & (Style_Down | Style_On)) ) {
		p->fillRect( x+3, y+3, w-6, h-6,
			     cg.brush( QColorGroup::Button ));



		if ( flags & Style_MouseOver ){
		    if (flags & Style_Horizontal)
			drawGradient( p, r, cg.button().light(110),true,true);
		    else
			drawGradient( p, r,cg.button().light(110),false,true);

		}else
		    if (flags & Style_Horizontal)
			drawGradient( p, r, cg.button(), true, true);
		    else
			drawGradient( p, r, cg.button(), false, true);


		p->setPen( cg.button() );
		// TopLeft point
		p->drawPoint( x, y );
		p->drawPoint( x+1, y );
		p->drawPoint( x, y+1 );
		// TopRightPoint
		p->drawPoint( x+w-2, y );
		p->drawPoint( x+w-1, y );
		p->drawPoint( x+w-1, y+1);
		
		p->setPen( cg.dark().dark() );
		p->drawLine( x+2, y+h-1, x+w-1, y+h-1 );
		p->drawPoint( x+w-2, y+h-2 );
		p->drawLine( x+w-1, y+2, x+w-1, y+h-2 );
		p->drawPoint( x+w-2, y+1 );
		
		p->drawPoint( x+w-1, y+h-1 );
		p->drawPoint( x+w-2, y+h-2 );

		p->setPen( cg.mid() );
		p->drawLine( x+3, y+h-2, x+w-4, y+h-2 );
		//p->drawPoint( x+w-3, y+h-3 );
		p->drawLine( x+w-2, y+3, x+w-2, y+h-4 );
		//p->drawPoint( x+w-3, y+2 );

		    
		p->setPen( cg.dark().dark() );
		p->drawLine( x+2, y, x+w-3, y );
		p->drawPoint( x+1, y+1 );
		p->drawLine( x, y+2, x, y+h-3 );
		p->drawPoint( x+1, y+h-2 );
		    
		p->setPen( cg.midlight() );
		p->drawLine( x+3, y+1, x+w-4, y+1 );
		p->drawPoint( x+2, y+2 );
		p->drawLine( x+1, y+3, x+1, y+h-4 );
		p->drawPoint( x+2, y+h-3 );
		
	    } else {
		p->fillRect( x + 2, y + 2, w - 4, h - 4,
			     cg.brush(QColorGroup::Dark) );




		if ( flags & Style_MouseOver ){
		    if (flags & Style_Horizontal)
			drawGradient( p, r, cg.dark().light(110),true, false);
		    else
			drawGradient( p, r, cg.dark().light(110),false,false);
		}else
		    if (flags & Style_Horizontal)
			drawGradient( p, r, cg.dark(), true, false);
		    else
			drawGradient( p, r, cg.dark(), false, false);



		p->setPen( cg.shadow() );
		p->drawLine( x+2, y+h-1, x+w-3, y+h-1 );
		p->drawPoint( x+w-2, y+h-2 );
		p->drawLine( x+w-1, y+2, x+w-1, y+h-3 );
		p->drawPoint( x+w-2, y+1 );
		
		//p->setPen( cg.dark().light(130) );
		p->drawLine( x+3, y+h-2, x+w-4, y+h-2 );
		p->drawPoint( x+w-3, y+h-3 );
		p->drawLine( x+w-2, y+3, x+w-2, y+h-4 );
		p->drawPoint( x+w-3, y+2 );

		
		p->setPen( cg.shadow() );
		p->drawLine( x+2, y, x+w-3, y );
		p->drawPoint( x+1, y+1 );
		p->drawLine( x, y+2, x, y+h-3 );
		p->drawPoint( x+1, y+h-2 );
		    
		p->setPen( cg.dark().dark(130) );
		p->drawLine( x+3, y+1, x+w-4, y+1 );
		p->drawPoint( x+2, y+2 );
		p->drawLine( x+1, y+3, x+1, y+h-4 );
		p->drawPoint( x+2, y+h-3 );
		
	    }	
	    p->setPen( oldPen );
	    break;
    }
	

    // SCROLLBAR
    case PE_ScrollBarSlider: {
	//### Small hack to ensure scrollbar gradients are drawn the right way
	flags ^= Style_Horizontal;

	drawPrimitive(PE_ButtonBevel, p, r, cg, flags | Style_Enabled | 
		      Style_Raised);

	    // Draw a scrollbar riffle (note direction after above changes)
	    if (flags & Style_Horizontal) {
		//drawGradient( p, r, cg.button(), true, false);
		if (r.height() >= 15) {
		    int x = r.x()+3;
		    int y = r.y() + (r.height()-7)/2;
		    int x2 = r.right()-3;
		    p->setPen(cg.light());
		    p->drawLine(x, y, x2, y);
		    p->drawLine(x, y+3, x2, y+3);
		    p->drawLine(x, y+6, x2, y+6);
		    
		    p->setPen(cg.mid());
		    p->drawLine(x, y+1, x2, y+1);
		    p->drawLine(x, y+4, x2, y+4);
		    p->drawLine(x, y+7, x2, y+7);
		}
	    } else {
		//drawGradient( p, r, cg.button(), false, false);
		if (r.width() >= 15) {
		    int y = r.y()+3;
		    int x = r.x() + (r.width()-7)/2;
		    int y2 = r.bottom()-3;
		    p->setPen(cg.light());
		    p->drawLine(x, y, x, y2);
		    p->drawLine(x+3, y, x+3, y2);
		    p->drawLine(x+6, y, x+6, y2);
		    
		    p->setPen(cg.mid());
		    p->drawLine(x+1, y, x+1, y2);
		    p->drawLine(x+4, y, x+4, y2);
		    p->drawLine(x+7, y, x+7, y2);
		}
	    }
	    break;
    }

    /*
    case PE_ScrollBarAddPage:
    case PE_ScrollBarSubPage:{
	
	p->setPen(cg.shadow());
	int x, y, w, h;
	r.rect(&x, &y, &w, &h);
	int x2 = x+w-1;
	int y2 = y+h-1;

	if (flags & Style_Horizontal) {
	    p->drawLine(x, y, x2, y);
	    p->drawLine(x, y2, x2, y2);
	    drawGradient( p, r, cg.light(), false, false);
	    //renderGradient(p, QRect(x, y+1, w, h-2),
	    //	   cg.mid(), false);
	} else {
	    p->drawLine(x, y, x, y2);
	    p->drawLine(x2, y, x2, y2);
	    drawGradient( p, r, cg.light(), true, false);
	    //	    renderGradient(p, QRect(x+1, y, w-2, h),
	    //	   cg.mid(), true);
	}
	
	//QWindowsStyle::drawPrimitive( pe, p, r, cg, flags, opt );
	//winstyle->drawPrimitive( pe, p, r, cg, flags, opt );
	break;
    }
    */
    case PE_ScrollBarAddLine: {
	drawPrimitive( PE_ButtonBevel, p, r, cg, (flags & Style_Enabled) |
		       ((flags & Style_Down) ? Style_Down : Style_Raised) );

	drawPrimitive( ((flags & Style_Horizontal) ? PE_ArrowRight : 
			PE_ArrowDown),p, r, cg, flags );
	break;
    }


    case PE_ScrollBarSubLine: {
	drawPrimitive( PE_ButtonBevel, p, r, cg, (flags & Style_Enabled) |
		       ((flags & Style_Down) ? Style_Down : Style_Raised) );

	drawPrimitive( ((flags & Style_Horizontal) ? PE_ArrowLeft : 
			PE_ArrowUp),p, r, cg, flags );
	break;
    }



    // GENERAL PANELS
    // -------------------------------------------------------------------
    case PE_Panel:
    case PE_PanelPopup: {
	//drawPrimitive(PE_ButtonBevel, p, r, cg, flags | Style_Enabled | 
	//      Style_Raised);
	bool sunken = flags & Style_Sunken;
	int lw = opt.isDefault() ? pixelMetric(PM_DefaultFrameWidth)
	    : opt.lineWidth();
	if (lw == 2 && sunken) {
	    QPen oldPen = p->pen();
	    int x,y,w,h;
	    r.rect(&x, &y, &w, &h);
	    int x2 = x+w-1;
	    int y2 = y+h-1;
	    p->setPen(cg.light());
	    p->drawLine(x, y2, x2, y2);
	    p->drawLine(x2, y, x2, y2);
	    p->setPen(cg.mid());
	    p->drawLine(x, y, x2, y);
	    p->drawLine(x, y, x, y2);
	    p->setPen(cg.midlight());
	    p->drawLine(x+1, y2-1, x2-1, y2-1);
	    p->drawLine(x2-1, y+1, x2-1, y2-1);
	    p->setPen(cg.dark());
	    p->drawLine(x+1, y+1, x2-1, y+1);
	    p->drawLine(x+1, y+1, x+1, y2-1);
	    p->setPen(oldPen);
	} else
	    QCommonStyle::drawPrimitive(pe, p, r, cg, flags, opt);
	break;
    }

    
    // MENU / TOOLBAR PANEL
    // -------------------------------------------------------------------
    case PE_PanelMenuBar:		// Menu
    case PE_PanelDockWindow: {		// Toolbar
	int x2 = r.x()+r.width()-1;
	int y2 = r.y()+r.height()-1;

	p->setPen(cg.light());
	p->drawLine(r.x(), r.y(), x2-1,  r.y());
	p->drawLine(r.x(), r.y(), r.x(), y2-1);
	p->setPen(cg.dark());
	p->drawLine(r.x(), y2, x2, y2);
	p->drawLine(x2, r.y(), x2, y2);
	
	// ### Qt should specify Style_Horizontal where appropriate
	drawGradient( p, QRect(r.x()+1, r.y()+1, x2-1, y2-1),
		      cg.background(), (r.width() < r.height()) && 
		      (pe != PE_PanelMenuBar), true );
	break;
    }
    
    
    // TOOLBAR/DOCK WINDOW HANDLE
    // -------------------------------------------------------------------
    case PE_DockWindowHandle: {
	
	// wild hacks are here. beware.
	QWidget* widget;
	if (p->device()->devType() == QInternal::Widget)
	    widget = dynamic_cast<QWidget*>(p->device());
	else
	    return;		// Don't paint on non-widgets 
	
	// Check if we are a normal toolbar or a hidden dockwidget.
	if ( widget->parent() && 
	     (widget->parent()->inherits("QToolBar") ||	// Normal toolbar
	      (widget->parent()->inherits("QMainWindow")) ))// Collapsed dock
	{
	    // Draw a toolbar handle
	    int x = r.x(); int y = r.y();
	    int x2 = r.x() + r.width()-1;
	    int y2 = r.y() + r.height()-1;

	    if (!(flags & Style_Horizontal)) {
		drawGradient( p, r, cg.background(), true, true);
		
		p->setPen(cg.light());
		p->drawLine(x+4, y+3, x2-4, y+3);
		p->drawLine(x+4, y+6, x2-4, y+6);
		
		p->setPen(cg.mid());
		p->drawLine(x+4, y+4, x2-4, y+4);
		p->drawLine(x+4, y+7, x2-4, y+7);

	    } else {
		drawGradient( p, r, cg.background(), false, true);
		    
		p->setPen(cg.light());
		p->drawLine(x+3, y+4, x+3, y2-4);
		p->drawLine(x+6, y+4, x+6, y2-4);
		
		p->setPen(cg.mid());
		p->drawLine(x+4, y+4, x+4, y2-4);
		p->drawLine(x+7, y+4, x+7, y2-4);
	    }
	} else
	    // We have a docked handle with a close button
	    //p->fillRect(r, cg.highlight());
	    //QWindowsStyle::drawPrimitive(pe, p, r, cg, flags, opt);
	    winstyle->drawPrimitive(pe, p, r, cg, flags, opt);
	// ### Fixme - Add text caption to docked handle

	break;
    }

    
    // TOOLBAR SEPARATOR
    // -------------------------------------------------------------------
    case PE_DockWindowSeparator: {
	drawGradient( p, r, cg.background(),
		      !(flags & Style_Horizontal), true);
	if ( !(flags & Style_Horizontal) ) {
	    p->setPen(cg.mid());
	    p->drawLine(4, r.height()/2, r.width()-5, r.height()/2);
	    p->setPen(cg.light());
	    p->drawLine(4, r.height()/2+1, r.width()-5, r.height()/2+1);
	} else {
	    p->setPen(cg.mid());
	    p->drawLine(r.width()/2, 4, r.width()/2, r.height()-5);
	    p->setPen(cg.light());
	    p->drawLine(r.width()/2+1, 4, r.width()/2+1, r.height()-5);
	}
	break;
    }

    default:
	//QWindowsStyle::drawPrimitive( pe, p, r, cg, flags, opt );
	winstyle->drawPrimitive( pe, p, r, cg, flags, opt );
	break;
    }

}

void hwmStyle::drawControl( ControlElement element,
				  QPainter *p,
				  const QWidget *widget,
				  const QRect &r,
				  const QColorGroup &cg,
				  SFlags how,
				  const QStyleOption& opt ) const
{
    switch( element ) {


    case CE_PopupMenuItem:
    {
	if (! widget || opt.isDefault())
	    break;

	const QPopupMenu *popupmenu = (const QPopupMenu *) widget;
	QMenuItem *mi = opt.menuItem();
	if ( !mi )
	    break;

	int tab = opt.tabWidth();
	int maxpmw = opt.maxIconWidth();
	bool dis = ! mi->isEnabled();
	bool checkable = popupmenu->isCheckable();
	bool act = how & Style_Active;
	int x, y, w, h;
	
	r.rect(&x, &y, &w, &h);
	
	if ( checkable ) {
	    // space for the checkmarks
	    if (use2000style)
		maxpmw = QMAX( maxpmw, 20 );
	    else
		maxpmw = QMAX( maxpmw, 12 );
	}

	int checkcol = maxpmw;
	
	if ( mi && mi->isSeparator() ) {                    // draw separator
	    p->setPen( cg.dark() );
	    p->drawLine( x, y, x+w, y );
	    p->setPen( cg.light() );
	    p->drawLine( x, y+1, x+w, y+1 );
	    return;
	}

	//	QBrush fill = (act ?
	//       cg.brush( QColorGroup::Highlight ) :
	//       cg.brush( QColorGroup::Button ));
	QBrush fill = (act ?
		       QColor("#669999")://"#008080"):
		       cg.brush( QColorGroup::Button ));
	p->fillRect( x, y, w, h, fill);
	if(act){
	    drawGradient( p, r, QColor("#669999")//"#008080")
			  , false, true);
	    p->setPen(cg.dark());
	    p->drawLine(x,y,x+w-1,y);
	    p->drawLine(x,y,x,y+h-1);
	    p->setPen(cg.light());
	    p->drawLine(x,y+h-2,x+w-1,y+h-2);
	    p->drawLine(x+w-1,y,x+w-1,y+h-1);

	}else
	    p->fillRect( x, y, w, h, fill);

	if ( !mi )
	    return;

	int xpos = x;
	QRect vrect = visualRect( QRect( xpos, y, checkcol, h ), r );
	int xvis = vrect.x();
	if ( mi->isChecked() ) {
	    if ( act && !dis )
		qDrawShadePanel( p, xvis, y, checkcol, h,
			cg, TRUE, 1, &cg.brush( QColorGroup::Button ) );
	    else {
		QBrush fill( cg.light(), Dense4Pattern );
		qDrawShadePanel( p, xvis, y, checkcol, h, cg, TRUE, 1,
				 &fill );
	    }
	} else if (! act)
	    p->fillRect(xvis, y, checkcol , h, cg.brush(QColorGroup::Button));

	if ( mi->iconSet() ) {              // draw iconset
	    QIconSet::Mode mode = dis ? QIconSet::Disabled : QIconSet::Normal;
	    if (act && !dis )
		mode = QIconSet::Active;
	    QPixmap pixmap;
	    if ( checkable && mi->isChecked() )
		pixmap = mi->iconSet()->pixmap( QIconSet::Small, mode, 
						QIconSet::On );
	    else
		pixmap = mi->iconSet()->pixmap( QIconSet::Small, mode );
	    int pixw = pixmap.width();
	    int pixh = pixmap.height();
	    if ( act && !dis && !mi->isChecked() )
		qDrawShadePanel( p, xvis, y, checkcol, h, cg, FALSE, 1,
				 &cg.brush( QColorGroup::Button ) );
	    QRect pmr( 0, 0, pixw, pixh );
	    pmr.moveCenter( vrect.center() );


	    QBrush fill = (act ?
			   QColor("#669999")://"#008080"):
			   cg.brush( QColorGroup::Button ));
	    if(act){
		drawGradient( p, r, QColor("#669999")//"#008080")
			      , false, true);
		p->setPen(cg.dark());
		p->drawLine(x,y,x+w-1,y);
		p->drawLine(x,y,x,y+h-1);
		p->setPen(cg.light());
		p->drawLine(x,y+h-1,x+w-1,y+h-1);
		p->drawLine(x+w-1,y,x+w-1,y+h-1);

	    }else
		p->fillRect( x, y, w, h, fill);

	    p->setPen( cg.text() );
	    p->drawPixmap( pmr.topLeft(), pixmap );

	    //	    fill = (act ?cg.brush( QColorGroup::Highlight ) :
	    //    cg.brush( QColorGroup::Button ));


	    //	    int xp = xpos + checkcol + 1;
	    //p->fillRect( visualRect( QRect( xp, y, w - checkcol - 1, h ), r ), fill);

	} else  if ( checkable ) {  // just "checking"...
	    if ( mi->isChecked() ) {
		int xp = xpos + windowsItemFrame;
		
		SFlags cflags = Style_Default;
		if (! dis)
		    cflags |= Style_Enabled;
		if (act)
		    cflags |= Style_On;
		
		drawPrimitive(PE_CheckMark, p,
			      visualRect( QRect(xp, y + windowsItemFrame,
						checkcol - 2*windowsItemFrame,
						h - 2*windowsItemFrame), r ),
			      cg, cflags);
	    }
	}

	p->setPen( act ? cg.highlightedText() : cg.buttonText() );
	
	QColor discol;
	if ( dis ) {
	    discol = cg.text();
	    p->setPen( discol );
	}

	int xm = windowsItemFrame + checkcol + windowsItemHMargin;
	xpos += xm;

	vrect = visualRect( QRect( xpos, y+windowsItemVMargin, w-xm-tab+1, 
				   h-2*windowsItemVMargin ), r );
	xvis = vrect.x();
	if ( mi->custom() ) {
	    p->save();
	    if ( dis && !act ) {
		p->setPen( cg.light() );
		mi->custom()->paint( p, cg, act, !dis,
				     xvis+1, y+windowsItemVMargin+1, 
				     w-xm-tab+1, h-2*windowsItemVMargin );
		p->setPen( discol );
	    }
	    mi->custom()->paint( p, cg, act, !dis,
				 xvis, y+windowsItemVMargin, w-xm-tab+1, 
				 h-2*windowsItemVMargin );
	    p->restore();
	}
	QString s = mi->text();
	if ( !s.isNull() ) {                        // draw text
	    int t = s.find( '\t' );
	    int text_flags = AlignVCenter|ShowPrefix | DontClip | SingleLine;
	    text_flags |= (QApplication::reverseLayout() ? AlignRight : 
			   AlignLeft );
	    if ( t >= 0 ) {                         // draw tab text
		int xp;
		xp = x + w - tab - ((use2000style) ? 20 : windowsRightBorder)
		    - windowsItemHMargin - windowsItemFrame + 1;
		int xoff = visualRect( QRect( xp, y+windowsItemVMargin, tab, 
					  h-2*windowsItemVMargin ), r ).x();
		if ( dis && !act ) {
		    p->setPen( cg.light() );
		    p->drawText( xoff+1, y+windowsItemVMargin+1, tab, 
			h-2*windowsItemVMargin, text_flags, s.mid( t+1 ));
		    p->setPen( discol );
		}
		p->drawText( xoff, y+windowsItemVMargin, tab, h-2*
			     windowsItemVMargin, text_flags, s.mid( t+1 ) );
		s = s.left( t );
	    }
	    if ( dis && !act ) {
		p->setPen( cg.light() );
		p->drawText( xvis+1, y+windowsItemVMargin+1, w-xm-tab+1, 
			     h-2*windowsItemVMargin, text_flags, s, t );
		p->setPen( discol );
	    }
	    p->drawText( xvis, y+windowsItemVMargin, w-xm-tab+1, 
			 h-2*windowsItemVMargin, text_flags, s, t );
	} else if ( mi->pixmap() ) {                        // draw pixmap
	    QPixmap *pixmap = mi->pixmap();
	    if ( pixmap->depth() == 1 )
		p->setBackgroundMode( OpaqueMode );
	    p->drawPixmap( xvis, y+windowsItemFrame, *pixmap );
	    if ( pixmap->depth() == 1 )
		p->setBackgroundMode( TransparentMode );
	}
	if ( mi->popup() ) {                        // draw sub menu arrow
	    int dim = (h-2*windowsItemFrame) / 2;
	    PrimitiveElement arrow;
	    arrow = ( QApplication::reverseLayout() ? PE_ArrowLeft : 
		      PE_ArrowRight );
	    xpos = x+w - windowsArrowHMargin - windowsItemFrame - dim;
	    vrect = visualRect( QRect(xpos, y + h / 2 - dim / 2, dim, dim), 
				r );
	    if ( act ) {
		if ( !dis )
		    discol = white;
		QColorGroup g2( discol, cg.highlight(),
				white, white,
				dis ? discol : white,
				discol, white );

		drawPrimitive(arrow, p, vrect,
			      g2, Style_Enabled);
	    } else {
		drawPrimitive(arrow, p, vrect,
			      cg, mi->isEnabled() ? Style_Enabled : 
			      Style_Default);
	    }
	}

	break;
    }

    // MENUBAR ITEM (sunken panel on mouse over)
    // -------------------------------------------------------------------
    /*
    case CE_MenuBarItem:
    {
	QMenuBar* mb = (QMenuBar*)widget;
	QRect pr = mb->rect();
	
	bool active  = how & Style_Active;
	bool focused = how & Style_HasFocus;
	
	if ( active && focused )
	    qDrawShadePanel(p, r.x(), r.y(), r.width(), r.height(),
			    cg, true, 1, &cg.brush(QColorGroup::Midlight));
	else
	    drawGradient( p, r, cg.background(), (how & Style_Horizontal)
	    , true, true);
	
	QCommonStyle::drawControl(element, p, widget, r, cg, how, opt);
	break;
    }
    */

    // MENUBAR ITEM (sunken panel on mouse over)
    // -------------------------------------------------------------------
    case CE_MenuBarItem:
    {
	QMenuBar  *mb = (QMenuBar*)widget;
	QMenuItem *mi = opt.menuItem();
	QRect      pr = mb->rect();
	    
	bool active  = how & Style_Active;
	bool focused = how & Style_HasFocus;
	
	if ( active && focused )
	    qDrawShadePanel(p, r.x(), r.y(), r.width(), r.height(),
			cg, true, 1, &cg.brush(QColorGroup::Midlight));
	else
	    drawGradient( p, r, cg.background(), (how & Style_Horizontal)
			  , true, true);

	    drawItem( p, r, AlignCenter | AlignVCenter | ShowPrefix
		      | DontClip | SingleLine, cg, how & Style_Enabled,
		      mi->pixmap(), mi->text() );
			
	    break;
	}

    /*
    case CE_PushButton: {
	if ( widget == hoverWidget )
	    how |= Style_MouseOver;

	drawPrimitive( PE_ButtonCommand, p, r, cg, how );
	break;
    }
    */
    
    case CE_PushButton:
    {
	QColorGroup myCg( cg );
	const QPushButton *btn;
	int x1, y1, x2, y2;
	bool useBevelButton;
	SFlags flags;
	flags = Style_Default;

	btn = (const QPushButton*)widget;

	// take care of the flags based on what we know...
	if ( btn->isDown() )
	    flags |= Style_Down;
	if ( btn->isOn() )
	    flags |= Style_On;
	if ( btn->isEnabled() )
	    flags |= Style_Enabled;
	if ( btn->isDefault() )
	    flags |= Style_Default;
	if (! btn->isFlat() && !(flags & Style_Down))
	    flags |= Style_Raised;

	if ( widget == hoverWidget )
	    flags |= Style_MouseOver;
	
	r.coords( &x1, &y1, &x2, &y2 );
	
	p->setPen( cg.foreground() );
	p->setBrush( QBrush(cg.button(), NoBrush) );
	
	QBrush fill;
	if ( btn->isDown() ) {
	    fill = cg.brush( QColorGroup::Dark );
	    // this could be done differently, but this
	    // makes a down Bezel drawn correctly...
	    myCg.setBrush( QColorGroup::Mid, fill );
	} else if ( btn->isOn() ) {
	    fill = QBrush( cg.mid(), Dense4Pattern );
	    myCg.setBrush( QColorGroup::Mid, fill );
	}
	// to quote the old hwmStlye drawPushButton...
	// small or square image buttons as well as toggle buttons are
	// bevel buttons (what a heuristic....)
	if ( btn->isToggleButton()
	     || ( btn->pixmap() &&
		  (btn->width() * btn->height() < 1600 ||
		   QABS( btn->width() - btn->height()) < 10 )) )
	    useBevelButton = TRUE;
	else
	    useBevelButton = FALSE;

	//int diw = pixelMetric( PM_ButtonDefaultIndicator, widget );
	if ( btn->isDefault() ) {
	    //x1 += 1;
	    //y1 += 1;
	    //x2 -= 1;
	    //y2 -= 1;
	    QColorGroup cg2( myCg );
	    SFlags myFlags = flags;
	    // don't draw the default button sunken, unless it's necessary.
	    if ( myFlags & Style_Down )
		myFlags ^= Style_Down;
	    if ( myFlags & Style_Sunken )
		myFlags ^= Style_Sunken;
	    
	    cg2.setColor( QColorGroup::Button, cg.mid() );

	    /*
	    if ( useBevelButton ) {
		drawPrimitive( PE_ButtonBevel, p, QRect( x1, y1,
							 x2 - x1 + 1,
							 y2 - y1 + 1 ),
			       myCg, myFlags, opt );
	    } else {
		drawPrimitive( PE_ButtonCommand, p, QRect( x1, y1,
							   x2 - x1 + 1,
							   y2 - y1 + 1 ),
			       cg2, myFlags, opt );
	    }
	    */
	    p->setPen(  cg.dark().dark() );
	    p->drawLine(x1+2,y2,x2-1,y2);
	    p->drawLine(x2,y1+2,x2,y2-1);
	}

	if ( btn->isDefault() || btn->autoDefault() ) {
	    //x1 += diw;
	    //y1 += diw;
	    x2 -= 1;//diw;
	    y2 -= 1;//diw;
	}
	
	if ( !btn->isFlat() || btn->isOn() || btn->isDown() ) {
	    if ( useBevelButton ) {
		// fix for toggle buttons...
		if ( flags & (Style_Down | Style_On) )
		    flags |= Style_Sunken;
		drawPrimitive( PE_ButtonBevel, p, QRect( x1, y1,
							 x2 - x1 + 1,
							 y2 - y1 + 1 ),
			       myCg, flags, opt );
	    } else {
		
		drawPrimitive( PE_ButtonCommand, p, QRect( x1, y1,
							   x2 - x1 + 1,
							   y2 - y1 + 1 ),
			       myCg, flags, opt );
	    }
	}
	

	if ( p->brush().style() != NoBrush )
	    p->setBrush( NoBrush );
	break;
    }
    
    case CE_PushButtonLabel:
    {
	const QPushButton *btn;
	bool on;
	int x, y, w, h;
	SFlags flags;
	flags = Style_Default;
	btn = (const QPushButton*)widget;
	on = btn->isDown() || btn->isOn();
	r.rect( &x, &y, &w, &h );
	if ( btn->isMenuButton() ) {
	    int dx = pixelMetric( PM_MenuButtonIndicator, widget );
	    
	    QColorGroup g = cg;
	    int xx = x + w - dx - 4;
	    int yy = y - 3;
	    int hh = h + 6;
	    /*
	    if ( !on ) {
		p->setPen( g.mid() );
		p->drawLine( xx, yy + 2, xx, yy + hh - 3 );
		p->setPen( g.button() );
		p->drawLine( xx + 1, yy + 1, xx + 1, yy + hh - 2 );
		p->setPen( g.light() );
		p->drawLine( xx + 2, yy + 2, xx + 2, yy + hh - 2 );
	    }
	    */
	    if ( btn->isEnabled() )
		flags |= Style_Enabled;
	    drawPrimitive( PE_ArrowDown, p, QRect(x + w - dx - 1, y + 2,
						  dx, h - 4),
			   g, flags, opt );	
	    w -= dx;
	}
	
	if ( btn->iconSet() && !btn->iconSet()->isNull() ) {
	    QIconSet::Mode mode = btn->isEnabled()
		? QIconSet::Normal : QIconSet::Disabled;
	    if ( mode == QIconSet::Normal && btn->hasFocus() )
		mode = QIconSet::Active;
	    QIconSet::State state = QIconSet::Off;
	    if ( btn->isToggleButton() && btn->isOn() )
		state = QIconSet::On;
	    QPixmap pixmap = btn->iconSet()->pixmap( QIconSet::Small,
						     mode, state );
	    int pixw = pixmap.width();
	    int pixh = pixmap.height();
	    p->drawPixmap( x + 2, y + h / 2 - pixh / 2, pixmap );
	    x += pixw + 4;
	    w -= pixw + 4;
	}

	drawItem( p, QRect( x, y, w, h ),
		  AlignCenter | ShowPrefix,
		  btn->colorGroup(), btn->isEnabled(),
		  btn->pixmap(), btn->text(), -1,
		  on ? &btn->colorGroup().brightText()
		  : &btn->colorGroup().buttonText() );
	if ( btn->hasFocus() )
	    drawPrimitive( PE_FocusRect, p,
			   subRect(SR_PushButtonFocusRect, widget),
			   cg, flags );
	break;
    }

    default:
	//QWindowsStyle::drawControl( element, p, widget, r, cg, how, opt );
	winstyle->drawControl( element, p, widget, r, cg, how, opt );
	break;
    }
}
    

void hwmStyle::drawComplexControl( ComplexControl control,
					 QPainter *p,
					 const QWidget *widget,
					 const QRect &r,
					 const QColorGroup &cg,
					 SFlags how,
					 SCFlags sub,
					 SCFlags subActive,
					 const QStyleOption& opt ) const
{
    switch ( control ) {
    // COMBOBOX
	case CC_ComboBox: {

	    // Draw box and arrow
	    if ( sub & SC_ComboBoxArrow ) {
		bool sunken = (subActive == SC_ComboBoxArrow);
		
		// Draw the combo
		int x,y,w,h;
		r.rect(&x, &y, &w, &h);

		int x2 = x+w-1;
		int y2 = y+h-1;

		p->setPen(cg.shadow());
		p->drawLine(x+1, y, x2-1, y);
		p->drawLine(x+1, y2, x2-1, y2);
		p->drawLine(x, y+1, x, y2-1);
		p->drawLine(x2, y+1, x2, y2-1);
		
		// Ensure the edge notches are properly colored
		p->setPen(cg.background());
		p->drawPoint(x,y);
		p->drawPoint(x,y2);
		p->drawPoint(x2,y);
		p->drawPoint(x2,y2);
		
		drawGradient( p, QRect(x+1, y+1, w-2, h-2),
			      cg.background(), false, true);

		    
		p->setPen(sunken ? cg.light() : cg.mid());
		p->drawLine(x2-1, y+2, x2-1, y2-1);
		p->drawLine(x+1, y2-1, x2-1, y2-1);

		p->setPen(sunken ? cg.mid() : cg.light());
		p->drawLine(x+1, y+1, x2-1, y+1);
		p->drawLine(x+1, y+2, x+1, y2-2);
		
		// Get the button bounding box
		QRect ar = QStyle::visualRect( querySubControlMetrics(
					CC_ComboBox, widget, SC_ComboBoxArrow)
					       , widget );
				
		// Are we enabled?
		if ( widget->isEnabled() )
		    how |= Style_Enabled;
		
		// Are we "pushed" ?
		if ( subActive & Style_Sunken )
		    how |= Style_Sunken;
		
		drawPrimitive(PE_ArrowDown, p, ar, cg, how);
	    }

	    // Draw an edit field if required
	    if ( sub & SC_ComboBoxEditField ) {
		const QComboBox * cb = (const QComboBox *) widget;
		QRect re = QStyle::visualRect( querySubControlMetrics( 
				CC_ComboBox, widget,SC_ComboBoxEditField), 
					       widget );

		// Draw the indent
		if (cb->editable()) {
		    p->setPen( cg.dark() );
		    p->drawLine( re.x(), re.y()-1, re.x()+re.width(), 
				 re.y()-1 );
		    p->drawLine( re.x()-1, re.y(), re.x()-1, re.y()+
				 re.height() );
		}
		
		if ( cb->hasFocus() ) {
		    p->setPen( cg.highlightedText() );
		    p->setBackgroundColor( cg.highlight() );
		} else {
		    p->setPen( cg.text() );
		    p->setBackgroundColor( cg.background() );
		}

		if ( cb->hasFocus() && !cb->editable() ) {
		    // Draw the contents
		    p->fillRect( re.x(), re.y(), re.width(), re.height(),
				 cg.brush( QColorGroup::Highlight ) );
		    
		    QRect re = QStyle::visualRect(subRect(
					SR_ComboBoxFocusRect, cb), widget);

		    drawPrimitive( PE_FocusRect, p, re, cg, 
				   Style_FocusAtBorder, 
				   QStyleOption(cg.highlight()));
		}
	    }
	    break;
	}

	// 3 BUTTON SCROLLBAR
	// -------------------------------------------------------------------
    case CC_ScrollBar: {
	const QScrollBar *sb = (const QScrollBar*)widget;
	
	QRect  addline, subline, subline2, addpage, subpage, slider, first, 
	    last;
	bool   maxedOut   = (sb->minValue()    == sb->maxValue());
	bool   horizontal = (sb->orientation() == Qt::Horizontal);
	SFlags sflags     = ((horizontal ? Style_Horizontal : Style_Default) |
			     (maxedOut   ? Style_Default : Style_Enabled));

	subline = querySubControlMetrics(control, widget, SC_ScrollBarSubLine
					 , opt);
	addline = querySubControlMetrics(control, widget, SC_ScrollBarAddLine
					 , opt);
	subpage = querySubControlMetrics(control, widget, SC_ScrollBarSubPage
					 ,opt);
	addpage = querySubControlMetrics(control, widget, SC_ScrollBarAddPage
					 , opt);
	slider  = querySubControlMetrics(control, widget, SC_ScrollBarSlider
					 ,  opt);
	first   = querySubControlMetrics(control, widget, SC_ScrollBarFirst
					 ,   opt);
	last    = querySubControlMetrics(control, widget, SC_ScrollBarLast
					 , opt);
	subline2 = addline;

	if (horizontal)
	    subline2.moveBy(-addline.width(), 0);
	else
	    subline2.moveBy(0, -addline.height());

	// Draw the up/left button set
	if ((sub & SC_ScrollBarSubLine) && subline.isValid()) {
	    drawPrimitive(PE_ScrollBarSubLine, p, subline, cg,
			  sflags | (subActive == SC_ScrollBarSubLine ?
				    Style_Down : Style_Default));

	    if (subline2.isValid())
		drawPrimitive(PE_ScrollBarSubLine, p, subline2, cg,
                              sflags | (subActive == SC_ScrollBarSubLine ?
					Style_Down : Style_Default));
	}
	
	if ((sub & SC_ScrollBarAddLine) && addline.isValid())
	    drawPrimitive(PE_ScrollBarAddLine, p, addline, cg,
			  sflags | ((subActive == SC_ScrollBarAddLine) ?
				    Style_Down : Style_Default));

	if ((sub & SC_ScrollBarSubPage) && subpage.isValid())
	    drawPrimitive(PE_ScrollBarSubPage, p, subpage, cg,
			  sflags | ((subActive == SC_ScrollBarSubPage) ?
				    Style_Down : Style_Default));

	if ((sub & SC_ScrollBarAddPage) && addpage.isValid())
	    drawPrimitive(PE_ScrollBarAddPage, p, addpage, cg,
			  sflags | ((subActive == SC_ScrollBarAddPage) ?
				    Style_Down : Style_Default));

	if ((sub & SC_ScrollBarFirst) && first.isValid())
	    drawPrimitive(PE_ScrollBarFirst, p, first, cg,
			  sflags | ((subActive == SC_ScrollBarFirst) ?
				    Style_Down : Style_Default));

	if ((sub & SC_ScrollBarLast) && last.isValid())
	    drawPrimitive(PE_ScrollBarLast, p, last, cg,
			  sflags | ((subActive == SC_ScrollBarLast) ?
				    Style_Down : Style_Default));

	if ((sub & SC_ScrollBarSlider) && slider.isValid()) {
	    drawPrimitive(PE_ScrollBarSlider, p, slider, cg,
			  sflags | ((subActive == SC_ScrollBarSlider) ?
				    Style_Down : Style_Default));
				// Draw focus rect
	    if (sb->hasFocus()) {
		QRect fr(slider.x() + 2, slider.y() + 2,
			 slider.width() - 5, slider.height() - 5);
		drawPrimitive(PE_FocusRect, p, fr, cg, Style_Default);
	    }
	} 

	break;
    }

    
    case CC_ToolButton: {
	const QToolButton *toolbutton = (const QToolButton *) widget;
	
	QRect button, menuarea;
	button   = querySubControlMetrics(control, widget, SC_ToolButton, opt);
	menuarea = querySubControlMetrics(control, widget, SC_ToolButtonMenu, opt);
	
	SFlags bflags = how,
	    mflags = how;
	
	if (subActive & SC_ToolButton)
	    bflags |= Style_Down;
	if (subActive & SC_ToolButtonMenu)
	    mflags |= Style_Down;
	
	if (sub & SC_ToolButton) {
	    // If we're pressed, on, or raised...
	    if (bflags & (Style_Down | Style_On | Style_Raised))
		drawPrimitive(PE_ButtonTool, p, button, cg, bflags, opt);

	    // Check whether to draw a background pixmap
	    else if ( toolbutton->parentWidget() &&
		      toolbutton->parentWidget()->backgroundPixmap() &&
		      !toolbutton->parentWidget()->backgroundPixmap()->isNull
		      () ) {
		QPixmap pixmap = *(toolbutton->parentWidget()->
				   backgroundPixmap());
		p->drawTiledPixmap( r, pixmap, toolbutton->pos() );
	    } 
	    else if (widget->parent() && widget->parent()->
		     inherits("QToolBar")){
		QToolBar* parent = (QToolBar*)widget->parent();
		QRect pr = parent->rect();

		drawGradient( p, r, cg.background(),
		parent->orientation() == Qt::Vertical, true);

		//drawGradient( p, r, cg.background(), 
		//parent->orientation() != Qt::Vertical,
		//r.x(), r.y(), pr.width()-2, pr.height()-2);
	    }
	}

	// Draw a toolbutton menu indicator if required
	if (sub & SC_ToolButtonMenu) {
	    if (mflags & (Style_Down | Style_On | Style_Raised))
		drawPrimitive(PE_ButtonDropDown, p, menuarea, cg, mflags,opt);
	    drawPrimitive(PE_ArrowDown, p, menuarea, cg, mflags, opt);
	}

	if (toolbutton->hasFocus() && !toolbutton->focusProxy()) {
				QRect fr = toolbutton->rect();
				fr.addCoords(3, 3, -3, -3);
				drawPrimitive(PE_FocusRect, p, fr, cg);
	}

	break;
    }

    

    // SLIDER
    // -------------------------------------------------------------------
    case CC_Slider: {
	const QSlider* slider = (const QSlider*)widget;
	bool horizontal = slider->orientation() == Horizontal;
	QRect groove = querySubControlMetrics(CC_Slider, widget,
					      SC_SliderGroove, opt);
	QRect handle = querySubControlMetrics(CC_Slider, 
					      widget,
					      SC_SliderHandle, opt);

	// Double-buffer slider for no flicker
	QPixmap pix(widget->size());
	QPainter p2;
	p2.begin(&pix);

	if ( slider->parentWidget() &&
	     slider->parentWidget()->backgroundPixmap() &&
	     !slider->parentWidget()->backgroundPixmap()->isNull() ) {
	    QPixmap pixmap = *(slider->parentWidget()->backgroundPixmap());
	    p2.drawTiledPixmap(r, pixmap, slider->pos());
	} else
	    pix.fill(cg.background());
	
	// Draw slider groove
	if ((sub & SC_SliderGroove) && groove.isValid())
	{
	    int gcenter = (horizontal ? groove.height() : groove.width()) / 2;
	    QRect gr;

	    if (horizontal)
		gr = QRect(groove.x(), groove.y()+gcenter-3, groove.width(), 
			   7);
	    else
		gr = QRect(groove.x()+gcenter-3, groove.y(), 7, 
			   groove.height());
	    int x,y,w,h;
	    gr.rect(&x, &y, &w, &h);
	    int x2=x+w-1;
	    int y2=y+h-1;
	    
				// Draw the slider groove.
	    p2.setPen(cg.dark());
	    p2.drawLine(x+2, y, x2-2, y);
	    p2.drawLine(x, y+2, x, y2-2);
	    p2.fillRect(x+2,y+2,w-4, h-4, 
			slider->isEnabled() ? cg.dark() : cg.mid());
	    p2.setPen(cg.shadow());
	    p2.drawRect(x+1, y+1, w-2, h-2);
	    p2.setPen(cg.light());
	    p2.drawPoint(x+1,y2-1);
	    p2.drawPoint(x2-1,y2-1);
	    p2.drawLine(x2, y+2, x2, y2-2);
	    p2.drawLine(x+2, y2, x2-2, y2);
	    
	    // Draw the focus rect around the groove
	    if (slider->hasFocus())
		drawPrimitive(PE_FocusRect, &p2, groove, cg);
	}

	// Draw the tickmarks
	if (sub & SC_SliderTickmarks)
	    QCommonStyle::drawComplexControl(control, &p2, widget, 
					     r, cg, how, SC_SliderTickmarks,
					     subActive, opt);

	// Draw the slider handle
	if ((sub & SC_SliderHandle) && handle.isValid()) {
	    int x,y,w,h;
	    handle.rect(&x, &y, &w, &h);
	    int x2 = x+w-1;
	    int y2 = y+h-1;
	    
	    p2.setPen(cg.mid());
	    p2.drawLine(x+1, y, x2-1, y);
	    p2.drawLine(x, y+1, x, y2-1);
	    p2.setPen(cg.shadow());
	    p2.drawLine(x+1, y2, x2-1, y2);
	    p2.drawLine(x2, y+1, x2, y2-1);
	    
	    p2.setPen(cg.light());
	    p2.drawLine(x+1, y+1, x2-1, y+1);
	    p2.drawLine(x+1, y+1, x+1,  y2-1);
	    p2.setPen(cg.dark());
	    p2.drawLine(x+2, y2-1, x2-1, y2-1);
	    p2.drawLine(x2-1, y+2, x2-1, y2-1);
	    p2.setPen(cg.midlight());
	    p2.drawLine(x+2, y+2, x2-2, y+2);
	    p2.drawLine(x+2, y+2, x+2, y2-2);
	    p2.setPen(cg.mid());
	    p2.drawLine(x+3, y2-2, x2-2, y2-2);
	    p2.drawLine(x2-2, y+3, x2-2, y2-2);
	    drawGradient(&p2, QRect(x+3, y+3, w-6, h-6), 
			   cg.button(), !horizontal, false);

	    // Paint riffles
	    if (horizontal) {
		p2.setPen(cg.light());
		p2.drawLine(x+5, y+4, x+5, y2-4);
		p2.drawLine(x+8, y+4, x+8, y2-4);
		p2.drawLine(x+11,y+4, x+11, y2-4);
		p2.setPen(slider->isEnabled() ? cg.shadow(): cg.mid());
		p2.drawLine(x+6, y+4, x+6, y2-4);
		p2.drawLine(x+9, y+4, x+9, y2-4);
		p2.drawLine(x+12,y+4, x+12, y2-4);
	    } else {
		p2.setPen(cg.light());
		p2.drawLine(x+4, y+5, x2-4, y+5);
		p2.drawLine(x+4, y+8, x2-4, y+8);
		p2.drawLine(x+4, y+11, x2-4, y+11);
		p2.setPen(slider->isEnabled() ? cg.shadow() : cg.mid());
		p2.drawLine(x+4, y+6, x2-4, y+6);
		p2.drawLine(x+4, y+9, x2-4, y+9);
		p2.drawLine(x+4, y+12, x2-4, y+12);
	    }
	    p2.end();
	    bitBlt((QWidget*)widget, r.x(), r.y(), &pix);
	}
	break;
    }
    
    default:
	//QWindowsStyle::drawComplexControl( control, p, widget, r, cg,
	//			   how, sub, subActive, opt );
	winstyle->drawComplexControl( control, p, widget, r, cg,
					   how, sub, subActive, opt );
	break;
    }
}

QStyle::SubControl hwmStyle::querySubControl( ComplexControl control,
                                                    const QWidget *widget,
                                                    const QPoint &pos,
                                                    const QStyleOption& opt ) const
{
    QStyle::SubControl ret =
	QCommonStyle::querySubControl(control, widget, pos, opt);

    // Enable third button
    if (control == CC_ScrollBar && ret == SC_None)
        ret = SC_ScrollBarSubLine;

    return ret;
}


// Many thanks to Brad for contributing this code.
QRect hwmStyle::querySubControlMetrics( ComplexControl control,
					const QWidget *widget,
					SubControl sc,
					const QStyleOption& opt ) const
{	
    QRect ret;

    switch (control) {
    case CC_ScrollBar: {
	const QScrollBar *sb = (const QScrollBar*)widget;
	bool horizontal = sb->orientation() == Qt::Horizontal;
	int sliderstart = sb->sliderStart();
	int sbextent    = pixelMetric(PM_ScrollBarExtent, widget);
	int maxlen      = (horizontal ? sb->width() : sb->height()) - 
	    (sbextent*3);
	int sliderlen;

	// calculate slider length
	if (sb->maxValue() != sb->minValue()) {
	    uint range = sb->maxValue() - sb->minValue();
	    sliderlen = (sb->pageStep() * maxlen) / (range + sb->pageStep());
	    
	    int slidermin = pixelMetric( PM_ScrollBarSliderMin, widget );
	    if ( sliderlen < slidermin || range > INT_MAX / 2 )
		sliderlen = slidermin;
	    if ( sliderlen > maxlen )
		sliderlen = maxlen;
	} else
	    sliderlen = maxlen;
	
	// Subcontrols
	switch (sc) {
	case SC_ScrollBarSubLine:
	    // top/left button
	    ret.setRect(0, 0, sbextent, sbextent);
	    break;
	    
	case SC_ScrollBarAddLine:
	    // bottom/right button
	    if (horizontal)
		ret.setRect(sb->width() - sbextent, 0, sbextent, sbextent);
	    else
		ret.setRect(0, sb->height() - sbextent, sbextent, sbextent);
	    break;

	case SC_ScrollBarSubPage:
	    // between top/left button and slider
	    if (horizontal)
		ret.setRect(sbextent, 0, sliderstart - sbextent, sbextent);
	    else
		ret.setRect(0, sbextent, sbextent, sliderstart - sbextent);
	    break;
	    
	case SC_ScrollBarAddPage:
	    // between bottom/right button and slider
	    if (horizontal)
		ret.setRect(sliderstart + sliderlen, 0, maxlen - sliderstart 
			    - sliderlen + sbextent, sbextent);
	    else
		ret.setRect(0, sliderstart + sliderlen, sbextent, 
			    maxlen - sliderstart - sliderlen + sbextent);
	    break;
	    
	case SC_ScrollBarGroove:
	    if (horizontal)
		ret.setRect(sbextent, 0, sb->width() - sbextent * 3, 
			    sb->height());
	    else	
		ret.setRect(0, sbextent, sb->width(), sb->height() - sbextent
			    * 3);
	    break;
	
	case SC_ScrollBarSlider:
	    if (horizontal)
		ret.setRect(sliderstart, 0, sliderlen, sbextent);
	    else
		ret.setRect(0, sliderstart, sbextent, sliderlen);
	    break;
	    
	default:
	    break;
	}
	break;
    }
    
    default:
	ret = QCommonStyle::querySubControlMetrics(control, widget, sc, opt);

	break;
    }
    return ret;
}


int hwmStyle::pixelMetric( PixelMetric metric, const QWidget *w ) const
{
    switch ( metric ) {
    case PM_MenuBarFrameWidth:
	return 1;
	//    case PM_MenuButtonIndicator:
	//    case PM_ButtonMargin:
	//return 0;

    default:
	return //QWindowsStyle::pixelMetric( metric, w );
	    winstyle->pixelMetric( metric, w );
    }
    
}
