/****************************************************************************
** deskitem meta object code from reading C++ file 'deskitem.h'
**
** Created: Fri Apr 2 23:56:19 2004
**      by: The Qt MOC ($Id$)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#undef QT_NO_COMPAT
#include "../.ui/deskitem.h"
#include <qmetaobject.h>
#include <qapplication.h>

#include <private/qucomextra_p.h>
#if !defined(Q_MOC_OUTPUT_REVISION) || (Q_MOC_OUTPUT_REVISION != 26)
#error "This file was generated using the moc from 3.3.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

const char *deskitem::className() const
{
    return "deskitem";
}

QMetaObject *deskitem::metaObj = 0;
static QMetaObjectCleanUp cleanUp_deskitem( "deskitem", &deskitem::staticMetaObject );

#ifndef QT_NO_TRANSLATION
QString deskitem::tr( const char *s, const char *c )
{
    if ( qApp )
	return qApp->translate( "deskitem", s, c, QApplication::DefaultCodec );
    else
	return QString::fromLatin1( s );
}
#ifndef QT_NO_TRANSLATION_UTF8
QString deskitem::trUtf8( const char *s, const char *c )
{
    if ( qApp )
	return qApp->translate( "deskitem", s, c, QApplication::UnicodeUTF8 );
    else
	return QString::fromUtf8( s );
}
#endif // QT_NO_TRANSLATION_UTF8

#endif // QT_NO_TRANSLATION

QMetaObject* deskitem::staticMetaObject()
{
    if ( metaObj )
	return metaObj;
    QMetaObject* parentObject = QDialog::staticMetaObject();
    static const QUMethod slot_0 = {"okBtn_clicked", 0, 0 };
    static const QUMethod slot_1 = {"ApplyBtn_clicked", 0, 0 };
    static const QUMethod slot_2 = {"iconBtn_clicked", 0, 0 };
    static const QUMethod slot_3 = {"languageChange", 0, 0 };
    static const QMetaData slot_tbl[] = {
	{ "okBtn_clicked()", &slot_0, QMetaData::Public },
	{ "ApplyBtn_clicked()", &slot_1, QMetaData::Public },
	{ "iconBtn_clicked()", &slot_2, QMetaData::Public },
	{ "languageChange()", &slot_3, QMetaData::Protected }
    };
    metaObj = QMetaObject::new_metaobject(
	"deskitem", parentObject,
	slot_tbl, 4,
	0, 0,
#ifndef QT_NO_PROPERTIES
	0, 0,
	0, 0,
#endif // QT_NO_PROPERTIES
	0, 0 );
    cleanUp_deskitem.setMetaObject( metaObj );
    return metaObj;
}

void* deskitem::qt_cast( const char* clname )
{
    if ( !qstrcmp( clname, "deskitem" ) )
	return this;
    return QDialog::qt_cast( clname );
}

bool deskitem::qt_invoke( int _id, QUObject* _o )
{
    switch ( _id - staticMetaObject()->slotOffset() ) {
    case 0: okBtn_clicked(); break;
    case 1: ApplyBtn_clicked(); break;
    case 2: iconBtn_clicked(); break;
    case 3: languageChange(); break;
    default:
	return QDialog::qt_invoke( _id, _o );
    }
    return TRUE;
}

bool deskitem::qt_emit( int _id, QUObject* _o )
{
    return QDialog::qt_emit(_id,_o);
}
#ifndef QT_NO_PROPERTIES

bool deskitem::qt_property( int id, int f, QVariant* v)
{
    return QDialog::qt_property( id, f, v);
}

bool deskitem::qt_static_property( QObject* , int , int , QVariant* ){ return FALSE; }
#endif // QT_NO_PROPERTIES
