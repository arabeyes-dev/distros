#include <qapplication.h>
#include "dclock.h"
#include <hwm_tbab.h>


int main(int argc, char **argv)
{
    QApplication a(argc, argv);

    defaults::read_config();

    QTranslator translator( 0 );
    translator.load( "hwmclock_" + defaults::lng ,defaults::get_cfile("lng"));
    a.installTranslator( &translator );
    if (defaults::lng == "ar")
	a.setReverseLayout ( true );
    
    dclock *dc = new dclock(0, "dc",Qt::WStyle_NoBorder|Qt::WMouseNoMask) ;
    //   dc->setBackgroundMode(Qt::X11ParentRelative);
    /*
	QImage img( get_cfile("images/tr2.png") );
	QPixmap p;
	p.convertFromImage( img );
	if ( !p.mask() )
	    if ( img.hasAlphaBuffer() ) {
		QBitmap bm;
		bm = img.createAlphaMask();
		p.setMask( bm );
	    } else {
		QBitmap bm;
		bm = img.createHeuristicMask();
		p.setMask( bm );
	    }

	dc->setBackgroundPixmap( p );
	if ( p.mask() )
	    dc->setMask( *p.mask() );
    */

    a.setMainWidget(dc);
    dc->show();
    return a.exec();
}
