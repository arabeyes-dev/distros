/****************************************************************************
** hnotepad.h
**
** Copyright (C) 2002 Haydar Net.  All rights reserved.
**
**
*****************************************************************************/

#ifndef HNOTEPAD_H
#define HNOTEPAD_H

#include <qwidget.h>
#include <qmenubar.h>
#include <qmultilineedit.h>
#include <qprinter.h>
#include <qmainwindow.h>
#include <qfile.h>
#include <qtranslator.h>
#include <qfont.h>

#include <iostream.h>
//#include <string.h>
//#include <stdlib.h>

class Editor : public QMainWindow
{
    Q_OBJECT

    QString filename;

 public:
    QString enc;
    Editor( QWidget *parent=0, const char *name="qwerty" );
    ~Editor();
    void set_font(QString f){ e->setFont(QFont(f)); };
    void setFilename(QString fn){ filename = fn;};
    void load( const QString& fileName, int code=-1 );
    int testCode(QString code);

 public slots:
    void newDoc();
    void load();
    bool save();
    void print();
    void addEncoding();
    void toUpper();
    void toLower();
    void font();
    void about();
    void find();
    void findNext();

 protected:
    void closeEvent( QCloseEvent * );

 private slots:
    void saveAsEncoding( int );
    void openAsEncoding( int );
    void textChanged();

 private:
    bool saveAs( const QString& fileName, int code=-1 );
    void rebuildCodecList();
    QMultiLineEdit *e;
#ifndef QT_NO_PRINTER
    QPrinter        printer;
#endif
    QPopupMenu	   *save_as;
    QPopupMenu	   *open_as;
    bool changed;
    QString findString;
};

#endif // HNOTEPAD_H
