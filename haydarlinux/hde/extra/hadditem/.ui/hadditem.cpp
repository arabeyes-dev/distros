/****************************************************************************
** Form implementation generated from reading ui file 'hadditem.ui'
**
** Created: Fri Apr 2 23:55:18 2004
**      by: The User Interface Compiler ($Id$)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#include "hadditem.h"

#include <qvariant.h>
#include <qpushbutton.h>
#include <qwidget.h>
#include <qlabel.h>
#include <qlineedit.h>
#include <qcombobox.h>
#include <qtextedit.h>
#include <qcheckbox.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>
#include <qimage.h>
#include <qpixmap.h>

#include "../hadditem.ui.h"
/*
 *  Constructs a hadditem as a child of 'parent', with the
 *  name 'name' and widget flags set to 'f'.
 *
 *  The wizard will by default be modeless, unless you set 'modal' to
 *  TRUE to construct a modal wizard.
 */
hadditem::hadditem( QWidget* parent, const char* name, bool modal, WFlags fl )
    : QWizard( parent, name, modal, fl )
{
    if ( !name )
	setName( "hadditem" );

    execPage = new QWidget( this, "execPage" );

    QWidget* privateLayoutWidget = new QWidget( execPage, "layout17" );
    privateLayoutWidget->setGeometry( QRect( 10, 0, 420, 200 ) );
    layout17 = new QHBoxLayout( privateLayoutWidget, 11, 6, "layout17"); 

    layout16 = new QVBoxLayout( 0, 0, 6, "layout16"); 

    execLbl = new QLabel( privateLayoutWidget, "execLbl" );
    layout16->addWidget( execLbl );

    layout11 = new QHBoxLayout( 0, 0, 6, "layout11"); 

    excEdit = new QLineEdit( privateLayoutWidget, "excEdit" );
    excEdit->setFrameShape( QLineEdit::LineEditPanel );
    excEdit->setFrameShadow( QLineEdit::Sunken );
    layout11->addWidget( excEdit );

    browseBtn = new QPushButton( privateLayoutWidget, "browseBtn" );
    browseBtn->setMaximumSize( QSize( 100, 32767 ) );
    layout11->addWidget( browseBtn );
    layout16->addLayout( layout11 );
    spacer1 = new QSpacerItem( 20, 30, QSizePolicy::Minimum, QSizePolicy::Expanding );
    layout16->addItem( spacer1 );

    layout14 = new QHBoxLayout( 0, 0, 6, "layout14"); 

    lineEdit5 = new QLineEdit( privateLayoutWidget, "lineEdit5" );
    layout14->addWidget( lineEdit5 );

    comboBox1 = new QComboBox( FALSE, privateLayoutWidget, "comboBox1" );
    comboBox1->setEnabled( FALSE );
    layout14->addWidget( comboBox1 );
    layout16->addLayout( layout14 );
    spacer11 = new QSpacerItem( 20, 60, QSizePolicy::Minimum, QSizePolicy::Expanding );
    layout16->addItem( spacer11 );
    layout17->addLayout( layout16 );

    textEdit1 = new QTextEdit( privateLayoutWidget, "textEdit1" );
    textEdit1->setMaximumSize( QSize( 140, 32767 ) );
    layout17->addWidget( textEdit1 );
    addPage( execPage, QString("") );

    itemsPage = new QWidget( this, "itemsPage" );

    QWidget* privateLayoutWidget_2 = new QWidget( itemsPage, "layout17" );
    privateLayoutWidget_2->setGeometry( QRect( 10, 0, 420, 200 ) );
    layout17_2 = new QHBoxLayout( privateLayoutWidget_2, 11, 6, "layout17_2"); 

    layout16_2 = new QVBoxLayout( 0, 0, 6, "layout16_2"); 

    desktopCheck = new QCheckBox( privateLayoutWidget_2, "desktopCheck" );
    desktopCheck->setChecked( TRUE );
    layout16_2->addWidget( desktopCheck );

    layout14_2 = new QHBoxLayout( 0, 0, 6, "layout14_2"); 
    spacer3 = new QSpacerItem( 30, 20, QSizePolicy::Fixed, QSizePolicy::Minimum );
    layout14_2->addItem( spacer3 );

    textLabel2 = new QLabel( privateLayoutWidget_2, "textLabel2" );
    layout14_2->addWidget( textLabel2 );

    desktopPlace = new QLineEdit( privateLayoutWidget_2, "desktopPlace" );
    layout14_2->addWidget( desktopPlace );
    layout16_2->addLayout( layout14_2 );
    spacer4 = new QSpacerItem( 20, 20, QSizePolicy::Minimum, QSizePolicy::Fixed );
    layout16_2->addItem( spacer4 );

    menuCheck = new QCheckBox( privateLayoutWidget_2, "menuCheck" );
    menuCheck->setChecked( TRUE );
    layout16_2->addWidget( menuCheck );

    layout15 = new QHBoxLayout( 0, 0, 6, "layout15"); 
    spacer5 = new QSpacerItem( 30, 20, QSizePolicy::Fixed, QSizePolicy::Minimum );
    layout15->addItem( spacer5 );

    textLabel2_2 = new QLabel( privateLayoutWidget_2, "textLabel2_2" );
    layout15->addWidget( textLabel2_2 );

    menuPlace = new QLineEdit( privateLayoutWidget_2, "menuPlace" );
    layout15->addWidget( menuPlace );
    layout16_2->addLayout( layout15 );
    spacer1_2 = new QSpacerItem( 20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding );
    layout16_2->addItem( spacer1_2 );
    layout17_2->addLayout( layout16_2 );

    textEdit2 = new QTextEdit( privateLayoutWidget_2, "textEdit2" );
    textEdit2->setMaximumSize( QSize( 140, 32767 ) );
    layout17_2->addWidget( textEdit2 );
    addPage( itemsPage, QString("") );

    iconPage = new QWidget( this, "iconPage" );

    QWidget* privateLayoutWidget_3 = new QWidget( iconPage, "layout20" );
    privateLayoutWidget_3->setGeometry( QRect( 10, 0, 420, 200 ) );
    layout20 = new QHBoxLayout( privateLayoutWidget_3, 11, 6, "layout20"); 

    layout19 = new QVBoxLayout( 0, 0, 6, "layout19"); 

    layout18 = new QHBoxLayout( 0, 0, 6, "layout18"); 
    spacer6 = new QSpacerItem( 40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout18->addItem( spacer6 );

    iconPix = new QLabel( privateLayoutWidget_3, "iconPix" );
    iconPix->setMinimumSize( QSize( 48, 48 ) );
    iconPix->setScaledContents( TRUE );
    layout18->addWidget( iconPix );

    iconBtn = new QPushButton( privateLayoutWidget_3, "iconBtn" );
    layout18->addWidget( iconBtn );
    spacer8 = new QSpacerItem( 40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout18->addItem( spacer8 );
    layout19->addLayout( layout18 );
    spacer7 = new QSpacerItem( 20, 110, QSizePolicy::Minimum, QSizePolicy::Expanding );
    layout19->addItem( spacer7 );
    layout20->addLayout( layout19 );

    textEdit3 = new QTextEdit( privateLayoutWidget_3, "textEdit3" );
    textEdit3->setMaximumSize( QSize( 140, 32767 ) );
    layout20->addWidget( textEdit3 );
    addPage( iconPage, QString("") );
    languageChange();
    resize( QSize(444, 278).expandedTo(minimumSizeHint()) );
    clearWState( WState_Polished );

    // signals and slots connections
    connect( browseBtn, SIGNAL( clicked() ), this, SLOT( browseBtn_clicked() ) );
    connect( iconBtn, SIGNAL( clicked() ), this, SLOT( iconBtn_clicked() ) );
    init();
}

/*
 *  Destroys the object and frees any allocated resources
 */
hadditem::~hadditem()
{
    // no need to delete child widgets, Qt does it all for us
}

/*
 *  Sets the strings of the subwidgets using the current
 *  language.
 */
void hadditem::languageChange()
{
    setCaption( tr( "Add Desktop/Menu Item" ) );
    execLbl->setText( tr( "Executable:" ) );
    browseBtn->setText( tr( "Browse" ) );
    comboBox1->clear();
    comboBox1->insertItem( QString::null );
    comboBox1->insertItem( tr( "ar" ) );
    comboBox1->insertItem( tr( "nl" ) );
    comboBox1->insertItem( tr( "es" ) );
    comboBox1->insertItem( tr( "fr" ) );
    comboBox1->insertItem( tr( "tr" ) );
    comboBox1->insertItem( tr( "fa" ) );
    setTitle( execPage, tr( "Select Executable" ) );
    desktopCheck->setText( tr( "Add Desktop Item" ) );
    textLabel2->setText( tr( "Place:" ) );
    desktopPlace->setText( tr( "~/.hwm/Desktop" ) );
    menuCheck->setText( tr( "Add Menu Item" ) );
    textLabel2_2->setText( tr( "Place:" ) );
    menuPlace->setText( tr( "~/.hwm/applnk/Lower/Programs" ) );
    setTitle( itemsPage, tr( "Select Items Types" ) );
    iconBtn->setText( tr( "Change Icon" ) );
    setTitle( iconPage, tr( "Select Icons" ) );
}

