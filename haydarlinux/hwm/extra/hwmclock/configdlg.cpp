/****************************************************************************
** Form implementation generated from reading ui file 'configdlg.ui'
**
** Created: Sun Oct 28 21:19:24 2001
**      by:  The User Interface Compiler (uic)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/
#include "configdlg.h"

#include <qvariant.h>   // first for gcc 2.7.2
#include <qmime.h>
#include <qdragobject.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>
#include <qimage.h>
#include <qpixmap.h>

#include <hcalender.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>

HCalender *hc;

static QPixmap uic_load_pixmap_configDlg( const QString &name )
{
    const QMimeSource *m = QMimeSourceFactory::defaultFactory()->data( name );
    if ( !m )
	return QPixmap();
    QPixmap pix;
    QImageDrag::decode( m, pix );
    return pix;
}

/* 
 *  Constructs a configDlg which is a child of 'parent', with the 
 *  name 'name' and widget flags set to 'f'.
 *
 *  The dialog will by default be modeless, unless you set 'modal' to
 *  TRUE to construct a modal dialog.
 */
configDlg::configDlg( QWidget* parent,  const char* name, WFlags fl )
    : QWidget( parent, name, fl )
{

    hc = new HCalender(this, "HCalender");

}

/*  
 *  Destroys the object and frees any allocated resources
 */
configDlg::~configDlg()
{
    // no need to delete child widgets, Qt does it all for us
}

