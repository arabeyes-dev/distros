/*  
*   File      : dclock.h
*   Written by: haydar@haydar.net
*   Copyright : GPL
*
*   Shows a label with current time.
*/

#include <qwidget.h>
#include <qdatetime.h>
#include <qtimer.h>
#include <qfont.h>
#include <qlabel.h>
#include <qpainter.h>
#include <qpixmap.h>
#include <defaults.h>
#include <libhde.h>

class dclock : public QLabel
{
	Q_OBJECT
	
	QTime time;
	void settime(QTime &);
	
 public slots:
	void timeout(void);

 protected:
	//virtual void drawContents ( QPainter * p );
	virtual void mouseDoubleClickEvent ( QMouseEvent * e );
	virtual void paintEvent ( QPaintEvent * );

 public:
	dclock(QWidget *parent=0, const char *name=0, WFlags f = 0);

};
