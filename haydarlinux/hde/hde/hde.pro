
TEMPLATE = app
TARGET   =  hde

SOURCES += main.cpp qapp.cpp toolbar.cpp xwindow.cpp procbar.cpp winlist.cpp\
	  menu.cpp pager.cpp apbar.cpp keyboard.cpp winfo.cpp rubber.cpp \
	  wborder.cpp desktp.cpp atoms.cpp dclock.cpp

HEADERS += qapp.h toolbar.h xwindow.h procbar.h winlist.h menu.h pager.h \
	  apbar.h keyboard.h winfo.h rubber.h wborder.h desktp.h atoms.h \
	  conf.h defs.h dclock.h


INCLUDEPATH += ../include /usr/X11R6/include 

LIBS += -L../lib -lhde

TRANSLATIONS    = ../files/lng/hde_ar.ts \
                  ../files/lng/hde_nl.ts \
                  ../files/lng/hde_fr.ts \
                  ../files/lng/hde_es.ts \
                  ../files/lng/hde_tr.ts

#CONFIG += qt warn_on
CONFIG    += qt thread 
LANGUAGE	= C++
