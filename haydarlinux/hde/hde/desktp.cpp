/* 
*  File      : desktp.cpp 
*  Written by: haydar@haydar.net
*  Copyright : GPL
*
*  Manages the Desktop
*/

#include <qdir.h>
#include <qpainter.h>
#include <qpixmap.h>
#include <qregexp.h> 
#include <qmessagebox.h>

#include <defaults.h>

#include "desktp.h"
//#include "qapp.h"

#include <X11/cursorfont.h>

//QIntDict <QString> exelist;

QString curItem;

desktp::desktp(QWidget *parent, const char *name, WFlags f) 
    : QIconView(parent, name, f)
{
    setGeometry(0, 0, QApplication::desktop()->width(), 
		QApplication::desktop()->height());
    curItem = QString();


    if(defaults::root_pix.isNull()){ 
	//	qapp::dw->setBackgroundColor(defaults::root_bg);
	QApplication::desktop()->setBackgroundColor(defaults::root_bg);
	setPaletteBackgroundColor(defaults::root_bg);
    }
    
      else{
	  QPixmap tst(defaults::root_pix);
	  QPixmap *rootpix = new QPixmap(QApplication::desktop()->width(), 
					 QApplication::desktop()->height());
	if(defaults::bgDisplay == "Center"){
	    	    
	    
	    QPainter p(rootpix);
	    
	    p.fillRect(0, 0, QApplication::desktop()->width(), 
		       QApplication::desktop()->height(), 
		       QBrush(defaults::root_bg));
	    int xi = (QApplication::desktop()->width()/2) - (tst.width()/2);
	    int yi = (QApplication::desktop()->height()/2) - (tst.height()/2);
	     
	    p.drawPixmap( xi, yi, defaults::root_pix);
	    p.end();
	    
	        
	}else if(defaults::bgDisplay == "Tile"){
	    QPainter p(rootpix);
	    p.fillRect(0, 0, QApplication::desktop()->width(), 
		       QApplication::desktop()->height(), 
		       QBrush(defaults::root_bg));
	    p.drawTiledPixmap( 0, 0, QApplication::desktop()->width(), 
			       QApplication::desktop()->height(), 
			       defaults::root_pix);
	    p.end();


	}else if(defaults::bgDisplay == "Stretch"){
	    QApplication::desktop()->setBackgroundColor(defaults::root_bg);
	    setPaletteBackgroundColor(defaults::root_bg);

	    QImage rootimg(defaults::root_pix);
    
	    rootpix->convertFromImage(rootimg.smoothScale(QApplication::
							  desktop()->width(), 
					QApplication::desktop()->height()));


	}

	    QApplication::desktop()->setBackgroundPixmap(*rootpix);
	    setPaletteBackgroundPixmap(*rootpix);
	    
    }

    
    setFrameStyle(NoFrame);
    setArrangement(TopToBottom);

    //setFocusPolicy(StrongFocus);
    
    setPaletteForegroundColor(Qt::white);
    //exelist.setAutoDelete(TRUE);

    //    rightmnu = new QPopupMenu(this);
    //    setSelectionMode( QIconView::Extended );

    connect( this, SIGNAL( doubleClicked( QIconViewItem * ) ),
	     this, SLOT( iconDblClicked( QIconViewItem * ) ) );
    connect( this, SIGNAL( returnPressed( QIconViewItem * ) ),
	     this, SLOT( iconReturnPressed( QIconViewItem * ) ) );

    connect( this, SIGNAL( rightButtonClicked( QIconViewItem*, const QPoint&)),
	     this, SLOT( rightClicked( QIconViewItem*, const QPoint&)) );

    connect( this, SIGNAL( clicked( QIconViewItem* ) ), this, 
	     SLOT( iconClicked( QIconViewItem* ) ));
    rightmnu = new QPopupMenu;
    rightmnu->insertItem(tr("New"), this, SLOT(newItem()));
    QPopupMenu *ai = new QPopupMenu;

    rightmnu->insertItem(tr("&Arrange icons"), ai);
    rightmnu->insertSeparator();
    rightmnu->insertItem(tr("Properties"), this, SLOT( prop() ));


    //    setDropEnabled(true);
    //    setAcceptDrops( TRUE );
    //setAcceptDrops( TRUE );
    //setAcceptDrags( TRUE );
    setAcceptDrops( true );

    show();

    connect( this, SIGNAL(dropped(QDropEvent*, 
				  const QValueList<QIconDragItem>&)),
	     this, SLOT(slotNewItem(QDropEvent*, 
				    const QValueList<QIconDragItem>&)) );

    refreshIV();
}

void desktp::dragEnterEvent(QDragEnterEvent* event)
{
  cout << "Drag Entered \n";
    event->accept(
        QTextDrag::canDecode(event) ||
        QImageDrag::canDecode(event) ||
	QUriDrag::canDecode(event) ||
	QIconDrag::canDecode(event)
    );
}

void desktp::dropEvent(QDropEvent* event)
{
  /*
  QImage image;
  QString text;

  if ( QImageDrag::decode(event, image) ) {
    insertImageAt(image, event->pos());
  } else if ( QTextDrag::decode(event, text) ) {
    insertTextAt(text, event->pos());
  }
  */
  QIconView::dropEvent(event);
}

void desktp::createIcon(QString path, QString fname){

    if(!QFileInfo::QFileInfo(path + "/" + fname).exists())
	return;

    if (path.right(7) == "Desktop"){
	QString svar;

	//	QIconViewItem *icn = new QIconViewItem( this );
	desktopItem *icn = new desktopItem( (QIconView *) this );

	//	setFocusPolicy(QWidget::ClickFocus);
	hdeItem ditem = defaults::readItem(path + "/" + fname);

	QString iname(ditem.name);

	iname = iname.replace( QRegExp("%2f"), "/" );
	icn->setText(iname);

	svar = ditem.icon;
	svar = getIconFile(svar, "large");
	if (svar == "" || ! QFile( svar ).exists())
	    icn->setPixmap(QPixmap(defaults::get_cfile("images/default.xpm")));
	else
	    icn->setPixmap(QPixmap(svar));
	    //	    icn->setPixmap(QPixmap(svar));
	
	//	svar = ditem.exec;
	svar = path + "/" + fname;
	//	cout << svar <<"\n";

	//exelist.insert( icn->index(), new QString(svar));
	icn->setPath(svar);
	icn->setFilename( fname );
	icn->setDropEnabled(true);
    }

}

void desktp::refreshIV(){

    clear();
        
    
    
    QDir d(defaults::get_cfile("Desktop"));
    d.setFilter( QDir::Files );

    const QFileInfoList *list = d.entryInfoList();
    QFileInfoListIterator it( *list );      // create list iterator
    QFileInfo *fi;                          // pointer for traversing

    while ( (fi=it.current()) ) {           // for each file...
	createIcon(d.path(), fi->fileName());
	++it;                               // go to the next list element
    }

    setAcceptDrops( TRUE );

}


void desktp::timeout(void)
{
    //    QApplication::restoreOverrideCursor();
}

void desktp::iconDblClicked( QIconViewItem *icn ){
  //QString cmd(*exelist.find(icn->index())); 
  desktopItem *ico = (desktopItem *)icn;
  QString cmd(ico->getPath());
  if(cmd.isNull())
    return;

  //    cout << cmd << endl;
  
  /*
    Cursor cur ;
    cur = XCreateFontCursor(qt_xdisplay(), XC_watch);
    XDefineCursor(qt_xdisplay(), winId(), cur);
  */
  execCmd(cmd); // fixed
}

void desktp::iconReturnPressed( QIconViewItem *icn ){
  //    QString cmd(*exelist.find(icn->index())); 
  desktopItem *ico = (desktopItem *)icn;
  QString cmd(ico->getPath());

  if(cmd.isNull())
    return;

  /*
    Cursor cur ;
    cur = XCreateFontCursor(qt_xdisplay(), XC_watch);
    XDefineCursor(qt_xdisplay(), winId(), cur);
  */
  
  //    QApplication::setOverrideCursor( Qt::WaitCursor );
  execCmd(cmd); // fixed
  // QApplication::restoreOverrideCursor();
}

void desktp::rightClicked( QIconViewItem* item, const QPoint& pos )
{

  //if(item)
  //	curItem = item->index();
  //else
  //curItem = 0;
  
  //    QString cmd;
  //    if(item)
  //cmd = *exelist.find(item->index()); 
  if(item){
    //cmd = *exelist.find(item->index()); 
    //    if(cmd.isNull())
    //return;
    
    //QString cmd(*exelist.find(item->index())); 
    desktopItem *ico = (desktopItem *)item;
    QString cmd(ico->getPath());
    if(cmd.isNull())
      rightmnu->exec(pos);
    else{
      curItem = cmd;
      
      cout << curItem << "\n";
      
      QPopupMenu *itemMenu = new QPopupMenu;
      itemMenu->insertItem(tr("&Rename"), this, SLOT( renameItem() ) );
      itemMenu->insertItem(tr("&Delete"), this, SLOT( deleteItem() ) );
      itemMenu->insertSeparator();
      itemMenu->insertItem(tr("&Properties"), this, SLOT( itemProp() ) );
      
      
      itemMenu->exec(pos);
    }
  }else
    rightmnu->exec(pos);
  
}


void desktp::newItem( void )
{
    execCmd( "hadditem" );
}

void desktp::prop( void )
{
    execCmd( "hDisplay" );
}

void desktp::deleteItem()
{

}

void desktp::renameItem()
{

}

void desktp::itemProp( )
{
    //    QString cmd(*exelist.find(item->index())); 
    //    QString cmd(*exelist.find(curItem)); 

    //    QString cmd;

    //    if(curItem != NULL )
    //cmd = *exelist.find(curItem->index());

//    if(cmd.isNull())
//return;
    QString cmd = "deskitem \"" + QFile::encodeName( curItem )+ "\"";
    cout << cmd << endl;
    execCmd( cmd );
}

QDragObject *desktp::dragObject()
{
  /*
    QString cmd(*exelist.find(currentItem()->index())); 

    if(cmd.isNull())
      return new QTextDrag( currentItem()->text(), this );
    

    return new QTextDrag( "file:" +cmd, this );
  */
  //  return new QTextDrag( currentItem()->text(), this );

  if ( !currentItem() )
    return 0;

  //QPoint orig =viewportToContents(viewport()->mapFromGlobal(QCursor::pos()));
  desktopIconDrag *drag = new desktopIconDrag( viewport() );
  /*
  drag->setPixmap( *currentItem()->pixmap(),
		   QPoint( currentItem()->pixmapRect().width() / 2, 
			   currentItem()->pixmapRect().height() / 2 ) );
  */
  drag->setPixmap( *currentItem()->pixmap(),
		   //		   currentItem()->pos() );
		   QPoint(0, 0) );
  for ( desktopItem *item = (desktopItem*)firstItem(); item;
	item = (desktopItem*)item->nextItem() ) {
    if ( item->isSelected() ) {
      QIconDragItem id;
      //      id.setData( QCString( item->filename() ) );
      //      QString cmd(*exelist.find(currentItem()->index()));
      desktopItem *ico = (desktopItem *)currentItem();
      QString cmd(ico->getPath());
      cout << cmd << " is selected" << endl; 
      id.setData( QCString( cmd ) );
      /*
      drag->append( id,
		    QRect( item->pixmapRect( FALSE ).x() - orig.x(),
			   item->pixmapRect( FALSE ).y() - orig.y(),
			   item->pixmapRect().width(),
			   item->pixmapRect().height() ),
		    QRect( item->textRect( FALSE ).x() - orig.x(),
			   item->textRect( FALSE ).y() - orig.y(),
			   item->textRect().width(), 
			   item->textRect().height() ),
		    //		    QString( item->filename() ) );
		    cmd );
      */
      //drag->append( id, item->pixmapRect(false), item->textRect(false), cmd);
      drag->append( id, QRect( item->pixmapRect(true).topLeft(),
			       item->pixmapRect(true).size()),
		    QRect( item->textRect(true).topLeft(),
			   item->textRect(true).size()) ,cmd );

    }
  }

  return drag;

}

/*
bool desktp::acceptDrop( const QMimeSource *mime ) const
{
    if ( mime->provides( "text/plain" ) || mime->provides( "text/uri-list" )
	 || mime->provides( "application/x-qiconlist" ) )
	return TRUE;
    return FALSE;
}
*/

 //void desktp::dropped( QDropEvent *evt, const QValueList<QIconDragItem>& )
void desktp::slotNewItem( QDropEvent *evt, const QValueList<QIconDragItem>& )
{

   if ( !QUriDrag::canDecode( evt ) ) {
        evt->ignore();
        return;
    }

    QStrList lst;
    QUriDrag::decode( evt, lst );

    QString str;
    if ( evt->action() == QDropEvent::Copy )
        str = "Copy\n\n";
    else
        str = "Move\n\n";
    for ( uint i = 0; i < lst.count(); ++i )
        str += QString( "   %1\n" ).arg( lst.at( i ) );
    //str += QString( "\n"
    //              "To\n\n"
    //              "   %1" ).arg( viewDir.absPath() );

    QMessageBox::information( this, evt->action() == QDropEvent::Copy ? "Copy" : "Move" , str, "Not Implemented" );
    if ( evt->action() == QDropEvent::Move )
        QMessageBox::information( this, "Remove" , str, "Not Implemented" );
    evt->acceptAction();
    /*
  QString label;
  
  if ( QTextDrag::decode( evt, label ) ) {
    desktopItem *item = new desktopItem( this );
    item->setText( label );
    item->setRenameEnabled( TRUE );
	cout << label << endl;//setText( label );
  }
    */
  /*
    QString label;
    if ( QTextDrag::decode( evt, label ) )
	cout << label << endl;//setText( label );
  */
}

void desktp::iconClicked( QIconViewItem * item )
{
    if(!item){
      curItem = QString();
      return;
    }
    //    QString cmd(*exelist.find(item->index())); 
    desktopItem *ico = (desktopItem *)item;
    QString cmd(ico->getPath());
    //curItem = cmd;
    //    if(cmd.isNull())
    //return;


    if(cmd.isNull())
	curItem = QString();
    else
	curItem = cmd;
}

/*****************************************************************************
 *
 * Class desktopItem
 *
 *****************************************************************************/


bool desktopItem::acceptDrop( const QMimeSource *mime ) const
{
  if ( mime->provides( "text/plain" ) )
    return TRUE;

  return FALSE;
}

void desktopItem::dropped( QDropEvent *evt, const QValueList<QIconDragItem>& )
{
  /*
  QString label;

  if ( QTextDrag::decode( evt, label ) )
    setText( label );
  */

if ( !QUriDrag::canDecode( evt ) ) {
        evt->ignore();
        return;
    }

    QStrList lst;
    QUriDrag::decode( evt, lst );

    QString str;
    if ( evt->action() == QDropEvent::Copy )
        str = "Copy\n\n";
    else
        str = "Move\n\n";
    for ( uint i = 0; i < lst.count(); ++i )
        str += QString( "   %1\n" ).arg( lst.at( i ) );
    //str += QString( "\n"
    //              "To\n\n"
    //              "   %1" ).arg( filename() );

    QMessageBox::information( iconView(), evt->action() == QDropEvent::Copy ? "Copy" : "Move" , str, "Not Implemented" );
    if ( evt->action() == QDropEvent::Move )
      QMessageBox::information( iconView(), "Remove" , str, "Not Implemented" );
    evt->acceptAction();
}


/*
bool desktopItem::acceptDrop( const QMimeSource *mime ) const
{
  if ( mime->provides( "text/plain" ) || mime->provides( "text/uri-list" )
       || mime->provides( "application/x-qiconlist" ) ){
    //  if ( mime->provides( "text/uri-list" ) ){
    cout << "TRUE\n";
    return TRUE;
  }
    cout << "FALSE\n";
  return FALSE;
}

void desktopItem::dropped( QDropEvent *evt, const QValueList<QIconDragItem>& )
{
  QString label;

  if ( QTextDrag::decode( evt, label ) )
    setText( label );
}
*/



/*****************************************************************************
 *
 * Class desktopIconDrag
 *
 *****************************************************************************/

desktopIconDrag::desktopIconDrag( QWidget * dragSource, const char* name )
    : QIconDrag( dragSource, name )
{
}

const char* desktopIconDrag::format( int i ) const
{
    if ( i == 0 )
        return "application/x-qiconlist";
    else if ( i == 1 )
        return "text/uri-list";
    else
        return 0;
}

QByteArray desktopIconDrag::encodedData( const char* mime ) const
{
    QByteArray a;
    if ( QString( mime ) == "application/x-qiconlist" ) {
        a = QIconDrag::encodedData( mime );
    } else if ( QString( mime ) == "text/uri-list" ) {
        QString s = urls.join( "\r\n" );
        a.resize( s.length() );
        memcpy( a.data(), s.latin1(), s.length() );
    }
    return a;
}

bool desktopIconDrag::canDecode( QMimeSource* e )
{
    return e->provides( "application/x-qiconlist" ) ||
        e->provides( "text/uri-list" );
}

void desktopIconDrag::append( const QIconDragItem &item, const QRect &pr,
                             const QRect &tr, const QString &url )
{
    QIconDrag::append( item, pr, tr );
    urls << url;
}
