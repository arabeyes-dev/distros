HEADERS  =  hnotepad.h ../include/conf.h
SOURCES  =  main.cpp hnotepad.cpp
TARGET   =  hnotepad
SRCMOC   =  moc_hnotepad.cpp
TRANSLATIONS    = lng/hnotepad_ar.ts \
                  lng/hnotepad_nl.ts

LIBS	+= -L../../hde/lib -lhde
INCLUDEPATH += ../../hde/include
