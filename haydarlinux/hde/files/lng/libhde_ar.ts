<!DOCTYPE TS><TS>
<context>
    <name>HCloseFrame</name>
    <message>
        <source>HCloseFrame</source>
        <translation></translation>
    </message>
    <message>
        <source>Caption</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>MUHARRAM</source>
        <translation>محرم</translation>
    </message>
    <message>
        <source>SAFAR</source>
        <translation>صفر</translation>
    </message>
    <message>
        <source>RABI&apos;I</source>
        <translation>ربيع الأول</translation>
    </message>
    <message>
        <source>RABI&apos;II</source>
        <translation>ربيع الثاني</translation>
    </message>
    <message>
        <source>JUMADA&apos;I</source>
        <translation>جمادى الأول</translation>
    </message>
    <message>
        <source>JUMADA&apos;II</source>
        <translation>جمادى الثاني</translation>
    </message>
    <message>
        <source>RAJAB</source>
        <translation>رجب</translation>
    </message>
    <message>
        <source>SHA&apos;BAN</source>
        <translation>شعبان</translation>
    </message>
    <message>
        <source>RAMADAN</source>
        <translation>رمظان</translation>
    </message>
    <message>
        <source>SHAWWAL</source>
        <translation>شوال</translation>
    </message>
    <message>
        <source>DHU AL-QA&apos;DAH</source>
        <translation>ذو القعدة</translation>
    </message>
    <message>
        <source>DHU AL-HIJJAH</source>
        <translation>ذو الحجّة</translation>
    </message>
    <message>
        <source>MHM</source>
        <translation>محر</translation>
    </message>
    <message>
        <source>RII</source>
        <translation>رب2</translation>
    </message>
    <message>
        <source>J0I</source>
        <translation>جم1</translation>
    </message>
    <message>
        <source>JII</source>
        <translation>جم2</translation>
    </message>
    <message>
        <source>RJB</source>
        <translation>رجب</translation>
    </message>
    <message>
        <source>SBN</source>
        <translation>شعب</translation>
    </message>
    <message>
        <source>RMD</source>
        <translation>رمظ</translation>
    </message>
    <message>
        <source>SWL</source>
        <translation>شول</translation>
    </message>
    <message>
        <source>DQD</source>
        <translation>ذقع</translation>
    </message>
    <message>
        <source>DHJ</source>
        <translation>ذحج</translation>
    </message>
</context>
<context>
    <name>itemsFrame</name>
    <message>
        <source>Your Name</source>
        <translation>إسمك</translation>
    </message>
    <message>
        <source>Mozilla Browser</source>
        <translation>متصفح موزيلا</translation>
    </message>
    <message>
        <source>Mozilla mail</source>
        <translation>بريد موزيلا</translation>
    </message>
    <message>
        <source>Run program</source>
        <translation>شغّل برنامج</translation>
    </message>
    <message>
        <source>All Programs</source>
        <translation>جميع البرامج</translation>
    </message>
    <message>
        <source>My Documents</source>
        <translation>المستندات</translation>
    </message>
    <message>
        <source>Recent Docoments</source>
        <translation>المستندات الحديثة</translation>
    </message>
    <message>
        <source>My Pictures</source>
        <translation>مجلّد الرسوم</translation>
    </message>
    <message>
        <source>My Sound Files</source>
        <translation>مجلّد الأصوات</translation>
    </message>
    <message>
        <source>Control Panel</source>
        <translation>لوحة التحكّم</translation>
    </message>
    <message>
        <source>Kill window</source>
        <translation>إنهاء إطار</translation>
    </message>
    <message>
        <source>End Session</source>
        <translation>إنهاء الجلسة</translation>
    </message>
    <message>
        <source>Form1</source>
        <translation></translation>
    </message>
</context>
</TS>
