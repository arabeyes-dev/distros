/*
*  File      : procbar.cpp
*  Copyright : GPL
*  Written by: haydar@haydar.net , alinden@gmx.de
*  Creates the buttons for iconified windows
*/

#include "defs.h"
#include "defaults.h"
#include "procbar.h"
#include "qapp.h"
#include <qsizepolicy.h> 

// max. button width
#define max_bwidth 200


tbutton::tbutton(QWidget *parent, const char *name): wbutton(parent, name)
{
    //    buttonTip = NULL;
}

void tbutton::paintEvent( QPaintEvent *pe ){
    if(QFileInfo(qapp::get_cfile(defaults::tbButton_bg)).isFile()){
	QPainter *p = new QPainter(this);
	if(QFileInfo(qapp::get_cfile(defaults::toolbar_bg)).isFile()){
	    //	    QPixmap qp(width(), height());
	    //QPainter painter(&qp);

	    QImage img(qapp::get_cfile(defaults::toolbar_bg));
	    if(! img.isNull())  // make scaled pixmap
	    {
		QPixmap pix;
		pix.convertFromImage(img.smoothScale(width(), height() ));
		//painter.drawPixmap(0,0, pix);

		//painter.end();
		//setBackgroundPixmap(qp);
		p->drawPixmap(0,0, pix);
	    }
	
	}else{
	    //	QPainter *p = new QPainter(this);
	
	    int h = (height() - 2)/2; // (height() - 4)/2;
	    QColor firstColor(palette().active().background());
	    p->setPen(firstColor.dark(100+h*2));
	    p->drawLine(0,0 , width(), 0); 

	    p->setPen(firstColor.light(100+h*2));
 
	    for ( int i = 1; i < 3; ++i ) {
		p->setPen(firstColor.light(100+(h*2)-i));
		p->drawLine(0,i , width(), i); 
	    }

	    for ( int i = 0; i < h-1; ++i ) {
		p->setPen(firstColor.dark(100 + ((h*2)-3)-(i*2)));
		p->drawLine(0,i+4 , width(), i+4); 
	    }


	    int j = 0;

	    for ( int i = height()-h; i < height()-1 ; ++i ) {
		j++;
		p->setPen(firstColor.light(100 +(j*2)));
		p->drawLine(0, i , width(), i); 
	    }


	    p->setPen(firstColor.dark(100+(h*2)));
 
	    for (int i = height()-2; i < height(); ++i ) {
		p->drawLine(0,i , width(), i); 
	    }
	}

	QImage img(qapp::get_cfile(defaults::tbButton_bg));
	if( (isDown() || isOn()) && 
	   (QFileInfo(qapp::get_cfile(defaults::tbButtondown_bg)).isFile()))
	    img = QImage(qapp::get_cfile(defaults::tbButtondown_bg));
	else if(hasMouse() && 
	   (QFileInfo(qapp::get_cfile(defaults::tbButtonmo_bg)).isFile()))
	    img = QImage(qapp::get_cfile(defaults::tbButtonmo_bg));

	if(! img.isNull())  // make scaled pixmap
	{
	    QPixmap pix;
	    //	if((defaults::startheight < 10) 
	    if( ((defaults::starticonplace == "TOP") || (defaults::starticonplace == "BUTTOM")) && (defaults::tbButtonHeight > 9) )
		pix.convertFromImage(img.smoothScale(width(), defaults::tbButtonHeight));
	    else
		pix.convertFromImage(img.smoothScale(width(), height()));

	    if( (defaults::starticonplace == "TOP")
		&& (defaults::tbButtonHeight > 9) ){
		p->drawPixmap(0,0, pix);
		p->drawPixmap(2,2, *pixmap());
	    }else if ( (defaults::starticonplace == "BUTTOM") 
		       && (defaults::tbButtonHeight > 9) ){
		p->drawPixmap(0, height()-defaults::tbButtonHeight, pix);
		p->drawPixmap(2,height()-defaults::tbButtonHeight +2, 
			      *pixmap());
	    }else{
		p->drawPixmap(0,0, pix);
		p->drawPixmap(2,2, *pixmap());
	    }
	p->end();
	}


    }else
	QPushButton::paintEvent(pe);
}

procbar::procbar(QWidget *parent, const char *name) : QWidget(parent, name)
{
    wlist.setAutoDelete(TRUE);
    setFont(defaults::toolbarfont);
    //    setBackgroundMode(Qt::X11ParentRelative); 
}

QPixmap *procbar::make_pixmap(const QString &txt, QPixmap *pix)
{
    /*
    int txtstart = 4;
    int h=height()-4;
    int hi=height();

    if( (defaults::starticonplace == "TOP") ||(defaults::starticonplace == "BUTTOM") && (defaults::tbButtonHeight > 9) ){
	h=defaults::tbButtonHeight-4;
	hi= defaults::tbButtonHeight;
    }

	QPixmap *newpix = new QPixmap(max_bwidth, hi );

    Q_CHECK_PTR(newpix);
    newpix->fill(foregroundColor());
    
    QBitmap mask(max_bwidth, hi, TRUE);  // all transparent
    QPainter painter(&mask);

	

    painter.setFont(font());
    
    if(! pix->isNull())   // draw scaled pixmap
    {
	bitBlt(newpix, 2, 2, pix, 0, 0);
	bitBlt(&mask, 2, 2, pix->mask(), 0, 0);
	txtstart = h+4;
    }

    //
    //
    //
    setPaletteForegroundColor(QColor("white"));
    //
    //
    //
    painter.drawText(txtstart, 1, max_bwidth-txtstart-6, h-2, 
		     AlignLeft|AlignVCenter, txt);
    painter.end();
    newpix->setMask(mask);
    */
    setPaletteForegroundColor(defaults::tbButton_fg);
    int txtstart = 4;
    int h=height()-4;
    int hi=height();

    if( (defaults::starticonplace == "TOP") ||(defaults::starticonplace == "BUTTOM") && (defaults::tbButtonHeight > 9) ){
	h=defaults::tbButtonHeight-4;
	hi= defaults::tbButtonHeight;
    }

    QPixmap *newpix = new QPixmap(max_bwidth, height() );

    Q_CHECK_PTR(newpix);
    newpix->fill(foregroundColor());

    QBitmap mask(max_bwidth, height(), TRUE);  // all transparent
    QPainter painter(&mask);

	

    painter.setFont(font());
    
    if(! pix->isNull())   // draw scaled pixmap
    {
	bitBlt(newpix, 2, 2, pix, 0, 0);
	bitBlt(&mask, 2, 2, pix->mask(), 0, 0);
	txtstart = h+4;
    }

    //setPaletteBackgroundColor(QColor("white"));
    painter.setBackgroundColor ( defaults::tbButton_fg);

    QRect cr(txtstart+1, 2, max_bwidth-txtstart-6, h-2);
    /*
    painter.setPen(QColor("#060606"));
    painter.setBrush(QColor("#060606"));
    painter.drawText( cr, AlignLeft|AlignVCenter, txt);
    */
    
    cr.moveBy(-2, -2);
    painter.setPen(QColor("#FBFBFB"));
    painter.setBrush(QColor("#FBFBFB"));
    painter.drawText( cr, AlignLeft|AlignVCenter, txt);
    //    painter.drawText(txtstart, 1, max_bwidth-txtstart-6, h-2, 
    //		     AlignLeft|AlignVCenter, txt);
    
    painter.end();

    newpix->setMask(mask);

    /*
    QPainter painter1(newpix);
    painter1.setPen(Qt::white);
    painter1.drawText( cr, AlignLeft|AlignVCenter, txt);
    //    painter1.drawText(txtstart, 1, max_bwidth-txtstart-6, h-2, 
    //	     AlignLeft|AlignVCenter, txt);

    painter1.end();
    */

    return(newpix);
}


void procbar::checkWindows(void){


}

void procbar::change_palette(const QPalette &bpal, xwindow *win)
{
    WINLIST *w;
	
    for(w=wlist.first(); w != NULL; w=wlist.next())
	if(w->win == win)
	    break;
	
    if(w == NULL)
	return;
	
    w->button->setPalette(bpal);
}

void procbar::change_text(QString *icname, xwindow *win)
{	
    WINLIST *w;
	
    for(w=wlist.first(); w != NULL; w=wlist.next())
	if(w->win == win)
	    break;
	
    if(w == NULL)
	return;
	
    delete w->pixmap;
    w->pixmap = make_pixmap(*icname, win->icon());
    draw_button(w);
}

bool procbar::remove_fg(void)   // return FALSE if nothing to delete
{
    WINLIST *w;
	
    for(w=wlist.first(); w != NULL; w=wlist.next())
    {
	if(w->win->isVisible())
	{
	    delete w->button;
	    delete w->pixmap;
	    wlist.remove(w);
			
	    return(TRUE);
	}
    }
    return(FALSE);
}

void procbar::draw_button(WINLIST *w)
{
    QPixmap pix(bwidth, height());
    bitBlt(&pix, 0, 0, w->pixmap, 0, 0);

    QBitmap mask(bwidth, height());
    bitBlt(&mask, 0, 0, w->pixmap->mask(), 0, 0);
    pix.setMask(mask);

    w->button->setPixmap(pix);
}

void procbar::draw_buttons(void)
{
    WINLIST *w;
	
    if(wlist.isEmpty())
	return;
	
    setUpdatesEnabled(FALSE);
		
    int h = height();
    int pos = 0;     // current position
    bwidth = ((width()-3)/wlist.count())-2;   // button width

    if(bwidth < h)         // scroll on overflow
    {
	bwidth = h;

	int tsize = wlist.count()*(h+1);
	int maxscr = ((tsize-width())/(h+1))+1;
	w=wlist.first();
	
	for(int i=0; i < maxscr; i++)
	{
	    w->button->hide();
	    w = wlist.next();
	}
	
    }else{
	w = wlist.first();
	
	if(bwidth > max_bwidth)
	    bwidth = max_bwidth-1;
    }
	
    if(QApplication::reverseLayout()){
	pos = width()-bwidth -2;
	while(w != NULL){
	    w->button->show();
	    if((defaults::tbButtonHeight>9)&& 
	       (defaults::starticonplace != "MIDDLE"))
		w->button->setGeometry(pos, 0, bwidth, h);
	    else
		w->button->setGeometry(pos, 3, bwidth, h-6);
	    draw_button(w);

	    if(! w->button->isVisible())
		w->button->show();

	    pos -= bwidth+2;
	    w = wlist.next();
	}
    }else{
	pos = pos + 2;
	while(w != NULL){
	    w->button->show();
	    if((defaults::tbButtonHeight>9)&& 
	       (defaults::starticonplace != "MIDDLE"))
		w->button->setGeometry(pos, 0, bwidth, h);
	    else
		w->button->setGeometry(pos, 3, bwidth, h-6);
	    draw_button(w);

	    if(! w->button->isVisible())
		w->button->show();

	    pos += bwidth+2;
	    w = wlist.next();
	}
    }
    setUpdatesEnabled(TRUE);
}

void procbar::add(xwindow *win)
{
	WINLIST *w;
	
	if(win == NULL)
		return;
	
	for(w = wlist.first(); w != NULL; w = wlist.next())
	{
	    if(w->win == win)    // window already on bar
	    {
		w->button->setOn(FALSE);
		return;
	    }
	}
	
	//	int i = wlist.count(); 
	//while(i--*max_bwidth > width() && remove_fg());
	
	w = new WINLIST;
	Q_CHECK_PTR(w);
	w->button = new tbutton(this, "procbar_button");
	Q_CHECK_PTR(w->button);

	w->button->lower();  // last to redraw
	w->pixmap = make_pixmap(win->icaption(), win->icon());
	w->win = win;

	w->button->setToggleButton(TRUE);
	connect(w->button, SIGNAL(toggled(bool)), win, SLOT(state(bool)));
	connect(w->button, SIGNAL(  right_press() ), win, SLOT(menuexec2() ));

	wlist.append(w);
	draw_buttons();
}

void procbar::set_on(xwindow *win, bool onstat)
{
	WINLIST *w;
	
	for(w = wlist.first(); w != NULL; w = wlist.next())
	{
	    disconnect(w->button, SIGNAL(toggled(bool)), w->win, 
		       SLOT(state(bool)));
	    w->button->setOn(false);
		
	    if(w->win == win)    // window on bar
	    {

		if(onstat)
		    w->button->setOn(true);
		else
		    w->button->setOn(false);

		connect(w->button, SIGNAL(toggled(bool)), win, 
			SLOT(state(bool)));
		//break;
	    }
	    connect(w->button, SIGNAL(toggled(bool)), w->win, 
		    SLOT(state(bool)));
	}
	return;
}	

void procbar::resizeEvent(QResizeEvent *)
{
    if(QFileInfo(qapp::get_cfile(defaults::toolbar_bg)).isFile()){
	QPixmap qp(width(), height());
	QPainter painter(&qp);

	QImage img(qapp::get_cfile(defaults::toolbar_bg));
	if(! img.isNull())  // make scaled pixmap
	{
	    QPixmap pix;
	    pix.convertFromImage(img.smoothScale(width(), height() ));
	    painter.drawPixmap(0,0, pix);

	    painter.end();
	    setBackgroundPixmap(qp);
	}
	
    }else if ( defaults::taskStyle == "hde" ){
	QPixmap qp(width(), height());
	QPainter painter(&qp);
	QColor firstColor(palette().active().background());

	qp.fill(firstColor);

	int i = 0;

	int h = (height() - 4) / 2;

	//
	//


	painter.setPen(firstColor.dark(100+h*2));
	painter.drawLine(0,0 , width(), 0); 

	painter.setPen(firstColor.light(100+h*2));
 
	for ( i = 1; i < 3; ++i ) {
	    painter.setPen(firstColor.light(100+(h*2)-i));
	    painter.drawLine(0,i , width(), i); 
	}

	for ( i = 0; i < h-1; ++i ) {
	    painter.setPen(firstColor.dark(100 + ((h*2)-3)-(i*2)));
	    painter.drawLine(0,i+4 , width(), i+4); 
	}



	int j = 0;

	for ( i = height()-h; i < height()-1 ; ++i ) {
	    j++;
	    painter.setPen(firstColor.light(100 +(j*2)));
	    painter.drawLine(0, i , width(), i); 
	}


	painter.setPen(firstColor.dark(100+(h*2)));
 
	for ( i = height()-2; i < height(); ++i ) {
	    painter.drawLine(0,i , width(), i); 
	}



	if(QApplication::reverseLayout()){
	    painter.setPen(firstColor.dark(150).dark(120));
	    painter.drawLine(0,0 , 0, height()); 
	}else{
	    painter.setPen(firstColor.dark(150).dark(120));
	    painter.drawLine(width()-1,0 , width()-1, height()); 
	}

	setBackgroundPixmap(qp);
	
	painter.end();
    }else{
	QPixmap qp(width(), height());
	QColor firstColor(palette().active().background());
	qp.fill(firstColor);
	QPainter paint(&qp);

	paint.setPen(firstColor.light(130));
	paint.drawLine(0 ,0 , width(), 0); 
	paint.drawLine(0 ,1 , width(), 1); 

	setBackgroundPixmap(qp);
	paint.end();	
    }

    draw_buttons();

}

void procbar::paletteChange(const QPalette &)
{
	WINLIST *w;

	for(w=wlist.first(); w != NULL; w = wlist.next())
	{
		delete w->pixmap;
		w->pixmap = make_pixmap(w->win->icaption(), w->win->icon());
	}	
	
	draw_buttons();
}

void procbar::remove(xwindow *win)   // remove from bar
{
	WINLIST *w;

	for(w=wlist.first(); w != NULL && w->win != win; w = wlist.next());

	if(w == NULL)
		return;

	delete w->button;
	delete w->pixmap;
	wlist.remove();

	draw_buttons();
}

void procbar::remove_all(void)
{
	WINLIST *w;

	for(w=wlist.first(); w != NULL; w = wlist.next())
	{
		delete w->button;
		delete w->pixmap;
	}
	wlist.clear();
}

procbar::~procbar(void)
{
	WINLIST *w;
	
	for(w=wlist.first(); w != NULL; w = wlist.next())
		delete w->pixmap;
}
