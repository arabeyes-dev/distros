/****************************************************************************
****************************************************************************/

#ifndef ITEMSFRAME_H
#define ITEMSFRAME_H

#include <qvariant.h>
#include <qpixmap.h>
#include <qframe.h>
#include "smenuitem.h"

class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;
//class QFrame;
class QLabel;

class perWidget : public QWidget
{
    Q_OBJECT

 public:
    perWidget( QWidget* parent = 0, const char* name = 0 );
    ~perWidget(){};
    void setPixmap(QPixmap pix){ pixmap = pix; drawIt();};
    void setText(QString txt){text = txt; drawIt();};
    void drawIt();

 protected:
    virtual void paintEvent( QPaintEvent * ){drawIt();};

 private:
    QPixmap pixmap;
    QString text;
};

class itemsFrame : public QFrame
{
    Q_OBJECT

public:
    itemsFrame( QWidget* parent = 0, const char* name = 0, WFlags fl = 0 );
    ~itemsFrame();

    void setPgramsMenu(QPopupMenu *pp);
    void readRecents();

    perWidget* personalFrame;
    QLabel* pixmapLabel1;
    QLabel* textLabel1;
    QFrame* programsFrame;

    //    QLabel* internetBrowser;
    sMenuItem *internetBrowser;
    sMenuItem *emailClient;
    QFrame* seperator;
    sMenuItem* recent1;
    sMenuItem* recent2;
    sMenuItem* recent3;
    sMenuItem* recent4;
    sMenuItem* recent5;
    sMenuItem* recent6;
    sMenuItem* runItem;
    QFrame* seperator1;
    sMenuItem* programs;
    QFrame* configsFrame;
    sMenuItem* docLbl;
    sMenuItem* recentLbl;
    sMenuItem* picLbl;
    sMenuItem* soundLbl;
    QFrame* seperator_2;
    sMenuItem* controlLbl;
    QFrame* exitFrame;
    sMenuItem* exitItem;
    sMenuItem* killItem;

protected:
    QPopupMenu *progsPop;
    QVBoxLayout* layout9;
    QHBoxLayout* layout1;
    QHBoxLayout* layout7;
    QVBoxLayout* layout4;
    QVBoxLayout* layout6;
    QHBoxLayout* layout8;

protected slots:
    virtual void languageChange();

 public slots:
    void closeMenu(int i){close();};
};

#endif // ITEMSFRAME_H
