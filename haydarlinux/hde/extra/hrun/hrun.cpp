/****************************************************************************
** Form implementation generated from reading ui file 'hrun.ui'
**
** Created: Thu Aug 16 01:33:30 2001
**      by:  The User Interface Compiler (uic)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/
#include "hrun.h"
#include <qtranslator.h>
#include <qfiledialog.h> 

//#include <X11/cursorfont.h>

static const char* const image0_data[] = { 
"32 32 13 1",
". c None",
"a c #000000",
"c c #000080",
"g c #0000ff",
"h c #008000",
"j c #00ff00",
"i c #00ffff",
"# c #303030",
"e c #800000",
"k c #800080",
"b c #808080",
"f c #ff0000",
"d c #ffffff",
"................................",
"................................",
"................................",
"................................",
"................................",
"........#aaaaaaaaaaaaaaaaaa#....",
"........abbbbbbbbbbbbbbbbbba....",
"........ab.................a....",
"........ab.cccccccccdadada.a....",
"........ab.cccccccccaaaaaa.a....",
"........ab.................a....",
"....aaaaab.ddddddddddddddd.a....",
"...aeeeeeeeeeadddddddddddd.a....",
"...aef..fffffaddgdddddhddd.a....",
"....aaaaaaaaadddiggdddhhdd.a....",
"....abdd...badddgg.ddhjgdd.a....",
"....abdd...baddk.kkddhhhdd.a....",
"....ab.b.a.baddddddddddddd.a....",
".....ab.a.baddddbbbddbbbdd.a....",
"......abdbaddddddddddddddd.a....",
"......abdbaddddddddddddddd.a....",
".....ab.b.ba...............a....",
"....ab.d...baaaaaaaaaaaaaaa#....",
"....abdda..ba...................",
"....abda.a.ba...................",
"....aaaaaaaaa...................",
"...aef..fffffa..................",
"....aaaaaaaaa...................",
"................................",
"................................",
"................................",
"................................"};


/* 
 *  Constructs a hrun which is a child of 'parent', with the 
 *  name 'name' and widget flags set to 'f'.
 *
 *  The dialog will by default be modeless, unless you set 'modal' to
 *  TRUE to construct a modal dialog.
 */
hrun::hrun( QWidget* parent,  const char* name, bool modal, WFlags fl )
    : QDialog( parent, name, modal, fl )
{
    QPixmap image0( ( const char** ) image0_data );
    if ( !name )
	setName( "hrun" );
    //resize( 390, 136 ); 
    setFixedSize( 390, 136 );
    setCaption( trUtf8( "Run", "" ) );
    setIcon( image0 );
    setIconText( trUtf8( "Run", "" ) );

    //    defaults::read_config();

    //    QWidget* privateLayoutWidget = new QWidget( this, "Layout4" );
    //privateLayoutWidget->setGeometry( QRect( 0, 0, 390, 137 ) ); 
    Layout4 = new QVBoxLayout( this, 10, 14, "Layout4" ); 
    //    Layout4->setSpacing( 14 );
    //Layout4->setMargin( 10 );

    descLbl = new QLabel( this, "descLbl" );
    descLbl->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)5, (QSizePolicy::SizeType)5, 0, 0, descLbl->sizePolicy().hasHeightForWidth() ) );
    descLbl->setBackgroundOrigin( QLabel::ParentOrigin );
    descLbl->setAutoMask( TRUE );
    descLbl->setLineWidth( 1 );
    descLbl->setText( trUtf8( "Please Enter the program to be run", "" ) );
    descLbl->setTextFormat( QLabel::AutoText );
    descLbl->setAlignment( int( QLabel::WordBreak | QLabel::AlignAuto | QLabel::AlignCenter ) );
    Layout4->addWidget( descLbl );

   Layout3 = new QHBoxLayout( 0, 0, 6, "Layout3"); 
   //    Layout3 = new QHBoxLayout; 
   //    Layout3->setSpacing( 6 );
   //    Layout3->setMargin( 0 );

    openLbl = new QLabel( this, "openLbl" );
    openLbl->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)4, (QSizePolicy::SizeType)4, 0, 0, openLbl->sizePolicy().hasHeightForWidth() ) );
    openLbl->setText( trUtf8( "Open: ", "" ) );
    Layout3->addWidget( openLbl );

    OpenCombo = new QComboBox( true, this, "OpenCombo" );
    OpenCombo->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)1, (QSizePolicy::SizeType)1, 0, 0, OpenCombo->sizePolicy().hasHeightForWidth() ) );
    //    OpenCombo->setEditable( TRUE );
    OpenCombo->setAutoCompletion( true );
    OpenCombo->setInsertionPolicy(QComboBox::AtTop);
    OpenCombo->setDuplicatesEnabled( FALSE );
    Layout3->addWidget( OpenCombo );
    Layout4->addLayout( Layout3 );

   Layout4_2 = new QHBoxLayout( 0, 0, 2, "Layout4_2"); 
   //    Layout4_2 = new QHBoxLayout; 
   //    Layout4_2->setSpacing( 3 );
   //    Layout4_2->setMargin( 0 );

    xtermChk = new QCheckBox( this, "xtermChk" );
    xtermChk->setText( trUtf8( "Use Xterm") );
    Layout4_2->addWidget( xtermChk );
    QSpacerItem* spacer = new QSpacerItem( 20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    Layout4_2->addItem( spacer );

    okBtn = new QPushButton( this, "okBtn" );
    okBtn->setText( trUtf8( "Ok" ) );
    okBtn->setDefault( true );
    QToolTip::add( okBtn, "Press OK to run the program");
    Layout4_2->addWidget( okBtn );

    cancelBtn = new QPushButton( this, "cancelBtn" );
    cancelBtn->setText( trUtf8( "Cancel", "" ) );
    Layout4_2->addWidget( cancelBtn );

    browseBtn = new QPushButton( this, "browseBtn" );
    browseBtn->setText( trUtf8( "Browse", "" ) );
    Layout4_2->addWidget( browseBtn );
    Layout4->addLayout( Layout4_2 );





    // signals and slots connections
    connect( okBtn, SIGNAL( clicked() ), this, SLOT( OKpressed() ) );
    connect( cancelBtn, SIGNAL( clicked() ), this, SLOT( Cancelpressed() ) );
    connect( browseBtn, SIGNAL( clicked() ), this, SLOT(browsePressed()));

    connect( (const QObject*)OpenCombo->lineEdit(), SIGNAL( returnPressed() ),
	     this, SLOT(OKpressed()) );

    // tab order
    setTabOrder( OpenCombo, okBtn );
    setTabOrder( okBtn, browseBtn );
    setTabOrder( browseBtn, cancelBtn );
    setTabOrder( cancelBtn, xtermChk );

    init();
}

void hrun::OKpressed(){
    //    QApplication::setOverrideCursor( Qt::WaitCursor );
    /*
Cursor cur ;
 cur = XCreateFontCursor(qt_xdisplay(), XC_watch);
 XDefineCursor(qt_xdisplay(), QApplication::desktop()->winId(), cur);
    */
    /*
    if(xtermChk->isChecked())
	execCmd("xterm -e " + OpenCombo->currentText() + " &");	
    else
	execCmd(OpenCombo->currentText() + " &" );
    */
    

    QProcess *proc = new QProcess( this );
    if(xtermChk->isChecked()){
	proc->addArgument( "xterm" );
	proc->addArgument( "-e" );
    }
    proc->addArgument( OpenCombo->currentText() );
    //    proc->addArgument( "&" );

    connect( proc, SIGNAL( processExited() ),
	     this, SLOT( runFinished()) );

    if ( !proc->start() ) {
	// error handling
	QApplication::restoreOverrideCursor();
    }
    

    hide();
    QTimer::singleShot( 3000, this, SLOT( runFinished() ) );
    //    QApplication::restoreOverrideCursor();
}

void hrun::runFinished(){
    //    QApplication::restoreOverrideCursor();
    /*
    Cursor cur ;
    cur = XCreateFontCursor(qt_xdisplay(), XC_arrow);
    XDefineCursor(qt_xdisplay(), QApplication::desktop()->winId(), cur);
    */
    saveChanges();
    //    close();
    qApp->quit();
}


void hrun::Cancelpressed()
{
    qApp->quit();
}

void hrun::browsePressed()
{
    QString s = QFileDialog::getOpenFileName( "~/" );
    OpenCombo->insertItem( s, 0 );
  
}

void hrun::saveChanges(){
    if(!QFileInfo( defaults::cfdir ).exists() )
	execCmd( "mkdir -p " + defaults::cfdir );
	
    QString cmp( defaults::cfdir + "/hrunfiles" );
    QFile file( cmp);
    if ( file.open( IO_WriteOnly ) ) {
	QTextStream stream( &file );
	for ( int ival = 0; ival < OpenCombo->count(); ival++)
	    stream << OpenCombo->text(ival) << "\n";

	file.close();


    }

}

void hrun::init(){

    if(defaults::cfdir.isNull()){
	QString fname(getenv("HOME"));

	if(! fname.isNull())
	    defaults::cfdir = fname + "/.hde";
    }
    QString cmp( defaults::cfdir + "/hrunfiles" );
    QFile file( cmp);
    if ( file.open( IO_ReadOnly ) ) {
	QTextStream stream( &file );
	QString line;
	while ( !stream.eof() ) {
	    line = stream.readLine();
	    OpenCombo->insertItem( line );
	}
	file.close();


    }
}


/*  
 *  Destroys the object and frees any allocated resources
 */
hrun::~hrun()
{
}
