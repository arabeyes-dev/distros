/****************************************************************************
****************************************************************************/

#include "startmenu.h"
#include "smenuitem.h"

#include <qvariant.h>
#include <qframe.h>
#include <qlabel.h>
#include <qpushbutton.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>
#include <qimage.h>
#include <qpixmap.h>
#include <qapplication.h>
//#include "defaults.h"
#include <qpainter.h>
#include <libhwm.h>
#include <defaults.h>

#include <iostream>
using std:: cout; using std::endl;

perWidget::perWidget( QWidget* parent, const char* name )
    : QWidget( parent, name )
{
}

void perWidget::drawIt()
{
  QPainter *p = new QPainter( this );
  QImage img( get_cfile( "images/menutop.png") );
  if(QApplication::reverseLayout())
      img = img.mirror(TRUE,FALSE);
  QPixmap pix;
  pix.convertFromImage(img);

  if(! pixmap.isNull()){
      if ( !pix.mask() )
	  if ( img.hasAlphaBuffer() ) {
	      QBitmap bm;
	      bm = img.createAlphaMask();
	      pix.setMask( bm );
	  } else {
	      QBitmap bm;
	      bm = img.createHeuristicMask();
	      pix.setMask( bm );
	  }
  }

  p->drawPixmap ( 0, 0, pix);//, width()-1, height()-1);

  setFont( QFont(font().family(), 18, QFont::Bold) );

  int i = 0;
  if(QApplication::reverseLayout()){
      if(! pixmap.isNull()){
	  //pixmap.resize(48,48);
	  p->drawPixmap( width() - 58, 10, pixmap);
	  i = 58;
      }

      if(!text.isEmpty()){
	  p->setPen( Qt::white );
	  p->drawText( 10 +1, (height()/2 ) - (QFontMetrics(font()).height()/2)
		       +1, width() - i - 20, height(), Qt::AlignRight, text );

	  p->setPen( Qt::black );
	  p->drawText( 10 -1, (height()/2 ) - (QFontMetrics(font()).height()/2)
		       -1, width() - i - 20, height(), Qt::AlignRight, text );

      }
  }else{
      if(! pixmap.isNull()){
	  p->drawPixmap( 10, 10, pixmap);
	  i = 58;
      }

      if(!text.isEmpty()){
	  p->setPen( Qt::white );
	  p->drawText( i + 10 + 1, (height()/2 ) - 
		       (QFontMetrics(font()).height()/2) +1, width() - 20, 
		       height(), Qt::AlignAuto, text );

	  p->setPen( Qt::black );
	  p->drawText( i + 10 -1, (height()/2 ) - 
		       (QFontMetrics(font()).height()/2) -1, width() - 20, 
		       height(), Qt::AlignAuto, text );

      }
  }


  p->end();

  if ( pix.mask() )
      setMask( *pix.mask() );
}


/* 
 *  Constructs a itemsFrame as a child of 'parent', with the 
 *  name 'name' and widget flags set to 'f'.
 */
itemsFrame::itemsFrame( QWidget* parent, const char* name, WFlags fl )
    : QFrame( parent, name, fl )
      //    : QFrame( parent, name, fl ),
      //      image0( (const char **) image0_data )
{
    if ( !name )
	setName( "itemsFrame" );
    
    setFixedSize( QSize(380, 486) );
    setFrameShape( QFrame::NoFrame );


    layout9 = new QVBoxLayout( this, 0, 0, "layout9"); 

    personalFrame = new perWidget( this, "personalFrame" );
    personalFrame->setFixedSize( QSize( 380, 65 ) );

    QImage img( get_cfile( "images/hl.png") );
    QPixmap pix;
    pix.convertFromImage(img.smoothScale(48,48));
    personalFrame->setPixmap( pix );

    layout9->addWidget( personalFrame );


    layout7 = new QHBoxLayout( 0, 0, 0, "layout7"); 

    programsFrame = new QFrame( this, "programsFrame" );
    programsFrame->setFixedSize( QSize( 190, 381 ) );
    programsFrame->setFrameShape( QFrame::NoFrame );
    programsFrame->setFrameShadow( QFrame::Plain );

    img = QImage( get_cfile( "images/progsmenu.png") );
    if(QApplication::reverseLayout())
      img = img.mirror(TRUE,FALSE);
    //QPixmap pix;
    pix.convertFromImage(img);
    programsFrame->setPaletteBackgroundPixmap( pix ); 


    layout4 = new QVBoxLayout( programsFrame, 0, 6, "layout4"); 

    layout4->addSpacing(5);

    internetBrowser = new sMenuItem( programsFrame, "internetBrowser" );
    //internetBrowser->setFixedSize( QSize( 180, 32 ) );
    internetBrowser->setMinimumSize( QSize( 180, 35 ) );
    internetBrowser->setCommand("mozilla");
    internetBrowser->setPixmap(QPixmap(get_ifile("mozilla-icon.png", "mid")));

    layout4->addWidget( internetBrowser );
    internetBrowser->connectItem( this, SLOT(close()) );

    emailClient = new sMenuItem( programsFrame, "emailClient" );
    emailClient->setMinimumSize( QSize( 180, 35 ) );
    emailClient->setCommand("mozilla -mail");
    emailClient->setPixmap(QPixmap(get_ifile("mozilla-mail-icon.png", "mid")));
    layout4->addWidget( emailClient );
    emailClient->connectItem( this, SLOT(close()) );

    seperator = new QFrame( programsFrame, "seperator" );
    seperator->setFrameShape( QFrame::HLine );
    seperator->setFrameShadow( QFrame::Sunken );
    // seperator->setFixedSize( QSize( 180, 10 ) );
    layout4->addWidget( seperator );

    recent1 = new sMenuItem( programsFrame, "recent1" );
    recent1->setMinimumSize( QSize( 180, 35 ) );
    layout4->addWidget( recent1 );
    recent1->connectItem( this, SLOT(close()) );

    recent2 = new sMenuItem( programsFrame, "recent2" );
    recent2->setMinimumSize( QSize( 180, 35 ) );
    layout4->addWidget( recent2 );
    recent2->connectItem( this, SLOT(close()) );

    recent3 = new sMenuItem( programsFrame, "recent3" );
    recent3->setMinimumSize( QSize( 180, 35 ) );
    layout4->addWidget( recent3 );
    recent3->connectItem( this, SLOT(close()) );

    recent4 = new sMenuItem( programsFrame, "recent4" );
    recent4->setMinimumSize( QSize( 180, 35 ) );
    layout4->addWidget( recent4 );
    recent4->connectItem( this, SLOT(close()) );

    recent5 = new sMenuItem( programsFrame, "resent5" );
    recent5->setMinimumSize( QSize( 180, 35 ) );
    layout4->addWidget( recent5 );
    recent5->connectItem( this, SLOT(close()) );

    recent6 = new sMenuItem( programsFrame, "recent6" );
    recent6->setMinimumSize( QSize( 180, 35 ) );
    layout4->addWidget( recent6 );
    recent6->connectItem( this, SLOT(close()) );

    layout4->addStretch(100);

    seperator1 = new QFrame( programsFrame, "seperator1" );
    seperator1->setFrameShape( QFrame::HLine );
    seperator1->setFrameShadow( QFrame::Sunken );
    
    //    seperator1->setFixedSize( QSize( 180, 10 ) );
    layout4->addWidget( seperator1 );

    programs = new sMenuItem( programsFrame, "programs" );
    programs->setMinimumSize( QSize( 180, 20 ) );
    progsPop = new QPopupMenu;
    programs->setPopup(progsPop);
    programs->setPixmap(QPixmap(get_ifile("images/grp.xpm", "mid")));
    layout4->addWidget( programs );

    layout4->addSpacing( 7 );

    layout7->addWidget( programsFrame );

    configsFrame = new QFrame( this, "configsFrame" );
    configsFrame->setFixedSize( QSize( 190, 381 ) );
    configsFrame->setFrameShape( QFrame::NoFrame );
    configsFrame->setFrameShadow( QFrame::Raised );

    img = QImage( get_cfile( "images/confsmenu.png" ) );
    if(QApplication::reverseLayout())
      img = img.mirror(TRUE,FALSE);
    pix.convertFromImage(img);
    configsFrame->setPaletteBackgroundPixmap( pix ); 
    //    configsFrame->setPaletteBackgroundPixmap( QPixmap::fromMimeSource( "confs.png" ));


    layout6 = new QVBoxLayout( configsFrame, 0, 6, "layout6"); 

    docLbl = new sMenuItem( configsFrame, "docLbl" );
    docLbl->setMinimumSize( QSize( 180, 32 ) );
    //docLbl->setMinimumHeight(  32  );

    img = QImage( get_ifile( "folder_txt.png", "mid", "filesystems" ) );
    if(QApplication::reverseLayout())
      img = img.mirror(TRUE,FALSE);
    pix.convertFromImage(img);
    docLbl->setPixmap( pix ); 

    layout6->addWidget( docLbl );

    recentLbl = new sMenuItem( configsFrame, "recentLbl" );
    recentLbl->setMinimumSize( QSize( 180, 32 ) );
    layout6->addWidget( recentLbl );

    img = QImage( get_ifile( "temporary.png", "mid", "filesystems" ) );
    if(QApplication::reverseLayout())
      img = img.mirror(TRUE,FALSE);
    pix.convertFromImage(img);
    recentLbl->setPixmap( pix ); 


    picLbl = new sMenuItem( configsFrame, "picLbl" );
    picLbl->setMinimumSize( QSize( 180, 32 ) );
    layout6->addWidget( picLbl );

    img = QImage( get_ifile( "folder_image.png", "mid", "filesystems" ) );
    if(QApplication::reverseLayout())
      img = img.mirror(TRUE,FALSE);
    pix.convertFromImage(img);
    picLbl->setPixmap( pix ); 


    soundLbl = new sMenuItem( configsFrame, "soundLbl" );
    soundLbl->setMinimumSize( QSize( 180, 32 ) );
    layout6->addWidget( soundLbl );
    img = QImage( get_ifile( "folder_sound.png", "mid", "filesystems" ) );
    if(QApplication::reverseLayout())
      img = img.mirror(TRUE,FALSE);
    pix.convertFromImage(img);
    soundLbl->setPixmap( pix ); 



    seperator_2 = new QFrame( configsFrame, "seperator_2" );
    seperator_2->setFrameShape( QFrame::HLine );
    seperator_2->setFrameShadow( QFrame::Sunken );
    layout6->addWidget( seperator_2 );


    controlLbl = new sMenuItem( configsFrame, "controlLbl" );
    controlLbl->setMinimumSize( QSize( 180, 32 ) );
    //controlLbl->setFixedSize( QSize( 180, 32 ) );
    layout6->addWidget( controlLbl );
    controlLbl->connectItem( this, SLOT(close()) );
    img = QImage( get_ifile( "hwinfo.png", "mid", "apps" ) );
    if(QApplication::reverseLayout())
      img = img.mirror(TRUE,FALSE);
    pix.convertFromImage(img);
    controlLbl->setPixmap( pix ); 


    QFrame *seperator_3 = new QFrame( configsFrame, "seperator_2" );
    seperator_3->setFrameShape( QFrame::HLine );
    seperator_3->setFrameShadow( QFrame::Sunken );
    layout6->addWidget( seperator_3 );



    runItem = new sMenuItem( configsFrame, "runItem" );
    runItem->setMinimumSize( QSize( 180, 35 ) );
    layout6->addWidget( runItem );
    runItem->connectItem( this, SLOT(close()) );
    runItem->setCommand("hrun");
    runItem->setPixmap(QPixmap(get_cfile("images/run.xpm")));

    layout6->addStretch(100);

    layout7->addWidget( configsFrame );

    layout9->addLayout( layout7 );


    exitFrame = new QFrame( this, "exitFrame" );
    exitFrame->setFixedSize( QSize( 380, 42 ) );
    exitFrame->setFrameShape( QFrame::NoFrame );
    exitFrame->setFrameShadow( QFrame::Raised );

    exitFrame->setPaletteBackgroundPixmap(get_cfile("images/menubuttom.png" ));

    layout8 = new QHBoxLayout( exitFrame, 0, 0, "layout8"); 

    //    layout8->addStretch(10);
    layout8->addSpacing( 100 );

    killItem = new sMenuItem( exitFrame, "killItem" );
    killItem->connectItem( this, SLOT(close()) );
    //killItem->setFixedSize(150, 32);
    //    killItem->setMinimumSize( QSize( 0, 24 ) );
    //killItem->setMaximumSize( QSize( 32767, 24 ) );
    //killItem->drawIt();
    layout8->addWidget( killItem );

    img = QImage( get_cfile( "images/xkill.png") );
    if(QApplication::reverseLayout())
      img = img.mirror(TRUE,FALSE);
    //QPixmap pix;
    pix.resize(24,24);
    pix.convertFromImage(img.smoothScale(24,24));


    killItem->setPixmap( pix );
    killItem->setPixSize( 24, 24 );

    //    layout8->addSpacing( 10 );

    exitItem = new sMenuItem( exitFrame, "exitItem" );
    //exitItem->setMinimumSize( QSize( 0, 24 ) );
    //exitItem->setMaximumSize( QSize( 32767, 24 ) );
    //    exitItem->setMinimumWidth(150);
    exitItem->connectItem( this, SLOT(close()) );
    //    connect( exitItem, SIGNAL(clicked()), this, SLOT(close()) );

    img = QImage( get_cfile( "images/exit.png") );
    if(QApplication::reverseLayout())
      img = img.mirror(TRUE,FALSE);
    //QPixmap pix;
    pix.resize(24,24);
    pix.convertFromImage(img.smoothScale(24,24));


    exitItem->setPixmap( pix );
    exitItem->setPixSize( 24, 24 );
    layout8->addWidget( exitItem );
    //  layout8->addSpacing( 10 );
    layout9->addWidget( exitFrame );


    languageChange();
    //resize( QSize(384, 461).expandedTo(minimumSizeHint()) );


}

/*
 *  Destroys the object and frees any allocated resources
 */
itemsFrame::~itemsFrame()
{
    // no need to delete child widgets, Qt does it all for us
}

/*
 *  Sets the strings of the subwidgets using the current
 *  language.
 */
void itemsFrame::languageChange()
{
    setCaption( tr( "Form1" ) );
    //    textLabel1->setText( tr( "Your Name" ) );
    personalFrame->setText( tr( "Your Name" ) );

    //    internetBrowser->setText( tr( "Internet Browser" ) );
    internetBrowser->setText( tr( "Mozilla Browser" ) );
    //    emailClient->setText( tr( "Email Client" ) );
    emailClient->setText( tr( "Mozilla mail" ) );
    /*
    recent1->setText( tr( "" ) );
    recent2->setText( tr( "" ) );
    recent3->setText( tr( "" ) );
    recent4->setText( tr( "" ) );
    recent5->setText( tr( "" ) );
    */
    runItem->setText( tr( "Run program" ) );
    programs->setText( tr( "All Programs" ) );
    docLbl->setText( tr( "My Documents" ) );
    recentLbl->setText( tr( "Recent Docoments" ) );
    picLbl->setText( tr( "My Pictures" ) );
    soundLbl->setText( tr( "My Sound Files" ) );
    controlLbl->setText( tr( "Control Panel" ) );

    killItem->setText( tr( "Kill window" ) );
    exitItem->setText( tr( "End Session" ) );

    readRecents();
}


void itemsFrame::setPgramsMenu(QPopupMenu *pp){
    progsPop = pp;
    connect(progsPop, SIGNAL(activated(int)), this, SLOT(closeMenu(int)) );
    programs->setPopup(progsPop);
}

void itemsFrame::readRecents()
{
    if(defaults::cfdir.isNull()){
	QString fname(getenv("HOME"));

	if(! fname.isNull())
	    defaults::cfdir = fname + "/.hwm";
    }
    QString cmp( defaults::cfdir + "/recentPrograms" );

    QFile file( defaults::cfdir + "/recentPrograms" );
    if(!file.exists()){
	if ( file.open( IO_WriteOnly ) ) {
	    QTextStream stream( &file );
	    stream << "";
	    file.close();
	}
    }

    HIniFile def(cmp);
    def.read();

    QString Recent1 = def.readString("Recent1", "");
    if( (Recent1.right(8) == ".desktop") || (Recent1.right(7) == ".hwmlnk") ){
	hwmItem item =  defaults::readItem(Recent1);
	if(item.error.isNull()){
	    recent1->setText( item.name );
	    recent1->setPixmap(QPixmap(getIconFile(item.icon, "mid")));
	    //	    recent1->setPixmap(get_ifile(item.icon, "mid"));
	    recent1->setCommand(Recent1);
	}
    }else{
	if(Recent1 != ""){
	    recent1->setText(Recent1);
	    recent1->setCommand(Recent1);
	}
    }

    QString Recent2 = def.readString("Recent2", "");
    if( (Recent2.right(8) == ".desktop") || (Recent2.right(7) == ".hwmlnk") ){
	hwmItem item =  defaults::readItem(Recent2);
	if(item.error.isNull()){
	    recent2->setText( item.name );
	    recent2->setPixmap(QPixmap(getIconFile(item.icon, "mid")));
	    //	    recent2->setPixmap(get_ifile(item.icon, "mid"));
	    recent2->setCommand(Recent2);
	}
    }else{
	if(Recent2 != ""){
	    recent2->setText(Recent2);
	    recent2->setCommand(Recent2);
	}
    }

    QString Recent3 = def.readString("Recent3", "");
    if( (Recent3.right(8) == ".desktop") || (Recent3.right(7) == ".hwmlnk") ){
	hwmItem item =  defaults::readItem(Recent3);
	if(item.error.isNull()){
	    recent3->setText( item.name );
	    //recent3->setPixmap(get_ifile(item.icon, "mid"));
	    recent3->setPixmap(QPixmap(getIconFile(item.icon, "mid")));
	    recent3->setCommand(Recent3);
	}
    }else{
	if(Recent3 != ""){
	    recent3->setText(Recent3);
	    recent3->setCommand(Recent3);
	}
    }

    QString Recent4 = def.readString("Recent4", "");
    if( (Recent4.right(8) == ".desktop") || (Recent4.right(7) == ".hwmlnk") ){
	hwmItem item =  defaults::readItem(Recent4);
	if(item.error.isNull()){
	    recent4->setText( item.name );
	    //    recent4->setPixmap(get_ifile(item.icon, "mid"));
	    recent4->setPixmap(QPixmap(getIconFile(item.icon, "mid")));
	    recent4->setCommand(Recent4);
	}
    }else{
	if(Recent4 != ""){
	    recent4->setText(Recent4);
	    recent4->setCommand(Recent4);
	}
    }


    QString Recent5 = def.readString("Recent5", "");
    if( (Recent5.right(8) == ".desktop") || (Recent5.right(7) == ".hwmlnk") ){
	hwmItem item =  defaults::readItem(Recent5);
	if(item.error.isNull()){
	    recent5->setText( item.name );
	    //	    recent5->setPixmap(get_ifile(item.icon, "mid"));
	    recent5->setPixmap(QPixmap(getIconFile(item.icon, "mid")));
	    recent5->setCommand(Recent5);
	}
    }else{
	if(Recent5 != ""){
	    recent5->setText(Recent5);
	    recent5->setCommand(Recent5);
	}
    }


    QString Recent6 = def.readString("Recent6", "");
    if( (Recent6.right(8) == ".desktop") || (Recent6.right(7) == ".hwmlnk") ){
	hwmItem item =  defaults::readItem(Recent6);
	if(item.error.isNull()){
	    recent6->setText( item.name );
	    //	    recent6->setPixmap(get_ifile(item.icon, "mid"));
	    recent6->setPixmap(QPixmap(getIconFile(item.icon, "mid")));
	    recent6->setCommand(Recent6);
	}
    }else{
	if(Recent6 != ""){
	    recent6->setText(Recent6);
	    recent6->setCommand(Recent6);
	}
    }

}
