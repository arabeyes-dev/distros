SOURCES	+= main.cpp
unix {
  UI_DIR = .ui
  MOC_DIR = .moc
  OBJECTS_DIR = .obj
}
FORMS	= deskitem.ui
IMAGES	= images/default.png
TRANSLATIONS    = ../../files/lng/deskitem_ar.ts \
                  ../../files/lng/deskitem_nl.ts \
                  ../../files/lng/deskitem_es.ts \
                  ../../files/lng/deskitem_fr.ts \
                  ../../files/lng/deskitem_tr.ts

INCLUDEPATH += ../../include
LIBS		+= -L../../lib -lhde

TEMPLATE	=app
CONFIG	+= qt warn_on release
LANGUAGE	= C++
