/****************************************************************************
** Form interface generated from reading ui file 'iconform.ui'
**
** Created: Fri Apr 2 23:55:10 2004
**      by: The User Interface Compiler ($Id$)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#ifndef ICONFORM_H
#define ICONFORM_H

#include <qvariant.h>
#include <qdialog.h>

class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;
class QSpacerItem;
class QLabel;
class QLineEdit;
class QPushButton;
class QIconView;
class QIconViewItem;

class iconForm : public QDialog
{
    Q_OBJECT

public:
    iconForm( QWidget* parent = 0, const char* name = 0, bool modal = FALSE, WFlags fl = 0 );
    ~iconForm();

    QLabel* textLabel3;
    QLineEdit* iconDir;
    QPushButton* iconDirBrowse;
    QLabel* textLabel4;
    QIconView* iconView1;
    QPushButton* okBtn;
    QPushButton* cancelBtn;

public slots:
    virtual void okBtn_clicked();

protected:
    QVBoxLayout* layout23;
    QHBoxLayout* layout21;
    QHBoxLayout* layout22;
    QSpacerItem* spacer9;

protected slots:
    virtual void languageChange();

private:
    void init();

};

#endif // ICONFORM_H
