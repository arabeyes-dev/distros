/****************************************************************************
**
**	clrform.h
**      by:  Haydar Alkaduhimi <haydar@haydar.net>
**
****************************************************************************/ 

#ifndef CLRFORM_H
#define CLRFORM_H

#include <qvariant.h>
#include <qstylefactory.h>
#include <qwidget.h>
#include <qlabel.h>
#include <qpushbutton.h> 

class QVBoxLayout; 
class QHBoxLayout; 
class QGridLayout; 
class QComboBox;
class QFrame;
//class QLabel;
class QLineEdit;
//class QPushButton;
class QSpinBox;

class aplBtn : public QPushButton
{ 
    Q_OBJECT

 public:
    aplBtn( QWidget* parent = 0, const char* name = 0 );
    ~aplBtn(){};

 signals:
    void enabledChang(bool);

 protected:
    virtual void enabledChange ( bool oldEnabled ){ emit enabledChang(isEnabled()); };
};

class clrLabel : public QLabel
{ 
    Q_OBJECT
 public:
    clrLabel( QWidget* parent = 0, const char* name = 0, WFlags fl = 0 );
    ~clrLabel(){};

 signals:
    void clicked();

 protected:
    virtual void mousePressEvent ( QMouseEvent * e ){emit clicked();};

};

class clrForm : public QWidget
{ 
    Q_OBJECT

public:
    clrForm( QWidget* parent = 0, const char* name = 0, WFlags fl = 0 );
    ~clrForm();

    clrLabel* desktop;
    QFrame* Frame4;
    clrLabel* activeTitle;
    QPushButton* activeButton;
    clrLabel* inTitle;
    QPushButton* PushButton1;
    QFrame* taskBar;
    QPushButton* taskButton;
    QFrame* Frame4_2;
    QLabel* TextLabel1_2;
    QComboBox* styleBox;
    QSpinBox* sizeBox;
    QLabel* TextLabel6;
    QLabel* TextLabel5;
    QComboBox* itemBox;
    QPushButton* color2Btn;
    QLabel* itemText;
    QLabel* TextLabel4;
    QLabel* textColorLbl;
    QPushButton* textColorBtn;
    QPushButton* color1Btn;
    QLabel* fontLabel;
    QLineEdit* fontEdit;
    QPushButton* fontBtn;
    QLabel* TextLabel1;
    QLineEdit* nameEdit;
    QPushButton* okBtn;
    QPushButton* cancelBtn;
    aplBtn* applyBtn;

    QColor active_bg;
    QColor active_bg2;
    QColor active_fg;
    QColor inactive_bg;
    QColor inactive_bg2;
    QColor inactive_fg;
    QColor root_bg;
    int windowbuttonsize;
    QString styleName;
    QFont borderfont;

    void setTitle();

 public slots:
    virtual void init();
    virtual void disableAll();
    virtual void itemBox_highlighted( const QString & );
    virtual void sizeBox_valueChanged( int );
    virtual void color1Btn_clicked();
    virtual void color2Btn_clicked();
    virtual void textColorBtn_clicked();
    virtual void activeButton_clicked();
    virtual void taskButton_pressed();
    virtual void fontBtn_clicked();
    virtual void applyBtn_clicked();
    virtual void okBtn_clicked();
    virtual void fontEdit_textChanged ( const QString & );
    virtual void desktop_clicked();
    virtual void inTitle_clicked();
    virtual void activeTitle_clicked();

    //virtual void styleBox_highlighted( int );
    virtual void styleBox_activated( const QString & );
    virtual void itemBox_activated( const QString & );
    void enabledChange(bool);

 signals:
    void enabledChanged( bool );

 protected:
    QVBoxLayout* Layout7;
    QHBoxLayout* Layout6;
    QGridLayout* Layout1;
    QGridLayout* Layout6_2;
    QHBoxLayout* Layout2;
    QHBoxLayout* Layout1_2;
    void itemChanged();
};

#endif // CLRFORM_H
