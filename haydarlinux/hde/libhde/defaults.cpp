
/*
*  File      : defaults.cpp
*  Written by: haydar@haydar.net
*  Copyright : GPL
*
*  reads the defaults file and makes information statically
*  accessible
*/

#include <qstylefactory.h>
#include <qtranslator.h>

#include "defs.h"
#include "lhdeconf.h"
#include "defaults.h"
#include "hinifile.h"
#include "libhde.h"

QDateTime lmtime; 
int defaults::tb_height = 24;                   // toolbar height
int defaults::windowbuttonsize = 16;            // upper window border
int defaults::lowerborderwidth = 22;            // lower window border
int defaults::lowerborderheight = 6;            // lower window border
int defaults::pager_height = 18;                // pager height
QFont defaults::borderfont;                     // window border font
QFont defaults::toolbarfont;                    // toolbar font
bool defaults::toolbar_top = FALSE;             // toolbar on top
bool defaults::sideBorders = TRUE;
bool defaults::topBorder = TRUE;
bool defaults::windowsBtns = FALSE;
bool defaults::clickToFocus = FALSE;
int defaults::vdesks = 3;                       // virtual desktops
bool defaults::show_menu = TRUE;                // show menu on toolbar
int defaults::autofocustime = 600;              // autofocus time/ms or 0 = off

int defaults::tc_height = 20;                   // toolbar contents height
QColor defaults::inactive_bg = Qt::darkGray;    // window inactive color
QColor defaults::active_bg = Qt::lightGray;  	// window active color
QColor defaults::inactive_bg2 = QColor("#c5c5c5");    // window inactive color
QColor defaults::active_bg2 = QColor("#c5c5c5");  	// window active color
QColor defaults::inactive_fg = Qt::white;    	// text inactive color
QColor defaults::active_fg = Qt::white;  	// text active color
QColor defaults::urgent_bg = Qt::red;           // window urgent color
QColor defaults::root_bg = Qt::black;           // root window background
QString defaults::root_pix;                     // root window background pixmap
bool defaults::starturgent = TRUE;              // new windows with urgent colors
QStack <QString> defaults::initexec;            // Loaded with Exec option
bool defaults::showclientmachines = FALSE;      // show client machine names on winlist
bool defaults::show_winlist = TRUE;             // show winlist on toolbar
bool defaults::start_restart = FALSE;           // restart running
char **defaults::argv;                          // copy for restart
int defaults::argc;                             // copy for restart
float defaults::tleftspace = 0.65;              // left frame in tiled desk
int defaults::maxontab = 600;                   // maximize with tab key in tiled mode
int defaults::wminframe = 3;                    // minimize right frame in tiling mode
bool defaults::sttiled[10];                     // deskops to be tiled after start
QString defaults::cfdir = NULL;                 // config directory
int defaults::tmx1=-1,defaults::tmx2,defaults::tmy1,defaults::tmy2; 
int defaults::smx1=-1,defaults::smx2,defaults::smy1,defaults::smy2;
QColor defaults::pager_active = Qt::darkGray;   // active background color
QColor defaults::pager_window = Qt::blue;       // window background color
QColor defaults::pager_visible = Qt::darkRed;   // visible window background color
int defaults::menuwidth = 65;
QString defaults::styleName = "";
QString defaults::titleStyle = "hde";
QString defaults::taskStyle = "hde";
QString defaults::apbarStyle = "hde";
QString defaults::lng = "";
QString defaults::coding = "";
QString defaults::bgdir = "";
QString defaults::bgDisplay = "Stretch";

QString defaults::minicon = "images/minbtn.xpm";
QString defaults::maxicon = "images/maxbtn.xpm";
QString defaults::resticon = "images/restorebtn.xpm";
QString defaults::closeicon = "images/button_right.xpm";
QString defaults::helpicon = "images/helpbtn.xpm";
QString defaults::lefticon = "images/button_left.xpm";
QString defaults::deficon = "images/default.xpm";
//QString defaults::starticon = "images/xpmenu.xpm";
QString defaults::starticon = "";
QString defaults::startdown = "";
QString defaults::starticonmo = "";
QString defaults::starticonplace = "MIDDLE";
int defaults::startheight = -1;
int defaults::startwidth = -1;


QString defaults::defaultBrowser = "htmlview";
QString defaults::defaultFM = "konqueror";

QStringList defaults::imagesDirs;

QString defaults::titleimage = "";
QString defaults::titlerimage = "";
QString defaults::titlelimage = "";
QString defaults::untitleimage = "";
QString defaults::untitlerimage = "";
QString defaults::untitlelimage = "";
int defaults::iconsize = 12;            // Icons Size
bool defaults::flaticons = false;

QString defaults::toolbar_bg = "";

QString defaults::tbButton_bg = "";
QString defaults::tbButtondown_bg = "";
QString defaults::tbButtonmo_bg = "";
int defaults::tbButtonHeight = -1;
QColor defaults::tbButton_fg = Qt::black;
QColor defaults::startButton_fg = Qt::black;
QColor defaults::apbar_fg = Qt::black;

QString defaults::wininfo_bg = "";
QString defaults::wininfodown_bg = "";
QString defaults::wininfomo_bg = "";
int defaults::wininfoHeight = -1;

QString defaults::appbar_bg = "";

bool defaults::withxpmenu = false;
bool defaults::hijriDate = false;

itemsFrame *defaults::xpmenu;

QString defaults::defaultIconsDir="";

QStyle *defstyle;

void defaults::readTheme(QString fname){
    QString sval;
    int ival;

    HIniFile theme(fname);
    theme.read();
    
    sval = theme.readString("Style","");

    defstyle = QStyleFactory::create(sval);
    styleName = sval;


    sval = theme.readString("Theme","");

    if(sval.lower() == "motif"){
	//	styleName = "motifStyle";
    }else if(sval.lower() == "windows"){
	//	styleName = "win98Style";
	titleStyle = "win";
	taskStyle  = "win";
	apbarStyle = "win";
	windowsBtns = true;
	clickToFocus = true;

    }else if(sval.lower() == "winxp"){
	titleStyle = "hde";
	taskStyle = "hde";
	apbarStyle = "hde";
	windowsBtns = true;
	clickToFocus = true;

	
    }else if(sval.lower() == "hdestyle"){
	//	styleName = "hdeStyle";
	titleStyle = "hde";
	taskStyle = "hde";
	apbarStyle = "hde";
	clickToFocus = true;
    }
    else{	
	cerr << "Unknown Style: " << sval << '\n';
	styleName = "hdeStyle";
    }


    minicon = theme.readString("minicon", minicon);
    maxicon = theme.readString("maxicon", maxicon);
    resticon = theme.readString("resticon", resticon);
    closeicon = theme.readString("closeicon", closeicon);
    helpicon = theme.readString("helpicon", helpicon);
    lefticon = theme.readString("lefticon", lefticon);
    deficon = theme.readString("deficon", deficon);

    starticon = theme.readString("starticon", starticon);
    startdown = theme.readString("startdown", startdown);
    starticonmo = theme.readString("starticonmo", starticonmo);

    starticonplace = theme.readString("StartIconPlace", starticonplace);

    startheight  =  theme.readInt("StartIconHeight", startheight);


    startwidth  =  theme.readInt("StartIconWidth", startheight);


    tbButton_bg = theme.readString("tbButton_bg", tbButton_bg);
    tbButtondown_bg = theme.readString("tbButtondown_bg", tbButtondown_bg);
    tbButtonmo_bg = theme.readString("tbButtonmo_bg", tbButtonmo_bg);


    wininfo_bg = theme.readString("wininfo_bg", wininfo_bg);
    wininfodown_bg = theme.readString("wininfodown_bg", wininfodown_bg);
    wininfomo_bg = theme.readString("wininfomo_bg", wininfomo_bg);

    wininfoHeight  =  theme.readInt("WinInfoHeight", wininfoHeight);

    appbar_bg = theme.readString("appbar_bg", appbar_bg);



    tbButtonHeight  =  theme.readInt("ToolButtonHeight", tbButtonHeight);


    clickToFocus = theme.readBool("clickToFocus", true);

    withxpmenu = theme.readBool("withxpmenu", withxpmenu);


    titleimage = theme.readString("TitleImage", titleimage);
    titlerimage = theme.readString("TitleRightImage", titlerimage);
    titlelimage = theme.readString("TitleLeftImage", titlelimage);

    untitleimage = theme.readString("UnactiveTitleImage", untitleimage);
    untitlerimage = theme.readString("UnactiveTitleRightImage", titlerimage);
    untitlelimage = theme.readString("UnactiveTitleLeftImage", titlelimage);

    flaticons = theme.readBool("flaticons", flaticons);

    sideBorders = theme.readBool("SideBorders",sideBorders);
    topBorder = theme.readBool("topBorder",sideBorders);

    windowsBtns = theme.readBool("windowsBtns", windowsBtns);

    clickToFocus = theme.readBool("clickToFocus", clickToFocus);


    toolbar_bg = theme.readString("ToolbarBackGround", toolbar_bg);

    sval = theme.readString("tbButtonForeground", tbButton_fg.name());
    if (sval != "" )
	tbButton_fg.setNamedColor(sval);


    sval = theme.readString("startForeground", startButton_fg.name());
    if (sval != "" )
	startButton_fg.setNamedColor(sval);


    sval = theme.readString("apbarForeground", apbar_fg.name());
    if (sval != "" )
	apbar_fg.setNamedColor(sval);



	ival  =  theme.readInt("ToolbarHeight", tb_height);
	if(ival < 10 || ival > 500)
	    cerr << "ToolbarHeight: Value out of range\n";
	else
	    tb_height = ival;
						

	ival  =  theme.readInt("TitlebarHeight", windowbuttonsize);
	if(ival < 6 || ival > 500)
	    cerr << "WindowButtonSize: Value out of range\n";
	else
	    windowbuttonsize = ival;

	ival  =  theme.readInt("WindowButtonSize", iconsize);
	if(ival < 3 || ival > 500)
	    cerr << "WindowButtonSize: Value out of range\n";
	else
	    iconsize = ival;

	ival  =  theme.readInt("LowerBorderHeight", lowerborderheight);
	if(ival < 1 || ival > 500)
	    cerr << "LowerBorderHeight: Value out of range\n";
	else
	    lowerborderheight = ival;
	

	ival  =  theme.readInt("LowerBorderWidth", lowerborderwidth);
	if(ival < 1 || ival > 500)
	    cerr << "LowerBorderWidth: Value out of range\n";
	else
	    lowerborderwidth = ival;



	ival  =  theme.readInt("PagerHeight", pager_height);
	if(ival < 4 && ival > 500)
	    cerr << "PagerHeight: Value out of range\n";
	else
	    pager_height = ival;



	// Colors & Backgrounds
	theme.setGroup("Color_Background");
	sval = theme.readString("InactiveWindowColor","");
	if (sval != "" )
	    inactive_bg.setNamedColor(sval);

	sval = theme.readString("activeWindowColor","");
	if (sval != "" )
	    active_bg.setNamedColor(sval);

	sval = theme.readString("InactiveWindowColor2","");
	if (sval != "" )
	    inactive_bg2.setNamedColor(sval);

	sval = theme.readString("activeWindowColor2","");
	if (sval != "" )
	    active_bg2.setNamedColor(sval);


	sval = theme.readString("InactiveTextColor","");
	if (sval != "" )
	    inactive_fg.setNamedColor(sval);

	sval = theme.readString("activeTextColor","");
	if (sval != "" )
	    active_fg.setNamedColor(sval);


	sval = theme.readString("UrgentWindowColor","");
	if (sval != "" )
	    urgent_bg.setNamedColor(sval);


}

void defaults::read_config(void){
    int ival,i,i2,iar[4];
    float fval;
    bool ok;

    borderfont.setPixelSize(windowbuttonsize-5);
    initexec.setAutoDelete(TRUE);

    static char *par[] = {

	"Exec",                   // 0



	"Maximize1",              // 1
	"Maximize2",              // 2




	"TileStart",              // 3


	NULL };
	
    QString fname,cline,sval, p1,p2;
    
    fname = get_cfile("defaults");

    QFileInfo fi(fname);
    
    if(fi.lastModified() == lmtime)
	return;
    
    lmtime == fi.lastModified();


    QFile istr(fname);

    HIniFile defaults(fname);

    defaults.read();


    if(fname.isNull() || ! istr.open(IO_ReadOnly)){
	if(! fname.isNull())
	    perror("cannot open defaults file");
    }else{

//    bool show_menu= defaults.readBool("show_menu",true);
//    QString charset = defaults.readString("charset","ISO 8859-1");

	ival  =  defaults.readInt("ToolbarHeight", 24);
	if(ival < 10 || ival > 500)
	    cerr << "ToolbarHeight: Value out of range\n";
	else
	    tb_height = ival;
						

	ival  =  defaults.readInt("TitlebarHeight", 16);
	if(ival < 6 || ival > 500)
	    cerr << "WindowButtonSize: Value out of range\n";
	else
	    windowbuttonsize = ival;

	ival  =  defaults.readInt("WindowButtonSize", iconsize);
	if(ival < 3 || ival > 500)
	    cerr << "WindowButtonSize: Value out of range\n";
	else
	    iconsize = ival;

	ival  =  defaults.readInt("LowerBorderHeight", 6);
	if(ival < 1 || ival > 500)
	    cerr << "LowerBorderHeight: Value out of range\n";
	else
	    lowerborderheight = ival;
	

	sideBorders = defaults.readBool("SideBorders",true);
	

	ival  =  defaults.readInt("LowerBorderWidth", 22);
	if(ival < 1 || ival > 500)
	    cerr << "LowerBorderWidth: Value out of range\n";
	else
	    lowerborderwidth = ival;



	ival  =  defaults.readInt("PagerHeight", 18);
	if(ival < 4 && ival > 500)
	    cerr << "PagerHeight: Value out of range\n";
	else
	    pager_height = ival;


	//	menutxt = defaults.readString("MenuText","Start");
	coding = defaults.readString("Encoding","");


	sval = defaults.readString("WindowFontName","arial");
	//	borderfont.setFamily(sval);
	borderfont.fromString(sval);

	ival  =  defaults.readInt("WindowFontHeight", 11);
	if(ival < 4 || ival > 500)
	    cerr << "WindowFontHeight: Value out of range\n";
	else
	    borderfont.setPixelSize(ival);




	sval = defaults.readString("IconFontName","arial");
	//toolbarfont.setFamily(sval);
	toolbarfont.fromString(sval);

	ival  =  defaults.readInt("IconFontHeight", 11);
	if(ival < 4 || ival > 500)
	    cerr << "IconFontHeight: Value out of range\n";
	else
	    toolbarfont.setPixelSize(ival);



	toolbar_top = defaults.readBool("ToolbarOnTop",false);





	show_menu = defaults.readBool("ShowMenus",true);;



	ival  =  defaults.readInt("AutoRaiseTime", 600);
	if(ival < 0 || ival > 100000)
	    cerr << "AutoRaiseTime: Value out of range\n";
	else
	    autofocustime = ival;







	starturgent = defaults.readBool("StartClientsUrgent",false);








	showclientmachines = defaults.readBool("ShowClientMachines",false);

	show_winlist = defaults.readBool("ShowWinlist",true);






	lng = defaults.readString("Language","default");

	if(lng == "default"){
	    QString lngTmp(QTextCodec::locale());
	    if (int indx = lngTmp.find('_') != -1)
		lng = lngTmp.left(indx+1);
	    else
		lng = lngTmp;
	}

	fval = defaults.readFloat("TileSplit", 65);
	if(fval < 1 || fval > 99)
	    cerr << "TileSplit: Value out of range\n";
	else
	    tleftspace = fval/100;


	ival  =  defaults.readInt("TileMaxWithTab", 600);
		if(ival < 0 || ival > 10000)
		    cerr << "TileMaxOnTab: Value out of range\n";
		else
		    maxontab = ival;


	ival  =  defaults.readInt("TileMinframe", 600);
	if(ival < 0 || ival > 10000)
	    cerr << "TileMinframe: Value out of range\n";
	else
	    wminframe = ival;

	windowsBtns = defaults.readBool("windowsBtns", windowsBtns);
	clickToFocus = defaults.readBool("clickToFocus", clickToFocus);


	// Pager
	defaults.setGroup("Pager");

	sval = defaults.readString("PagerActiveColor","");
	if (sval != "" )
	    pager_active.setNamedColor(sval);


	sval = defaults.readString("PagerVisibleColor","");
	if (sval != "" )
	    pager_visible.setNamedColor(sval);


	sval = defaults.readString("PagerInvisibleColor","");
	if (sval != "" )
	    pager_window.setNamedColor(sval);

	ival  =  defaults.readInt("VirtualDesktops", 3);
	if(ival < 1 || ival > MAXDESKS){
	    cerr << "VirtualDesktops: Value out of range\n";
	    vdesks = 3;
	}else
	    vdesks = ival;


	defaultBrowser = defaults.readString("defaultBrowser",defaultBrowser);
	defaultFM = defaults.readString("defaultFileManager",defaultFM);


	// Colors & Backgrounds
	defaults.setGroup("Color_Background");
	sval = defaults.readString("InactiveWindowColor","");
	if (sval != "" )
	    inactive_bg.setNamedColor(sval);

	sval = defaults.readString("activeWindowColor","");
	if (sval != "" )
	    active_bg.setNamedColor(sval);

	sval = defaults.readString("InactiveWindowColor2","");
	if (sval != "" )
	    inactive_bg2.setNamedColor(sval);

	sval = defaults.readString("activeWindowColor2","");
	if (sval != "" )
	    active_bg2.setNamedColor(sval);


	sval = defaults.readString("InactiveTextColor","");
	if (sval != "" )
	    inactive_fg.setNamedColor(sval);

	sval = defaults.readString("activeTextColor","");
	if (sval != "" )
	    active_fg.setNamedColor(sval);


	sval = defaults.readString("UrgentWindowColor","");
	if (sval != "" )
	    urgent_bg.setNamedColor(sval);


	bgdir = getenv("HOME");
	bgdir = defaults.readString("BGDIR", bgdir);

	sval = defaults.readString("RootBackgroundColor","");
	if (sval != "" )
	    root_bg.setNamedColor(sval);

#ifdef Q_WS_X11 

	sval = defaults.readString("RootBackgroundPicture","");
	if((access((const char *)sval, R_OK) == -1)|| sval == "")
	    perror((const char *)sval);
	else	
	    root_pix = sval;

	bgDisplay = defaults.readString("bgDisplay", bgDisplay);
#endif

	//QStringList defaults::imagesDirs;
	//QStringList drs;
	sval = defaults.readString("ImagesDirs","");
	imagesDirs.clear();
	
	defaultIconsDir = defaults.readString("DefaultIconsDir", 
					      defaultIconsDir);
	if(defaultIconsDir != "")
	    imagesDirs.append(defaultIconsDir);

	if(QFile(QString(getenv("HOME")) + "/.hde").exists())
		imagesDirs.append( QString(getenv("HOME")) + "/.hde");
        if(QFile().exists(QString(CONFDIR) ))
	    imagesDirs.append( QString(CONFDIR));
	   

//        if(QFile().exists(QStringList::split(";", sval)))
 //		imagesDirs += QStringList::split(";", sval);
        if(QFile().exists("/usr/share/pixmaps"))
	    imagesDirs.append( "/usr/share/pixmaps" );
       if(QFile().exists("/usr/share/icons/crystalsvg"))
            imagesDirs += "/usr/share/icons/crystalsvg";

        if(QFile().exists("/usr/share/icons"))
	    imagesDirs += "/usr/share/icons";
	if(QFile().exists("/usr/share/icons/hicolor"))
	    imagesDirs += "/usr/share/icons/hicolor";
//        if(QFile().exists("/usr/share/icons/Crystal"))
//	    imagesDirs += "/usr/share/icons/Crystal";
	

	withxpmenu = defaults.readBool("withxpmenu", withxpmenu);

	// Date & Time
	defaults.setGroup("DateTime");
	hijriDate = defaults.readBool("hijriDate", hijriDate);

	// Style & Themes

	defaults.setGroup("Style");
	sval = defaults.readString("Style","platinum");

	defstyle = QStyleFactory::create(sval);
	//qApp->setStyle(sval);
	styleName = sval;

	if(sval.lower() == "motif"){
	    //	    defstyle = QStyleFactory::create("Motif");
	    //    styleName = "motifStyle";
	}else if(sval.lower() == "windows"){
	    //defstyle = QStyleFactory::create("Windows");
	    //styleName = "win98Style";
	    titleStyle = "win";
	    taskStyle = "win";
	    apbarStyle = "win";
	    windowsBtns = true;
	    clickToFocus = true;
	    /*
	}else if(sval.lower() == "platinum"){
	    //defstyle = QStyleFactory::create("Platinum");
	    styleName = "platinumStyle";
	}else if(sval.lower() == "motifplus"){
	    //defstyle = QStyleFactory::create("MotifPlus");
	    styleName = "motifPlusStyle";
	}else if(sval.lower() == "cde"){
	    //defstyle = QStyleFactory::create("CDE");
	    styleName = "cdeStyle";


	}else if(sval.lower() == "sgi"){
	    //defstyle = QStyleFactory::create("SGI");
	    styleName = "SGIStyle";
	    */

	}else if(sval.lower() == "winxp"){
	    //    defstyle = new hdeStyle;
	    //defstyle = QStyleFactory::create("hdeStyle");
	    //  styleName = "WinXPStyle";
	    titleStyle = "hde";
	    taskStyle = "hde";
	    apbarStyle = "hde";
	    windowsBtns = true;
	    clickToFocus = true;
	
	}else if(sval.lower() == "hdestyle"){
	    //defstyle = new hdeStyle;
	    //defstyle = QStyleFactory::create("hdeStyle");
	    //styleName = "hdeStyle";
	    titleStyle = "hde";
	    taskStyle = "hde";
	    apbarStyle = "hde";
	    clickToFocus = true;
	}
	else{	
	    cerr << "Unknown Style: " << sval << '\n';
	    styleName = "hdeStyle";
	}
	sval = "";

	//	if(defstyle)
	//qApp->setStyle(defstyle);


	sval = defaults.readString("Theme","");
	if(sval != "")
	    readTheme(sval);

	if(defstyle)
	  qApp->setStyle(defstyle);

	// Execute at Startup
	if(start_restart == FALSE){
	    defaults.setGroup("Execute");
	    ival  =  defaults.readInt("Exec_Number", 0);

	    if (ival > 0){
		for(int i = 0; i < ival; i++){
		    sval = defaults.readString(QString("Exec[%1]").arg(i+1), 
					       "");
		    if(sval != "")
			initexec.push(new QString(sval));
		}
	    }
	}



	while(! istr.atEnd()){
	    istr.readLine(cline, 1024);
	    QTextIStream tis(&cline);
		
	    tis >> p1;
	    if(p1.isEmpty() || *(const char *)p1 == '#')
		continue;

	    tis >> p2;
	    if(p2.isEmpty())
		continue;

	    i = 0;
	    do{
		if(p1 == par[i])
		    break;
	    }
	    while(par[++i] != NULL);
	
	    switch(i){
	
		
	
		
	
	
		/*		
	    case 0:
		if(start_restart == FALSE)
		    initexec.push(new QString(p2+tis.readLine()));

		break;
		*/





	    case 1:
	    case 2:
		i2 = 0;
		while(1){
		    iar[i2] = p2.toInt(&ok);

		    if(ok == FALSE || i2 > 2)
			break;
		    
		    tis >> p2;

		    if(p2.isEmpty())
			break;

		    i2++;
		}
		if(i2 < 3){
		    cerr << "invalid parameter for maximize\n";
		    break;
		}
		
		if(i == 25){
		    tmx1 = iar[0]; tmy1 = iar[1]; tmx2 = iar[2]; tmy2 = iar[3];
		}else{
		    smx1 = iar[0]; smy1 = iar[1]; smx2 = iar[2]; smy2 = iar[3];
		}	
		
		break;	





	    case 3:
		for(i2=0; i2 < 10; i2++){
		    ival = p2.toInt(&ok);
		    
		    if(ok == FALSE)
			break;

		    if(ival < 1 || ival > 10){
			cerr << "TileStart: value out of range\n";
			continue;
		    }

		    sttiled[ival-1] = TRUE;

		    tis >> p2;

		    if(p2.isEmpty())
			break;
		}
					

	    default:
		;//		cerr << "WM: unknown parameter: " << p1 << '\n';
	    }
	}
	istr.close();
    }	

    /*	
    QFontInfo info(borderfont);
    if(info.family() != borderfont.family())
	cerr << "WM: no match for font " << borderfont.family() << ", using " << info.family() << " instead\n";
    

    QFontInfo tinfo(toolbarfont);
    if(tinfo.family() != toolbarfont.family())
	cerr << "WM: no match for font " << toolbarfont.family() << ", using " << tinfo.family() << " instead\n";
    */	

    tc_height = tb_height-4;

    if(pager_height > tb_height)
	pager_height = tb_height;
	
    if(borderfont.pixelSize() > windowbuttonsize-3){
	windowbuttonsize = borderfont.pixelSize()+3;
	cerr << "WM: windowborder too small for font, set to " << windowbuttonsize << '\n';
    }

    if(toolbarfont.pixelSize() > tc_height-4){
	tc_height = toolbarfont.pixelSize()+4;
	tb_height = tc_height+4;
	cerr << "WM: toolbar contents too small for font, set to " << tc_height << '\n';
    }


    QTranslator libtranslator( 0 );
    libtranslator.load( "libhde_" + defaults::lng , get_cfile("lng") );
    //cout << get_cfile("lng") << "/libhde_" <<defaults::lng <<  endl;

    qApp->installTranslator( &libtranslator );


}


void defaults::read_cfg(void){
    int ival;
    //int i,i2,iar[4];
    //float fval;
    //bool ok;


    QString fname,cline,sval, p1,p2;
    
    fname = get_cfile("defaults");
    //QFile istr(fname);

    QFileInfo fi(fname);
    
    if(fi.lastModified() == lmtime)
	return;
    
    lmtime == fi.lastModified();

    HIniFile defaults(fname);

    defaults.read();


    if(fname.isNull() ){
	if(! fname.isNull())
	    perror("cannot open defaults file");
    }else{

	ival  =  defaults.readInt("ToolbarHeight", 24);
	if(ival < 10 || ival > 500)
	    cerr << "ToolbarHeight: Value out of range\n";
	else
	    tb_height = ival;
						

	ival  =  defaults.readInt("WindowButtonSize", 16);
	if(ival < 6 || ival > 500)
	    cerr << "WindowButtonSize: Value out of range\n";
	else
	    windowbuttonsize = ival;


	ival  =  defaults.readInt("LowerBorderHeight", 6);
	if(ival < 1 || ival > 500)
	    cerr << "LowerBorderHeight: Value out of range\n";
	else
	    lowerborderheight = ival;
	

	sideBorders = defaults.readBool("SideBorders",true);
	

	ival  =  defaults.readInt("LowerBorderWidth", 22);
	if(ival < 1 || ival > 500)
	    cerr << "LowerBorderWidth: Value out of range\n";
	else
	    lowerborderwidth = ival;


	coding = defaults.readString("Encoding","");

	lng = defaults.readString("Language","default");

	if(lng == "default"){
	    QString lngTmp(QTextCodec::locale());
	    if (int indx = lngTmp.find('_') != -1)
		lng = lngTmp.left(indx+1);
	    else
		lng = lngTmp;
	}


	//	bgdir = getenv("HOME");
	//	bgdir = defaults.readString("BGDIR", bgdir);

	//	bgDisplay = defaults.readString("bgDisplay", bgDisplay);


	
	// Style & Themes
	defaults.setGroup("Style");

	sval = defaults.readString("Style","platinum");

	//if(locate("lib", sval).isNull())
	//cout << sval << "\n";

	qApp->setStyle(sval);
	styleName = sval;

	/*
	if(sval.lower() == "motif"){
	    defstyle = QStyleFactory::create("Motif");
	    styleName = "motifStyle";
	}else if(sval.lower() == "windows"){
	    defstyle = QStyleFactory::create("Windows");
	    styleName = "win95Style";
	}else if(sval.lower() == "platinum"){
	    defstyle = QStyleFactory::create("Platinum");
	    styleName = "platinumStyle";
	}else if(sval.lower() == "motifplus"){
	    defstyle = QStyleFactory::create("MotifPlus");
	    styleName = "motifPlusStyle";
	}else if(sval.lower() == "cde"){
	    defstyle = QStyleFactory::create("CDE");
	    styleName = "cdeStyle";


	}else if(sval.lower() == "sgi"){
	    defstyle = QStyleFactory::create("SGI");
	    styleName = "SGIStyle";

	}else if(sval.lower() == "winxp"){
	    //defstyle = new hdeStyle;
	    defstyle = QStyleFactory::create("hdeStyle");
	    styleName = "WinXPStyle";


	}else if(sval.lower() == "hdestyle"){
	    //defstyle = new hdeStyle;
	    defstyle = QStyleFactory::create("hdeStyle");
	    styleName = "hdeStyle";
	}
	else{	
	    cerr << "Unknown Style: " << sval << '\n';
	    styleName = "hdeStyle";
	}	
	*/	
    }

    //    if(defstyle)
    //qApp->setStyle(defstyle);

	sval = "";
	sval = defaults.readString("Theme","");
	if(sval != "")
	    readTheme(sval);

    QTranslator libtranslator( 0 );
    libtranslator.load( "libhde_" + defaults::lng , get_cfile("lng") );

    cout << get_cfile("lng") << endl;
    qApp->installTranslator( &libtranslator );
}



QString defaults::get_cfile(char *name){
    QString fname;

    if(cfdir.isNull()){
	QString fname(getenv("HOME"));

	if(! fname.isNull())
	    cfdir = fname + "/.hde";
    }

    // user config dir
    fname = defaults::cfdir;
    fname += '/';
    fname += name;
    
    QFileInfo fil(fname);
	
    if(fil.isReadable())
	return(fname);
	
    fname = CONFDIR;   // system config dir
    fname += "/";
    fname += name;
	
    QFileInfo fi(fname);

    if(fi.isReadable())
	return(fname);
		
    perror((const char *)fname);
    fname = QString();
    return(fname);
}

hdeItem defaults::readItem(QString path){
    hdeItem item;
    //QString svar = QFileInfo(path).baseName(false);
    QString svar = QFileInfo(path).fileName();

    QString prg, exec= "";

    //    cout << QFile(path).name() << "\n";




    //    if(QFileInfo::QFileInfo(path).exists()){
    QFile file( path );
    if ( file.exists()){
      if ( file.open( IO_ReadOnly ) ) {
        QTextStream stream( &file );
        QString line;
        while ( !stream.atEnd() ) {
	  line = stream.readLine();
	  if(line.contains("[Desktop Entry]" ) > 0){
	    item.isDesktop = true;
	    break;
	  }else
	    item.isDesktop = false;
	}
        file.close();
      }

      if(!item.isDesktop){
	item.name = svar;
	//cout << "the file \"" << path << "\" is not a Desktop Entry\n";
      }else{
	HIniFile lnkIni(path);

	lnkIni.read();
	lnkIni.setGroup("Desktop Entry");

	QString enc = lnkIni.readString("Encoding", "NOENC");
	svar = lnkIni.readString("Name", svar);
	if(lng != "default"){
	    svar = lnkIni.readString("Name["+ lng + "]" , 
				lnkIni.readString("Name["+ lng.left(2) + "]", 
						  svar));

	    QString charset = hde_langinfo(lng);

	    if (enc != "NOENC"){
	  	if(enc == "UTF-8") 
		    svar = QString::fromUtf8( svar );
		else
		    svar = QTextCodec::codecForName( enc )->toUnicode(svar);

	  }else
	      svar = QTextCodec::codecForName( charset )->toUnicode(svar);
	}else{
	    svar = lnkIni.readString("Name",svar);
	}

	prg = lnkIni.readString("Icon", "");

	QString type = lnkIni.readString("Type", "");
	/* Values are: Application, Link, FSDevice, MimeType, Directory, 
	   Service, ServiceType */ 

	if(type == "Link" || type == "Directory" ){
	    exec = lnkIni.readString("URL", "");
	    if(defaults::lng != "default")
		exec = lnkIni.readString("URL["+ lng + "]" ,
				lnkIni.readString("URL["+ lng.left(2)
				+ "]", lnkIni.readString("URL", exec)));

	}else{
	    exec = lnkIni.readString("Exec", "");
	    if(defaults::lng != "default")
		exec = lnkIni.readString("Exec["+ lng + "]" ,
				lnkIni.readString("Exec["+ lng.left(2)
				+ "]", lnkIni.readString("Exec", exec)));
	}

	if(type == "Link" )
	    exec = defaultBrowser+ " " + exec;
	else if(type == "Directory")
	    exec = defaultFM+ " " + exec;

	item.name = svar;
	item.icon = prg;
	item.exec = exec;
	item.path = path;
      }
    }else{
	item.error = path + ": file not found";	
    }
    return item;
}

void defaults::addToRecentPrograms(QString path)
{
    //    cout << path << "\n";

    if(defaults::cfdir.isNull()){
	QString fname(getenv("HOME"));

	if(! fname.isNull())
	    defaults::cfdir = fname + "/.hde";
    }
    QString cmp( defaults::cfdir + "/recentPrograms" );

    QFile file( defaults::cfdir + "/recentPrograms" );
    if(!file.exists()){
	if ( file.open( IO_WriteOnly ) ) {
	    QTextStream stream( &file );
	    stream << "";
	    file.close();
	}
    }

    HIniFile def(cmp);
    def.read();

    QString Recent6 = def.readString("Recent6", "");
    if(Recent6 == path)
	return;
    QString Recent5 = def.readString("Recent5", "");
    if(Recent5 == path)
	return;
    QString Recent4 = def.readString("Recent4", "");
    if(Recent4 == path)
	return;
    QString Recent3 = def.readString("Recent3", "");
    if(Recent3 == path)
	return;
    QString Recent2 = def.readString("Recent2", "");
    if(Recent2 == path)
	return;
    QString Recent1 = def.readString("Recent1", "");
    if(Recent1 == path)
	return;

    def.writeString("Recent6", Recent5) ;
    def.writeString("Recent5", Recent4) ;
    def.writeString("Recent4", Recent3) ;
    def.writeString("Recent3", Recent2) ;
    def.writeString("Recent2", Recent1) ;
    def.writeString("Recent1", path) ;

    def.write(); 

    if(withxpmenu)
	xpmenu->readRecents();
}

QString defaults::get_fullname()
{
    QString fullname, cline;

    QString fname(getenv("USER"));

    QFile istr("/etc/passwd");

    if(fname.isNull() || ! istr.open(IO_ReadOnly)){
	if(! fname.isNull())
	    return fname;
    }else{
	while(! istr.atEnd()){
	    istr.readLine(cline, 1024);
	    
	    int indx =0;
	    indx = cline.find(":", indx);
	    if(fname == cline.left(indx)){
		for(int i=0; i < 3; i++)
		    indx = cline.find(":", indx+1);

		QString tmpname = cline.mid(indx+1, 
					    cline.find(":", indx+1) - indx -1);
		
		if(! tmpname.isNull())
		    fullname = tmpname;
	    }
	}

	istr.close();
    }


    return fullname;
}
