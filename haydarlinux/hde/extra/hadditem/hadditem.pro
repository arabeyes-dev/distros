SOURCES	+= main.cpp
unix {
  UI_DIR = .ui
  MOC_DIR = .moc
  OBJECTS_DIR = .obj
}
FORMS	= hadditem.ui \
	iconform.ui

TRANSLATIONS    = ../../files/lng/hadditem_ar.ts \
                  ../../files/lng/hadditem_nl.ts \
                  ../../files/lng/hadditem_es.ts \
                  ../../files/lng/hadditem_fr.ts \
                  ../../files/lng/hadditem_tr.ts

TEMPLATE	=app
CONFIG	+= qt warn_on release
LANGUAGE	= C++
INCLUDEPATH += ../../include
LIBS		+= -L../../lib -lhde
