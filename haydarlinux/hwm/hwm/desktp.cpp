/* 
*  File      : desktp.cpp 
*  Written by: haydar@haydar.net
*  Copyright : GPL
*
*  Manages the Desktop
*/

#include <qdir.h>
#include <qpainter.h>
#include <qpixmap.h>
#include <qregexp.h> 

#include <defaults.h>

#include "desktp.h"
//#include "qapp.h"

#include <X11/cursorfont.h>

QIntDict <QString> exelist;


desktp::desktp(QWidget *parent, const char *name, WFlags f) 
    : QIconView(parent, name, f)
{
    setGeometry(0, 0, QApplication::desktop()->width(), 
		QApplication::desktop()->height());

    /*    
    //    setPaletteBackgroundColor(Qt::darkCyan);
    //    setBackgroundMode(Qt::X11ParentRelative); 
	QApplication::desktop()->setBackgroundColor(defaults::root_bg);
	setPaletteBackgroundColor(defaults::root_bg);
    */

    if(defaults::root_pix.isNull()){ 
	//	qapp::dw->setBackgroundColor(defaults::root_bg);
	QApplication::desktop()->setBackgroundColor(defaults::root_bg);
	setPaletteBackgroundColor(defaults::root_bg);
    }
    
      else{
	  QPixmap tst(defaults::root_pix);
	  QPixmap *rootpix = new QPixmap(QApplication::desktop()->width(), 
					 QApplication::desktop()->height());
	if(defaults::bgDisplay == "Center"){
	    	    
	    
	    QPainter p(rootpix);
	    
	    p.fillRect(0, 0, QApplication::desktop()->width(), 
		       QApplication::desktop()->height(), 
		       QBrush(defaults::root_bg));
	    int xi = (QApplication::desktop()->width()/2) - (tst.width()/2);
	    int yi = (QApplication::desktop()->height()/2) - (tst.height()/2);
	     
	    p.drawPixmap( xi, yi, defaults::root_pix);
	    p.end();
	    
	        
	}else if(defaults::bgDisplay == "Tile"){
	    QPainter p(rootpix);
	    p.fillRect(0, 0, QApplication::desktop()->width(), 
		       QApplication::desktop()->height(), 
		       QBrush(defaults::root_bg));
	    p.drawTiledPixmap( 0, 0, QApplication::desktop()->width(), 
			       QApplication::desktop()->height(), 
			       defaults::root_pix);
	    p.end();


	}else if(defaults::bgDisplay == "Stretch"){
	    QApplication::desktop()->setBackgroundColor(defaults::root_bg);
	    setPaletteBackgroundColor(defaults::root_bg);

	    QImage rootimg(defaults::root_pix);
    
	    rootpix->convertFromImage(rootimg.smoothScale(QApplication::
							  desktop()->width(), 
					QApplication::desktop()->height()));


	}

	    QApplication::desktop()->setBackgroundPixmap(*rootpix);
	    setPaletteBackgroundPixmap(*rootpix);
	    
    }

    setFrameStyle(NoFrame);
    setArrangement(TopToBottom);

    setFocusPolicy(StrongFocus);

    setPaletteForegroundColor(Qt::white);
    exelist.setAutoDelete(TRUE);

    //    rightmnu = new QPopupMenu(this);
    //    setSelectionMode( QIconView::Extended );

    connect( this, SIGNAL( doubleClicked( QIconViewItem * ) ),
	     this, SLOT( iconDblClicked( QIconViewItem * ) ) );
    connect( this, SIGNAL( returnPressed( QIconViewItem * ) ),
	     this, SLOT( iconReturnPressed( QIconViewItem * ) ) );

    connect( this, SIGNAL( rightButtonClicked( QIconViewItem*, const QPoint&)),
	     this, SLOT( rightClicked( QIconViewItem*, const QPoint&)) );

    rightmnu = new QPopupMenu;
    rightmnu->insertItem(tr("New"));
    QPopupMenu *ai = new QPopupMenu;
    rightmnu->insertItem(tr("&Arrange icons"), ai);
    rightmnu->insertSeparator();
    rightmnu->insertItem(tr("Properties"));

    show();
    refreshIV();
}



void desktp::createIcon(QString path, QString fname){

    if(!QFileInfo::QFileInfo(path + "/" + fname).exists())
	return;

    if (path.right(7) == "Desktop"){
	QString svar;

	QIconViewItem *icn = new QIconViewItem( this );

	//	setFocusPolicy(QWidget::ClickFocus);
	hwmItem ditem = defaults::readItem(path + "/" + fname);

	QString iname(ditem.name);

	iname = iname.replace( QRegExp("%2f"), " " );
	icn->setText(iname);

	svar = ditem.icon;
	svar = getIconFile(svar, "large");
	if (svar == "" || ! QFile( svar ).exists())
	    icn->setPixmap(QPixmap(defaults::get_cfile("images/default.xpm")));
	else
	    icn->setPixmap(QPixmap(svar));
	    //	    icn->setPixmap(QPixmap(svar));
	
	//	svar = ditem.exec;
	svar = path + "/" + fname;
	//	cout << svar <<"\n";

	exelist.insert( icn->index(), new QString(svar));


    }

}

void desktp::refreshIV(){

    clear();
        
    
    
    QDir d(defaults::get_cfile("Desktop"));
    d.setFilter( QDir::Files );

    const QFileInfoList *list = d.entryInfoList();
    QFileInfoListIterator it( *list );      // create list iterator
    QFileInfo *fi;                          // pointer for traversing

    while ( (fi=it.current()) ) {           // for each file...
	createIcon(d.path(), fi->fileName());
	++it;                               // go to the next list element
    }
}

void desktp::timeout(void)
{
    //    QApplication::restoreOverrideCursor();
}

void desktp::iconDblClicked( QIconViewItem *icn ){
    QString cmd(*exelist.find(icn->index())); 

    if(cmd.isNull())
	return;

    //    cout << cmd << endl;

    /*
    Cursor cur ;
    cur = XCreateFontCursor(qt_xdisplay(), XC_watch);
    XDefineCursor(qt_xdisplay(), winId(), cur);
    */
    execCmd(cmd); // fixed
}

void desktp::iconReturnPressed( QIconViewItem *icn ){
    QString cmd(*exelist.find(icn->index())); 

    if(cmd.isNull())
	return;

    /*
    Cursor cur ;
    cur = XCreateFontCursor(qt_xdisplay(), XC_watch);
    XDefineCursor(qt_xdisplay(), winId(), cur);
    */

    //    QApplication::setOverrideCursor( Qt::WaitCursor );
    execCmd(cmd); // fixed
    // QApplication::restoreOverrideCursor();
}

void desktp::rightClicked( QIconViewItem* item, const QPoint& pos )
{
QString cmd;
if(item)
    cmd = *exelist.find(item->index()); 

    if(cmd.isNull()){
	rightmnu->exec(pos);
    }else{
	QPopupMenu *itemMenu = new QPopupMenu;
	itemMenu->insertItem(tr("&Rename"));
	itemMenu->insertItem(tr("&Delete"));
	itemMenu->insertSeparator();
	itemMenu->insertItem(tr("&Properties"));
	itemMenu->exec(pos);
    }
}
