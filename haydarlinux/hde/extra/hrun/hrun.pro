HEADERS  +=  hrun.h
SOURCES  +=  main.cpp hrun.cpp
TARGET   =  hrun
SRCMOC   +=  moc_hrun.cpp
TRANSLATIONS    = ../../files/lng/hrun_ar.ts \
                  ../../files/lng/hrun_nl.ts \
                  ../../files/lng/hrun_es.ts \
                  ../../files/lng/hrun_fr.ts \
                  ../../files/lng/hrun_tr.ts

INCLUDEPATH += ../../include /usr/X11R6/include
LIBS += -L../../lib -lhde
CONFIG    += qt thread
LANGUAGE	= C++
