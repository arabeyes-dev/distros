#include <qpainter.h> 
#include <qapplication.h>
#include <qfontmetrics.h>
#include <qtimer.h> 

#include <qbitmap.h>

#include <libhwm.h>
#include "smenuitem.h"
#include "defs.h"


sMenuItem::sMenuItem( QWidget *parent, const char *name )
    : QWidget( parent, name )
{
  haspopup = false;
  pixWidth = 0;
  pixHeight = 0;
}


void sMenuItem::drawIt(void)
{

    /*
    if( (pixWidth > 0 ) && ( pixHeight > 0 ) ){
	QImage img = pix.convertToImage();
	pix.resize(pixWidth, pixHeight);
	pix.convertFromImage(img.smoothScale(pixWidth, pixHeight) );
    }

    cout << "Pix Width: " << QString::number(pix.width()) << " Pix Height: " <<
	QString::number(pix.height()) << "\n";
    */

   QPainter *p = new QPainter( this );

  QFont f(font());
  p->setFont( f );
  
  int wd = 30;
  if(! str.isEmpty())
    wd = QFontMetrics(f).width(str) + 10;
  if( !pix.isNull() )
    wd += 32;

  if(haspopup)
    wd += 20;

  setMinimumWidth(wd);

  //  resize(wd, 32);

  
    if( !orig_pic.isNull() ){
	QImage img = orig_pic.convertToImage ();
	if( (pixWidth > 0 ) && ( pixHeight > 0 ) )
	    pix.convertFromImage(img.smoothScale(pixWidth, pixHeight) );
	else
	    pix.convertFromImage(img.smoothScale(height(),height()));
    }
  

  if(hasMouse()){
      p->fillRect(5, 0 ,width()-10,height(), palette().active().highlight() );
      p->setPen( palette().active().highlightedText() );
  }else{
      //       erase(1,1,width()-1,height()-1);
      //      erase(contentsRect());
      p->setPen( palette().active().foreground () );
      erase(5, 0,width()-10,height());
  }

  int texts = 2;
  if(QApplication::reverseLayout()){
      texts = 10;
    if( !pix.isNull() ){
	//    cout << "Pix Width: " << QString::number(pix.width()) << " Pix Height: " <<
	//	QString::number(pix.height()) << "\n";
	/*
	p->drawPixmap ( width() - pix.width() -5, (heightSpacing/2) + 
			(height()/2 ) - (pix.height()/2), pix, 0, 0,
			pix.width()- heightSpacing, pix.height()- 
			(heightSpacing/2));
	*/
	p->drawPixmap( width() - pix.width()-5,(height()/2 )-(pix.height()/2),
		       pix, 0, 0, pix.width(), pix.height());
	texts = 15;
    }

    if(! str.isEmpty())
      p->drawText( 5 , (height()/2 ) - (QFontMetrics(font()).height()/2) , 
		   width()-pix.width()-texts, height(), Qt::AlignRight, str );


    if(haspopup){
      p->drawLine( 10 ,(height()/2)-3, 10 , (height()/2)+3);
      p->drawLine( 9 ,(height()/2)-2, 9, (height()/2)+2);
      p->drawLine( 8 ,(height()/2)-1, 8, (height()/2)+1);
      p->drawLine( 7 ,(height()/2), 7, (height()/2));
    }
  

  }else{
    if( !pix.isNull() ){
      p->drawPixmap ( 5, (height()/2 ) - (pix.height()/2), pix, 0, 0, 
		      pix.width(), pix.height());
      texts = pix.width() +2;
    }

    if(! str.isEmpty())
      p->drawText( texts +5 , (height()/2 ) - (QFontMetrics(font()).height()/2), width()-6, height(), Qt::AlignAuto, str );
  
    if(haspopup){
      p->drawLine( width() - 10 ,(height()/2)-3,width() - 10, (height()/2)+3);
      p->drawLine( width() - 9 ,(height()/2)-2,width() - 9, (height()/2)+2);
      p->drawLine( width() - 8 ,(height()/2)-1,width() - 8, (height()/2)+1);
      p->drawLine( width() - 7 ,(height()/2),width() - 7, (height()/2));
    }
  }

  p->end();

//  setBackgroundPixmap(*pixi);

}

void sMenuItem::enterEvent ( QEvent * )
{ 
  drawIt(); 
  if(haspopup)
    QTimer::singleShot( 1000, this, SLOT(pop()) );
}

bool sMenuItem::connectItem( const QObject * receiver, const char * member )
{
  connect( this, SIGNAL( clicked() ), receiver, member);
  return false;
}

bool sMenuItem::disconnectItem( const QObject * receiver, const char * member)
{
  disconnect( this, SIGNAL( clicked() ), receiver, member);
  return false;
}

void sMenuItem::mouseReleaseEvent ( QMouseEvent * e )
{
  if(haspopup)
    pop();
  else
    emit clicked();
}

/*
void sMenuItem::mousePressEvent ( QMouseEvent * e )
{
  emit clicked();
}
*/

void sMenuItem::pop(void)
{
  if(hasMouse()){
      QPoint p = mapToGlobal(QPoint(0, 0));
      QSize s(popup->sizeHint());
      p.setY(p.y()-s.height() + height() + 5);
    if(QApplication::reverseLayout()){
	p.setX(p.x()-s.width() +5);
    }else{
	p.setX(p.x() + width() +5);
    }
      popup->exec(p);
  }
}

void sMenuItem::setCommand(QString com)
{
    command = com;
    disconnect( this,SLOT(execCommand()));
    connectItem(this, SLOT(execCommand()));
}



void sMenuItem::execCommand(void)
{
    /*
    pid_t pid;
    
    if(command.isNull())
	return;

    if((pid = fork()) == -1){
	perror("fork()");
	return;
    }	
    if(pid)
	return;
    */

    execCmd(command);
}

void sMenuItem::setPixmap(QPixmap pic)
{ 
    /*
    if( (pixWidth > 0 ) && ( pixHeight > 0 ) )
	pix = QPixmap(pixWidth, pixHeight);
    else
	pix = QPixmap(pic.size());
    */
    pix = pic;
    orig_pic = pic;

    drawIt();
}
