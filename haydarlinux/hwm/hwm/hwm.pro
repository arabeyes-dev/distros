
TEMPLATE = app
TARGET   =  hwm

SOURCES += main.cpp qapp.cpp toolbar.cpp xwindow.cpp procbar.cpp winlist.cpp\
	  menu.cpp pager.cpp apbar.cpp keyboard.cpp winfo.cpp rubber.cpp \
	  wborder.cpp desktp.cpp atoms.cpp

HEADERS += qapp.h toolbar.h xwindow.h procbar.h winlist.h menu.h pager.h \ 
	  apbar.h keyboard.h winfo.h rubber.h wborder.h desktp.h atoms.h \
	  conf.h defs.h


INCLUDEPATH += ../include /usr/X11R6/include 

LIBS += -L../lib -lhwm

TRANSLATIONS    = ../files/lng/hwm_ar.ts \
                  ../files/lng/hwm_nl.ts \
                  ../files/lng/hwm_fr.ts \
                  ../files/lng/hwm_es.ts \
                  ../files/lng/hwm_tr.ts

#CONFIG += qt warn_on
CONFIG    += qt thread 
LANGUAGE	= C++
