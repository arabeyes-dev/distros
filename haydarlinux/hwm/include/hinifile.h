/* CConfigFile.h */ 
   
#ifndef CCONFIGFILE_H 
#define CCONFIGFILE_H 
   
#include "hinigroup.h" 

#include <qstringlist.h>  
   
typedef QMap<QString, HIniGroup*> ConfigMap; 
   
class HIniFile { 
 public: 
    HIniFile(); 
    HIniFile(const QString& filename); 
    ~HIniFile(); 
   
    // clean up 
    void clear(); 
    
    // file 
    void setName(const QString& filename); 
    const QString& name() const { return fname; } 
    void write(); 
    void read(); 
   
    // current group 
    void setGroup(const QString& group); 
    const QString& group() const; 
    
    // group info 
    unsigned int numGroups() const { return map.count(); } 
    QStringList groupList(); 
    
    // write entries 
    void writeEntry(const QString& key, const QString& value); 
    void writeString(const QString& key, const QString& value); 
    void writeInt(const QString& key, int value); 
    void writeDouble(const QString& key, double value); 
    void writeFloat(const QString& key, float value); 
    void writeBool(const QString& key, bool value); 
    
    // read entries 
    const QString& readString(const QString& key,const QString& default_value);
    int readInt(const QString& key, int default_value); 
    double readDouble(const QString& key, double default_value);
    float readFloat(const QString& key, float default_value);
    bool readBool(const QString& key, bool default_value); 
   
 private:    
    QString fname; 
    ConfigMap map; 
    HIniGroup* currentgroup; 
};	 
   
#endif 
