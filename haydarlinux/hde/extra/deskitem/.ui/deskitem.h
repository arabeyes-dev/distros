/****************************************************************************
** Form interface generated from reading ui file 'deskitem.ui'
**
** Created: Fri Apr 2 23:55:57 2004
**      by: The User Interface Compiler ($Id$)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#ifndef DESKITEM_H
#define DESKITEM_H

#include <qvariant.h>
#include <qdialog.h>

class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;
class QSpacerItem;
class QPushButton;
class QTabWidget;
class QWidget;
class QLineEdit;
class QComboBox;
class QFrame;
class QLabel;
class QGroupBox;
class QCheckBox;

class deskitem : public QDialog
{
    Q_OBJECT

public:
    deskitem( QWidget* parent = 0, const char* name = 0, bool modal = FALSE, WFlags fl = 0 );
    ~deskitem();

    QPushButton* okBtn;
    QPushButton* ApplyBtn;
    QPushButton* cancelBtn;
    QTabWidget* itemTab;
    QWidget* tab;
    QPushButton* iconBtn;
    QLineEdit* nameEdit;
    QComboBox* langCombo;
    QFrame* spaceFrame;
    QLabel* execLabel;
    QLineEdit* execEdit;
    QFrame* spaceFrame_2;
    QLabel* textLabel10;
    QLabel* textLabel12;
    QLabel* mimeLbl;
    QLabel* typeLbl;
    QFrame* spaceFrame_3;
    QLabel* accessedLbl;
    QLabel* modifiedLbl;
    QLabel* textLabel18;
    QLabel* textLabel16;
    QFrame* spaceFrame_4;
    QLabel* textLabel20;
    QLabel* sizeLbl;
    QWidget* permissionsTab;
    QGroupBox* groupBox1;
    QLabel* textLabel1;
    QComboBox* ownerBox;
    QComboBox* groupBox;
    QLabel* textLabel2;
    QGroupBox* groupBox2;
    QCheckBox* aWrite;
    QLabel* textLabel4;
    QCheckBox* aRead;
    QCheckBox* gRead;
    QCheckBox* oRead;
    QCheckBox* aExec;
    QLabel* textLabel5;
    QLabel* textLabel3;
    QCheckBox* gExec;
    QCheckBox* oWrite;
    QCheckBox* oExec;
    QCheckBox* gWrite;
    QFrame* line1;
    QLabel* textView;
    QLabel* textLabel7;
    QLabel* numberView;
    QLabel* textLabel6;

    bool setFile( QString filename );
    virtual bool newFunction();

public slots:
    virtual void okBtn_clicked();
    virtual void ApplyBtn_clicked();
    virtual void iconBtn_clicked();

protected:
    QHBoxLayout* Layout2;
    QSpacerItem* Spacer1;
    QVBoxLayout* layout18;
    QHBoxLayout* layout6;
    QVBoxLayout* layout5;
    QHBoxLayout* layout4;
    QSpacerItem* spacer2;
    QHBoxLayout* layout3;
    QGridLayout* layout15;
    QGridLayout* layout16;
    QHBoxLayout* layout17;
    QGridLayout* layout10;
    QVBoxLayout* layout14;
    QGridLayout* layout14_2;
    QGridLayout* layout13;

protected slots:
    virtual void languageChange();

};

#endif // DESKITEM_H
