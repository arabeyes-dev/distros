<!DOCTYPE TS><TS>
<context>
    <name>Editor</name>
    <message>
        <source>&amp;Yes</source>
        <translation>&amp;نعم</translation>
    </message>
    <message>
        <source>&amp;No</source>
        <translation>&amp;كلا</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>إلغاء الأمر</translation>
    </message>
    <message>
        <source>&amp;File</source>
        <translation>&amp;ملف</translation>
    </message>
    <message>
        <source>&amp;New</source>
        <translation>&amp;جديد</translation>
    </message>
    <message>
        <source>&amp;Open</source>
        <translation>&amp;فتح</translation>
    </message>
    <message>
        <source>&amp;Save</source>
        <translation>&amp;حفظ</translation>
    </message>
    <message>
        <source>Open &amp;as</source>
        <translation>فتح &amp;بإسم</translation>
    </message>
    <message>
        <source>Save &amp;as</source>
        <translation>حفظ ب&amp;إسم</translation>
    </message>
    <message>
        <source>Add &amp;encoding</source>
        <translation>إظافة &amp;مشفر</translation>
    </message>
    <message>
        <source>&amp;Print</source>
        <translation>&amp;طباعة</translation>
    </message>
    <message>
        <source>&amp;Close</source>
        <translation>إ&amp;غلاق</translation>
    </message>
    <message>
        <source>&amp;Quit</source>
        <translation>إن&amp;هاء المهمة</translation>
    </message>
    <message>
        <source>&amp;Edit</source>
        <translation>&amp;تحرير</translation>
    </message>
    <message>
        <source>To &amp;uppercase</source>
        <translation>حروف &amp;كبيرة</translation>
    </message>
    <message>
        <source>To &amp;lowercase</source>
        <translation>حروف &amp;صغيرة</translation>
    </message>
    <message>
        <source>&amp;Select Font</source>
        <translation>اختيار الأ&amp;حرف</translation>
    </message>
    <message>
        <source>HWM Notepad</source>
        <translation></translation>
    </message>
    <message>
        <source>Save changes to Document?</source>
        <translation>حفظ التغييرات؟</translation>
    </message>
    <message>
        <source>&amp;Undo</source>
        <translation>&amp;تراجع</translation>
    </message>
    <message>
        <source>&amp;Redo</source>
        <translation>ت&amp;كرار</translation>
    </message>
    <message>
        <source>&amp;Copy</source>
        <translation>&amp;نسخ</translation>
    </message>
    <message>
        <source>&amp;Paste</source>
        <translation>&amp;لصق</translation>
    </message>
    <message>
        <source>Cu&amp;t</source>
        <translation>&amp;قص</translation>
    </message>
    <message>
        <source>Clear</source>
        <translation>مسح</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation>&amp;تعليمات</translation>
    </message>
    <message>
        <source>&amp;About</source>
        <translation>&amp;حول</translation>
    </message>
    <message>
        <source>&amp;Find</source>
        <translation>&amp;بحث</translation>
    </message>
    <message>
        <source>Find &amp;Next</source>
        <translation>بحث ال&amp;تالي</translation>
    </message>
</context>
</TS>
