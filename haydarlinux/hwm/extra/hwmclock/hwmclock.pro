HEADERS  =  dclock.h 
SOURCES  =  main.cpp dclock.cpp 
TARGET   =  hwmclock
SRCMOC   =  moc_dclock.cpp
TRANSLATIONS    = ../../files/lng/hwmclock_ar.ts \
                  ../../files/lng/hwmclock_es.ts \
                  ../../files/lng/hwmclock_fr.ts \
                  ../../files/lng/hwmclock_nl.ts


INCLUDEPATH += ../../include
LIBS		+= -L../../lib -lhwm
CONFIG    += qt thread

#INCLUDEPATH	+=  /usr/local/hwm/include
#LIBS		+= -L/usr/local/lib -lhwm
