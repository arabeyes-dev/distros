/* main.cpp */

#include "defs.h"
#include "conf.h"
#include "keyboard.h"
#include "winfo.h"
#include "qapp.h"
#include "defaults.h"
#include <qtooltip.h>
#include "config.h"

using std:: cout; using std::endl;

#define ChildMask (SubstructureRedirectMask|SubstructureNotifyMask)
#define ButtonMask (ButtonPressMask|ButtonReleaseMask)
#define MouseMask (ButtonMask|PointerMotionMask)

bool wm_init=TRUE;
char*		displayName = NULL;    /* Display name */

// get already running clients


void make_bpixmaps(void){
    int hh;

    /*
    if ((defaults::styleName == "win98Style") || 
	(defaults::styleName == "win95Style"))
    */
    /*
    if ( defaults::titleStyle == "win")
	hh = defaults::windowbuttonsize-10;
    else
	hh = defaults::windowbuttonsize-8;

    int wh = defaults::windowbuttonsize-8;
    */

    if ( defaults::titleStyle == "win")
	hh = defaults::iconsize-2;
    else
	hh = defaults::iconsize;

    int wh = defaults::iconsize;

    QImage limg(qapp::get_cfile(defaults::lefticon));

    QImage rimg(qapp::get_cfile(defaults::closeicon));
    QImage nimg(qapp::get_cfile(defaults::minicon));
    QImage ximg(qapp::get_cfile(defaults::maxicon));
    QImage himg(qapp::get_cfile(defaults::helpicon));
    QImage eimg(qapp::get_cfile(defaults::resticon));

    QImage pimg(qapp::get_cfile(defaults::deficon));

    QImage timg(qapp::get_cfile(defaults::titleimage));
    QImage trimg(qapp::get_cfile(defaults::titlerimage));
    QImage tlimg(qapp::get_cfile(defaults::titlelimage));
    QImage utimg(qapp::get_cfile(defaults::untitleimage));
    QImage utrimg(qapp::get_cfile(defaults::untitlerimage));
    QImage utlimg(qapp::get_cfile(defaults::untitlelimage));


    qapp::leftwinpix = new QPixmap();
    qapp::rightwinpix = new QPixmap();
    qapp::minbtnpix = new QPixmap();
    qapp::maxbtnpix = new QPixmap();
    qapp::helpbtnpix = new QPixmap();
    qapp::restorebtnpix = new QPixmap();
    qapp::defaultpix = new QPixmap();
	
    qapp::titlepix = new QPixmap();
    qapp::titlerightpix = new QPixmap();
    qapp::titleleftpix = new QPixmap();
    qapp::untitlepix = new QPixmap();
    qapp::untitlerightpix = new QPixmap();
    qapp::untitleleftpix = new QPixmap();

    if(! limg.isNull())
	qapp::leftwinpix->convertFromImage(limg.smoothScale(wh, hh));
	
    if(! rimg.isNull())
	qapp::rightwinpix->convertFromImage(rimg.smoothScale(wh, hh));

    if(! nimg.isNull())
	qapp::minbtnpix->convertFromImage(nimg.smoothScale(wh, hh));

    if(! ximg.isNull())
	qapp::maxbtnpix->convertFromImage(ximg.smoothScale(wh, hh));

    if(! himg.isNull())
	qapp::helpbtnpix->convertFromImage(himg.smoothScale(wh, hh));

    if(! eimg.isNull())
	qapp::restorebtnpix->convertFromImage(eimg.smoothScale(wh, hh));

    if(! pimg.isNull())
	qapp::defaultpix->convertFromImage(pimg.smoothScale(defaults::windowbuttonsize-8, defaults::windowbuttonsize-8));


    if(! timg.isNull())
	qapp::titlepix->convertFromImage(timg.smoothScale(timg.width(), 
						defaults::windowbuttonsize));
    if(! trimg.isNull())
	if(QApplication::reverseLayout())
	    qapp::titlerightpix->convertFromImage(trimg.mirror(TRUE,FALSE).smoothScale(trimg.width(), defaults::windowbuttonsize));
	else
	    qapp::titlerightpix->convertFromImage(trimg.smoothScale(trimg.width(),defaults::windowbuttonsize));

    if(! tlimg.isNull())
	if(QApplication::reverseLayout())
	    qapp::titleleftpix->convertFromImage(tlimg.mirror(TRUE,FALSE).smoothScale(tlimg.width(), defaults::windowbuttonsize));
	else
	    qapp::titleleftpix->convertFromImage(tlimg.smoothScale(tlimg.width(), defaults::windowbuttonsize));

    if(! utimg.isNull())
	qapp::untitlepix->convertFromImage(utimg.smoothScale(utimg.width(), 
						defaults::windowbuttonsize));
    if(! utrimg.isNull())
	if(QApplication::reverseLayout())
	    qapp::untitlerightpix->convertFromImage(utrimg.mirror(TRUE,FALSE).smoothScale(utrimg.width(), defaults::windowbuttonsize) );
	else
	    qapp::untitlerightpix->convertFromImage(utrimg.smoothScale(utrimg.width(), defaults::windowbuttonsize) );

    if(! utlimg.isNull())
	if(QApplication::reverseLayout())
	    qapp::untitleleftpix->convertFromImage(utlimg.mirror(TRUE,FALSE).smoothScale(utlimg.width(), defaults::windowbuttonsize));
	else
	    qapp::untitleleftpix->convertFromImage(utlimg.smoothScale(utlimg.width(), defaults::windowbuttonsize));

}

void getrunprocs(void){
    Window w,w1,w2,*wins;
    uint nwins,cwin;
    XWindowAttributes attr;

    if(XQueryTree(qapp::dpy, qapp::root, &w1, &w2, &wins, 
		  &nwins) == 0 || ! nwins)
	return;
	
    bool surgent = defaults::starturgent;
    defaults::starturgent = FALSE;

    for(cwin=0; cwin < nwins; cwin++){
	w = wins[cwin];

	if(w == qapp::dsktp->winId())
	    continue;
		
	if(w == qapp::tb->winId())
	    continue;

	XGetWindowAttributes(qapp::dpy, w, &attr);

	if(attr.map_state == IsViewable && ! attr.override_redirect)
	    qapp::run_client(w);
    }
    XSync(qapp::dpy, FALSE);
    defaults::starturgent = surgent;

  if(nwins)
    XFree(wins);

}


void sig_hup(int){
    qapp::sighup = TRUE;
}

// terminate
void sig_term(int){
    tb_pb->remove_all();
    clients.clear();
    XSync(qapp::dpy, FALSE);

    QApplication::exit();
    exit(0);
}

// start programs from init stack
void startprg(void){
    pid_t pid;
    QString prg;
    while(! defaults::initexec.isEmpty()){
	prg = *defaults::initexec.pop();
		
	if((pid = fork()) == 0){
	    execl("/bin/sh", "sh", "-c", (const char *)prg, NULL);
	    perror("Exec");
	    exit(1);
	}
		
	if(pid == -1)
	    perror("fork");
    }
}

// create scaled window button pixmaps


int xerrors(Display *d, XErrorEvent *event){
    char	msg[100];
    char	req[100];
    char	nmb[100];

    if(wm_init == TRUE && event->error_code == BadAccess){
	cerr << "Another WM is already running\n";
	exit(1);
    }

    if( (event->error_code == BadDrawable) || 
	(event->error_code == BadWindow) ||
	(event->error_code == BadMatch) ){
	//cout << "BAD WINDOW!!\n";
	// BadPixmap BadColor
	return 0;
    }

#ifndef DEBUGMSG
    if(event->error_code == BadWindow || event->error_code == BadMatch)
	return 0;
#endif

    XGetErrorText(d, event->error_code, msg, sizeof(msg));
    sprintf(nmb, "%d", event->request_code);
    XGetErrorDatabaseText(d, "XRequest", nmb, nmb, req, sizeof(req));

    cerr << "WM: " << req << " on resource " << event->resourceid 
	 << " failed: " << msg << '\n';
		
	return 0;
}


void exec_cmd( QString cmd ){
    execCmd(cmd); // fixed
}

int main(int argc, char **argv){
    for(int i=1; i < argc; i++){
	if(strcmp(argv[i],"--version")== 0||strcmp(argv[i],"-v") == 0){
	    cerr << "Haydar Windows Manager version: "<< DVERSION;
	    return(0);
	}
		
	if(strcmp(argv[i], "-restart") == 0)
	    defaults::start_restart = TRUE;

	if(strcmp(argv[i], "-c") == 0 && argc > i+1)
	    defaults::cfdir = argv[++i];
    }

    // get config dir
    if(defaults::cfdir.isNull()){
	QString fname(getenv("HOME"));

	if(! fname.isNull())
	    defaults::cfdir = fname + "/.hde";
    }

    if(QFileInfo( defaults::cfdir ).exists() ){
	QString homedir(getenv("HOME"));
	if( !QFileInfo( defaults::cfdir + "/Desktop" ).exists() ){
	    if(QFileInfo( homedir + "/Desktop" ).exists())
		exec_cmd( "ln -s " + homedir + "/Desktop " + defaults::cfdir 
			  + "/Desktop" );	
	    else
		exec_cmd( "cp -aR " + qapp::get_cfile("userfiles/Desktop ") +
			  defaults::cfdir );
	}
	if(!QFileInfo( defaults::cfdir + "/appdefaults" ).exists())
	    exec_cmd( "cp -aR " + qapp::get_cfile("appdefaults ") + 
		      defaults::cfdir );
	if(!QFileInfo( defaults::cfdir + "/defaults" ).exists())
	    exec_cmd( "cp -aR " + qapp::get_cfile("defaults ") + 
		      defaults::cfdir );
    }else{
	exec_cmd( "mkdir -p " + defaults::cfdir );
	QString homedir(getenv("HOME"));
	if( !QFileInfo( defaults::cfdir + "/Desktop" ).exists() ){
	    QString homedir(getenv("HOME"));
	    if(QFileInfo( homedir + "/Desktop" ).exists())
		exec_cmd( "ln -s " + homedir + "/Desktop " + defaults::cfdir 
			  + "/Desktop" );	
	    else
		exec_cmd( QString("cp -aR ") + CONFDIR +"/userfiles/Desktop " +
			  defaults::cfdir );
	}
	exec_cmd( QString("cp -aR ") + CONFDIR + "/appdefaults " + 
		      defaults::cfdir );
	exec_cmd( QString("cp -aR ") + CONFDIR + "/defaults " + 
		      defaults::cfdir );
    }

    defaults::argc = argc;
    defaults::argv = argv;
    
     /* 
    if ((qapp::dpy = XOpenDisplay(displayName)) == NULL) {
	cout << "Can't open display" <<  XDisplayName(displayName)<< endl;
	exit(1);
    }
     */

    Display* dpy = XOpenDisplay( NULL );

    //	qapp::dpy = XOpenDisplay(NULL);
	if (!dpy) {
	    cerr << "can't open display '"<< getenv("DISPLAY");
	    cerr << "' (is $DISPLAY set properly?)" << endl;
		exit(1);
	}
    XCloseDisplay( dpy );
    dpy = 0;

 //   qapp::dpy = qt_xdisplay();

    //    qapp a(qapp::dpy, argc, argv);
    qapp a( argc, argv);
 
    // a.setStyle("Windows");

    //    qapp::dw = (desktopW *)a.desktop();
    defaults::read_config();
    QTranslator translator( 0 );
    QTranslator qt_translator( 0 );
    translator.load( "hde_" + defaults::lng , qapp::get_cfile("lng") );

    QTranslator libtranslator( 0 );
    libtranslator.load( "libhde_" + defaults::lng , qapp::get_cfile("lng") );

   QString QTDIR = getenv( "QTDIR" );

    qt_translator.load( QString( "qt_%1" ).arg( defaults::lng ), 
			QTDIR + "/translations" );

    a.installTranslator( &qt_translator );
    a.installTranslator( &translator );
    a.installTranslator( &libtranslator );
    //    if (defaults::lng == "ar")
    //	a.setReverseLayout ( true );
    clients.setAutoDelete(TRUE);


    a.setOverrideCursor( QCursor(Qt::WaitCursor) );

    //    QWidget *dskWid = new QWidget(0, "DesktopWidget", Qt::WType_Desktop 
    //				  | Qt::WPaintUnclipped);
    //    qapp::dsktp = new desktp(dskWid, "Desktop");
    qapp::dsktp = new desktp(0, "DesktopWidget", Qt::WResizeNoErase | Qt::WStyle_Customize | Qt::WStyle_NoBorder);
    qapp::dsktp->show();
    //    cout << "Good\n";
    
    qapp::tb = new Toolbar(0, "Toolbar");
    qapp::read_cprops();
    make_bpixmaps();
    qapp::winf = new winfo(0, "winfo");
    
    //    a.setMainWidget(qapp::dsktp);

    //        a.setMainWidget(qapp::tb);

    //qapp::ws = new QWorkspace( qapp::dsktp );
    //qapp::ws->setScrollBarsEnabled( true );
    //qapp::ws->setGeometry(0,0, a.desktop()->width(),a.desktop()->height());

    a.setMainWidget(qapp::dsktp);

	
    XSetErrorHandler(xerrors);

    XIconSize *isize;
    if((isize = XAllocIconSize()) != NULL){
	isize->min_width = isize->min_height = 2;
	isize->max_width = isize->max_height = 200;
	isize->width_inc = isize->height_inc = 1;

	XSetIconSizes(qapp::dpy, qapp::root, isize, 1);
	XFree(isize);
    }	

    qapp::upal =new QPalette(defaults::urgent_bg, defaults::urgent_bg);
    qapp::ipal =new QPalette(defaults::inactive_bg,defaults::inactive_bg);
    qapp::apal =new QPalette(defaults::active_bg,defaults::active_bg);
    
    
    XSelectInput(qapp::dpy, qapp::root,SubstructureRedirectMask);
    a.syncX(); // trigger error now


    if(defaults::sttiled[0]){
	qapp::tdesks[0] = TRUE;
	defaults::sttiled[0] = FALSE;
    }
    tb_wl->set_pixmap();

    getrunprocs();
    defaults::start_restart = FALSE;
    
    // select windowmanager privileges
    XSelectInput(qapp::dpy, qapp::root,
                 KeyPressMask |
                 PropertyChangeMask |
                 ColormapChangeMask |
                 SubstructureRedirectMask |
                 SubstructureNotifyMask
                 );
    /*
    XSelectInput(qapp::dpy, qapp::root,
		 LeaveWindowMask | EnterWindowMask |
		 PropertyChangeMask | SubstructureRedirectMask | 
		 KeyPressMask | ButtonPressMask | ButtonReleaseMask | 
		 SubstructureNotifyMask | ButtonMask | PointerMotionMask);
    */
    /*
		 SubstructureNotifyMask|SubstructureRedirectMask| 
		 ButtonPressMask|
		 PropertyChangeMask|
		 KeyPressMask|
		 ColormapChangeMask|
		 EnterWindowMask);
    */
    a.syncX(); // trigger error now
    //      XSync(qapp::dpy, FALSE);

    struct sigaction act;
	
    act.sa_handler = sig_hup;
    act.sa_flags = SA_RESTART;
    sigaction(SIGHUP, &act, NULL);

    act.sa_handler = sig_term;
    sigaction(SIGTERM, &act, NULL);


    keyboard::init();
    wm_init = FALSE;
    startprg();

    a.restoreOverrideCursor();

    //return(a.exec());
    a.exec();

    return 0;
}
