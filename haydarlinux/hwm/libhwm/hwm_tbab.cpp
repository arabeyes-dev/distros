/*  
*   File      : dclock.cpp
*   Written by: haydar@haydar.net
*   Copyright : GPL
*
*   Shows a label with current time.
*/

#include <hwm_tbab.h>
#include <defaults.h>



hwm_tbab::hwm_tbab(QWidget *parent, const char *name) : QLabel(parent, name)
{
    /*
#ifdef Q_WS_X11 
    XClassHint ch;
    ch.res_name = "hwm_tbab";
    ch.res_class = "hwm_tbab";

    XSetClassHint(qt_xdisplay(), winId(), &ch);
#endif
    */
	Display *dsp = x11Display(); // get the display
	WId win = winId();           // get the window
	int r;
	int data = 1;
	r = XInternAtom(dsp, "KWM_DOCKWINDOW", false);
	XChangeProperty(dsp, win, r, r, 32, 0, (uchar *)&data, 1);
	r = XInternAtom(dsp, "_KDE_NET_WM_SYSTEM_TRAY_WINDOW_FOR", false);
	XChangeProperty(dsp, win, r, XA_WINDOW, 32, 0, (uchar *)&data, 1);

}

void hwm_tbab::drawFrame ( QPainter * p ){
    if (defaults::styleName == "hwmStyle"){
	QColor firstColor(palette().active().background());
	
	int h = (height()-4) /2;
	int i = 0;

	for ( i = 0; i < h-1; ++i ) {
	    p->setPen(firstColor.light(100 + (h*3)-(i*3)));
	    p->drawLine(0,i , width(), i); 
	}

	int j = 0;
	for ( i = height()-h-1; i < height() ; ++i ) {
	    j++;
	    p->setPen(firstColor.dark(100 +(j*3)));
	    p->drawLine(0, i , width(), i); 
	}

	p->end();   
    }else{

    }
}
