HEADERS  +=  biff.h
SOURCES  +=  main.cpp biff.cpp
TARGET   =  biff
SRCMOC   +=  moc_biff.cpp
#TRANSLATIONS    = ../files/lng/hrun_ar.ts \
#                  ../files/lng/hrun_nl.ts

INCLUDEPATH += ../../include /usr/X11R6/include
LIBS += -L../../lib -lhwm
CONFIG    += qt thread
LANGUAGE        = C++

