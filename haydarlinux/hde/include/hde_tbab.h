/*  
*   File      : hde_tbab.h
*   Written by: haydar@haydar.net
*   Copyright : GPL
*
*   Shows a label with current time.
*/

#include <qlabel.h>

class hde_tbab : public QLabel
{

protected:
	virtual void drawFrame ( QPainter * p );
public:
	hde_tbab(QWidget *parent=0, const char *name=0);
};
