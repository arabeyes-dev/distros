#include <qapplication.h>
#include "hdisplay.h"

int main( int argc, char ** argv )
{
    QApplication a( argc, argv );

    defaults::read_config();

    QTranslator translator( 0 );
    translator.load( "hdisplay_" + defaults::lng ,defaults::get_cfile("lng"));
    a.installTranslator( &translator );
    if (defaults::lng == "ar")
	a.setReverseLayout ( true );

    hDisplay *w = new hDisplay;
    w->show();
    a.connect( &a, SIGNAL( lastWindowClosed() ), &a, SLOT( quit() ) );
    return a.exec();
}
