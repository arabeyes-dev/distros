/* 
*  File      : winlist.cpp
*  Copyright : GPL
*
*  Creates the winlist button
*/

#include "defs.h"
#include "defaults.h"
#include "qapp.h"
#include "toolbar.h"
#include "winlist.h"
//#include "moc_winlist.cpp"

#include "winlist.h"

winlist::winlist(QWidget *parent, const char *name) : wbutton(parent, name)
{
    if((defaults::wininfoHeight>9)&& (defaults::starticonplace != "MIDDLE"))
	setFixedSize(defaults::wininfoHeight, defaults::tb_height);
    else
	setFixedSize(defaults::tc_height, defaults::tc_height);

    wmenu = new QPopupMenu(this);
    Q_CHECK_PTR(wmenu);
    connect(wmenu, SIGNAL(highlighted(int)), SLOT(highlight_pager(int)));

    int wh = width()-4;
    QImage oimg(qapp::get_cfile("images/winlist.xpm"));
    QImage timg(qapp::get_cfile("images/tiled.xpm"));

    if(! oimg.isNull())  // scale pixmap
	winlistpix.convertFromImage(oimg.smoothScale(wh, wh));

    if(! timg.isNull()) 
	tiledpix.convertFromImage(timg.smoothScale(wh, wh));
}


void winlist::set_pixmap(void)
{
    static int showtiled = 0;

    if(qapp::is_tileddesk()){
	if(showtiled == 1)
	    return;

	setPixmap(tiledpix);
	showtiled = 1;
    }else{
	if(showtiled == 2)
	    return;

	setPixmap(winlistpix);
	showtiled = 2;
    }	
}


void winlist::paintEvent( QPaintEvent *p ){
    if(QFileInfo(qapp::get_cfile(defaults::wininfo_bg)).isFile()){
	QPainter *p = new QPainter(this);
	if(QFileInfo(qapp::get_cfile(defaults::toolbar_bg)).isFile()){
	    //	    QPixmap qp(width(), height());
	    //QPainter painter(&qp);

	    QImage img(qapp::get_cfile(defaults::toolbar_bg));
	    if(! img.isNull())  // make scaled pixmap
	    {
		QPixmap pix;
		pix.convertFromImage(img.smoothScale(width(), height() ));
		//painter.drawPixmap(0,0, pix);

		//painter.end();
		//setBackgroundPixmap(qp);
		p->drawPixmap(0,0, pix);
	    }
	
	}else{
	    //	QPainter *p = new QPainter(this);
	
	    int h = (height() - 2)/2; // (height() - 4)/2;
	    QColor firstColor(palette().active().background());
	    p->setPen(firstColor.dark(100+h*2));
	    p->drawLine(0,0 , width(), 0); 

	    p->setPen(firstColor.light(100+h*2));
 
	    for ( int i = 1; i < 3; ++i ) {
		p->setPen(firstColor.light(100+(h*2)-i));
		p->drawLine(0,i , width(), i); 
	    }

	    for ( int i = 0; i < h-1; ++i ) {
		p->setPen(firstColor.dark(100 + ((h*2)-3)-(i*2)));
		p->drawLine(0,i+4 , width(), i+4); 
	    }


	    int j = 0;

	    for ( int i = height()-h; i < height()-1 ; ++i ) {
		j++;
		p->setPen(firstColor.light(100 +(j*2)));
		p->drawLine(0, i , width(), i); 
	    }


	    p->setPen(firstColor.dark(100+(h*2)));
 
	    for (int i = height()-2; i < height(); ++i ) {
		p->drawLine(0,i , width(), i); 
	    }
	}

	QImage img(qapp::get_cfile(defaults::wininfo_bg));
	if(isDown() && 
	   (QFileInfo(qapp::get_cfile(defaults::wininfodown_bg)).isFile()))
	    img = QImage(qapp::get_cfile(defaults::wininfodown_bg));
	else if(hasMouse() && 
	   (QFileInfo(qapp::get_cfile(defaults::wininfomo_bg)).isFile()))
	    img = QImage(qapp::get_cfile(defaults::wininfomo_bg));

	if(! img.isNull())  // make scaled pixmap
	{
	    QPixmap pix;
	    //	if((defaults::startheight < 10) 
	    if( ((defaults::starticonplace == "TOP") || (defaults::starticonplace == "BUTTOM")) && (defaults::wininfoHeight > 9) )
		pix.convertFromImage(img.smoothScale(width(), defaults::wininfoHeight));
	    else
		pix.convertFromImage(img.smoothScale(width(), height()));

	    if( (defaults::starticonplace == "TOP")
		&& (defaults::wininfoHeight > 9) ){
		p->drawPixmap(0,0, pix);
		p->drawPixmap(2,2, *pixmap());
	    }else if ( (defaults::starticonplace == "BUTTOM") 
		       && (defaults::tbButtonHeight > 9) ){
		p->drawPixmap(0, height()-defaults::wininfoHeight, pix);
		p->drawPixmap(2,height()-defaults::wininfoHeight +2, 
			      *pixmap());
	    }else{
		p->drawPixmap(0,0, pix);
		p->drawPixmap(2,2, *pixmap());
	    }
	}



	
	p->end();
    }else
	wbutton::paintEvent( p );
}


// returns TRUE if client is fully obscured by other toplevel windows
bool winlist::isobscured(xwindow *client, Window *wins, uint nwins){
    xwindow *nclient;
    uint cwin;
    
    for(cwin=0; cwin < nwins; cwin++)  // find current window
	if(wins[cwin] == client->winId())
	    break;

    if(cwin >= nwins)
	return FALSE;

    QRegion sr(client->frameGeometry());
	
    while(++cwin < nwins){
	if((nclient = qapp::pwindows.find(wins[cwin])) == NULL || 
	   ! nclient->isVisible())
	    continue;
		
	sr -= QRegion(nclient->frameGeometry());
	
	if(sr.isEmpty())
	    return TRUE;
    }
    return FALSE;
}

// returns TRUE if client is below tiled windows

bool winlist::isbottom(xwindow *client, Window *wins, uint nwins){
    xwindow *nclient;
    uint cwin;

    for(cwin=0; cwin < nwins; cwin++) 
	if(wins[cwin] == client->winId())
	    break;

    if(cwin >= nwins)
	return FALSE;

    while(++cwin < nwins){
	if((nclient = qapp::pwindows.find(wins[cwin])) == NULL || 
	   ! nclient->isVisible() || ! qapp::is_curdesk(nclient))
	    continue;

	if(nclient->is_tiled())
	    return TRUE;
    }
    return FALSE;
}	

void winlist::mouseReleaseEvent(QMouseEvent *event){
    if(event->button() == QMouseEvent::RightButton)
	setDown(FALSE);
}

void winlist::mousePressEvent(QMouseEvent *event)
{
    if(event->button() == QMouseEvent::RightButton)  // hidden to foreground
    {
	setDown(TRUE);
	hidden_win();
    }
    else if(event->button() == QMouseEvent::LeftButton)
	start_popup();
}

void winlist::hidden_win(void)
{
    Window w1,w2,*wins;
    uint nwins;

    if(XQueryTree(qt_xdisplay(), qt_xrootwin(), &w1, &w2, &wins, &nwins) == 0
       || ! nwins)
	return;

    xwindow *win;
    int dwidth = QApplication::desktop()->width();
    
    for(win = clients.first(); win != NULL; win = clients.next()){
	if(win->isVisible() && win->x() <= dwidth && win->x() >= 0 && 
	   isobscured(win, wins, nwins)){
	    win->raise();
	    XFree(wins);
	    return;
	}	
    }

    if(qapp::is_tileddesk()){
	for(win = clients.first(); win != NULL; win = clients.next()){
	    if(win->isVisible() &&  win->x() <= dwidth && win->x() >= 0 && 
	       ! win->is_tiled() && isbottom(win, wins, nwins)){
		win->raise();
		XFree(wins);
		return;
	    }
	}
    }
    XFree(wins);
}

void winlist::highlight_pager(int id){
    xwindow *win;

    if(qapp::smode)
	return;

    tb_pg->draw_pager();
    
    if((win = clients.at(id)) == NULL)
	return;
		
    tb_pg->add(win, TRUE);
}

void winlist::start_popup(void){
    if(qapp::menu_open)
	return;

    xwindow *focusclient = qapp::focusclient;
    qapp::menu_open = TRUE;
    
    setDown(TRUE);
    popup_list();

    tb_pg->draw_pager();
    qapp::menu_open = FALSE;

    XSetInputFocus(qt_xdisplay(), qt_xrootwin(), RevertToPointerRoot, 
		   CurrentTime);
    if(focusclient != NULL && clients.find(focusclient) != -1)
	focusclient->setchildfocus(CurrentTime);
}

void winlist::popup_list(void){
    xwindow *win;
    QString wname;
    Window rw,cw;
    int rx,ry,wx,wy;
    unsigned mr;

    for(win = clients.first(); win != NULL; win = clients.next()){
	if(win->iswithdrawn() || ((win->get_pflags() & qapp::WindowListSkip) 
				  && ! win->hidden_win()))
	    continue;

	wname = "";
	QTextOStream wnm(&wname);
		
	if(! win->isVisible())
	    wnm << '<';
			
	wnm << win->ccaption().left(100);
	
	if(! win->isVisible())
	    wnm << '>';

	if(win->get_tnumber())
	    wnm << '<' << win->get_tnumber() << '>';
	
	if(! win->getmachine().isNull())
	    wnm << " (" << win->getmachine().left(20) << ')';

	wmenu->insertItem(QIconSet(*win->icon()) ,wname, win, 
			  SLOT(focus_mouse_wlist()), 0, clients.at());
	//wmenu->insertItem(wname, win, SLOT(focus_mouse_wlist()), 0, 
	//clients.at());
    }

    if(! defaults::show_winlist || qapp::smode)  // show at mouse position
    {
	XQueryPointer(qt_xdisplay(), qt_xrootwin(), &rw, &cw, &rx, &ry, &wx, 
		      &wy, &mr);
	wmenu->exec(QPoint(rx, ry));
    }else{
	if(! defaults::toolbar_top)   // menu above button
	{
	    QPoint p = mapToGlobal(QPoint(0, 0));
	    QSize s(wmenu->sizeHint());
	    p.setY(p.y()-s.height());
	    if(QApplication::reverseLayout())
		p.setX(p.x()+width()-s.width());
	    wmenu->exec(p);
	}
	else
	{
	    if(QApplication::reverseLayout())
		wmenu->exec(mapToGlobal(QPoint(width()-
				wmenu->sizeHint().width(), height())));
	    else
		wmenu->exec(mapToGlobal(QPoint(0, height())));
	}
    }	
		
    wmenu->clear();
    setDown(FALSE);
}
