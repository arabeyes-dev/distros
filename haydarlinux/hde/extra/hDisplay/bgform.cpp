/****************************************************************************
**
**	bgform.cpp
**      by:  Haydar Alkaduhimi <haydar@haydar.net>
**
****************************************************************************/
#include "bgform.h"

#include <qvariant.h>
#include <qcombobox.h>
#include <qlabel.h>
#include <qlistbox.h>
#include <qpushbutton.h>
#include <qmime.h>
#include <qdragobject.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>
#include <qimage.h>
#include <qpixmap.h>
#include <qdir.h>

#include <qmessagebox.h> 

#include <defaults.h>
#include <hinifile.h>
#include <libhde.h>

static QPixmap uic_load_pixmap_bgForm( const QString &name )
{
    const QMimeSource *m = QMimeSourceFactory::defaultFactory()->data( name );
    if ( !m )
	return QPixmap();
    QPixmap pix;
    QImageDrag::decode( m, pix );
    return pix;
}


apBtn::apBtn( QWidget* parent,  const char* name )
    : QPushButton( parent, name )
{

}

/* 
 *  Constructs a bgForm which is a child of 'parent', with the 
 *  name 'name' and widget flags set to 'f'.
 */
bgForm::bgForm( QWidget* parent,  const char* name, WFlags fl )
    : QWidget( parent, name, fl )
{
    if ( !name )
	setName( "bgForm" );
    resize( 370, 405 ); 
    setCaption( trUtf8( "Desktop" ) );


    QWidget* privateLayoutWidget = new QWidget( this, "Layout15" );
    privateLayoutWidget->setGeometry( QRect( 10, 11, 350, 390 ) ); 
    Layout15 = new QVBoxLayout( privateLayoutWidget, 0, 6, "Layout15"); 

    Layout5 = new QHBoxLayout( 0, 0, 6, "Layout5"); 
    QSpacerItem* spacer = new QSpacerItem( 0, 0, QSizePolicy::Expanding, QSizePolicy::Minimum );
    Layout5->addItem( spacer );

    topImg = new QLabel( privateLayoutWidget, "topImg" );
    topImg->setPixmap( uic_load_pixmap_bgForm( "monitor.png" ) );
    topImg->setScaledContents( TRUE );

    bgImage = new QLabel( topImg, "bgImage" );
    bgImage->setGeometry( QRect( 14, 17, 151, 111 ) ); 
    bgImage->setScaledContents( TRUE );
    Layout5->addWidget( topImg );
    QSpacerItem* spacer_2 = new QSpacerItem( 0, 0, QSizePolicy::Expanding, QSizePolicy::Minimum );
    Layout5->addItem( spacer_2 );
    Layout15->addLayout( Layout5 );

    Layout14 = new QHBoxLayout( 0, 0, 6, "Layout14"); 

    Layout3 = new QVBoxLayout( 0, 0, 6, "Layout3"); 

    TextLabel1 = new QLabel( privateLayoutWidget, "TextLabel1" );
    TextLabel1->setText( trUtf8( "Chose your background:" ) );
    Layout3->addWidget( TextLabel1 );

    imagesList = new QListBox( privateLayoutWidget, "imagesList" );
    imagesList->insertItem( trUtf8( "No Background" ) );
    Layout3->addWidget( imagesList );
    Layout14->addLayout( Layout3 );

    Layout13 = new QVBoxLayout( 0, 0, 6, "Layout13"); 

    browseBtn = new QPushButton( privateLayoutWidget, "browseBtn" );
    browseBtn->setText( trUtf8( "Browse" ) );
    Layout13->addWidget( browseBtn );
    QSpacerItem* spacer_3 = new QSpacerItem( 0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding );
    Layout13->addItem( spacer_3 );

    TextLabel2 = new QLabel( privateLayoutWidget, "TextLabel2" );
    TextLabel2->setText( trUtf8( "Position:" ) );
    Layout13->addWidget( TextLabel2 );

    displayBox = new QComboBox( FALSE, privateLayoutWidget, "displayBox" );
    displayBox->insertItem( trUtf8( "Center" ) );
    displayBox->insertItem( trUtf8( "Tile" ) );
    displayBox->insertItem( trUtf8( "Stretch" ) );
    displayBox->setCurrentItem( 2 );
    Layout13->addWidget( displayBox );
    QSpacerItem* spacer_4 = new QSpacerItem( 0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding );
    Layout13->addItem( spacer_4 );
    Layout14->addLayout( Layout13 );
    Layout15->addLayout( Layout14 );

    Layout1 = new QHBoxLayout( 0, 1, 0, "Layout1"); 
    QSpacerItem* spacer_5 = new QSpacerItem( 0, 0, QSizePolicy::Expanding, QSizePolicy::Minimum );
    Layout1->addItem( spacer_5 );

    OKBtn = new QPushButton( privateLayoutWidget, "OKBtn" );
    OKBtn->setText( trUtf8( "OK" ) );
    Layout1->addWidget( OKBtn );

    cancelBtn = new QPushButton( privateLayoutWidget, "cancelBtn" );
    cancelBtn->setText( trUtf8( "Cancel" ) );
    Layout1->addWidget( cancelBtn );

    applyBtn = new apBtn( privateLayoutWidget, "applyBtn" );
    applyBtn->setEnabled( FALSE );
    applyBtn->setText( trUtf8( "Apply" ) );
    Layout1->addWidget( applyBtn );
    Layout15->addLayout( Layout1 );

    QSpacerItem* spacer_6 = new QSpacerItem( 0, 10, QSizePolicy::Expanding, 
					     QSizePolicy::Minimum );
    Layout15->addItem(spacer_6);


    // signals and slots connections
    connect( cancelBtn, SIGNAL( clicked() ), this, SLOT( close() ) );
    connect( applyBtn, SIGNAL( clicked() ), this, SLOT( applyClick() ) );
    connect( OKBtn, SIGNAL( clicked() ), this, SLOT( okClick() ) );
    connect( displayBox, SIGNAL( activated(const QString&) ), this, 
	     SLOT( displayChanged(const QString&) ) ); 

    connect( browseBtn, SIGNAL( clicked() ), this, SLOT( browseClick() ) );

    connect( imagesList, SIGNAL( highlighted(const QString&) ), this, 
	     SLOT( imageSelected(const QString& ) ) );


    connect( applyBtn, SIGNAL( enabledChang(bool) ), this, 
	     SLOT( enabledChange(bool) ) );

    init();

}

/*  
 *  Destroys the object and frees any allocated resources
 */
bgForm::~bgForm()
{
    // no need to delete child widgets, Qt does it all for us
}

void bgForm::init()
{

    defaults::read_config();
    if(defaults::bgDisplay == "Center" )
	bgDisplay = trUtf8( "Center" );
    else if(defaults::bgDisplay ==  "Tile" )
	bgDisplay = trUtf8( "Tile" );
    if(defaults::bgDisplay == "Stretch" )
	bgDisplay = trUtf8( "Stretch" );

    //    bgDisplay = defaults::bgDisplay;
    bgdir = defaults::bgdir;
    root_pix = defaults::root_pix;

    displayBox->setCurrentText( bgDisplay ); 
 
    readCurentDir();
    //QMessageBox::information( this, "Application name", root_pix );
    selCurentImg();

    applyBtn->setEnabled(false);

}

void bgForm::selCurentImg( )
{
    QString im;
    im = root_pix.right( root_pix.length() - bgdir.length()-1 );
    if(im.isNull()){
	imagesList->setCurrentItem(0);
	return;
	//imagesList->setSelected(imagesList->item(2), true); 
    }
    for(uint i = 0; i < imagesList->count(); ++i)
    {
	if(imagesList->item(i)->text() == im)
	    imagesList->setCurrentItem(i);
	    //QMessageBox::information( this, "Application name", im );
    } 

}

void bgForm::applyClick()
{
    QString fname;
    fname = defaults::get_cfile("defaults");
 
    HIniFile def(fname);
    def.read(); 
    def.setGroup("Color_Background"); 

    if(bgDisplay == trUtf8( "Center" ))
	def.writeString("bgDisplay", "Center");
    if(bgDisplay == trUtf8( "Tile" ))
	def.writeString("bgDisplay", "Tile");
    if(bgDisplay == trUtf8( "Stretch" ))
	def.writeString("bgDisplay", "Stretch");

    //    def.writeString("bgDisplay", bgDisplay);
    def.writeString("BGDIR", bgdir);
    def.writeString("RootBackgroundPicture", root_pix);
    def.write(); 

    //    QMessageBox::information( this, "Application name", root_pix );


    applyBtn->setEnabled(false);

}

void bgForm::browseClick()
{
    QFileDialog* fd = new QFileDialog( this, "file dialog", TRUE );
    fd->setMode( QFileDialog::Directory );

    fd->setDir( bgdir );

    QString s = fd->getExistingDirectory( bgdir, this );
    if( !s.isNull() ){
	bgdir = s;
	readCurentDir();
	applyBtn->setEnabled(true);
    }
}

void bgForm::imageSelected(const QString& im)
{
    if(im != trUtf8( "No Background" )){
	root_pix = bgdir + im;
	//	bgImage->setPixmap(root_pix);
	//cout << root_pix << endl;
	drawImage();

    }else{
	root_pix = "";
	bgImage->setBackgroundPixmap(root_pix);

    }
    //    QMessageBox::information( this, "Application name", im );

    applyBtn->setEnabled(true);
	
}

void bgForm::enabledChange(bool ec)
{ 
    //QMessageBox::information( this, "Application name", "TEST" );
    //    emit enabledChanged( applyBtn->isEnabled() ); 
    emit enabledChanged( ec ); 
}

void bgForm::drawImage()
{
	QDesktopWidget *d = QApplication::desktop();
	int dw = d->width();     // returns desktop width
	int dh = d->height();    // returns desktop height


	int imw = QPixmap(root_pix).width();
	int imh = QPixmap(root_pix).height();

	if(bgDisplay == trUtf8( "Center" )){
	    
	    QPixmap *rootpix = new QPixmap(bgImage->width(), 
					     bgImage->height());
	    QPainter p(rootpix);
	    p.fillRect(0, 0, bgImage->width(), bgImage->height(), 
		       QBrush(defaults::root_bg));

	    int xi = 0;
	    int yi = 0;

	    if(bgImage->width() > rootpix->width())
		xi = ((imw * bgImage->width() / dw)/2) - (rootpix->width()/2);
	    else
		xi = (rootpix->width()/2) - ((imw * bgImage->width() / dw)/2);

	    if(bgImage->height() > rootpix->height())
		yi = ( (imh * bgImage->height() / dh ) /2 ) - 
		    (bgImage->height()/2);
	    else
		yi = (bgImage->height()/2) -
		    ( (imh * bgImage->height() / dh ) /2 );

	    QImage rootimg(root_pix);
    
	    QPixmap rootpx;
	    rootpx.convertFromImage(rootimg.smoothScale(imw * bgImage->width()
				/ dw,	imh * bgImage->height() / dh ));



	    p.drawPixmap( xi, yi, rootpx);
	    p.end();

	    bgImage->setBackgroundPixmap(*rootpix);
	    
	}else if(bgDisplay == trUtf8( "Tile" )){

	    QPixmap *rootpix = new QPixmap(bgImage->width(), 
					   bgImage->height());

	    QImage rootimg(root_pix);
    
	    QPixmap rootpx;
	    rootpx.convertFromImage(rootimg.smoothScale(imw * bgImage->width()
				/ dw,	imh * bgImage->height() / dh ));

	    QPainter p(rootpix);
	    p.fillRect(0, 0, bgImage->width(), bgImage->height(), 
		       QBrush(defaults::root_bg));
	    p.drawTiledPixmap( 0, 0, bgImage->width(), 
			       bgImage->height(), 
			       rootpx);
	    p.end();

	    bgImage->setBackgroundPixmap(*rootpix);

	}else if(bgDisplay == trUtf8( "Stretch" )){
	    bgImage->setBackgroundColor(defaults::root_bg);

	    QImage rootimg(root_pix);
    
	    QPixmap *rootpix = new QPixmap();
	    rootpix->convertFromImage(rootimg.smoothScale(bgImage->width(), 
						bgImage->height()));

	    bgImage->setBackgroundPixmap(*rootpix);

	}
}



void bgForm::displayChanged(const QString& disp)
{
    bgDisplay = disp;
    drawImage();
    applyBtn->setEnabled(true);
}

void bgForm::readCurentDir()
{
    imagesList->clear();
    imagesList->insertItem( trUtf8( "No Background" ) );

    QDir d(bgdir);
    if(!d.exists())
	return;

    d.setFilter( QDir::Files  | QDir::Hidden  );
    d.setSorting( QDir::Name);

    const QFileInfoList *list = d.entryInfoList();
    QFileInfoListIterator it( *list );
    QFileInfo *fi;

    while ( (fi = it.current()) != 0 ) {
	if(fi->extension( FALSE ) == "jpg" ||
	   fi->extension( FALSE ) == "jpeg" ||
	   fi->extension( FALSE ) == "gif" ||
	   fi->extension( FALSE ) == "bmp" ||
	   fi->extension( FALSE ) == "png" ||
	   fi->extension( FALSE ) == "xpm")
	    imagesList->insertItem( fi->fileName().latin1() );
	++it;
    }

}

void bgForm::okClick()
{
    if( applyBtn->isEnabled() )
	applyClick();
    close();
}
