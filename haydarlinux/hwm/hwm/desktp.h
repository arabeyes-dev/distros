/* desktp.h */

#include "defs.h"

#ifndef DESKTP_H
#define DESKTP_H

class desktp : public QIconView
{
	Q_OBJECT
	    
 protected:
    void createIcon(QString path, QString fname);

 public slots:
    void timeout(void);
    void iconDblClicked(QIconViewItem *);
    void iconReturnPressed( QIconViewItem * );
    void rightClicked( QIconViewItem* item, const QPoint& pos );

 public:
    //static bool desktopready;
    QPopupMenu *rightmnu;
    desktp(QWidget *parent=0, const char *name=0, WFlags f = 0);
    void refreshIV(void);

 signals:
    /*
      void clicked( QIconViewItem * );
      void clicked( QIconViewItem *, const QPoint & );
      void pressed( QIconViewItem * );
      void pressed( QIconViewItem *, const QPoint & );
    
        virtual void doubleClicked( QIconViewItem *item );
        virtual void returnPressed( QIconViewItem *item );
      void rightButtonClicked( QIconViewItem* item, const QPoint& pos );
    
      void rightButtonPressed( QIconViewItem* item, const QPoint& pos );
      void mouseButtonPressed( int button, QIconViewItem* item, const QPoint& pos );
      void mouseButtonClicked( int button, QIconViewItem* item, const QPoint& pos );
      void contextMenuRequested( QIconViewItem* item, const QPoint &pos );
    */

};
#endif
