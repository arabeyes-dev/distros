/****************************************************************************
 **
 **				hcloseframe.h
 **			=========================
 **
 **  begin                : Tue Jul 02 2002
 **  copyright            : (C) 2002 by Haydar Alkaduhimi
 **  email                : haydar@haydar.net
 **
****************************************************************************/
#ifndef HCLOSEFRAME_H
#define HCLOSEFRAME_H

#include <qvariant.h>
#include <qwidget.h>
#include <qptrlist.h> 

class QVBoxLayout; 
class QHBoxLayout; 
class QGridLayout; 
class QLabel;
class QToolButton;

class HCloseFrame : public QWidget
{ 
    Q_OBJECT

 public:
    HCloseFrame( QWidget* parent = 0, const char* name = 0, WFlags fl = 0 );
    ~HCloseFrame();

    void changeCaption(QString cap);
    void addWidget(QWidget *w);

    void hideWidgets();
    void showWidgets();

    QLabel* captionLable;
    QToolButton* closeBtn;
    QToolButton* hideBtn;

    bool hidden;

 protected:
    QPtrList<QWidget> widgetList;
    QVBoxLayout* wdgetLayout;
    QHBoxLayout* captionLayout;

 public slots:
    void showHideWidgets();


};

#endif // HCLOSEFRAME_H
