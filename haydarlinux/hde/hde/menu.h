/* menu.h */

#ifndef MENU_H
#define MENU_H

#include <defs.h>

class  menuMenu: public QPopupMenu
{
  Q_OBJECT

  typedef struct 
  {
    menuMenu *menu;   // return menu
    int id;   // return id
    QString path;    // the original path
  } MENULIST;
  QList <MENULIST> glist;        // list of visible buttons

 private:
  char *menuname;
 protected:
  //  virtual void drawContents ( QPainter * p );
  //  void drawItem ( QPainter * p, int tab_, QMenuItem * mi, bool act, int x, 
  //	  int y, int w, int h );

 public:
  menuMenu(QWidget *parent=0, const char *name=0);
  int insertMenu( QPixmap pix, QString name, menuMenu *menu, 
		  QString path);
  int insertItm( QPixmap pix, QString name, QString path);
  menuMenu *checkMenu(QString path);
  int checkItm(QString path);
  char * menu_name(void){return menuname;};
};

class menu : public QPushButton
{
    Q_OBJECT

  menuMenu *basemenu;
  QPixmap mainpix;
  QPixmap maintxt;
  QIntDict <QString> mdict;     // menu item id's
  QList <menuMenu> mlist;     // submenue list for deletion
  bool killop;                  // window kill operation active
  QDateTime lmtime;             // last file modification time
  static uchar kcursor_bits[];  // cursor bitmap
  static uchar kcursor_mask[];  // cursor bitmap

  void addMenu(menuMenu  *cm, QString path, QString fname);
  void addLink(menuMenu  *cm, QString path, QString fname);
  void addMenu2(menuMenu *cm, QString path, QString fname);
  void addLink2(menuMenu *cm, QString path, QString fname);
  void readNewMenu( QString dir);
  void readNewMenu(void);
  void readUpper(void);
  void readLower(void);
  void recurse(menuMenu *cm, QString path, QString fname);
  void recurse2(menuMenu *cm, QString path, QString fname);
	
 private slots:	
  void run_cmd(int);          // start command with menu id
  void winkill(void);
  void winkill_all();
  //	void drawbg(void);

 protected:
  virtual void mousePressEvent(QMouseEvent *);
  virtual void resizeEvent(QResizeEvent *);
  void paintEvent( QPaintEvent * );

 public:
  menu(QWidget *parent, const char *name);
  void start_popup(void);
  void readmenu(void);
};
#endif
