<!DOCTYPE TS><TS>
<context>
    <name>Editor</name>
    <message>
        <source>&amp;Yes</source>
        <translation>&amp;Ja</translation>
    </message>
    <message>
        <source>&amp;No</source>
        <translation>&amp;Nee</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annuleren</translation>
    </message>
    <message>
        <source>&amp;File</source>
        <translation>&amp;Bestand</translation>
    </message>
    <message>
        <source>&amp;New</source>
        <translation>&amp;Nieuw</translation>
    </message>
    <message>
        <source>&amp;Open</source>
        <translation>&amp;Openen</translation>
    </message>
    <message>
        <source>&amp;Save</source>
        <translation>&amp;Opslaan</translation>
    </message>
    <message>
        <source>Open &amp;as</source>
        <translation>Openen &amp;als</translation>
    </message>
    <message>
        <source>Save &amp;as</source>
        <translation>Opslaan &amp;als</translation>
    </message>
    <message>
        <source>Add &amp;encoding</source>
        <translation>Voeg &amp;encoding toe</translation>
    </message>
    <message>
        <source>&amp;Print</source>
        <translation>A&amp;fdrukken</translation>
    </message>
    <message>
        <source>&amp;Close</source>
        <translation>&amp;Sluiten</translation>
    </message>
    <message>
        <source>&amp;Quit</source>
        <translation>S&amp;toppen</translation>
    </message>
    <message>
        <source>&amp;Edit</source>
        <translation>B&amp;ewerken</translation>
    </message>
    <message>
        <source>To &amp;uppercase</source>
        <translation>&amp;Grote letters</translation>
    </message>
    <message>
        <source>To &amp;lowercase</source>
        <translation>&amp;Kleine letters</translation>
    </message>
    <message>
        <source>&amp;Select Font</source>
        <translation>&amp;Lettertype</translation>
    </message>
    <message>
        <source>HWM Notepad</source>
        <translation>HWM Kladbalk</translation>
    </message>
    <message>
        <source>Save changes to Document?</source>
        <translation>Veranderingen in het document opslaan?</translation>
    </message>
    <message>
        <source>&amp;Undo</source>
        <translation>&amp;Ongedaan maken</translation>
    </message>
    <message>
        <source>&amp;Redo</source>
        <translation>Op&amp;nieuw</translation>
    </message>
    <message>
        <source>&amp;Copy</source>
        <translation>&amp;Kopiëren</translation>
    </message>
    <message>
        <source>&amp;Paste</source>
        <translation>&amp;Plakken</translation>
    </message>
    <message>
        <source>Cu&amp;t</source>
        <translation>Kn&amp;ippen</translation>
    </message>
    <message>
        <source>Clear</source>
        <translation>Wissen</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation>&amp;Help</translation>
    </message>
    <message>
        <source>&amp;About</source>
        <translation>&amp;Info</translation>
    </message>
    <message>
        <source>&amp;Find</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Find &amp;Next</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
