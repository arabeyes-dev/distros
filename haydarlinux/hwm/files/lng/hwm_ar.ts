<!DOCTYPE TS><TS>
<context>
    <name>desktp</name>
    <message>
        <source>New</source>
        <translation>جديد</translation>
    </message>
    <message>
        <source>&amp;Arrange icons</source>
        <translation>&amp;ترتيب الرموز</translation>
    </message>
    <message>
        <source>Properties</source>
        <translation>إعدادات</translation>
    </message>
    <message>
        <source>&amp;Rename</source>
        <translation>&amp;تغيير الاسم</translation>
    </message>
    <message>
        <source>&amp;Delete</source>
        <translation>&amp;حذف</translation>
    </message>
    <message>
        <source>&amp;Properties</source>
        <translation>&amp;إعدادات</translation>
    </message>
</context>
<context>
    <name>menu</name>
    <message>
        <source>Kill window</source>
        <translation>القضاء على الإطار</translation>
    </message>
    <message>
        <source>End Session</source>
        <translation>إنهاء المرحلة</translation>
    </message>
</context>
<context>
    <name>uborder</name>
    <message>
        <source>Press to get context help</source>
        <translation type="obsolete">اضغط للحصول على المساعدة</translation>
    </message>
    <message>
        <source>Press to maximmize</source>
        <translation type="obsolete">اضغط للتصغير</translation>
    </message>
    <message>
        <source>Press RightButton to maximmize, left button to minimize</source>
        <translation type="obsolete">اضغط الزر الأيمن للتكبير والأيسر للتصغير</translation>
    </message>
    <message>
        <source>Press to close</source>
        <translation type="obsolete">إغلاق</translation>
    </message>
</context>
<context>
    <name>winfo</name>
    <message>
        <source>Sticky</source>
        <translation>عالق</translation>
    </message>
    <message>
        <source>Skip windowlist</source>
        <translation>تجاهل في قائمة الأطر</translation>
    </message>
    <message>
        <source>Small frame</source>
        <translation>غلاف صغير</translation>
    </message>
    <message>
        <source>No resize</source>
        <translation>بلا تغيير الحجم</translation>
    </message>
    <message>
        <source>No tiling</source>
        <translation>بلا تجانب</translation>
    </message>
    <message>
        <source>Skip keyboard access</source>
        <translation>تجاهل دخول لوحة المفاتيح</translation>
    </message>
    <message>
        <source>Skip in screenmode</source>
        <translation>تجاهل نوعية الشاشة</translation>
    </message>
    <message>
        <source>Apply to all in class</source>
        <translation>تطبيق لنفس النوعية</translation>
    </message>
    <message>
        <source> Title</source>
        <translation>   عنوان</translation>
    </message>
    <message>
        <source> Name</source>
        <translation>   إسم</translation>
    </message>
    <message>
        <source> Class</source>
        <translation>   نوعية</translation>
    </message>
    <message>
        <source> Location</source>
        <translation>   مكان</translation>
    </message>
    <message>
        <source> Invocation</source>
        <translation>   إسم</translation>
    </message>
    <message>
        <source> Geometry</source>
        <translation>المساحة</translation>
    </message>
    <message>
        <source> Permanent </source>
        <translation>دائمي</translation>
    </message>
    <message>
        <source> Temporary </source>
        <translation>مؤقت</translation>
    </message>
    <message>
        <source>  Cancel   </source>
        <translation>الغاء الأمر</translation>
    </message>
</context>
<context>
    <name>xwindow</name>
    <message>
        <source>&amp;Restore</source>
        <translation>&amp;إعادة الحجم</translation>
    </message>
    <message>
        <source>&amp;Iconify</source>
        <translation>&amp;تصغير</translation>
    </message>
    <message>
        <source>Ma&amp;ximize</source>
        <translation>ت&amp;كبير</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>إغلاق</translation>
    </message>
    <message>
        <source>Press to iconify</source>
        <translation>اضغط للتصغير</translation>
    </message>
    <message>
        <source>Press to maximize</source>
        <translation>اضغط للتكبير</translation>
    </message>
    <message>
        <source>Press RightButton to maximmize, left button to minimize</source>
        <translation>اضغط الزر الأيمن للتكبير والأيسر للتصغير</translation>
    </message>
    <message>
        <source>Press to get context help</source>
        <translation>اضغط للحصول على المساعدة</translation>
    </message>
    <message>
        <source>Press to close</source>
        <translation>إغلاق</translation>
    </message>
</context>
</TS>
