/****************************************************************************
** Form interface generated from reading ui file 'hadditem.ui'
**
** Created: Fri Apr 2 23:55:10 2004
**      by: The User Interface Compiler ($Id$)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#ifndef HADDITEM_H
#define HADDITEM_H

#include <qvariant.h>
#include <qwizard.h>

class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;
class QSpacerItem;
class QWidget;
class QLabel;
class QLineEdit;
class QPushButton;
class QComboBox;
class QTextEdit;
class QCheckBox;

class hadditem : public QWizard
{
    Q_OBJECT

public:
    hadditem( QWidget* parent = 0, const char* name = 0, bool modal = FALSE, WFlags fl = 0 );
    ~hadditem();

    QWidget* execPage;
    QLabel* execLbl;
    QLineEdit* excEdit;
    QPushButton* browseBtn;
    QLineEdit* lineEdit5;
    QComboBox* comboBox1;
    QTextEdit* textEdit1;
    QWidget* itemsPage;
    QCheckBox* desktopCheck;
    QLabel* textLabel2;
    QLineEdit* desktopPlace;
    QCheckBox* menuCheck;
    QLabel* textLabel2_2;
    QLineEdit* menuPlace;
    QTextEdit* textEdit2;
    QWidget* iconPage;
    QLabel* iconPix;
    QPushButton* iconBtn;
    QTextEdit* textEdit3;

public slots:
    virtual void nextClicked();
    virtual void browseBtn_clicked();
    virtual void iconBtn_clicked();

protected:
    QHBoxLayout* layout17;
    QVBoxLayout* layout16;
    QSpacerItem* spacer1;
    QSpacerItem* spacer11;
    QHBoxLayout* layout11;
    QHBoxLayout* layout14;
    QHBoxLayout* layout17_2;
    QVBoxLayout* layout16_2;
    QSpacerItem* spacer4;
    QSpacerItem* spacer1_2;
    QHBoxLayout* layout14_2;
    QSpacerItem* spacer3;
    QHBoxLayout* layout15;
    QSpacerItem* spacer5;
    QHBoxLayout* layout20;
    QVBoxLayout* layout19;
    QSpacerItem* spacer7;
    QHBoxLayout* layout18;
    QSpacerItem* spacer6;
    QSpacerItem* spacer8;

protected slots:
    virtual void languageChange();

private:
    void init();

};

#endif // HADDITEM_H
