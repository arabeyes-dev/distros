/*  
*   File      : hrichedit.h
*   Written by: haydar@haydar.net
*   Copyright : GPL
*
*   
*/

#include <qtextedit.h>
#include <qwidget.h>


class HRichEdit : public QTextEdit
{
    Q_OBJECT

	//protected:
    //virtual void drawFrame ( QPainter * p );
public:
    HRichEdit(QWidget *parent=0, const char *name=0);
}; 
