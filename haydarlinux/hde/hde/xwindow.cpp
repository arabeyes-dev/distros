/****************************************************************************
 **
 **				xwindow.cpp
 **			=========================
 **
 **  begin                : 2001 based on qlwm
 **  copyright            : (C) 2001 - 2002 by Haydar Alkaduhimi, 
 **  email                : haydar@haydar.net
 **  License              : GPL
 **
 **  This is constructed for every normal window
 **
 **  Special Thanks to Alexander Linden <alinden@gmx.de>
 **
 ****************************************************************************/

#include "defs.h"
#include "defaults.h"
#include "qapp.h"
#include "winfo.h"
#include "toolbar.h"
#include "rubber.h"
//#include "moc_xwindow.cpp"
//#include "atoms.h"

#include "xwindow.h"


//bool xwindow::firsttime = true;
int borderw, tmpflags = 0;
bool dontmouse, decorated;
//Atoms *atoms;

bool xwindow::isdecorated(void){

    //test
#include <X11/Xproto.h>

    //Atom XA_MOTIF_WM_HINTS = 0;
    /*
    struct MWMHints {
	CARD32 mwmFlags;
	CARD32 mwmFunctions;
	CARD32 mwmDecorations;
	INT32 mwmInputMode;
        ulong functions;
    };  
    */
    struct MWMHints {
	ulong mwmFlags;
	ulong mwmFunctions;
	ulong mwmDecorations;
	long mwmInputMode;
    };  


#define MWM_HINTS_DECORATIONS         (1L << 1)
// bitmasks for decorations
#define MWM_DECOR_ALL                 (1L << 0)
#define MWM_DECOR_BORDER              (1L << 1)
#define MWM_DECOR_RESIZEH             (1L << 2)
#define MWM_DECOR_TITLE               (1L << 3)
#define MWM_DECOR_MENU                (1L << 4)
#define MWM_DECOR_MINIMIZE            (1L << 5)
#define MWM_DECOR_MAXIMIZE            (1L << 6)

#define MWM_HINTS_FUNCTIONS (1L << 0)
#define MWM_FUNC_ALL        (1L << 0)
#define MWM_FUNC_RESIZE     (1L << 1)
#define MWM_FUNC_MOVE       (1L << 2)
#define MWM_FUNC_MINIMIZE   (1L << 3)
#define MWM_FUNC_MAXIMIZE   (1L << 4)
#define MWM_FUNC_CLOSE      (1L << 5)


    MWMHints* hints = NULL;
    //    unsigned long flags = 0;
    Atom atype;
    int aformat;
    unsigned long nitems, bytes_remain;
  

   //XA_MOTIF_WM_HINTS = XInternAtom(qt_xdisplay(), "_MOTIF_WM_HINTS", FALSE);

    //    XGetWindowProperty(qt_xdisplay(), clientid, qapp::atoms->motif_wm_hints, 0, 20, 
    XGetWindowProperty(qt_xdisplay(), clientid, qapp::atoms->motif_wm_hints, 0,
		       5, FALSE, qapp::atoms->motif_wm_hints, &atype, &aformat,
		       &nitems, &bytes_remain, (uchar **)&hints);

    if (hints) {
	bool notdecor =false;
	if (hints->mwmFlags & MWM_HINTS_DECORATIONS) {
	    //cout << hints->mwmDecorations << "\n\n";
	    if(hints->mwmDecorations == 0)
		return false;

	    if (hints->mwmDecorations & MWM_DECOR_ALL){
		//cout << "decore all\n";
		return true;
	    }


	    if (hints->mwmFlags &  MWM_DECOR_TITLE ){
		//cout<< "TITLE\n";
		notdecor = false;
	    }
	       
	    if (hints->mwmFlags &  MWM_DECOR_BORDER){
		//cout<< "BORDER\n";
		notdecor = true;
	    }
	       

	    if (hints->mwmFlags & MWM_DECOR_MENU ){
		//cout<< "MENU\n";
		notdecor = true;
	    }

	    if (hints->mwmFlags & MWM_DECOR_MINIMIZE){
		//cout<< "MINIMIZE\n";
		notdecor = true;
	    }

	    if (hints->mwmFlags  & MWM_DECOR_MAXIMIZE){
		//cout<< "MAX\n";
		notdecor = true;
	    }

	    return notdecor;
	}

	if (hints->mwmFlags & MWM_HINTS_FUNCTIONS) {
            bool set_value = (( hints->mwmFunctions & MWM_FUNC_ALL ) == 0 );

            if( hints->mwmFunctions & MWM_FUNC_RESIZE ){

            }if( hints->mwmFunctions & MWM_FUNC_MOVE ){

	    }if( hints->mwmFunctions & MWM_FUNC_MINIMIZE ){
		iconify();
	    }if( hints->mwmFunctions & MWM_FUNC_MAXIMIZE ){
		t_maximize();
	    }if( hints->mwmFunctions & MWM_FUNC_CLOSE ){

	    }
	}

	XFree(hints);
    }
    //test
    return true;
}

xwindow::xwindow(Window w, QWidget *parent) : QWidget(parent) 
{
	dt = QApplication::desktop();
	maxstate = 0;
	mrb = NULL;
	clientid = w;
	actpal = TRUE;
	urgpal = FALSE;
	cmapwins = NULL;
	sstate = FALSE;
	tstate = FALSE;
	trsize = FALSE;
	ncmaps = 0;
	withdrawnstate = unmapped = TRUE;
	withccontexthelp = false;
	/*
    setMouseTracking(true);
    QWidget::setMouseTracking(TRUE);
	*/

    setFocusPolicy( StrongFocus );
    //qApp->enter_loop();
    //connectModal( that, FALSE );

	qapp::free_mouse();

	//	atoms = new Atoms();

	iconmenu = new QPopupMenu(this, "iconmenu");
	Q_CHECK_PTR(iconmenu);

	iconmenu->insertItem(*qapp::restorebtnpix, tr("&Restore"), this, 
			     SLOT(t_maximize()));
        //iconmenu->insertItem("&Restore", this, SLOT(t_maximize())); 
	 iconmenu->insertItem(*qapp::minbtnpix,tr("&Iconify"), this, 
			      SLOT(iconify()));
	iconmenu->insertItem(*qapp::maxbtnpix,tr("Ma&ximize"), this, 
			     SLOT(t_maximize()));
	iconmenu->insertSeparator();
	iconmenu->insertItem(*qapp::rightwinpix,tr("Close") + "\t\t ALT + F4",
			     this, SLOT(wdestroy()) );
	
	iconmenu->setItemEnabled( iconmenu->idAt(0), false );
	iconmenu->setItemEnabled( iconmenu->idAt(2), true );

	//iconmenu->setAccel ( QAccel::stringToKey("ALT+F4"), 4);


	// get ClassHint
	get_classhint();
	clientname = res_class;

	// get flags for WM_COMMAND
	
	char **argv;
	int argc;
	if(XGetCommand(qt_xdisplay(), w, &argv, &argc) && argc)
	{
		int ncm=0;
		while(1)
		{
			command += argv[ncm];

			if(argc > ++ncm)
				command += ' ';
			else
				break;
		}
		
		if(clientname.isEmpty() && ! command.isEmpty())
		{
			char *base;
			if((base = strrchr(argv[0], '/')) == NULL)
				base = argv[0];
			else
				base++;

			clientname = base;
		}	
		XFreeStringList(argv);
	}
	clientname = clientname.stripWhiteSpace();
	pflags = get_clientflags();
	
	if(pflags & qapp::SmallFrame)
	{
		uborderh = defaults::lowerborderheight;
		borderh = 2*uborderh;
	}
	else
	{
		uborderh = defaults::windowbuttonsize;
		borderh = defaults::windowbuttonsize+defaults::lowerborderheight;
	}


	if (defaults::sideBorders){
	    borderw = 2*defaults::lowerborderheight;
	}else{
	    borderw = 0;
	}


	//setBackgroundColor(QColor(Qt::black));
	
	// check for nonrectangular window
	shaped = (qapp::servershapes)?(query_shape()):(FALSE);

	if(shaped || (!isdecorated())|| (pflags & qapp::NoResize)){
		borderh -= defaults::lowerborderheight;
		borderw = 0;
	}

	if (shaped || !isdecorated()){
	    uborderh = 0;
	    borderw = 0;
	    borderh -= defaults::windowbuttonsize;
	}

	// save window geometry
	Window root;
	uint bwidth,depth;
	XGetGeometry(qt_xdisplay(), w, &root, &pos_x, &pos_y, &init_w, &init_h, &bwidth, &depth);
	
	base_w = init_w;
	base_h = init_h;
	//	init_h += borderh;
	//	init_w += borderw;

       	

	// reparent
	XSetWindowBorderWidth(qt_xdisplay(), w, 0);
	XSetWindowBorderWidth(qt_xdisplay(), winId(), 0 );
	XReparentWindow(qt_xdisplay(), w, winId(), borderw/2, uborderh);
	XAddToSaveSet(qt_xdisplay(), w);

	// get TransientForHint
	transfor = None;
	XGetTransientForHint(qt_xdisplay(), w, &transfor);

	// set Font
	setFont(defaults::borderfont);

	// get colormap and check for WM_COLORMAP_WINDOWS property
	get_colormaps();

	// get WM_CLIENT_MACHINE
	XTextProperty prop;
	if(XGetWMClientMachine(qt_xdisplay(), w, &prop) && prop.nitems && prop.format == 8)
	{
		clientfqdn = (char *)prop.value;

		if(defaults::showclientmachines)
		{
			int pos = clientfqdn.find('.');
			if(pos == -1)
				clientmachine = clientfqdn;
			else	
				clientmachine = clientfqdn.left(pos);
		}
	}	

	// get WMHints
	get_wmhints();

	// get WMNormalhints
	get_wmnormalhints();
	
	int cw = init_w;
	int ch = init_h;
	getsize(&cw, &ch);
		
	// window position

	if(wmnflags & USPosition)
	{
		if(wmnflags & PWinGravity && 
			(wingrav == SouthWestGravity || wingrav == SouthEastGravity))
				pos_y -= uborderh;
	}

	// a transient window with program specified position looks like a dialog box,
	// otherwise use autoplacement
	
	else if(! defaults::start_restart && (transfor == None || ! (wmnflags & PPosition)))
	{
		if(qapp::next_x+cw > dt->width())
		{
			pos_x = 0;
			if(cw < dt->width())
				qapp::next_x = 2*defaults::windowbuttonsize;
		}
		else
		{
			pos_x = qapp::next_x;
			qapp::next_x += 2*defaults::windowbuttonsize;
		}	

		int sy,ey;
		if(defaults::toolbar_top)
		{
			sy = defaults::tb_height;
			ey = dt->height();
		}
		else
		{
			sy = 0;
			ey = dt->height()-defaults::tb_height;
		}
		
		if(qapp::next_y+ch > ey)
		{
			pos_y = sy;
			if(ch < dt->height())
				qapp::next_y = sy+2*defaults::windowbuttonsize;
		}
		else
		{
			pos_y = qapp::next_y;
			qapp::next_y += 2*defaults::windowbuttonsize;
		}
	}	



	// move and resize
 	
	//	XResizeWindow(qt_xdisplay(), clientid, cw-borderw, ch-borderh);
	XResizeWindow(qt_xdisplay(), clientid, cw, ch);
	
	if(pos_y < 0)
		pos_y = 0;

	
	setGeometry(pos_x, pos_y, cw+ borderw, ch+ borderh);

    	// overwrite Qt-defaults because we need SubstructureNotifyMask

		
	XSelectInput(qt_xdisplay(), winId(),
  		 KeyPressMask | KeyReleaseMask |
  		 ButtonPressMask | ButtonReleaseMask |
  		 KeymapStateMask |
   		 ButtonMotionMask |
  		 PointerMotionMask |
		 EnterWindowMask | LeaveWindowMask |
  		 FocusChangeMask |
  		 ExposureMask |
		 StructureNotifyMask |
		 SubstructureRedirectMask |
		 SubstructureNotifyMask);
	
/*
	
	XSelectInput(qt_xdisplay(), client_id(),
  		 KeyPressMask | KeyReleaseMask |
  		 ButtonPressMask | ButtonReleaseMask |
  		 KeymapStateMask |
   		 ButtonMotionMask |
  		 PointerMotionMask |
		 EnterWindowMask | LeaveWindowMask |
  		 FocusChangeMask |
  		 ExposureMask |
		 StructureNotifyMask |
		 SubstructureRedirectMask |
		 SubstructureNotifyMask);
*/
	
	XSetWindowAttributes attr;
	attr.event_mask = ColormapChangeMask;
	XChangeWindowAttributes(qt_xdisplay(), w, CWEventMask, &attr);
		
	// get WM protocols
	getwmprotocols();

	// create window borders
	create_wborder();
 
	// add client to lookup tables
	
	qapp::cwindows.insert(w, this);
	qapp::pwindows.insert(winId(), this);

	// get WM_NAME and set window title
	get_wmname();

	if(shaped)
		reshape();

	// init autofocus timer
	
	focustimer = new QTimer(this);
	Q_CHECK_PTR(focustimer);
	connect(focustimer, SIGNAL(timeout()), SLOT(timertimeout()));

	tfocustimer = new QTimer(this);  // used for tiled mode
	Q_CHECK_PTR(tfocustimer);
	connect(tfocustimer, SIGNAL(timeout()), SLOT(tile_maximize()));
	
	qapp::send_configurenotify(this);
	
	if(! urgent && ! defaults::starturgent)
	{
	    setactive();
	}
	else 
	{
	    seturgent();
	}	


		
	if(false){//hidden_win()){
	    //map();
	}else{
	
	    if(defaults::start_restart)  // restore original state
	    {
		int clstate = get_clientstate();

		if(clstate == IconicState)
		{
			iconify();
		}	
		else if(clstate == WithdrawnState)
		{
			withdraw();
		}
		else map();
	    }	
	    else map();
	}
 
	//TEST
	//focus_mouse(true);
	XWarpPointer(qt_xdisplay(), None, winId(), 0, 0, 0, 0, width()/2, 
		     uborderh/2);
	//TEST


#ifdef DEBUGMSG
	cerr << "class xwindow constructed (WId:" << winId() << ")\n";
#endif	
}


void xwindow::create_wborder(void){
    lbdr = NULL;
    ubdr = NULL;
    lsbdr = NULL;
    rsbdr = NULL;


    layout = new QVBoxLayout(this);
    Q_CHECK_PTR(layout);
    layout->setMargin(0);

    //if (! shaped){
    if(pflags & qapp::SmallFrame){
	midmove = new wframe(this, "smallframe");
	Q_CHECK_PTR(midmove);
	midmove->setFixedHeight(defaults::lowerborderheight);
	midmove->frameName = "smallframe";
	layout->add(midmove);
		
	connect(midmove, SIGNAL(left_press(QMouseEvent *)), 
		SLOT(press_move(QMouseEvent *)));
	connect(midmove, SIGNAL(left_release(QMouseEvent *)), 
		SLOT(release_move(QMouseEvent *)));
	connect(midmove, SIGNAL(right_press()), SLOT(s_maximize()));
	connect(midmove, SIGNAL(mid_press()), SLOT(show_info()));
	connect(midmove, SIGNAL(mouse_move(QMouseEvent *)), 
		SLOT(move_move(QMouseEvent *)));

    }else{
	ubdr = new uborder((transfor == None), this, "upperborder", withccontexthelp);
	Q_CHECK_PTR(ubdr);
	layout->add(ubdr);
	midmove = ubdr->midframe;


	if( pflags & qapp::NoResize){
	    ubdr->set_max();
    //	    ubdr->luframe->hide();
    //	    ubdr->ruframe->hide();
    //    ubdr->uframe->hide();
    //    ubdr->layout2->setMargin((defaults::lowerborderheight/2)+2);
	}

	if (!defaults::topBorder )
	    ubdr->noTopBorder();

	if(transfor == None){
	    if(defaults::windowsBtns){
		connect(ubdr->minbtn, SIGNAL(clicked()), 
			SLOT(iconify() ));
		ubdr->minbtn->setTipText(tr("Press to iconify"));

		connect(ubdr->maxbtn, SIGNAL(clicked()), 
			SLOT(t_maximize() ));
		ubdr->maxbtn->setTipText(tr("Press to maximize"));
	    }else{
		connect(ubdr->leftframe,SIGNAL(right_release()), 
			SLOT(t_maximize()));
		ubdr->leftframe->setTipText(tr("Press RightButton to maximmize, left button to minimize"));
	
		connect(ubdr->leftframe, SIGNAL(mid_release()), 
			SLOT(toggle_tiled()));
		connect(ubdr->leftframe,SIGNAL(left_release(QMouseEvent *)), 
			SLOT(iconify()));
	    }
	}

	connect(ubdr->iconframe,SIGNAL(left_press()),SLOT(menuexec()));
	/*
	if(! wicon.isNull()){
	    QPixmap *iconpix = new QPixmap();
	    iconpix->convertFromImage(wicon.convertToImage().smoothScale(
		defaults::windowbuttonsize-6,defaults::windowbuttonsize-6));
	    ubdr->iconframe->setPixmap(*iconpix);
	}
	*/

	if(withccontexthelp){
	    connect(ubdr->helpbtn, SIGNAL(clicked()), SLOT(showhelp() ));
	    ubdr->helpbtn->setTipText(tr("Press to get context help"));
	}
	connect(ubdr->rightframe, SIGNAL(clicked()), SLOT(wdestroy()));
	ubdr->rightframe->setTipText(tr("Press to close"));
	connect(midmove, SIGNAL(right_press()), SLOT(s_maximize()));
	connect(midmove, SIGNAL(left_press(QMouseEvent *)), 
		SLOT(press_move(QMouseEvent *)));
	connect(midmove, SIGNAL(left_release(QMouseEvent *)), 
		SLOT(release_move(QMouseEvent *)));
	connect(midmove, SIGNAL(mid_press()), SLOT(show_info()));
	connect(midmove, SIGNAL(mouse_move(QMouseEvent *)), 
		SLOT(move_move(QMouseEvent *)));
    }	
    
    if (shaped || !isdecorated()){//|| !is_tileable()){
	ubdr->hide();
    }



    if((defaults::sideBorders) && isdecorated() &&  ! shaped && 
       ! (pflags & qapp::NoResize)){

	layout1 = new QHBoxLayout();
	Q_CHECK_PTR(layout1);
	layout1->setMargin(0);

    
	layout->addLayout(layout1, 10);

	lsbdr = new lsborder(this, "leftsideborder");
	Q_CHECK_PTR(lsbdr);
	layout1->add(lsbdr);
		
	if(QApplication::reverseLayout()){
	    connect(lsbdr->lframe, SIGNAL(press(QMouseEvent *)), 
		    SLOT(press_rightresize(QMouseEvent *)));
	    connect(lsbdr->lframe, SIGNAL(release(QMouseEvent *)), 
		    SLOT(release_rightresize(QMouseEvent *)));
	    connect(lsbdr->lframe, SIGNAL(mouse_move(QMouseEvent *)), 
		    SLOT(move_rightresize(QMouseEvent *)));

	    connect(lsbdr->midframe, SIGNAL(press(QMouseEvent *)), 
	        SLOT(press_rsresize(QMouseEvent *)));
	    connect(lsbdr->midframe, SIGNAL(release(QMouseEvent *)), 
	        SLOT(release_rsresize(QMouseEvent *)));
	    connect(lsbdr->midframe, SIGNAL(mouse_move(QMouseEvent *)), 
	        SLOT(move_rsresize(QMouseEvent *)));
	}else{
	
	    connect(lsbdr->lframe, SIGNAL(press(QMouseEvent *)), 
		    SLOT(press_leftresize(QMouseEvent *)));
	    connect(lsbdr->lframe, SIGNAL(release(QMouseEvent *)), 
		    SLOT(release_leftresize(QMouseEvent *)));
	    connect(lsbdr->lframe, SIGNAL(mouse_move(QMouseEvent *)), 
		    SLOT(move_leftresize(QMouseEvent *)));

	    connect(lsbdr->midframe, SIGNAL(press(QMouseEvent *)), 
		    SLOT(press_lsresize(QMouseEvent *)));
	    connect(lsbdr->midframe, SIGNAL(release(QMouseEvent *)), 
		    SLOT(release_lsresize(QMouseEvent *)));
	    connect(lsbdr->midframe, SIGNAL(mouse_move(QMouseEvent *)), 
		    SLOT(move_lsresize(QMouseEvent *)));
	}






	layout1->addStretch();
   

	rsbdr = new rsborder(this, "rightsideborder");
	Q_CHECK_PTR(rsbdr);
	layout1->add(rsbdr);
	
	if(QApplication::reverseLayout()){
	    connect(rsbdr->lframe, SIGNAL(press(QMouseEvent *)), 
		    SLOT(press_leftresize(QMouseEvent *)));
	    connect(rsbdr->lframe, SIGNAL(release(QMouseEvent *)), 
		    SLOT(release_leftresize(QMouseEvent *)));
	    connect(rsbdr->lframe, SIGNAL(mouse_move(QMouseEvent *)), 
		    SLOT(move_leftresize(QMouseEvent *)));

	    connect(rsbdr->midframe, SIGNAL(press(QMouseEvent *)), 
		    SLOT(press_lsresize(QMouseEvent *)));
	    connect(rsbdr->midframe, SIGNAL(release(QMouseEvent *)), 
		    SLOT(release_lsresize(QMouseEvent *)));
	    connect(rsbdr->midframe, SIGNAL(mouse_move(QMouseEvent *)), 
		    SLOT(move_lsresize(QMouseEvent *)));
	}else{
	
	    connect(rsbdr->lframe, SIGNAL(press(QMouseEvent *)), 
		    SLOT(press_rightresize(QMouseEvent *)));
	    connect(rsbdr->lframe, SIGNAL(release(QMouseEvent *)), 
		    SLOT(release_rightresize(QMouseEvent *)));
	    connect(rsbdr->lframe, SIGNAL(mouse_move(QMouseEvent *)), 
		    SLOT(move_rightresize(QMouseEvent *)));

	    connect(rsbdr->midframe, SIGNAL(press(QMouseEvent *)), 
		    SLOT(press_rsresize(QMouseEvent *)));
	    connect(rsbdr->midframe, SIGNAL(release(QMouseEvent *)), 
		    SLOT(release_rsresize(QMouseEvent *)));
	    connect(rsbdr->midframe, SIGNAL(mouse_move(QMouseEvent *)), 
		    SLOT(move_rsresize(QMouseEvent *)));
	}

    


    }
    else
	layout->addStretch();









    if(isdecorated() && ! shaped && ! (pflags & qapp::NoResize)){
	lbdr = new lborder(this, "lowerborder");
	Q_CHECK_PTR(lbdr);
	layout->add(lbdr);
	
	if(QApplication::reverseLayout()){
	    connect(lbdr->leftframe, SIGNAL(press(QMouseEvent *)), 
	    	    SLOT(press_rightresize(QMouseEvent *)));
	    connect(lbdr->leftframe, SIGNAL(release(QMouseEvent *)), 
	    	    SLOT(release_rightresize(QMouseEvent *)));
	    connect(lbdr->leftframe, SIGNAL(mouse_move(QMouseEvent *)), 
	    	    SLOT(move_rightresize(QMouseEvent *)));
	    connect(lbdr->rightframe, SIGNAL(press(QMouseEvent *)), 
	    	    SLOT(press_leftresize(QMouseEvent *)));
	    connect(lbdr->rightframe, SIGNAL(release(QMouseEvent *)), 
	    	    SLOT(release_leftresize(QMouseEvent *)));
	    connect(lbdr->rightframe, SIGNAL(mouse_move(QMouseEvent *)), 
	    	    SLOT(move_leftresize(QMouseEvent *)));

	}else{
	
	    connect(lbdr->leftframe, SIGNAL(press(QMouseEvent *)), 
		    SLOT(press_leftresize(QMouseEvent *)));
	    connect(lbdr->leftframe, SIGNAL(release(QMouseEvent *)), 
		    SLOT(release_leftresize(QMouseEvent *)));
	    connect(lbdr->leftframe, SIGNAL(mouse_move(QMouseEvent *)), 
		    SLOT(move_leftresize(QMouseEvent *)));
	    connect(lbdr->rightframe, SIGNAL(press(QMouseEvent *)), 
		    SLOT(press_rightresize(QMouseEvent *)));
	    connect(lbdr->rightframe, SIGNAL(release(QMouseEvent *)), 
		    SLOT(release_rightresize(QMouseEvent *)));
	    connect(lbdr->rightframe, SIGNAL(mouse_move(QMouseEvent *)), 
		    SLOT(move_rightresize(QMouseEvent *)));
	}
	
	connect(lbdr->midframe, SIGNAL(press(QMouseEvent *)), 
		SLOT(press_midresize(QMouseEvent *)));
	connect(lbdr->midframe, SIGNAL(release(QMouseEvent *)), 
		SLOT(release_midresize(QMouseEvent *)));
	connect(lbdr->midframe, SIGNAL(mouse_move(QMouseEvent *)), 
		SLOT(move_midresize(QMouseEvent *)));
    }

    
}


void xwindow::menuexec(void){ 
    if(QApplication::reverseLayout()){
	QSize s(iconmenu->sizeHint());
	iconmenu->popup(QPoint(x()+width()-s.width(),y()+ uborderh));
    }else
	iconmenu->popup( QPoint(x(),y()+ uborderh) );
    //    iconmenu->popup( QPoint(0,0) );
}


int xwindow::getcwidth(void){
    return(width()-borderw);
    //return(width());
}

int xwindow::get_clientx(void){ 
    //    return(x()+(borderw/2)); 
    return(x()); 
}


void xwindow::getsize(int *pw, int *ph)   // adjust for possible width and height including border
{
	int w = *pw;
	int h = *ph;
	
	if(inc_w == 0)
		w = init_w;

	if(inc_h == 0)
		h = init_h;
	

	if(w > max_w)
		w = max_w;

	if(h > max_h)
		h = max_h;

	if(w < min_w)
		w = min_w;

	if(h < min_h)
		h = min_h;

	if(inc_w > 1)
	{
		int i = (w-base_w)/inc_w;
		w = base_w+i*inc_w;
		
		if(w < min_w)
			w += inc_w;
	}

	if(inc_h > 1)	
	{
		int j = (h-base_h)/inc_h;
		h = base_h+j*inc_h;
		
		if(h < min_h)
			h += inc_h;
	}	
	*pw = w;
	*ph = h;
}

void xwindow::resize_request(int cx, int cy, int cw, int ch)  // client requested resize
{
	if(cy < 0)
		cy = 0;

	int brdrh = borderh;
	int brdrw = borderw;

	if(shaped || !isdecorated() || (pflags & qapp::NoResize) ){
	    brdrw = 0;
	    brdrh = 0;
	}
	
	ch += brdrh;
	cw += brdrw;

	setGeometry(cx, cy, cw, ch);

	//
	
	if(shaped){

	}else if( !isdecorated() || (pflags & qapp::NoResize)){
	    //setGeometry(px, py, pw+(borderw/2), ph+(uborderh));
	    ch -= uborderh;
	}else{
	    cw -= brdrw;
	    ch -= brdrh;
	    //  getsize(&cw, &ch);
	}
		
	XResizeWindow(qt_xdisplay(), clientid, cw, ch);
	
	//

	//XResizeWindow(qt_xdisplay(), clientid, cw-borderw, ch-borderh);
	

	iconmenu->setItemEnabled( iconmenu->idAt(0), false );
	iconmenu->setItemEnabled( iconmenu->idAt(2), true );
	if(defaults::windowsBtns && transfor == None)
	    ubdr->maxbtn->setPixmap(*qapp::maxbtnpix);
    
	maxstate = 0;
}

void xwindow::resize_client(int px, int py, int pw, int ph)  // dimensions include borders
{
	int nw = width();
	int nh = height();

	int brdrh = borderh;
	int brdrw = borderw;
	//
	if(shaped || !isdecorated() || (pflags & qapp::NoResize) ){
	    brdrw = 0;
	    brdrh = 0;
	}
	
	//	ph += brdrh /2;
	//pw += brdrw/2;
	//

	if(px != x() || py != y() || pw != nw || ph != nh)  // parent
		setGeometry(px, py, pw, ph);

	if(pw != nw || ph != nh) // client
	{
#ifdef DEBUGMSG
		cerr << "resize child window (WId:" << clientid << ")\n";
#endif
		//XResizeWindow(qt_xdisplay(), clientid, pw-borderw, ph-borderh);
		
		if(shaped){

		}else if( !isdecorated() || (pflags & qapp::NoResize)){
		    //setGeometry(px, py, pw+(borderw/2), ph+(uborderh));
		    ph -= borderh;
		}else{
		    pw -= brdrw;
		    ph -= brdrh ;
		    //	    getsize(&pw, &ph);
		}
		
		XResizeWindow(qt_xdisplay(), clientid, pw, ph);
		

	}
}


void xwindow::t_maximize(void)
{
	int cw,ch;
	
	if(qapp::smode)
	{
		focus_mouse();
		return;
	}

	if(maxstate != 1)  // maximize
	{

	    
	    //TEST
	    int tstflags = 0;
	    tmpflags = pflags;
	    tstflags  |= qapp::NoResize;
	    set_pflags(tstflags);
	    //TEST
	    

		if(maxstate == 0)
		{
			icx = x();
			icy = y();
			icw = width();
			ich = height();
		}

		if(defaults::tmx1 == -1 || tstate || ((pflags & qapp::NoTile) && qapp::is_tileddesk()))   // not user configured
		{
			cw = dt->width();
			//ch = dt->height()-defaults::tb_height-1;
			ch = dt->height()-defaults::tb_height;
			getsize(&cw, &ch);
		
			resize_client(0, defaults::toolbar_top?defaults::tb_height+1:0, cw, ch);
		}
		else
		{
			cw = defaults::tmx2-defaults::tmx1;
			ch = defaults::tmy2-defaults::tmy1;
			getsize(&cw, &ch);

			resize_client(defaults::tmx1, defaults::tmy1, cw, ch);
		}
		iconmenu->setItemEnabled( iconmenu->idAt(0), true );
		iconmenu->setItemEnabled( iconmenu->idAt(2), false );
		if(defaults::windowsBtns && transfor == None)
		    if(ubdr != NULL)
			ubdr->maxbtn->setPixmap(*qapp::restorebtnpix);


		maxstate = 1;
	}
	else
	{
		int tstflags = 0;
		tstflags = tmpflags ;
		//		pflags  |= qapp::NoResize;
		set_pflags(tstflags);
		resize_client(icx, icy, icw, ich);
		iconmenu->setItemEnabled( iconmenu->idAt(0), false );
		iconmenu->setItemEnabled( iconmenu->idAt(2), true );
		maxstate = 0;
		if(defaults::windowsBtns && transfor == None)
		    if(ubdr != NULL) 
			ubdr->maxbtn->setPixmap(*qapp::maxbtnpix);

		
		
	}

	/*
	cout << "TEST 1\n";

		if (shaped || !isdecorated())
		    XMoveWindow(qt_xdisplay(), clientid, 0, 0);
		else
		    XMoveWindow(qt_xdisplay(), clientid, borderw/2, uborderh);
	cout << "TEST 2\n";
	*/

	raise();
}

void xwindow::s_maximize(void)
{
	int cw,ch;
	
	if(qapp::smode)
	{
		focus_mouse();
		return;
	}

	if(tstate)
	{
		qapp::tile_maximize(this);
		return;
	}
	
	if(maxstate != 2)  // maximize
	{
		if(maxstate == 0)
		{
			icx = x();
			icy = y();
			icw = width();
			ich = height();
		}

		if(defaults::smx1 == -1 || ((pflags & qapp::NoTile) && qapp::is_tileddesk()))
		{
			cw = dt->width();
			ch = dt->height();
			getsize(&cw, &ch);
		
			resize_client(0, 0, cw, ch);
		}
		else
		{
			cw = defaults::smx2-defaults::smx1;
			ch = defaults::smy2-defaults::smy1;
			getsize(&cw, &ch);

			resize_client(defaults::smx1, defaults::smy1, cw, ch);
		}
		maxstate = 2;
	}
	else
	{
		resize_client(icx, icy, icw, ich);
		maxstate = 0;
	}

	iconmenu->setItemEnabled( iconmenu->idAt(0), false );
	iconmenu->setItemEnabled( iconmenu->idAt(2), true );
	if(defaults::windowsBtns && transfor == None)
	    if(ubdr != NULL)
		ubdr->maxbtn->setPixmap(*qapp::maxbtnpix);
	

	raise();
}

void xwindow::press_move(QMouseEvent *event)
{
	if(mrb != NULL)
		return;
		
	mousepos = event->pos()+midmove->pos();  // offset
	midmove->grabMouse(QCursor(Qt::sizeAllCursor));
	mrb = new rubber(0, "move-rubberband");
	move_move(event);   // draw frame
}

void xwindow::release_move(QMouseEvent *event)
{
    if(mrb == NULL)
	return;
		
    delete mrb;
    mrb = NULL;
    midmove->releaseMouse();
    move(event->globalPos()-mousepos);
	
    iconmenu->setItemEnabled( iconmenu->idAt(0), false );
    iconmenu->setItemEnabled( iconmenu->idAt(2), true );

    
    if(defaults::windowsBtns && transfor == None)
	if (ubdr != NULL)
	    ubdr->maxbtn->setPixmap(*qapp::maxbtnpix);
    

    maxstate = 0;
    raise();
    qapp::send_configurenotify(this);

    if(tstate && ! qapp::is_curdesk(this))
	tstate = FALSE;
    if(ubdr != NULL){
	ubdr->drawbg();
	ubdr->midframe->drawbg();
	ubdr->iconframe->drawbg();
    }
    if(lbdr != NULL)
	lbdr->drawbg();

    if(lsbdr != NULL){
	lsbdr->drawbg();
    }

    if(rsbdr != NULL){
	rsbdr->drawbg();
    }
}

void xwindow::move_move(QMouseEvent *event)
{
	if(mrb == NULL)
		return;
		
	QPoint p = event->globalPos()-mousepos;
	mrb->draw(p.x(), p.y(), width(), height());
}

void xwindow::press_lsresize(QMouseEvent *event)
{
	if(mrb != NULL)
		return;
		
	mousepos = event->globalPos();
	if(QApplication::reverseLayout())
	    rsbdr->midframe->grabMouse(QCursor(Qt::sizeAllCursor));
	else
	    lsbdr->midframe->grabMouse(QCursor(Qt::sizeAllCursor));

	mrb = new rubber(base_w, base_h, inc_w, inc_h, 0, 
			 "leftside-rubberband");
	move_lsresize(event);   // draw frame
}

void xwindow::release_lsresize(QMouseEvent *event)
{
	if(mrb == NULL)
		return;
		
	delete mrb;
	mrb = NULL;
	if(QApplication::reverseLayout())
	    rsbdr->midframe->releaseMouse();
	else
	    lsbdr->midframe->releaseMouse();

	QPoint dpos = event->globalPos()-mousepos;
	int resw = width()-dpos.x();
	int resh = height();

	getsize(&resw, &resh);
	int resx = x()-resw+width();
	
	if(resx > x()+width())
		resx = x()+width();

	resize_client(resx, y(), resw, resh);

	iconmenu->setItemEnabled( iconmenu->idAt(0), false );
	iconmenu->setItemEnabled( iconmenu->idAt(2), true );
	if(defaults::windowsBtns && transfor == None)
	    //if(ubdr->maxbtn!=NULL)
	    if(ubdr != NULL)
		ubdr->maxbtn->setPixmap(*qapp::maxbtnpix);
	
	maxstate = 0;
	raise();

	if(tstate && ! qapp::is_curdesk(this))
		tstate = FALSE;
    if(ubdr != NULL){
	ubdr->drawbg();
	ubdr->midframe->drawbg();
	ubdr->iconframe->drawbg();
    }
    if(lbdr != NULL)
	lbdr->drawbg();

    if(lsbdr != NULL){
	lsbdr->drawbg();
    }

    if(rsbdr != NULL){
	rsbdr->drawbg();
    }
}

void xwindow::move_lsresize(QMouseEvent *event)
{
	if(mrb == NULL)
		return;
	
	QPoint dpos = event->globalPos()-mousepos;
	int resw = width()-dpos.x();
	int resh = height();

	getsize(&resw, &resh);
	int resx = x()-resw+width();
	
	if(resx+min_w > x()+width())
		resx = x()+width()-min_w;
		
	mrb->draw(resx, y(), resw, resh);
}

void xwindow::press_leftresize(QMouseEvent *event)
{
	if(mrb != NULL)
		return;
		
	mousepos = event->globalPos();

	if(QApplication::reverseLayout())
	    lbdr->rightframe->grabMouse(QCursor(Qt::sizeAllCursor));
	else
	    lbdr->leftframe->grabMouse(QCursor(Qt::sizeAllCursor));

	mrb = new rubber(base_w, base_h, inc_w, inc_h, 0, "left-rubberband");
	move_leftresize(event);   // draw frame
}

void xwindow::release_leftresize(QMouseEvent *event)
{
	if(mrb == NULL)
		return;
		
	delete mrb;
	mrb = NULL;

	if(QApplication::reverseLayout())
	    lbdr->rightframe->releaseMouse();
	else
	    lbdr->leftframe->releaseMouse();

	QPoint dpos = event->globalPos()-mousepos;
	int resw = width()-dpos.x();
	int resh = height()+dpos.y();

	getsize(&resw, &resh);
	int resx = x()-resw+width();
	
	if(resx > x()+width())
		resx = x()+width();

	resize_client(resx, y(), resw, resh);

	iconmenu->setItemEnabled( iconmenu->idAt(0), false );
	iconmenu->setItemEnabled( iconmenu->idAt(2), true );
	if(defaults::windowsBtns && transfor == None)
	    //if(ubdr->maxbtn!=NULL)
	    if(ubdr != NULL)
		ubdr->maxbtn->setPixmap(*qapp::maxbtnpix);
	
	maxstate = 0;
	raise();

	if(tstate && ! qapp::is_curdesk(this))
		tstate = FALSE;
    if(ubdr != NULL){
	ubdr->drawbg();
	ubdr->midframe->drawbg();
	ubdr->iconframe->drawbg();
    }
    if(lbdr != NULL)
	lbdr->drawbg();

    if(lsbdr != NULL){
	lsbdr->drawbg();
    }

    if(rsbdr != NULL){
	rsbdr->drawbg();
    }
}

void xwindow::move_leftresize(QMouseEvent *event)
{
	if(mrb == NULL)
		return;
	
	QPoint dpos = event->globalPos()-mousepos;
	int resw = width()-dpos.x();
	int resh = height()+dpos.y();

	getsize(&resw, &resh);
	int resx = x()-resw+width();
	
	if(resx+min_w > x()+width())
		resx = x()+width()-min_w;
		
	mrb->draw(resx, y(), resw, resh);
}


void xwindow::press_rsresize(QMouseEvent *event)
{
	if(mrb != NULL)
		return;
		
	mousepos = event->globalPos();
	if(QApplication::reverseLayout())
	    lsbdr->midframe->grabMouse(QCursor(Qt::sizeAllCursor));
	else
	    rsbdr->midframe->grabMouse(QCursor(Qt::sizeAllCursor));

	mrb = new rubber(base_w, base_h, inc_w, inc_h, 0, 
			 "rightside-rubberband");
	move_rsresize(event);   // draw frame
}

void xwindow::release_rsresize(QMouseEvent *event)
{
	if(mrb == NULL)
		return;
		
	delete mrb;
	mrb = NULL;	
	if(QApplication::reverseLayout())
	    lsbdr->midframe->releaseMouse();
	else
	    rsbdr->midframe->releaseMouse();

	QPoint dpos = event->globalPos()-mousepos;
	int resw = width()+dpos.x();
	int resh = height();
	
	getsize(&resw, &resh);
	resize_client(x(), y(), resw, resh);

	iconmenu->setItemEnabled( iconmenu->idAt(0), false );
	iconmenu->setItemEnabled( iconmenu->idAt(2), true );
	if(defaults::windowsBtns && transfor == None)
	    if(ubdr != NULL)
		//if(ubdr->maxbtn!=NULL)
		ubdr->maxbtn->setPixmap(*qapp::maxbtnpix);
	
	maxstate = 0;
	raise();
	
	if(tstate && ! qapp::is_curdesk(this))
		tstate = FALSE;

    if(ubdr != NULL){
	ubdr->drawbg();
	ubdr->midframe->drawbg();
	ubdr->iconframe->drawbg();
    }
    if(lbdr != NULL)
	lbdr->drawbg();

    if(lsbdr != NULL){
	lsbdr->drawbg();
    }

    if(rsbdr != NULL){
	rsbdr->drawbg();
    }

}

void xwindow::move_rsresize(QMouseEvent *event)
{
	if(mrb == NULL)
		return;
		
	QPoint dpos = event->globalPos()-mousepos;
	int resw = width()+dpos.x();
	int resh = height();

	getsize(&resw, &resh);
	mrb->draw(x(), y(), resw, resh);
}







void xwindow::press_rightresize(QMouseEvent *event)
{
	if(mrb != NULL)
		return;
		
	mousepos = event->globalPos();

	if(QApplication::reverseLayout())
	    lbdr->leftframe->grabMouse(QCursor(Qt::sizeAllCursor));
	else
	    lbdr->rightframe->grabMouse(QCursor(Qt::sizeAllCursor));

	mrb = new rubber(base_w, base_h, inc_w, inc_h, 0, "right-rubberband");
	move_rightresize(event);   // draw frame
}

void xwindow::release_rightresize(QMouseEvent *event)
{
	if(mrb == NULL)
		return;
		
	delete mrb;
	mrb = NULL;	

	if(QApplication::reverseLayout())
	    lbdr->leftframe->releaseMouse();
	else
	    lbdr->rightframe->releaseMouse();

	QPoint dpos = event->globalPos()-mousepos;
	int resw = width()+dpos.x();
	int resh = height()+dpos.y();
	
	getsize(&resw, &resh);
	resize_client(x(), y(), resw, resh);

	iconmenu->setItemEnabled( iconmenu->idAt(0), false );
	iconmenu->setItemEnabled( iconmenu->idAt(2), true );
	if(defaults::windowsBtns && transfor == None)
	    //	    if(ubdr->maxbtn!=NULL)
	    if(ubdr != NULL)
		ubdr->maxbtn->setPixmap(*qapp::maxbtnpix);
	
	maxstate = 0;
	raise();
	
	if(tstate && ! qapp::is_curdesk(this))
		tstate = FALSE;
    if(ubdr != NULL){
	ubdr->drawbg();
	ubdr->midframe->drawbg();
	ubdr->iconframe->drawbg();
    }
    if(lbdr != NULL)
	lbdr->drawbg();

    if(lsbdr != NULL){
	lsbdr->drawbg();
    }

    if(rsbdr != NULL){
	rsbdr->drawbg();
    }
}

void xwindow::move_rightresize(QMouseEvent *event)
{
	if(mrb == NULL)
		return;
		
	QPoint dpos = event->globalPos()-mousepos;
	int resw = width()+dpos.x();
	int resh = height()+dpos.y();

	getsize(&resw, &resh);
	mrb->draw(x(), y(), resw, resh);
}

void xwindow::press_midresize(QMouseEvent *event)
{
	if(mrb != NULL)
		return;
		
	mousepos = event->globalPos();
	lbdr->midframe->grabMouse(QCursor(Qt::sizeAllCursor));
	mrb = new rubber(base_w, base_h, inc_w, inc_h, 0, "mid-rubberband");
	move_midresize(event);   // draw frame
}

void xwindow::release_midresize(QMouseEvent *event)
{
	if(mrb == NULL)
		return;
		
	delete mrb;
	mrb = NULL;
	lbdr->midframe->releaseMouse();
	QPoint dpos = event->globalPos()-mousepos;
	int resh = height()+dpos.y();
	int resw = width();
		
	getsize(&resw, &resh);	
	resize_client(x(), y(), resw, resh);

	iconmenu->setItemEnabled( iconmenu->idAt(0), false );
	iconmenu->setItemEnabled( iconmenu->idAt(2), true );
	if(defaults::windowsBtns && transfor == None)
	    //if(ubdr->maxbtn!=NULL)
	    if(ubdr != NULL)
		ubdr->maxbtn->setPixmap(*qapp::maxbtnpix);
	
	maxstate = 0;
	raise();
	
	if(tstate && ! qapp::is_curdesk(this))
		tstate = FALSE;
    if(ubdr != NULL){
	ubdr->drawbg();
	ubdr->midframe->drawbg();
	ubdr->iconframe->drawbg();
    }
    if(lbdr != NULL)
	lbdr->drawbg();

    if(lsbdr != NULL){
	lsbdr->drawbg();
    }

    if(rsbdr != NULL){
	rsbdr->drawbg();
    }
}

void xwindow::move_midresize(QMouseEvent *event)
{
	if(mrb == NULL)
		return;
		
	QPoint dpos = event->globalPos()-mousepos;
	int resh = height()+dpos.y();
	int resw = width();

	getsize(&resw, &resh);
	mrb->draw(x(), y(), resw, resh);
}

void xwindow::show_info(void)
{
    qapp::winf->show_info(this, wmname, clientfqdn, command, res_name, 
			  res_class, inc_w, inc_h, base_w, base_h);
}

void xwindow::unscreen(void)
{
	if(sstate)  // screen mode
	{
		resize_client(scx, scy, scw, sch);
		sstate = FALSE;
	}
}

bool xwindow::is_tileable(void)
{
	if((pflags & (qapp::Sticky|qapp::NoTile)) || transfor != None)
		return FALSE;

	return TRUE;	
}
bool xwindow::is_modal(void)
{
    if((pflags &WType_Dialog)){ // Qt::WType_Modal){
	return TRUE;
    }
    return FALSE;
}

void xwindow::toggle_tiled(void)
{
	qapp::toggle_tiled();
}

void xwindow::tile_maximize(void)
{
	qapp::tile_maximize(this);
	XWarpPointer(qt_xdisplay(), None, winId(), 0, 0, 0, 0, width()/2, uborderh/2);
}

void xwindow::minimize_frame(bool mf)
{
	if(pflags & qapp::SmallFrame)
		return;

	int ch = height()-borderh;
	int cw = width()-borderw;
		
	if(mf)
	{
		if(uborderh == defaults::lowerborderheight)
			return;

		ubdr->set_small();
		uborderh = defaults::lowerborderheight;
		borderh = 2*uborderh;
		borderw = 2*uborderh;
	}
	else
	{
		if(uborderh == defaults::windowbuttonsize)
			return;

		ubdr->set_normal();
		uborderh = defaults::windowbuttonsize;
		borderh = defaults::windowbuttonsize + defaults::lowerborderheight;
		borderw = 2* defaults::lowerborderheight;
		set_title();
	}
	
	if(shaped || (pflags & qapp::NoResize) || !isdecorated()){
		borderh -= defaults::lowerborderheight;
		borderw = 0;
	}

	get_wmnormalhints();
	resize(cw+borderw, ch+borderh);
	XMoveWindow(qt_xdisplay(), clientid, borderw/2, uborderh);
}

int xwindow::set_tile(int cx, int cy, int cw, int ch)
{
	if(tstate == FALSE)
	{
		tcx = x();
		tcy = y();
		tcw = width();
		tch = height();

		tstate = TRUE;
	}
	iconmenu->setItemEnabled( iconmenu->idAt(0), false );
	iconmenu->setItemEnabled( iconmenu->idAt(2), true );
	if(defaults::windowsBtns && transfor == None)
	    //if(ubdr->maxbtn!=NULL)
	    if(ubdr != NULL)
		ubdr->maxbtn->setPixmap(*qapp::maxbtnpix);
	
	maxstate = 0;
	
	int cw2 = cw;
	int ch2 = ch;
	
	if(! trsize)
	{
		getsize(&cw2, &ch2);

		if(cw2 > cw)
			cw2 = cw;

		if(ch2 > ch)
			ch2 = ch;
		
		resize_client(cx, cy, cw2, ch2);
	}	
	raise();

	return(ch - ch2);
}

void xwindow::unset_tile(void)
{
	if(! tstate)
		return;

	resize_client(tcx, tcy, tcw, tch);

	iconmenu->setItemEnabled( iconmenu->idAt(0), false );
	iconmenu->setItemEnabled( iconmenu->idAt(2), true );
	if(defaults::windowsBtns && transfor == None)
	    //if(ubdr->maxbtn!=NULL)
	    if(ubdr != NULL)
		ubdr->maxbtn->setPixmap(*qapp::maxbtnpix);
	
	maxstate = 0;
	minimize_frame(FALSE);
	
	tstate = FALSE;
}

void xwindow::setinactive(void)
{
    /*
    if(transfor != None)
	return;
    */

    //    qapp::dsktp->setFocus();
    if(actpal || urgpal){
	setPalette(*qapp::ipal);  // set inactive colors
	stopautofocus();
	actpal = FALSE;
    }

    if(urgpal){
	tb_pb->change_palette(QApplication::palette(), this);
	urgpal = FALSE;
    }
    if(ubdr != NULL){
	ubdr->drawbg();
	ubdr->midframe->drawbg();
	ubdr->iconframe->drawbg();
    }
    if(lbdr != NULL){
	lbdr->drawbg();
    }

    if(lsbdr != NULL){
	lsbdr->drawbg();
    }

    if(rsbdr != NULL){
	rsbdr->drawbg();
    }
    setPaletteForegroundColor(defaults::inactive_fg);   
}

void xwindow::setactive(void)
{
    //    dontmouse = true;
    //    state(true);
    //    dontmouse = false;
    if(urgpal || ! actpal){
	setPalette(*qapp::apal, this);
	actpal = TRUE;
    }

    if(urgpal){
	tb_pb->change_palette(QApplication::palette(), this);
	urgpal = FALSE;
    }	
    if(ubdr != NULL){
	ubdr->drawbg();
	ubdr->midframe->drawbg();
	ubdr->iconframe->drawbg();
    }
    if(lbdr != NULL)
	lbdr->drawbg();

    if(lsbdr != NULL){
	lsbdr->drawbg();
    }

    if(rsbdr != NULL){
	rsbdr->drawbg();
    }
    setPaletteForegroundColor(defaults::active_fg);
}

void xwindow::seturgent(void)
{
    if(! urgpal){
	setPalette(*qapp::upal);
	tb_pb->change_palette(*qapp::upal, this);
	urgpal = TRUE;
    }  
}
void xwindow::timertimeout()
{
        if(mrb != NULL || qapp::winf->get_client() != NULL)
                return;

#ifdef DEBUGMSG
        cerr << "raise (WId:" << winId() << ")\n";
#endif

        trsize = FALSE;
        XRaiseWindow(qt_xdisplay(), winId());

        dontmouse = true;
        state(true);
        dontmouse = false;
}


void xwindow::raise()
{
	if(mrb != NULL || qapp::winf->get_client() != NULL)
		return;
		
#ifdef DEBUGMSG
	cerr << "raise (WId:" << winId() << ")\n";
#endif

	trsize = FALSE;
	XRaiseWindow(qt_xdisplay(), winId());



//        dontmouse = true;
//        state(true);
//        dontmouse = false;
}

void xwindow::map(void)
{
	withdrawnstate = whidden = FALSE;
	if (!(get_pflags() & qapp::WindowListSkip))
	    //
	    if(transfor == None)
		tb_pb->add(this);
	//
	    else{
		    prot_takefocus = true; 
		    send_wmprotocol(qapp::wm_take_focus, CurrentTime);
		}

	if(map_iconic)  // InitialState WMHint
	{
	    //	tb_pb->add(this);
		set_clientstate(IconicState);

		if(urgent || urgpal)
		{
			urgpal = FALSE;
			seturgent();
		}	
	}	
	else
	{
		if(! isVisible())
			tb_pb->set_on(this, true);
			
		map_normal();
	}	
	qapp::focus_window(this);
	//	qapp::free_mouse();
}

void xwindow::map_normal(void)
{
	if(! isVisible())
	{
#ifdef DEBUGMSG
		cerr << "map client\n";
#endif	
		unmapped = FALSE;
		
		if(is_tileable() && qapp::is_tileddesk()) 
		{
			if(clients.find(this) == -1)
				return;

			qapp::tile_order(trsize?this:qapp::tmaxclient);
		}	
		else  // tiled window mapped on untiled desk
		{
			unset_tile();
		}

		show();
//		XMapWindow(qt_xdisplay(), clientid);
		set_clientstate(NormalState);
		//
		//XReparentWindow(qt_xdisplay(), clientid, winId(), borderw/2, uborderh);
		if (shaped || !isdecorated())
		    XMoveWindow(qt_xdisplay(), clientid, 0, 0);
		else if( pflags & qapp::NoResize)
		    XMoveWindow(qt_xdisplay(), clientid, 0, uborderh);
		else
		    XMoveWindow(qt_xdisplay(), clientid, borderw/2, uborderh);

		XMapWindow(qt_xdisplay(), clientid);
 	//
		XSync(qt_xdisplay(), FALSE);
	}
	raise();
}

void xwindow::unmap(void)
{
	stopautofocus();

	if(! isVisible())
		return;

	unmapped = TRUE;	
	
	hide();
	XUnmapWindow(qt_xdisplay(), clientid);

	if(tstate && qapp::is_curdesk(this))
	{
		if(qapp::tmaxclient == this)
			qapp::tmaxclient = NULL;
			
		qapp::tile_order(qapp::tmaxclient);
	}
}

void xwindow::showhelp(void){
    if (withccontexthelp) {
	XEvent ev;
	long mask;

	memset(&ev, 0, sizeof(ev));
	ev.xclient.type = ClientMessage;
	ev.xclient.window = clientid;
	ev.xclient.message_type = qapp::wm_protocols;
	ev.xclient.format = 32;
	ev.xclient.data.l[0] = qapp::atoms->net_wm_context_help;
	ev.xclient.data.l[1] = CurrentTime;
	mask = 0L;

	if (clientid == qt_xrootwin())
	    mask = SubstructureRedirectMask;	    /* magic! */
	XSendEvent(qt_xdisplay(), clientid, False, mask, &ev);


	//QWhatsThis::enterWhatsThisMode();
    }

}



void xwindow::iconify(void)  // transition to iconic
{
	if(qapp::tmaxclient == this && qapp::is_tileddesk())
		trsize = TRUE;
	
	unmap();
	tb_pb->set_on(this, false);

	withdrawnstate = FALSE;
	set_clientstate(IconicState);

	//tb_pb->add(this);  // add to procbar
}

void xwindow::whide(void)  
{
	unmap();
	withdrawnstate = FALSE;
	set_clientstate(IconicState);

	tb_pb->remove(this);
	whidden = TRUE;
}

void xwindow::withdraw(void)
{
	unmap();
	withdrawnstate = TRUE;
	set_clientstate(WithdrawnState);
	tb_pb->remove(this);
	if(mouseGrabber()!=0)
	    mouseGrabber()->releaseMouse();
	releaseMouse();
	qapp::mousegrapped = false;

#ifdef DEBUGMSG
	cerr << "changed to withdrawn (WId:" << winId() << ")\n";
#endif
}

void xwindow::focus_mouse_wlist(void)
{
	focus_mouse(TRUE);
}

void xwindow::focus_mouse(bool wlist)  // map and set mouse (generates enter event -> focus)
{
	int pw,ph;
	
	if(qapp::smode)  // screen mode
	{
		if(! isVisible())
			return;
			
		if(sstate == FALSE)
		{
			scx = x();
			scy = y();
			scw = width();
			sch = height();

			pw = dt->width()+(2*defaults::lowerborderheight);
			ph = dt->height()+uborderh+defaults::lowerborderheight;
			setGeometry(-borderw/2, -uborderh, pw, ph);
			//setGeometry(0, 0, pw + borderw/2, ph+ uborderh + borderw/2);
			
			if(inc_w > 1)
			{
				int i = (pw-base_w)/inc_w;
				pw = base_w+i*inc_w;
			}

			if(inc_h > 1)	
			{
				int j = (ph-base_h)/inc_h;
				ph = base_h+j*inc_h;
			}	

			XResizeWindow(qt_xdisplay(), clientid, pw-borderw, ph-borderh);
			//XResizeWindow(qt_xdisplay(), clientid, pw, ph);
			sstate = TRUE;
		}
		raise();
		return;
	}
	//map();	
	qapp::focus_window(this);


	int mid = x()+(width()/2);
	
	if(mid > dt->width())
	{
		tb_pg->change_desk(qapp::adesk+(x()/(dt->width()-1)));
	}	
	else if(mid < 0)
	{
		tb_pg->change_desk(qapp::adesk+(x()/(dt->width()+1))-1);
	}
	else if(defaults::maxontab && ! wlist && tstate)
	{
		// access in tiled state will swap windows after timeout
		
		qapp::stopautofocus();
		tfocustimer->start(defaults::maxontab, TRUE);
	}	
	XWarpPointer(qt_xdisplay(), None, winId(), 0, 0, 0, 0, width()/2, uborderh/2);
}

void xwindow::setchildfocus(long timestamp) // set to active and focus to child
{
#ifdef DEBUGMSG
	cerr << "change active client to (WId:" << winId() << ")\n";
#endif

	setactive();

	if(inputfield)  // InputHint
		XSetInputFocus(qt_xdisplay(), clientid, RevertToPointerRoot, CurrentTime);
		
	if(prot_takefocus)  // WM_TAKE_FOCUS protocol
		send_wmprotocol(qapp::wm_take_focus, timestamp);

}

void xwindow::stopautofocus(void)
{
	if(defaults::autofocustime)
		focustimer->stop();

	if(tstate)
		tfocustimer->stop();
}

void xwindow::startautofocus(void)  // start autofocus timer
{
	if(! defaults::autofocustime || mrb != NULL)
		return;
		
	// do not raise if another window appears within this window

	Window w1,w2,*wins;
	uint nwins,i;
	XWindowAttributes attr;
	
	if(XQueryTree(qt_xdisplay(), qt_xrootwin(), &w1, &w2, &wins, &nwins) && nwins)
	{
		for(i=0; i < nwins; i++)  // find current window
			if(wins[i] == winId())
				break;
				
		int cx2 = x()+width(); int cy2 = y()+height();
		
		while(++i < nwins)
		{
			XGetWindowAttributes(qt_xdisplay(), wins[i], &attr);

			if(attr.map_state !=  IsViewable)
					continue;

			int nx2 = attr.x+attr.width; int ny2 = attr.y+attr.height;
			
			if(attr.x >= x() && nx2 <= cx2 && attr.y >= y() && ny2 <= cy2)
			{
				XFree(wins);
				return;
			}	
		}	
		if(nwins)
		    XFree(wins);
	}	
	focustimer->start(defaults::autofocustime, TRUE);
}

void xwindow::getwmprotocols(void)
{
	Atom *protocols;
	int nprot,i;
	
	prot_delete = FALSE;
	prot_takefocus = FALSE;

	if(XGetWMProtocols(qt_xdisplay(), clientid, &protocols, &nprot)){
	    for(i=0; i < nprot; i++){
		if(protocols[i] == qapp::wm_delete){
		    prot_delete = TRUE;
		}	
		else if(protocols[i] == qapp::wm_take_focus){
		    prot_takefocus = TRUE;
		}
		else if(protocols[i] == qapp::atoms->net_wm_context_help){
		    withccontexthelp = true;
		    //cout << "help\n\n";
		}
		/*
		  else if(protocols[i] == Qt::WStyle_Dialog)
		  {
		  //withccontexthelp = true;
		  cout << "WStyle_Dialog\n\n";
		  }
		*/
	    }
	    //    XFree(protocols);
	}
	if(protocols)
	    XFree(protocols);
}

void xwindow::send_wmprotocol(long data0, long data1)  // send protocol message to child window
{
	XEvent event;
	
	memset(&event, 0, sizeof(event));
	event.xclient.type = ClientMessage;
	event.xclient.window = clientid;
	event.xclient.message_type = qapp::wm_protocols;
	event.xclient.format = 32;
	event.xclient.data.l[0] = data0;
	event.xclient.data.l[1] = data1;
	
	XSendEvent(qt_xdisplay(), clientid, False, 0L, &event);
}

int xwindow::get_clientstate(void)
{
	Atom ret_type;
	int format;
	unsigned long n,extra;
	ulong *data;
	int ret_state;

	if(XGetWindowProperty(qt_xdisplay(), clientid, qapp::wm_state, 0L, 1L, FALSE, qapp::wm_state, &ret_type, &format, &n, &extra, (uchar **)&data) != Success || n == 0)
	{
		cerr << "WM: cannot get wm_state for " << clientid << '\n';
		return(None);
	}	
		
	ret_state = (int)*data;
	if(data)
	    XFree(data);
	//
	//get_wmname();	
	//
	return(ret_state);
}

void xwindow::set_clientstate(int state)
{
	ulong data[2];
	data[0] = (ulong)state;
	data[1] = (ulong)None;

	XChangeProperty(qt_xdisplay(), clientid, qapp::wm_state, qapp::wm_state, 32, PropModeReplace, (uchar *)data, 2);

	//
	//get_wmname();	
	//


}

void xwindow::wdestroy(void)  // destroy client
{
    if(mouseGrabber()!=0)
	mouseGrabber()->releaseMouse();
    releaseMouse();
    qapp::mousegrapped = false;

	if(prot_delete)  // soft way
	{
		send_wmprotocol(qapp::wm_delete, CurrentTime);
#ifdef DEBUGMSG
		cerr << "soft kill\n";
#endif
	}
	else 
	{ 
		XKillClient(qt_xdisplay(), clientid);
		XSync(qt_xdisplay(), FALSE);
#ifdef DEBUGMSG
		cerr << "hard kill\n";
#endif		
	}	
}

void xwindow::get_colormaps(void)
{
	Window *w;
	int nm=0;

	XWindowAttributes attr;
	XGetWindowAttributes(qt_xdisplay(), clientid, &attr);
	cmap = attr.colormap;

	if(! XGetWMColormapWindows(qt_xdisplay(), clientid, &w, &nm))
		return;

	if(nm > 1000)
	{
		cerr << "More than 1000 colormaps for " << clientname << " - property rejected\n";
		return;
	}	
	
	if(ncmaps) 
		delete [] cmapwins;

	if(nm == 0)
	{
		cmapwins = NULL;
		ncmaps = 0;
		
		return;
	}

#ifdef DEBUGMSG
	cerr << "client has WM_COLORMAP_WINDOWS property\n";
#endif	
		
	cmapwins = new Window[nm];
	Q_CHECK_PTR(cmapwins);
	ncmaps = nm;
	
	for(int i=0; i < nm; i++) 
		cmapwins[i] = w[i];
	if(w)
	    XFree(w);
}

void xwindow::setcmapfocus(void)
{
	XWindowAttributes attr;
	bool installed = FALSE;

	for(int i = ncmaps-1; i >= 0; i--)
	{
		Window w = cmapwins[i];
		
		if(w == clientid)
			installed = TRUE;
			
		XGetWindowAttributes(qt_xdisplay(), w, &attr);
		qapp::install_colormap(attr.colormap);
	}	
    	if(! installed)
   		qapp::install_colormap(cmap);

}

bool xwindow::query_shape(void){ 
  int bShaped, cShaped;
  int xbs, ybs, xcs, ycs;
  unsigned int wbs, hbs, wcs, hcs;

  XShapeSelectInput(qt_xdisplay(), clientid, ShapeNotifyMask);	
    XShapeQueryExtents(qt_xdisplay(), clientid, &bShaped, &xbs, &ybs, &wbs, &hbs, &cShaped, &xcs, &ycs, &wcs, &hcs);

    if (bShaped ){//||(pflags &WStyle_Tool)) {
	return TRUE;
    }

    return FALSE;
}

void xwindow::reshape(void)
{
#ifdef DEBUGMSG
	cerr << "reshaping client\n";
#endif

	shaped = TRUE;
	XShapeCombineShape(qt_xdisplay(), winId(), ShapeBounding, borderw/2, uborderh, clientid, ShapeBounding, ShapeSet);

	XRectangle tr;
	
	tr.x = tr.y = 0;
	tr.width = width();
	tr.height = uborderh;
	XShapeCombineRectangles(qt_xdisplay(), winId(), ShapeBounding, 0, 0, &tr, 1, ShapeUnion, Unsorted);
    
}

void xwindow::get_classhint(void)
{
	XClassHint ch;
	
	if(XGetClassHint(qt_xdisplay(), clientid, &ch))
	{
		res_name = ch.res_name;
		res_class = ch.res_class;
		
		
#ifdef DEBUGMSG
	cerr << "res_name set to " << res_name << '\n';
#endif
	}	
	if(ch.res_name)
	    XFree(ch.res_name);
	if(ch.res_class)
	    XFree(ch.res_class);
}

int xwindow::get_clientflags(void)
{
	QString cclname(res_name);   // check for name,class format
	cclname += ',';
	cclname += res_class;
	int clf = qapp::cprops[cclname];

	if(clf)
		return(clf);

	if((clf = qapp::cprops[clientname]))
		return(clf|qapp::ClassOnly);

	//
	//get_wmname();	
	//
	return(0);	
}	

void xwindow::set_pflags(int tflags)
{
	int flags;
	
	if(tflags == -1)
		flags = get_clientflags();
	else
		flags = tflags;

	if(flags == pflags)
		return;

	int oldflags = pflags;
	pflags = flags;

	if(pflags & (qapp::NoTile|qapp::Sticky))
		unset_tile();

	int mask = qapp::SmallFrame|qapp::NoResize;
	
       	if((flags & mask) != (oldflags & mask))  // rebuild window frame
	{
	
#ifdef DEBUGMSG
	cerr << "rebuilding window frame (WId:" << winId() << ")\n";
#endif	

		delete mrb;
		delete midmove;
		delete ubdr;
		delete lbdr;
		delete lsbdr;
		delete rsbdr;
		delete layout;
		
		int ch = height()-borderh;
		int cw = width()-borderw;
	
		if(pflags & qapp::SmallFrame)
		{
			uborderh = defaults::lowerborderheight;
			borderh = 2*uborderh;
		}
		else
		{
		    uborderh = defaults::windowbuttonsize;
		    borderh = defaults::windowbuttonsize+
			defaults::lowerborderheight;
		}
	
		if(defaults::sideBorders){
		    borderw = 2*defaults::lowerborderheight;
		}else{
		    borderw = 0;
		}

		if(shaped || (pflags & qapp::NoResize) || !isdecorated()){
			borderh -= defaults::lowerborderheight;
			borderw = 0;
		}

		get_wmnormalhints();
		//resize(width(), ch+borderh);
		resize(cw+borderw, ch+borderh);
		XMoveWindow(qt_xdisplay(), clientid, borderw/2, uborderh);
		
		create_wborder();
		wmname = "";
		get_wmname();

		if(shaped)
		    reshape();

		if(ubdr != NULL)
		    ubdr->show();
		else
		    midmove->show();

		if(lbdr != NULL)
		    lbdr->show();


		if(lsbdr != NULL)
		    lsbdr->show();

		if(rsbdr != NULL)
		    rsbdr->show();
	}

	if(! (oldflags & qapp::Sticky) && (flags & qapp::Sticky))  // move to current desk
	{
		int dx = x()/dt->width();

		if(x() < 0)
			dx--;
			
		move(x()-(dx*dt->width()), y());
	}
}

void xwindow::get_wmnormalhints(void)
{
	XSizeHints *xsizehints;
	if((xsizehints = XAllocSizeHints()) == NULL)
		sleep(1);

#ifdef DEBUGMSG
	cerr << "reading WMNormalHints (WId:" << winId() << ")\n";
#endif	
		
	long hints;
	if(XGetWMNormalHints(qt_xdisplay(), clientid, xsizehints, &hints) == 0)
		xsizehints->flags = 0;

	wmnflags = xsizehints->flags;
	wingrav = xsizehints->win_gravity;
		
	bool pminsize = FALSE;
	
	// set max,min and base size, results include window borders

	if(wmnflags & PMinSize) 
	{
	    min_w = xsizehints->min_width+borderw;
	    min_h = xsizehints->min_height+borderh;
	    pminsize = TRUE;   // to get base size if unspecified
	} 
	else 
	{
	    min_w = borderw;
	    min_h = borderh;
	}
	if(wmnflags & PMaxSize)
	{
	    max_w = xsizehints->max_width+borderw;
	    max_h = xsizehints->max_height+borderh;
		
	    if(max_w > dt->width())
		max_w = dt->width();

	    if(max_h > dt->height())
		max_h = dt->height();
	}
	else
	{
	    max_w = dt->width();
	    max_h = dt->height();
	}
	
	if(wmnflags & PBaseSize)
	{
	    base_w = xsizehints->base_width+borderw;
	    base_h = xsizehints->base_height+borderh;

	    if(! pminsize)  // get from base if unspecified
	    {
		min_w = base_w;
		min_h = base_h;
	    }
	}
	else if(pminsize)
	{
		base_w = xsizehints->min_width;
		base_h = xsizehints->min_height;
	}
	
	if(wmnflags & PResizeInc)
	{
		inc_w = xsizehints->width_inc;
		inc_h = xsizehints->height_inc;
	}
	else
		inc_w = inc_h = 1;

	if(xsizehints)
	    XFree(xsizehints);
}		

void xwindow::get_wmhints(void)  // get WMHints
{
	XWMHints *xwmhints;
	map_iconic = FALSE;
	inputfield = TRUE;
	urgent = FALSE;
	
	if((xwmhints = XGetWMHints(qt_xdisplay(), clientid)) != NULL)
	{
#ifdef DEBUGMSG
	cerr << "reading WMHints (WId:" << winId() << ")\n";
#endif	

		if(xwmhints->flags & StateHint && xwmhints->initial_state == IconicState)
			map_iconic = TRUE;

		if(! (xwmhints->flags & InputHint))  // focus
			inputfield = FALSE;
			
		if(xwmhints->flags & IconPixmapHint)
			get_servericon(xwmhints->icon_pixmap, (xwmhints->flags & IconMaskHint)?(xwmhints->icon_mask):(None));

		if(xwmhints->flags & (1L << 8))
		{
			urgent = TRUE;
			seturgent();  // set urgent colors on window border and icon
		}
		else if(urgpal)
		{
			if(actpal)
				setactive();
			else
				setinactive();
		}
	}
	if(xwmhints)
	    XFree(xwmhints);
}

void xwindow::set_title(void)
{
	if((pflags & qapp::SmallFrame) || (uborderh == defaults::lowerborderheight))
		return;

	QString *wmn = &wmname;
	QString tnm;

	if(tnumber)
	{
		QTextOStream tns(&tnm);
		
		tns << wmname << '<' << tnumber << '>';
		wmn = &tnm;
	}	
	midmove->set_text(*wmn, transfor == None);

	if(! wicon.isNull()){
	    QPixmap *iconpix = new QPixmap();
	    iconpix->convertFromImage(wicon.convertToImage().smoothScale(
		defaults::windowbuttonsize-6,defaults::windowbuttonsize-6));
	    ubdr->iconframe->setPixmap(*iconpix);
	}
	tb_pb->change_text(&icname, this);

	setPalette(actpal?*qapp::apal:*qapp::ipal);
}

void xwindow::get_wmname(void)  // get WM_NAME and ICON_NAME and set caption and border pixmap
{
    get_wmhints();

	char *name;
	xwindow *client;
	//	QString oldwname = render(wmname, defaults::coding);
	QString oldwname = wmname;
	QString oldiname = icname;

	/*	
	if(XFetchName(qt_xdisplay(), clientid, &name) && name != NULL)
	{
		wmname = name;
		XFree(name);
		
	}
	else  // use class hints
		wmname = res_name;
	*/

	//
	//

        XTextProperty tp;
        char **text;
        int count;
        if ( XGetTextProperty( qt_xdisplay(), clientid, &tp, XA_WM_NAME) != 0 && tp.value != NULL ) {
            if ( tp.encoding == XA_STRING )
                wmname = QString::fromLocal8Bit( (const char*) tp.value );
            else if ( XmbTextPropertyToTextList( qt_xdisplay(), &tp, &text, &count) == Success &&
                      text != NULL && count > 0 ) {
                wmname = QString::fromLocal8Bit( text[0] );
                XFreeStringList( text );
            }
        }
	if(tp.value)
	    XFree( tp.value );

	//
	//


	if(XGetIconName(qt_xdisplay(), clientid, &name) && name != NULL)
	{
	    //		icname = name;
	    icname = QTextCodec::codecForName("utf8")->toUnicode(name);
		XFree(name);
	}
	else 
		icname = wmname;
		
#ifdef DEBUGMSG
	cerr << "WM_NAME set to " << wmname << '\n';
#endif

	if(wmname != oldwname){
	    // look for other windows with the same title
	    
	    tnumber = 0;

	    QList<xwindow> clst = clients;
	    
	    for(client = clst.first(); client != NULL; client = clst.next()){
		if(client != this && ! (client->get_pflags() &qapp::SmallFrame)
		   && wmname == client->ccaption() && client->get_tnumber() 
		   >= tnumber)
		    tnumber = client->get_tnumber()+1;
	    }
	    set_title();
	}
		
	if(icname != oldiname)  // change icon text on procbar
		tb_pb->change_text(&icname, this);

	//	cout << "wmname = \"" << wmname << "\"\n";

    /*
	char *name;
	xwindow *client;
	QString oldwname = wmname;

	QString oldiname = icname;
	
	if(XFetchName(qt_xdisplay(), clientid, &name) && name != NULL)
	{
		wmname = name;
		XFree(name);
		
	}
	else  // use class hints
		wmname = res_name;

	if(XGetIconName(qt_xdisplay(), clientid, &name) && name != NULL)
	{
		icname = name;
		XFree(name);
	}
	else 
		icname = wmname;
		
#ifdef DEBUGMSG
	cerr << "WM_NAME set to " << wmname << '\n';
#endif

	//wmname = render(wmname, defaults::coding);
	//char *charset = hde_langinfo(defaults::lng);
	//if ( QString::compare(charset,  "default") != 0){
	//  wmname = QTextCodec::codecForName(charset)->toUnicode(wmname);
	//  icname = QTextCodec::codecForName(charset)->toUnicode(icname);
	//}
	//wmname = tr(wmname, "");
	//icname = render(icname, defaults::coding);
	wmname = QTextCodec::codecForName("utf8")->toUnicode(wmname);
	icname = QTextCodec::codecForName("utf8")->toUnicode(icname);
	//icname = tr(icname, "");
	if(wmname != oldwname)
	{
		// look for other windows with the same title
	
		tnumber = 0;

		QList<xwindow> clst = clients;
		
		for(client = clst.first(); client != NULL; client = clst.next())
		{
			if(client != this && ! (client->get_pflags() & qapp::SmallFrame) && wmname == client->ccaption() && client->get_tnumber() >= tnumber)
				tnumber = client->get_tnumber()+1;
		}
		set_title();
	}	
		
	if(icname != oldiname)  // change icon text on procbar
		tb_pb->change_text(&icname, this);

    */
	set_title();
}

void xwindow::get_servericon(Pixmap icon, Pixmap mask)  // get pixmap from server and scale it
{
	int ix,iy;
	uint iw,ih,bwidth,depth;
	Window w;

	if(! XGetGeometry(qt_xdisplay(), icon, &w, &ix, &iy, &iw, &ih, &bwidth, &depth))
		return;
		
	QPixmap pix(iw, ih, (int)depth);
	pix.detach();
	GC gc = qt_xget_temp_gc(qt_xscreen(), depth == 1);  // no XFreeGC necessary




	XCopyArea(qt_xdisplay(), icon, pix.handle(), gc, 0, 0, iw, ih, 0 ,0);
	
	if(mask != None)  // apply icon_mask
	{
		gc = qt_xget_temp_gc(qt_xscreen(), TRUE);

		QBitmap bmap(iw, ih);
		bmap.detach();
		XCopyArea(qt_xdisplay(), mask, bmap.handle(), gc, 0, 0, iw, ih, 0, 0);
		pix.setMask(bmap);
	}
	
	// scale Pixmap

	QWMatrix m;
	double wh = defaults::tc_height-4;
	m.scale(wh/iw, wh/ih);
	wicon = pix.xForm(m);
}

void xwindow::state(bool on)
{
    if(defaults::withxpmenu)
	defaults::xpmenu->close();

    if(on){
	map_normal();
	tb_pb->set_on(this, true);
	
	if(!dontmouse){
	    focus_mouse_wlist();
	    //raise();

	    //setchildfocus(CurrentTime);
	    //setcmapfocus();
	    dontmouse = false;
	}
	
    }else{
	iconify();
	tb_pb->set_on(this, false);
    }

    tb_pb->draw_buttons();
}

/*
void xwindow::mousePressEvent ( QMouseEvent * e ){
    //    cout << "ungr\n";
}
*/

xwindow::~xwindow(void)
{
	delete [] cmapwins;
	delete mrb;

	if(qapp::winf->get_client() == this)
		qapp::winf->release_cancel();
	
	tb_pb->remove(this);  // remove from procbar
	qapp::cwindows.remove(clientid);
	qapp::pwindows.remove(winId());
	
#ifdef DEBUGMSG
	cerr << "class xwindow destructed (WId:" << winId() << ")\n";
#endif	
}
