/****************************************************************************
**
**	clrform.cpp
**      by:  Haydar Alkaduhimi <haydar@haydar.net>
**
****************************************************************************/ 

#include "clrform.h"

#include <qvariant.h>
#include <qcombobox.h>
#include <qframe.h>
#include <qlabel.h>
#include <qlineedit.h>
#include <qpushbutton.h>
#include <qspinbox.h>
#include <qmime.h>
#include <qdragobject.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>
#include <qimage.h>
#include <qpixmap.h>

#include <qmessagebox.h> 
#include <qcolordialog.h> 
#include <qfontdialog.h> 


#include <defaults.h> 
#include <hinifile.h>
#include <libhde.h>

static QPixmap uic_load_pixmap_clrForm( const QString &name )
{
    const QMimeSource *m = QMimeSourceFactory::defaultFactory()->data( name );
    if ( !m )
	return QPixmap();
    QPixmap pix;
    QImageDrag::decode( m, pix );
    return pix;
}

static QPixmap colorPix( QColor pc )
{
    QPixmap pix( 100, 100);
    pix.fill( pc );

    return pix;
}


clrLabel::clrLabel( QWidget* parent,  const char* name, WFlags fl )
    : QLabel( parent, name, fl )
{

}

aplBtn::aplBtn( QWidget* parent,  const char* name )
    : QPushButton( parent, name )
{

}

/* 
 *  Constructs a clrForm which is a child of 'parent', with the 
 *  name 'name' and widget flags set to 'f'.
 */
clrForm::clrForm( QWidget* parent,  const char* name, WFlags fl )
    : QWidget( parent, name, fl )
{
    if ( !name )
	setName( "clrForm" );
    resize( 371, 405 ); 
    setCaption( trUtf8( "Appearance" ) );

    QWidget* privateLayoutWidget = new QWidget( this, "Layout7" );
    privateLayoutWidget->setGeometry( QRect( 10, 10, 350, 390 ) ); 
    Layout7 = new QVBoxLayout( privateLayoutWidget, 0, 6, "Layout7"); 

    desktop = new clrLabel( privateLayoutWidget, "desktop" );
    desktop->setMinimumSize( QSize( 0, 200 ) );
    desktop->setPaletteBackgroundColor( QColor( 0, 0, 170 ) );
    desktop->setFrameShape( QFrame::StyledPanel );
    desktop->setFrameShadow( QFrame::Sunken );

    Frame4 = new QFrame( desktop, "Frame4" );
    Frame4->setGeometry( QRect( 10, 10, 270, 91 ) ); 
    QPalette pal;
    QColorGroup cg;
    cg.setColor( QColorGroup::Foreground, black );
    cg.setColor( QColorGroup::Button, QColor( 224, 223, 227) );
    cg.setColor( QColorGroup::Light, white );
    cg.setColor( QColorGroup::Midlight, QColor( 239, 239, 241) );
    cg.setColor( QColorGroup::Dark, QColor( 112, 111, 113) );
    cg.setColor( QColorGroup::Mid, QColor( 149, 149, 151) );
    cg.setColor( QColorGroup::Text, black );
    cg.setColor( QColorGroup::BrightText, white );
    cg.setColor( QColorGroup::ButtonText, black );
    cg.setColor( QColorGroup::Base, white );
    cg.setColor( QColorGroup::Background, QColor( 239, 239, 241) );
    cg.setColor( QColorGroup::Shadow, black );
    cg.setColor( QColorGroup::Highlight, QColor( 0, 0, 128) );
    cg.setColor( QColorGroup::HighlightedText, white );
    cg.setColor( QColorGroup::Link, black );
    cg.setColor( QColorGroup::LinkVisited, black );
    pal.setActive( cg );
    cg.setColor( QColorGroup::Foreground, black );
    cg.setColor( QColorGroup::Button, QColor( 224, 223, 227) );
    cg.setColor( QColorGroup::Light, white );
    cg.setColor( QColorGroup::Midlight, white );
    cg.setColor( QColorGroup::Dark, QColor( 112, 111, 113) );
    cg.setColor( QColorGroup::Mid, QColor( 149, 149, 151) );
    cg.setColor( QColorGroup::Text, black );
    cg.setColor( QColorGroup::BrightText, white );
    cg.setColor( QColorGroup::ButtonText, black );
    cg.setColor( QColorGroup::Base, white );
    cg.setColor( QColorGroup::Background, QColor( 239, 239, 241) );
    cg.setColor( QColorGroup::Shadow, black );
    cg.setColor( QColorGroup::Highlight, QColor( 0, 0, 128) );
    cg.setColor( QColorGroup::HighlightedText, white );
    cg.setColor( QColorGroup::Link, QColor( 0, 0, 255) );
    cg.setColor( QColorGroup::LinkVisited, QColor( 255, 0, 255) );
    pal.setInactive( cg );
    cg.setColor( QColorGroup::Foreground, QColor( 128, 128, 128) );
    cg.setColor( QColorGroup::Button, QColor( 224, 223, 227) );
    cg.setColor( QColorGroup::Light, white );
    cg.setColor( QColorGroup::Midlight, white );
    cg.setColor( QColorGroup::Dark, QColor( 112, 111, 113) );
    cg.setColor( QColorGroup::Mid, QColor( 149, 149, 151) );
    cg.setColor( QColorGroup::Text, QColor( 128, 128, 128) );
    cg.setColor( QColorGroup::BrightText, white );
    cg.setColor( QColorGroup::ButtonText, QColor( 128, 128, 128) );
    cg.setColor( QColorGroup::Base, white );
    cg.setColor( QColorGroup::Background, QColor( 239, 239, 241) );
    cg.setColor( QColorGroup::Shadow, black );
    cg.setColor( QColorGroup::Highlight, QColor( 0, 0, 128) );
    cg.setColor( QColorGroup::HighlightedText, white );
    cg.setColor( QColorGroup::Link, QColor( 0, 0, 255) );
    cg.setColor( QColorGroup::LinkVisited, QColor( 255, 0, 255) );
    pal.setDisabled( cg );
    Frame4->setPalette( pal );
    Frame4->setFrameShape( QFrame::StyledPanel );
    Frame4->setFrameShadow( QFrame::Raised );

    inTitle = new clrLabel( Frame4, "inTitle" );
    inTitle->setGeometry( QRect( 2, 2, 266, 19 ) ); 
    inTitle->setPaletteBackgroundColor( QColor( 131, 131, 131 ) );
    inTitle->setFrameShape( QFrame::NoFrame );
    inTitle->setFrameShadow( QFrame::Plain );
    inTitle->setText( " " + trUtf8( "Inactive Window" ) );

    QBoxLayout *hl1 = new QHBoxLayout(inTitle);
    hl1->addStretch(5);
    PushButton1 = new QPushButton( inTitle, "PushButton1" );
    PushButton1->setGeometry( QRect( 248, 2, 16, 16 ) ); 
    PushButton1->setFixedSize(16,16);
    PushButton1->setText( trUtf8( "X" ) );
    hl1->addWidget(PushButton1);
    /*
    inText = new QLabel( inTitle, "inText" );
    inText->setGeometry( QRect( 0, 0, 97, 20 ) ); 
    inText->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)4, (QSizePolicy::SizeType)5, 0, 0, inText->sizePolicy().hasHeightForWidth() ) );
    inText->setText( trUtf8( "Inactive Window" ) );
    //    inText->setBackgroundOrigin( QLabel::ParentOrigin ); 
    //    inText->setBackgroundMode(Qt::X11ParentRelative); 
    */

    taskBar = new QFrame( desktop, "taskBar" );
    taskBar->setGeometry( QRect( 0, 176, 349, 24 ) ); 
    cg.setColor( QColorGroup::Foreground, black );
    cg.setColor( QColorGroup::Button, QColor( 224, 223, 227) );
    cg.setColor( QColorGroup::Light, white );
    cg.setColor( QColorGroup::Midlight, QColor( 239, 239, 241) );
    cg.setColor( QColorGroup::Dark, QColor( 112, 111, 113) );
    cg.setColor( QColorGroup::Mid, QColor( 149, 149, 151) );
    cg.setColor( QColorGroup::Text, black );
    cg.setColor( QColorGroup::BrightText, white );
    cg.setColor( QColorGroup::ButtonText, black );
    cg.setColor( QColorGroup::Base, white );
    cg.setColor( QColorGroup::Background, QColor( 224, 223, 227) );
    cg.setColor( QColorGroup::Shadow, black );
    cg.setColor( QColorGroup::Highlight, QColor( 0, 0, 128) );
    cg.setColor( QColorGroup::HighlightedText, white );
    cg.setColor( QColorGroup::Link, black );
    cg.setColor( QColorGroup::LinkVisited, black );
    pal.setActive( cg );
    cg.setColor( QColorGroup::Foreground, black );
    cg.setColor( QColorGroup::Button, QColor( 224, 223, 227) );
    cg.setColor( QColorGroup::Light, white );
    cg.setColor( QColorGroup::Midlight, white );
    cg.setColor( QColorGroup::Dark, QColor( 112, 111, 113) );
    cg.setColor( QColorGroup::Mid, QColor( 149, 149, 151) );
    cg.setColor( QColorGroup::Text, black );
    cg.setColor( QColorGroup::BrightText, white );
    cg.setColor( QColorGroup::ButtonText, black );
    cg.setColor( QColorGroup::Base, white );
    cg.setColor( QColorGroup::Background, QColor( 224, 223, 227) );
    cg.setColor( QColorGroup::Shadow, black );
    cg.setColor( QColorGroup::Highlight, QColor( 0, 0, 128) );
    cg.setColor( QColorGroup::HighlightedText, white );
    cg.setColor( QColorGroup::Link, QColor( 0, 0, 255) );
    cg.setColor( QColorGroup::LinkVisited, QColor( 255, 0, 255) );
    pal.setInactive( cg );
    cg.setColor( QColorGroup::Foreground, QColor( 128, 128, 128) );
    cg.setColor( QColorGroup::Button, QColor( 224, 223, 227) );
    cg.setColor( QColorGroup::Light, white );
    cg.setColor( QColorGroup::Midlight, white );
    cg.setColor( QColorGroup::Dark, QColor( 112, 111, 113) );
    cg.setColor( QColorGroup::Mid, QColor( 149, 149, 151) );
    cg.setColor( QColorGroup::Text, QColor( 128, 128, 128) );
    cg.setColor( QColorGroup::BrightText, white );
    cg.setColor( QColorGroup::ButtonText, QColor( 128, 128, 128) );
    cg.setColor( QColorGroup::Base, white );
    cg.setColor( QColorGroup::Background, QColor( 224, 223, 227) );
    cg.setColor( QColorGroup::Shadow, black );
    cg.setColor( QColorGroup::Highlight, QColor( 0, 0, 128) );
    cg.setColor( QColorGroup::HighlightedText, white );
    cg.setColor( QColorGroup::Link, QColor( 0, 0, 255) );
    cg.setColor( QColorGroup::LinkVisited, QColor( 255, 0, 255) );
    pal.setDisabled( cg );
    taskBar->setPalette( pal );
    taskBar->setFrameShape( QFrame::StyledPanel );
    taskBar->setFrameShadow( QFrame::Raised );

    taskButton = new QPushButton( taskBar, "taskButton" );
    taskButton->setGeometry( QRect( 1, 1, 49, 22 ) ); 
    taskButton->setText( trUtf8( "Start" ) );

    Frame4_2 = new QFrame( desktop, "Frame4_2" );
    Frame4_2->setGeometry( QRect( 30, 40, 280, 100 ) ); 
    cg.setColor( QColorGroup::Foreground, black );
    cg.setColor( QColorGroup::Button, QColor( 224, 223, 227) );
    cg.setColor( QColorGroup::Light, white );
    cg.setColor( QColorGroup::Midlight, QColor( 239, 239, 241) );
    cg.setColor( QColorGroup::Dark, QColor( 112, 111, 113) );
    cg.setColor( QColorGroup::Mid, QColor( 149, 149, 151) );
    cg.setColor( QColorGroup::Text, black );
    cg.setColor( QColorGroup::BrightText, white );
    cg.setColor( QColorGroup::ButtonText, black );
    cg.setColor( QColorGroup::Base, white );
    cg.setColor( QColorGroup::Background, QColor( 224, 223, 227) );
    cg.setColor( QColorGroup::Shadow, black );
    cg.setColor( QColorGroup::Highlight, QColor( 0, 0, 128) );
    cg.setColor( QColorGroup::HighlightedText, white );
    cg.setColor( QColorGroup::Link, black );
    cg.setColor( QColorGroup::LinkVisited, black );
    pal.setActive( cg );
    cg.setColor( QColorGroup::Foreground, black );
    cg.setColor( QColorGroup::Button, QColor( 224, 223, 227) );
    cg.setColor( QColorGroup::Light, white );
    cg.setColor( QColorGroup::Midlight, white );
    cg.setColor( QColorGroup::Dark, QColor( 112, 111, 113) );
    cg.setColor( QColorGroup::Mid, QColor( 149, 149, 151) );
    cg.setColor( QColorGroup::Text, black );
    cg.setColor( QColorGroup::BrightText, white );
    cg.setColor( QColorGroup::ButtonText, black );
    cg.setColor( QColorGroup::Base, white );
    cg.setColor( QColorGroup::Background, QColor( 224, 223, 227) );
    cg.setColor( QColorGroup::Shadow, black );
    cg.setColor( QColorGroup::Highlight, QColor( 0, 0, 128) );
    cg.setColor( QColorGroup::HighlightedText, white );
    cg.setColor( QColorGroup::Link, QColor( 0, 0, 255) );
    cg.setColor( QColorGroup::LinkVisited, QColor( 255, 0, 255) );
    pal.setInactive( cg );
    cg.setColor( QColorGroup::Foreground, QColor( 128, 128, 128) );
    cg.setColor( QColorGroup::Button, QColor( 224, 223, 227) );
    cg.setColor( QColorGroup::Light, white );
    cg.setColor( QColorGroup::Midlight, white );
    cg.setColor( QColorGroup::Dark, QColor( 112, 111, 113) );
    cg.setColor( QColorGroup::Mid, QColor( 149, 149, 151) );
    cg.setColor( QColorGroup::Text, QColor( 128, 128, 128) );
    cg.setColor( QColorGroup::BrightText, white );
    cg.setColor( QColorGroup::ButtonText, QColor( 128, 128, 128) );
    cg.setColor( QColorGroup::Base, white );
    cg.setColor( QColorGroup::Background, QColor( 224, 223, 227) );
    cg.setColor( QColorGroup::Shadow, black );
    cg.setColor( QColorGroup::Highlight, QColor( 0, 0, 128) );
    cg.setColor( QColorGroup::HighlightedText, white );
    cg.setColor( QColorGroup::Link, QColor( 0, 0, 255) );
    cg.setColor( QColorGroup::LinkVisited, QColor( 255, 0, 255) );
    pal.setDisabled( cg );
    Frame4_2->setPalette( pal );
    Frame4_2->setFrameShape( QFrame::StyledPanel );
    Frame4_2->setFrameShadow( QFrame::Raised );

    activeTitle = new clrLabel( Frame4_2, "activeTitle" );
    activeTitle->setGeometry( QRect( 2, 2, 276, 19 ) ); 
    activeTitle->setPaletteBackgroundColor( QColor( 0, 0, 127 ) );
    activeTitle->setFrameShape( QFrame::NoFrame );
    activeTitle->setFrameShadow( QFrame::Plain );
    activeTitle->setText( " " + trUtf8( "Active Window" ) );

    QBoxLayout *hl = new QHBoxLayout(activeTitle);
    hl->addStretch(5);
    //    activeButton = new QPushButton( activeTitle, "activeButton" );
    activeButton = new QPushButton( activeTitle, "activeButton" );
    activeButton->setGeometry( QRect( 258, 2, 16, 16 ) ); 
    activeButton->setFixedSize(16,16);
    activeButton->setText( trUtf8( "X" ) );
    hl->addWidget(activeButton);
    /*
    activeText = new QLabel( activeTitle, "activeText" );
    activeText->setGeometry( QRect( 2, 0, 89, 18 ) ); 
    activeText->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)4, (QSizePolicy::SizeType)5, 0, 0, activeText->sizePolicy().hasHeightForWidth() ) );
    activeText->setPaletteForegroundColor( QColor( 255, 255, 255 ) );
    activeText->setText( trUtf8( "Active Window" ) );
    //    activeText->setBackgroundOrigin( QLabel::ParentOrigin ); 
    //    activeText->setBackgroundMode(Qt::X11ParentRelative); 
    */

    Layout7->addWidget( desktop );

    Layout6 = new QHBoxLayout( 0, 0, 6, "Layout6"); 

    TextLabel1_2 = new QLabel( privateLayoutWidget, "TextLabel1_2" );
    TextLabel1_2->setText( trUtf8( "Style:" ) );
    Layout6->addWidget( TextLabel1_2 );

    styleBox = new QComboBox( FALSE, privateLayoutWidget, "styleBox" );
    Layout6->addWidget( styleBox );
    Layout7->addLayout( Layout6 );

    Layout1 = new QGridLayout( 0, 1, 1, 0, 6, "Layout1"); 

    sizeBox = new QSpinBox( privateLayoutWidget, "sizeBox" );
    sizeBox->setMinimumSize( QSize( 40, 0 ) );
    sizeBox->setMaximumSize( QSize( 40, 32767 ) );

    Layout1->addWidget( sizeBox, 1, 1 );

    TextLabel6 = new QLabel( privateLayoutWidget, "TextLabel6" );
    TextLabel6->setText( trUtf8( "Size:" ) );

    Layout1->addWidget( TextLabel6, 0, 1 );

    TextLabel5 = new QLabel( privateLayoutWidget, "TextLabel5" );
    TextLabel5->setText( trUtf8( "Color2:" ) );

    Layout1->addWidget( TextLabel5, 0, 3 );

    itemBox = new QComboBox( FALSE, privateLayoutWidget, "itemBox" );
    itemBox->insertItem( trUtf8( "Desktop" ) );
    itemBox->insertItem( trUtf8( "Active titlebar" ) );
    itemBox->insertItem( trUtf8( "Inactive titlebar" ) );
    itemBox->insertItem( trUtf8( "Titlebar button" ) );
    itemBox->insertItem( trUtf8( "Start button" ) );

    Layout1->addWidget( itemBox, 1, 0 );

    color2Btn = new QPushButton( privateLayoutWidget, "color2Btn" );
    color2Btn->setMinimumSize( QSize( 40, 0 ) );
    color2Btn->setMaximumSize( QSize( 40, 23 ) );
    color2Btn->setText( trUtf8( "" ) );

    Layout1->addWidget( color2Btn, 1, 3 );

    itemText = new QLabel( privateLayoutWidget, "itemText" );
    itemText->setText( trUtf8( "Item:" ) );

    Layout1->addWidget( itemText, 0, 0 );

    TextLabel4 = new QLabel( privateLayoutWidget, "TextLabel4" );
    TextLabel4->setText( trUtf8( "Color1:" ) );

    Layout1->addWidget( TextLabel4, 0, 2 );

    color1Btn = new QPushButton( privateLayoutWidget, "color1Btn" );
    color1Btn->setMinimumSize( QSize( 40, 0 ) );
    color1Btn->setMaximumSize( QSize( 40, 23 ) );
    color1Btn->setText( trUtf8( "" ) );

    Layout1->addWidget( color1Btn, 1, 2 );
    Layout7->addLayout( Layout1 );

    Layout6_2 = new QGridLayout( 0, 1, 1, 0, 6, "Layout6_2"); 


    fontLabel = new QLabel( privateLayoutWidget, "fontLabel" );
    fontLabel->setText( trUtf8( "Font:" ) );

    TextLabel1 = new QLabel( privateLayoutWidget, "TextLabel1" );
    TextLabel1->setText( trUtf8( "Name:" ) );

    textColorLbl = new QLabel( privateLayoutWidget, "textColorLbl" );
    textColorLbl->setText( trUtf8( "Text Color:" ) );





    Layout2 = new QHBoxLayout( 0, 0, 6, "Layout2"); 

    fontEdit = new QLineEdit( privateLayoutWidget, "fontEdit" );
    Layout2->addWidget( fontEdit );

    fontBtn = new QPushButton( privateLayoutWidget, "fontBtn" );
    fontBtn->setMinimumSize( QSize( 60, 0 ) );
    fontBtn->setMaximumSize( QSize( 60, 32767 ) );
    fontBtn->setText( trUtf8( "Choose" ) );
    Layout2->addWidget( fontBtn );



    nameEdit = new QLineEdit( privateLayoutWidget, "nameEdit" );
    nameEdit->setMinimumSize( QSize( 80, 0 ) );
    nameEdit->setMaximumSize( QSize( 80, 32767 ) );



    textColorBtn = new QPushButton( privateLayoutWidget, "textColorBtn" );
    textColorBtn->setMinimumSize( QSize( 40, 0 ) );
    textColorBtn->setMaximumSize( QSize( 50, 23 ) );




    Layout6_2->addWidget( fontLabel, 0, 0 );
    Layout6_2->addWidget( TextLabel1, 0, 1 );
    Layout6_2->addWidget( textColorLbl, 0, 2 );

    Layout6_2->addLayout( Layout2, 1, 0 );
    Layout6_2->addWidget( nameEdit, 1, 1 );
    Layout6_2->addWidget( textColorBtn, 1, 2 );



    Layout7->addLayout( Layout6_2 );

    Layout1_2 = new QHBoxLayout( 0, 1, 0, "Layout1_2"); 
    QSpacerItem* spacer = new QSpacerItem( 20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    Layout1_2->addItem( spacer );

    okBtn = new QPushButton( privateLayoutWidget, "okBtn" );
    okBtn->setText( trUtf8( "OK" ) );
    Layout1_2->addWidget( okBtn );

    cancelBtn = new QPushButton( privateLayoutWidget, "cancelBtn" );
    cancelBtn->setText( trUtf8( "Cancel" ) );
    Layout1_2->addWidget( cancelBtn );

    applyBtn = new aplBtn( privateLayoutWidget, "applyBtn" );
    applyBtn->setEnabled( FALSE );
    applyBtn->setText( trUtf8( "Apply" ) );
    Layout1_2->addWidget( applyBtn );
    Layout7->addLayout( Layout1_2 );

    QSpacerItem* spacer_6 = new QSpacerItem( 0, 10, QSizePolicy::Expanding, 
					     QSizePolicy::Minimum );
    Layout7->addItem(spacer_6);

    // signals and slots connections
    connect( cancelBtn, SIGNAL( clicked() ), this, SLOT( close() ) );
    connect( itemBox, SIGNAL( highlighted(const QString&) ), this, SLOT( itemBox_highlighted(const QString&) ) );
    connect( sizeBox, SIGNAL( valueChanged(int) ), 
	     this, SLOT( sizeBox_valueChanged(int) ) );
    connect( color1Btn, SIGNAL( clicked() ), this, 
	     SLOT( color1Btn_clicked() ) );
    connect( color2Btn, SIGNAL( clicked() ), this, 
	     SLOT( color2Btn_clicked() ) );

    connect( textColorBtn, SIGNAL( clicked() ), this, 
	     SLOT( textColorBtn_clicked() ) );

    connect( activeButton, SIGNAL( clicked() ), this, 
	     SLOT( activeButton_clicked() ) );
    connect( PushButton1, SIGNAL( clicked() ), this, 
	     SLOT( activeButton_clicked() ) );

    connect( taskButton, SIGNAL( pressed() ), this, 
	     SLOT( taskButton_pressed() ) );

    connect( fontBtn, SIGNAL( clicked() ), this, 
	     SLOT( fontBtn_clicked() ) );
    connect( applyBtn, SIGNAL( clicked() ), this, SLOT( applyBtn_clicked() ) );
    connect( okBtn, SIGNAL( clicked() ), this, SLOT( okBtn_clicked() ) );
    connect( styleBox, SIGNAL( activated(const QString &) ), this, 
	     SLOT( styleBox_activated(const QString &) ) );
    connect( itemBox, SIGNAL( activated(const QString&) ), this, 
	     SLOT( itemBox_activated(const QString&) ) );

    connect( desktop, SIGNAL( clicked() ), this, SLOT( desktop_clicked() ) );
    connect( activeTitle, SIGNAL( clicked() ), this, 
	     SLOT( activeTitle_clicked() ) );
    connect( inTitle, SIGNAL( clicked() ), this, SLOT( inTitle_clicked() ) );

    connect( fontEdit, SIGNAL( textChanged ( const QString & ) ), this, 
	     SLOT( fontEdit_textChanged ( const QString & ) ) );


    connect( applyBtn, SIGNAL( enabledChang(bool) ), this, 
	     SLOT( enabledChange(bool) ) );

    init();
}

/*  
 *  Destroys the object and frees any allocated resources
 */
clrForm::~clrForm()
{
    // no need to delete child widgets, Qt does it all for us
}

void clrForm::init()
{
    defaults::read_config();
    QString bgDisplay = defaults::bgDisplay; 

    active_bg = defaults::active_bg;
    active_bg2 = defaults::active_bg2;
    inactive_bg = defaults::inactive_bg;
    inactive_bg2 = defaults::inactive_bg2;
    active_fg = defaults::active_fg;
    inactive_fg = defaults::inactive_fg;
    windowbuttonsize = defaults::windowbuttonsize;

    root_bg = defaults::root_bg;
    borderfont = defaults::borderfont;

    QStringList gstyles = QStyleFactory::keys();
    gstyles.sort();
    styleBox->insertStringList(gstyles);

    desktop->setPaletteBackgroundColor(root_bg);
    sizeBox->setValue(windowbuttonsize);

    color1Btn->setPixmap( colorPix( root_bg ) );

    styleName = defaults::styleName;
    styleBox->setCurrentText( styleName ); 

    itemChanged();
    setTitle();

    applyBtn->setEnabled(false);
}
void clrForm::enabledChange(bool ec)
{ 
    //QMessageBox::information( this, "Application name", "TEST" );
    //    emit enabledChanged( applyBtn->isEnabled() ); 
    emit enabledChanged( ec ); 
}

void clrForm::setTitle()
{
    QPixmap qp(activeTitle->width(), activeTitle->height());
    gradient(qp, active_bg, active_bg2, QString(""), 1);
    activeTitle->setBackgroundPixmap(qp);
    activeTitle->setPaletteForegroundColor( active_fg );
    activeTitle->setFont(borderfont);
    QPixmap qp1(inTitle->width(), inTitle->height());
    gradient(qp1, inactive_bg, inactive_bg2, QString(""), 1);
    inTitle->setBackgroundPixmap(qp1);
    inTitle->setPaletteForegroundColor( inactive_fg );
    inTitle->setFont(borderfont);

}

void clrForm::disableAll()
{
    sizeBox->setEnabled(false);
    color1Btn->setEnabled(false);
    color2Btn->setEnabled(false);
    fontEdit->setEnabled(false);
    fontBtn->setEnabled(false);
    nameEdit->setEnabled(false);
    textColorBtn->setEnabled(false);
}


void clrForm::itemBox_highlighted( const QString & )
{
}


void clrForm::sizeBox_valueChanged( int val )
{
	windowbuttonsize = val;
	applyBtn->setEnabled(true);
}


void clrForm::color1Btn_clicked()
{
    QString clr1 = QColorDialog::getColor().name();
    color1Btn->setPixmap( colorPix( clr1 ) );

    if( itemBox->currentText ()   == trUtf8("Desktop") )
    {
	root_bg = clr1;
	desktop->setPaletteBackgroundColor(root_bg);
    }
    else if( itemBox->currentText ()   == trUtf8("Active titlebar") )
    {
	active_bg = clr1;
	setTitle();
    }
    else if( itemBox->currentText ()   == trUtf8("Inactive titlebar") )
    {
	inactive_bg = clr1;
	setTitle();
    }

    applyBtn->setEnabled(true);
}


void clrForm::color2Btn_clicked()
{
    QString clr2 = QColorDialog::getColor().name();
    color2Btn->setPixmap( colorPix( clr2 ) );

    if( itemBox->currentText ()   == trUtf8("Active titlebar") )
    {
	active_bg2 = clr2;
	setTitle();
    }
    else if( itemBox->currentText ()   == trUtf8("Inactive titlebar") )
    {
	inactive_bg2 = clr2;
	setTitle();
    }

    applyBtn->setEnabled(true);
}


void clrForm::textColorBtn_clicked()
{
    QString clr = QColorDialog::getColor().name();
    textColorBtn->setPixmap( colorPix( clr ) );

    if( itemBox->currentText ()   == trUtf8("Active titlebar") )
    {
	active_fg = clr;
	setTitle();
    }
    else if( itemBox->currentText ()   == trUtf8("Inactive titlebar") )
    {
	inactive_fg = clr;
	setTitle();
    }

    applyBtn->setEnabled(true);
}



void clrForm::activeButton_clicked()
{
    itemBox->setCurrentItem(3);
    itemChanged();
}


void clrForm::taskButton_pressed()
{
    itemBox->setCurrentItem(4);
    itemChanged();
}


void clrForm::fontBtn_clicked()
{
    //borderfont
    bool ok;
    QFont font = QFontDialog::getFont( &ok, borderfont, this );
    if ( ok ) {
	borderfont = font;
	fontEdit->setText( borderfont.toString());
	applyBtn->setEnabled(true);
    }
}

void clrForm::desktop_clicked()
{
    //    QMessageBox::information( this, "TEST", "Desktop" );
    itemBox->setCurrentItem(0);
    itemChanged();
}


void clrForm::inTitle_clicked()
{
    itemBox->setCurrentItem(2);
    itemChanged();
}


void clrForm::activeTitle_clicked()
{
    itemBox->setCurrentItem(1);
    itemChanged();
}


void clrForm::applyBtn_clicked()
{
    QString fname;
    fname = defaults::get_cfile("defaults");
 
    HIniFile def(fname);
    def.read(); 

    def.writeInt("WindowButtonSize", windowbuttonsize );
    def.writeString("WindowFontName", borderfont.toString()) ;

    def.setGroup("Color_Background"); 
    def.writeString("RootBackgroundColor", root_bg.name() );
    def.writeString("activeWindowColor", active_bg.name() );
    def.writeString("activeWindowColor2", active_bg2.name() );
    def.writeString("InactiveWindowColor", inactive_bg.name() );
    def.writeString("InactiveWindowColor2", inactive_bg2.name() );

    def.writeString("activeTextColor", active_fg.name() );
    def.writeString("InactiveTextColor", inactive_fg.name() );


    def.setGroup("Style");
    def.writeString("Style", styleName);

    def.write(); 
 




    applyBtn->setEnabled(false);
}


void clrForm::okBtn_clicked()
{
    if( applyBtn->isEnabled() ) 
	applyBtn_clicked();
    close();
}


void clrForm::styleBox_activated( const QString & )
{
    styleName = styleBox->currentText();
    QApplication::setStyle(QStyleFactory::create(styleName));
    applyBtn->setEnabled(true);
}

void clrForm::fontEdit_textChanged ( const QString & )
{
    setTitle();
    applyBtn->setEnabled(true);
}

void clrForm::itemChanged()
{
    if( itemBox->currentText ()   == trUtf8("Desktop") )
    {
	disableAll();
	color1Btn->setEnabled(true);
	color1Btn->setPixmap( colorPix( root_bg ) );
    }
    else if( itemBox->currentText ()   == trUtf8("Active titlebar") )
    {
	disableAll();
	color1Btn->setEnabled(true);
	color2Btn->setEnabled(true);
	fontEdit->setEnabled(true);
	fontBtn->setEnabled(true);
	textColorBtn->setEnabled(true);
	color1Btn->setPixmap( colorPix( active_bg ) );
	color2Btn->setPixmap( colorPix( active_bg2 ) );
	textColorBtn->setPixmap( colorPix( active_fg ) );
	fontEdit->setText( borderfont.toString());
     }
    else if( itemBox->currentText ()   == trUtf8("Inactive titlebar") )
    {
	disableAll();
	color1Btn->setEnabled(true);
	color2Btn->setEnabled(true);
	fontEdit->setEnabled(true);
	fontBtn->setEnabled(true);
	textColorBtn->setEnabled(true);
	color1Btn->setPixmap( colorPix( inactive_bg ) );
	color2Btn->setPixmap( colorPix( inactive_bg2 ) );
	textColorBtn->setPixmap( colorPix( inactive_fg ) );
	fontEdit->setText( borderfont.toString());
     }
    else if( itemBox->currentText ()   == trUtf8("Titlebar button") )
    {
	disableAll();
	sizeBox->setEnabled(true);
     }
    else if( itemBox->currentText ()   == trUtf8("Start button") )
    {
	disableAll();
	nameEdit->setEnabled(true);
     }
    else 
    {
	disableAll();
    }

}  

void clrForm::itemBox_activated( const QString & )
{
    itemChanged();
}  
